let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   // .sass('resources/assets/sass/app.scss', 'public/css')
   .scripts([
     'resources/app-assets/vendors/js/tables/datatable/datatables.min.js',
     'resources/app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js',
     'resources/app-assets/vendors/js/tables/buttons.colVis.min.js',
     'resources/app-assets/vendors/js/ui/popper.min.js',
     'resources/app-assets/js/core/libraries/bootstrap.min.js',
     'resources/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js',
     'resources/app-assets/vendors/js/pickers/daterange/daterangepicker.js',
     'resources/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js',
     'resources/app-assets/vendors/js/pickers/date/bootstrap-datepicker.min.js',
     'resources/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js',
     'resources/app-assets/vendors/js/ui/unison.min.js',
     'resources/app-assets/vendors/js/ui/jquery-sliding-menu.js',
     'resources/app-assets/vendors/js/forms/select/select2.full.min.js',
     'resources/app-assets/vendors/js/extensions/toastr.min.js',
     'resources/app-assets/js/scripts/tables/datatables-extensions/datatable-fixed-column.js',
     'resources/app-assets/js/scripts/tooltip/tooltip.min.js"',
     'resources/app-assets/vendors/js/tables/datatable/dataTables.editor.min.js',
   ], 'public/js/template.js')
   .styles([
     'resources/app-assets/css/bootstrap.css',
     'resources/app-assets/fonts/feather/style.css',
     'resources/app-assets/fonts/font-awesome/css/font-awesome.css',
     'resources/app-assets/fonts/flag-icon-css/css/flag-icon.css',
     'resources/app-assets/vendors/css/extensions/pace.css',
     'resources/app-assets/vendors/css/tables/datatable/datatables.min.css',
     'resources/app-assets/vendors/css/forms/selects/select2.css',
     'resources/app-assets/vendors/css/pickers/daterange/daterangepicker.css',
     'resources/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css',
     'resources/app-assets/vendors/css/pickers/date/bootstrap-datepicker.min.css',
     'resources/app-assets/vendors/css/animate/animate.css',
     'resources/app-assets/vendors/css/extensions/toastr.css',
     'resources/app-assets/css/bootstrap-extended.css',
     'resources/app-assets/css/colors.css',
     'resources/app-assets/css/components.css',
     'resources/app-assets/css/core/menu/menu-types/vertical-menu.css',
     'resources/app-assets/css/core/colors/palette-gradient.css',
     'resources/app-assets/fonts/simple-line-icons/style.css',
     'resources/app-assets/css/pages/timeline.css',
     'resources/app-assets/css/core/colors/palette-tooltip.min.css',
     'resources/app-assets/css/style.css',
     'resources/app-assets/css/style-responsive.css'
   ], 'public/css/template.css');
