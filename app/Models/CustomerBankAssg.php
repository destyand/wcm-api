<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerBankAssg extends Model {

    protected $table = 'wcm_customer_bank_assg';

    public $incrementing = false;

    protected $fillable = [
        'name', 'customer_id', 'sales_org_id', 'bank_id', 'start_date', 'end_date', 
        'status', 'created_by', 'updated_by','created_at', 'updated_at',
    ];

    public static function getBankAssg($where = []){
        $query = DB::table('wcm_customer_bank_assg AS tb1')
            ->leftJoin('wcm_bank AS tb2', 'tb2.id', '=', 'tb1.bank_id')
            ->select('tb2.*')
            ->where($where);
        
        return $query;
    }
}
