<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogOrderItem extends Model
{
    //
    protected $table = "wcm_log_order_items";
    protected $fillable = ["error_id", "error_number", "error_type", "error_message"];

    public function logOrder()
    {
        return $this->belongsTo("App\Models\LogOrder");
    }
}
