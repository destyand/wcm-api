<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class CertificateRelease extends Model
{
    //
    protected $table = 'wcm_certificate_release';
    protected $fillable = [
        'number', 'status', 'sales_org_id','customer_id','sales_group_id','created_by', 'updated_by', 'created_at', 'updated_at',
    ];


    public static function detail($where = []) {
        $query = DB::table('wcm_certificate_release as wcr')
                ->leftjoin('wcm_certificate_release_items as wcri', 'wcr.id', '=', 'wcri.certificate_release_id')
                ->leftjoin('view_detail_certificate_release as vdcr', 'vdcr.id', '=', 'wcri.distrib_report_id')
                ->select('vdcr.*',
                        'wcr.status', DB::raw("
                                (CASE
                                    WHEN wcr.status = 'y' THEN 'Active'
                                    WHEN wcr.status = 'n' THEN 'Inactive'
                                    WHEN wcr.status = 'p' THEN 'Suspend'
                                    WHEN wcr.status = 'd' THEN 'Draft'
                                    WHEN wcr.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name")
                )
                ->where($where);
        return $query;
    }
}
