<?php

namespace App\Models;

use App\Rules\CheckActiveSupplyRule;
use App\Rules\OwnedSalesOrg;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SupplyPoint extends Model
{

    protected $table    = 'wcm_supply_point';
    protected $fillable = [
        'number', 'sales_org_id', 'sales_office_id', 'sales_group_id',
        'plant_id', 'scnd_sales_group_id', 'status', 'created_by', 'updated_by',
    ];

    public static function ruleCreate()
    {
        $rules = [
            'sales_org_id'        => ['required', new OwnedSalesOrg],
            'sales_office_id'     => 'required|exists:wcm_sales_office,id',
//            'sales_group_id'      => 'required|exists:wcm_sales_group,id',
            'plant_id'            => 'required|exists:wcm_plant,id',
            'scnd_sales_group_id' => 'required|exists:wcm_sales_group,id',
            'status'              => 'required',
            'active_check'        => [new CheckActiveSupplyRule]
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getAll($where = [])
    {
        $query = DB::table('wcm_supply_point AS tb1')
            ->join('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
            ->join('wcm_sales_office AS tb3', 'tb1.sales_office_id', '=', 'tb3.id')
            ->join('wcm_sales_group AS tb4', 'tb1.sales_group_id', '=', 'tb4.id')
            ->join('wcm_plant AS tb5', 'tb1.plant_id', '=', 'tb5.id')
            ->join('wcm_sales_group AS tb6', 'tb1.scnd_sales_group_id', '=', 'tb6.id')
            ->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.sales_org_id', 'tb2.name AS sales_org_name', 'tb1.sales_office_id',
                'tb3.name AS sales_office_name', 'tb1.sales_group_id', 'tb4.name AS sales_group_name', 'tb1.plant_id', 'tb5.code AS plant_code',
                'tb5.name AS plant_name', 'tb1.scnd_sales_group_id', 'tb6.name AS scnd_sales_group_name',
                'tb1.status', DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            )
            ->whereIn('tb1.status', ['y', 'n'])
            ->where($where);

        return $query;
    }

    public static function getExportedColumns() {
        return [
            'tb2.name as Produsen',
            'tb3.name as Provinsi',
            'tb4.name as Kabupaten',
            'tb5.code as Kode Gudang',
            'tb5.name as Gudang',
            'tb6.name as Kabupaten Kedua',
            DB::raw("(CASE
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as Status")
        ];
    }

}
