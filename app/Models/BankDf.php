<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BankDf extends Model
{
    //
    protected $table = 'wcm_customer_bank_assg';
    protected $fillable = [
        'customer_id','sales_org_id','bank_id','start_date','end_date','status','created_by', 'updated_by','created_at', 'updated_at'
    ];

    public static function ruleCreate() {
        $rules = [
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'bank_id' => 'required|exists:wcm_bank,id',
            'start_date' => 'required',
            'end_date' => 'required',
            'status' => 'required',
        ];
        return $rules;
    }

    public static function ruleUpdate() {
        $rules = [
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'bank_id' => 'required|exists:wcm_bank,id',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
        return $rules;
    }


    public static function getAll($where=[]) {

        $query=DB::table('wcm_customer_bank_assg as tb1')
                   ->join('wcm_customer as tb2' ,'tb2.id','=','tb1.customer_id')
                   ->join('wcm_sales_org as tb3', 'tb3.id','=','tb1.sales_org_id')
                   ->join('wcm_bank as tb4','tb4.id','=','tb1.bank_id')
                   ->select('tb1.id','tb1.uuid','tb2.uuid as customer_uuid','tb2.id as customer_id','tb2.full_name as customer_name','tb3.id as sales_org_id','tb3.name as sales_org_name','tb4.id as bank_id','tb4.name as bank_name',
                            DB::raw("CONVERT ( VARCHAR, tb1.start_date, 105 ) as start_date") ,
                            DB::raw("CONVERT ( VARCHAR, tb1.end_date, 105 ) as end_date"),'tb1.status',
                            DB::raw("
                                (CASE  
                                    WHEN tb1.status = 'y' THEN 'Active'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name"))
                   ->where($where);
        return $query;
    }
}
