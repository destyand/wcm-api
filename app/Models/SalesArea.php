<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SalesArea extends Model {

    protected $table = 'wcm_sales_area';
    protected $fillable = [
        'sales_org_id', 'distrib_channel_id', 'sales_division_id',
        'from_date', 'thru_date', 'status', 'created_by', 'updated_by',
    ];

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getSalesArea($customer_id, $sales_org_id) {
        $query = DB::table('wcm_sales_area AS tb1')
                ->join('wcm_customer_sales_area AS tb2', 'tb1.id', '=', 'tb2.sales_area_id')
                ->select('tb1.*')
                ->where('tb2.customer_id', $customer_id)
                ->where('tb1.sales_org_id', $sales_org_id);
        
        return $query->first();
    }

}
