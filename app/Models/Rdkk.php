<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Rules\RetailSalesUnitExistSPJB;
use App\Rules\CustumerRetailRelationActive;
use App\Rules\SalesUnits;

class Rdkk extends Model
{
    protected $table    = 'wcm_rdkks';
    protected $fillable = [
        'planting_period_id', 'customer_id', 'retail_id', 'planting_area','land_area',
        'planting_from_date', 'planting_thru_date', 'sales_unit_id', 'commodity', 'status',
        'create_at', 'update_at', 'created_by', 'updated_by','reference_rdkk','indeks_pertanaman'
    ];
    public static function RuleCreate()
    {
        $rules = [
            //'number'             => 'required|unique:wcm_rdkks,number',
            'planting_period_id' => 'required|exists:wcm_planting_period,id',
            'customer_id'        => 'required|exists:wcm_customer,id',
            'retail_id'          => 'required|exists:wcm_retail,id',
            // 'farmer_group_id'    => 'required|exists:wcm_farmer_groups,id',
            'planting_area'      => 'required|numeric|min:0',
            //'sales_org_id'       => 'required|exists:wcm_sales_org,id',
            'sales_unit_id'      => 'required|exists:wcm_sales_unit,id',
            //'commodity'          => 'required',
            //'reference_rdkk'     => "nullable|exists:wcm_rdkks,number",
            // 'planting_from_date' => 'required|date',
            // 'planting_thru_date' => 'required|date',
            'status'             => 'required',
            // 'type'               => 'required|in:required,approved',
            'indeks_pertanaman'  => 'required|numeric|min:0',
            'land_area'         => 'required|numeric|min:0',

        ];
        return $rules;
    }

    public static function RuleUpdate()
    {
        $rules = [
            'planting_area' => 'required|numeric|min:0',
            //'commodity'     => 'required',
            'status'        => 'required',
            'reference_rdkk'     => "nullable|exists:wcm_rdkks,number",
            'type'               => 'required|in:required,approved',
            'indeks_pertanaman'  => 'required|numeric|min:0',
            'land_area'         => 'required|numeric|min:0',
        ];
        return $rules;
    }

    public static function RuleImport()
    {
        $rules = [
            'planting_period_id' => 'required|exists:wcm_planting_period,id',
            'customer_id'        => 'required|exists:wcm_customer,id',
            'retail_id'          => 'required|exists:wcm_retail,id',
            // 'farmer_group_id'    => 'required|exists:wcm_farmer_groups,id',
            // 'planting_area'      => 'required|numeric|min:0',
            // 'sales_org_id'       => 'required|exists:wcm_sales_org,id',
            'sales_unit_id'      => ['required', new SalesUnits],
            'status'             => 'required',
            'required_qty'       =>'array|min:1',
            'required_qty.*'     => 'required|numeric|min:0',
            'indeks_pertanaman'  => 'required',
            'retail_in_sales_unit_spjb' => [new RetailSalesUnitExistSPJB],
            'customer_retail_relation' => [new CustumerRetailRelationActive]
        ];
        return $rules;
    }
    public static function RuleRdkk()
    {
        $rules = [
            'number' => 'required|unique:wcm_rdkks,number',
        ];
        return $rules;
    }
    public static function getAll($where = [])
    {
        $user = Auth::check() ? Auth::user() : null;
        $userSalesOrg = $user ? $user->salesOrgAssign->pluck("sales_org_id")->filter() : collect();
        $customerArea = DB::table("wcm_customer as wc")
            ->join("wcm_customer_sales_area as wcsa", "wc.id", "wcsa.customer_id")
            ->join("wcm_sales_area as wsa", "wcsa.sales_area_id", "wsa.id")
            ->when($user && (($user->role_id ?? 0) != 4), function($q) use ($userSalesOrg){
                
                $q->whereIn("wsa.sales_org_id", $userSalesOrg->toArray());
            })
            ->select("wc.*")
            ->distinct();

        $query = DB::table('wcm_rdkks AS tb1')
            ->join('view_rdkk_qty AS tb2', 'tb2.rdkk_id', '=', 'tb1.id')
            ->join('wcm_planting_period AS tb3', 'tb1.planting_period_id', '=', 'tb3.id')
            ->join('wcm_sales_unit AS tb6', 'tb1.sales_unit_id', '=', 'tb6.id')
            ->join('wcm_sales_group AS tb7', 'tb6.sales_group_id', '=', 'tb7.id')
            ->join('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
            ->join(DB::raw("({$customerArea->toSql()}) as tb9"), 'tb1.customer_id', '=', 'tb9.id')
            ->join('wcm_retail AS tb10', 'tb1.retail_id', '=', 'tb10.id')
            ->leftjoin('wcm_rdkks AS tb15','tb1.reference_rdkk','=','tb15.number')
            ->select('tb1.id', 'tb1.planting_area', 'tb1.uuid', 'tb1.number', 'tb1.status', 'tb1.commodity', DB::raw("
                (CASE
                    WHEN tb1.status = 'y' THEN 'Active'
                    WHEN tb1.status = 'n' THEN 'Inactive'
                    WHEN tb1.status = 'p' THEN 'Suspend'
                    WHEN tb1.status = 'd' THEN 'Draft'
                    WHEN tb1.status = 's' THEN 'Submited'
                    WHEN tb1.status = 'z' THEN 'Partial Active'
                    ELSE '-'
                END) as status_name"), 'tb9.id as customer_id', 'tb9.full_name as customer_name',
                'tb1.retail_id', 'tb10.code as retail_code', 'tb10.name as retail_name',
                'tb8.id as sales_office_id', 'tb8.name as sales_office_name',
                'tb7.id as sales_group_id', 'tb7.name as sales_group_name', 'tb6.id as sales_unit_id', 'tb6.name as sales_unit_name',
                'tb3.id as farmer_period_id', 'tb3.desc as farmer_period_name', 'tb2.P01', 'tb2.P02', 'tb2.P03', 'tb2.P04', 'tb2.P05', 'tb2.P51', 'tb2.P54', 'tb1.created_by', 'tb1.updated_by', DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at"),
                "tb1.reference_rdkk", "tb15.uuid as reference_rdkk_uuid","tb1.indeks_pertanaman","tb1.land_area"
            )
            ->where($where);
        $products = Product::pluck('id')->unique();
        $products->each(function($item) use(&$query) {
            $user = request()->user();

            $query->addSelect(
                DB::raw("(".
                    DB::table("wcm_rdkk_item")
                    ->select("status")
                    ->whereRaw("rdkk_id = tb1.id")
                    ->whereRaw("product_id='{$item}'")
                    ->toSql()
                .") as {$item}_status")
            )
            ->addSelect(
                DB::raw("(" .
                    DB::table("wcm_rdkk_item")
                        ->select(DB::raw("ISNULL(sum(approved_qty),0)"))
                        ->whereRaw("rdkk_id = tb1.id")
                        ->whereRaw("product_id='{$item}'")
                        ->toSql()
                .") as {$item}_approved_qty")
            );
            if (!$user->customer_id) {
                $filters = $user->filterRegional;
                if ($filters) {
                    $matrix = DB::table("wcm_matrix_rdkk")
                        ->select(DB::raw("CASE WHEN count(product_id) > 0 then 'y' else 'n' end"))
                        ->whereRaw("product_id='{$item}'")
                        ->whereRaw("sales_group_id = tb7.id");
                        
                        if(isset($filters["sales_org_id"]) && count ($filters["sales_org_id"]) > 0){
                            $matrix->whereRaw("sales_org_id in ('".implode("','", $filters["sales_org_id"]) ."')");
                        }

                        if(isset($filters["sales_group_id"]) && count ($filters["sales_group_id"]) > 0){
                            $matrix->whereRaw("sales_group_id in ('".implode("','", $filters["sales_group_id"]) ."')");
                        }

                        $query->addSelect(
                            DB::raw("(" .$matrix->toSql().") as {$item}_can_approve")
                        );
                } else {
                    $matrix = DB::table("wcm_matrix_rdkk")
                    ->select(DB::raw("CASE WHEN count(product_id) > 0 then 'y' else 'n' end"))
                    ->whereRaw("product_id='{$item}'")
                    ->whereRaw("sales_group_id = tb7.id");                   
                    $query->addSelect(
                        DB::raw("(" .$matrix->toSql().") as {$item}_can_approve")
                    );
                }
            } else {
                $query->addSelect(DB::raw("'n' as {$item}_can_approve"));
            }
        });
        $query->mergeBindings($customerArea);
        return $query;
    }

    public static function getProduct($where = [])
    {
        $query = DB::table('wcm_rdkk_item AS tb1')
            ->join('wcm_product AS tb2', 'tb1.product_id', '=', 'tb2.id')
            ->select('tb1.id', 'tb1.required_qty', 'tb2.id as product_id', 'tb2.name as product_name'
            )
            ->where($where);

        return $query;
    }
    
    public function item()
    {
        return $this->hasMany('App\Models\RdkkItem');
    }

    public function items()
    {
        return $this->hasMany('App\Models\RdkkItem',"rdkk_id", "id");
    }

    public function touchStatus()
    {
        $childsStatus = $this->items()->pluck('status')->unique();

        if ($childsStatus->count() == 1) {
            $status = $childsStatus->first();
        } elseif ($childsStatus->count() > 1) {
            $status = "z";
        }

        $this->update(compact('status'));

    }

    
}
