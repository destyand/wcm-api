<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class PlantingPeriod extends Model
{
    protected $table = 'wcm_planting_period';
    protected $fillable = [
        'code', 'desc', 'status',
        'from_date', 'thru_date', 'create_at', 'update_at', 'created_by', 'updated_by',
    ];
    public static function RuleCreate() {
        $rules = [
            'code'      => 'required|numeric|unique:wcm_planting_period',
            'desc'      => 'required|unique:wcm_planting_period',
            'from_date' => 'required',
            'thru_date' => 'required'
        ];
        return $rules;
    }

    public static function RuleUpdate() {
        $rules = [
            'code'      => 'required|numeric',
            'desc'      => 'required',
            'from_date' => 'required',
            'thru_date' => 'required'
        ];
        return $rules;
    }    


    public static function getAll($where=[])
    {
        $query=DB::table('wcm_planting_period')
                ->select('uuid','code', 'desc', DB::raw("
                                (CASE
                                    WHEN status = 'y' THEN 'Active'
                                    WHEN status = 'n' THEN 'Inactive'
                                    WHEN status = 'p' THEN 'Suspend'
                                    WHEN status = 'd' THEN 'Draft'
                                    WHEN status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status"),DB::raw("CONVERT ( VARCHAR, from_date, 105 ) as from_date"),DB::raw("CONVERT ( VARCHAR, thru_date, 105 ) as thru_date"), DB::raw("CONVERT ( VARCHAR, created_at, 105 ) as created_at"), DB::raw("CONVERT ( VARCHAR, updated_at, 105 ) as updated_at"))
                ->where($where);
        return $query;
    }

    public function scopeCurrent($q)
    {
        return $q->where("desc", date('Y'))
            ->first() ?: 0; 
    }

    public static function getExportedColumns()
    {
        return [
            'code AS Kode', 
            'desc AS Deskripsi', 
            DB::raw("
            (CASE
                WHEN status = 'y' THEN 'Active'
                WHEN status = 'n' THEN 'Inactive'
                WHEN status = 'p' THEN 'Suspend'
                WHEN status = 'd' THEN 'Draft'
                WHEN status = 's' THEN 'Submited'
                ELSE '-'
            END) as Status"),
	        DB::raw("CONVERT ( VARCHAR, from_date, 105 ) AS From_date"),
	        DB::raw("CONVERT ( VARCHAR, thru_date, 105 ) AS Thru_date")
        ];
    }
}
