<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForgetPassword extends Model
{
    //
    protected $table='user_forget_password';
    protected $fillable=['uuid','user_id','link_create_date','link_valid_to_date','status'];

     public static function ruleSendEmail() {
        $rules = [
            'email' => 'required|exists:users,email',
        ];
        return $rules;
    }

    public static function forgetpassword ()
    {
    	 $rules = [
            'password' => 'required|min:5',
            're-password' => 'required|min:5',
        ];
        return $rules;
    }
}
