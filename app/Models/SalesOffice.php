<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOffice extends Model {

    protected $table = 'wcm_sales_office';
    protected $fillable = [
        'id', 'name', 'code','status', 'created_by', 'updated_by',
    ];

    public $incrementing = false;
}
