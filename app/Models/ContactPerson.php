<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model
{
    //
    protected $table = 'wcm_contact';

    public $incrementing = false;

    protected $fillable = [
        'name','hp_no','tlp_no','email','customer_id','status','created_by','updated_by','created_at','updated_at',
    ];
}
