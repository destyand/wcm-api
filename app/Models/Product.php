<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model {

    //
    protected $table = "wcm_product";
    protected $fillable = [
        'id', 'name', 'multiplier', 'ident_measr_name', 'measr_name', 'measr_symbol', 'measr_desc'
    ];
    public $incrementing = false;

    public static function limit() {
        $query = DB::table('wcm_product as tb1')
                ->leftjoin('wcm_product_limit AS tb2', 'tb1.id', '=', 'tb2.product_id')
                ->select('tb1.*', 'tb2.qty as limit')
                ->get();
        return $query;
    }

}
