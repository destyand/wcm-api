<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Delivery extends Model
{

    protected $table    = 'wcm_delivery';
    protected $fillable = [
        'number', 'order_id', 'delivery_date', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

    public static function getHeader($where = [], $exported = false)
    {
        $query = DB::table('wcm_delivery as tb1')
        // ->leftjoin(DB::raw('( SELECT SUM (delivery_qty) as delivery_qty,delivery_id,product_id FROM [dbo].[wcm_delivery_item] GROUP BY delivery_id,product_id ) AS tb3'), 'tb3.delivery_id', '=', 'tb1.id')
            ->leftjoin('wcm_delivery_item AS tb3', 'tb3.delivery_id', '=', 'tb1.id')
            ->leftjoin('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
            ->leftjoin(DB::raw('( SELECT SUM (qty) as qty,order_id FROM [dbo].[wcm_order_item] GROUP BY order_id ) AS tb9'), 'tb9.order_id', '=', 'tb1.order_id')
            ->leftjoin('wcm_sales_org as tb4', 'tb4.id', '=', 'tb2.sales_org_id')
            ->leftJoin('wcm_customer AS tb5', 'tb2.customer_id', '=', 'tb5.id')
            ->leftJoin('wcm_sales_office AS tb6', 'tb2.sales_office_id', '=', 'tb6.id')
            ->leftJoin('wcm_sales_group AS tb7', 'tb2.sales_group_id', '=', 'tb7.id')
            ->leftjoin('wcm_product as tb8', 'tb3.product_id', '=', 'tb8.id');
            if ($exported) {
                $query->select("tb2.number", "tb4.name as sales_org_name", "tb5.full_name as customer_name", "tb2.customer_id",
                    "tb2.so_number as code_so", DB::raw('CONVERT(VARCHAR(10), tb2.order_date, 105) as order_date'), 'tb9.qty as quantity_qty', 'tb1.number as no_doc', 'tb8.name as product_name', 'tb3.delivery_qty', DB::raw('CONVERT(VARCHAR(10), tb1.delivery_date, 105) as delivery_date'),
                    'tb1.created_by', DB::raw('CONVERT(VARCHAR(10), tb1.created_at, 105) as created_at'));
            } else {
                $query->select(
                    'tb2.number', 'tb2.sales_org_id', 'tb4.name as sales_org_name',
                    'tb2.customer_id', 'tb5.full_name as customer_name',
                    DB::raw('CONVERT(VARCHAR(10), tb2.order_date, 105) as order_date'), 'tb9.qty as quantity_qty',
                    'tb2.so_number as code_so', 'tb1.number as no_doc',
                    'tb3.delivery_id', 'tb8.id as product_id', 'tb8.name as product_name',
                    'tb3.delivery_qty', DB::raw('CONVERT(VARCHAR(10), tb1.delivery_date, 105) as delivery_date'),
                    'tb1.created_by', DB::raw('CONVERT(VARCHAR(10), tb1.created_at, 105) as created_at')
                );
            }

        $query->where($where);

        return $query;
    }

    public function items()
    {
        return $this->hasMany("App\Models\DeliveryItem", "delivery_id");
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getForCalculationDO($where = [], $whereStr = null)
    {
        $query = DB::table("wcm_delivery_item as tb1")
            ->leftJoin('wcm_delivery as tb2', 'tb2.id', '=', 'tb1.delivery_id')
            ->select('tb1.*')
            ->where($where);
        // ->where('tb2.delivery_date','<=',$whereStr);
        return $query;
    }

    public static function getRulesCreate()
    {
        return [
            "number"        => "required|unique:wcm_delivery",
            "kode_so"       => "required|exists:wcm_orders,so_number",
            "delivery_date" => "required|date|date_format:Y-m-d",
            "delivery_qty"  => "required|numeric",
            "product_id"    => "required|exists:wcm_product,id",
        ];
    }

}
