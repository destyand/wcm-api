<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustBankPayment extends Model
{
    protected $table = 'wcm_cust_payment_method';
}
