<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Rules\RegionRule;

class InitialStocks extends Model {

    //
    protected $table = 'wcm_initial_stocks';
    protected $fillable = [
        'type', 'sales_org_id', 'customer_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id',
        'retail_id', 'month', 'year', 'refrerensi_code', 'remark', 'status',
        'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

    public static function ruleCreate() {
        $rules = [
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id' => 'required|exists:wcm_sales_group,id',
            'month' => 'required',
            'year' => 'required',
        ];
        return $rules;
    }

    public static function ruleUpload() {
        $rules = [
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id' => 'required|exists:wcm_sales_group,id',
            'month' => 'required',
            'year' => 'required',
            'region'      => ['required',new RegionRule],
        ];
        return $rules;
    }

    public static function getInitialStockF5($where,$exported=false){
        $query = DB::table('view_initial_stock_f5 as tb1')
                ->leftJoin('wcm_months as tb2', 'tb2.id', '=', 'tb1.month');
        if($exported){
            $query->select('sales_org_name','customer_name','status_draft','status_submit','sales_office_name','sales_group_name','tb2.name as month_name','year','p01_qty','p01_reduce','p02_qty','p02_reduce','p03_qty','p03_reduce','p04_qty','p04_reduce','p05_qty','p05_reduce');
        }else{
            $query->select('tb1.*','tb2.name as month_name');
        }
                $query->where($where);

        return $query;
    }

    public function setMonthAttribute($value){
        $this->attributes['month'] = str_pad($value, 2, '0', STR_PAD_LEFT);
    }

    public function salesOrg () {
        return $this->hasOne('App\Models\SalesOrg', 'id','sales_org_id');
    }

    public function salesOffice () {
        return $this->hasOne('App\Models\SalesOffice', 'id','sales_office_id');
    }

    public function salesGroup () {
        return $this->hasOne('App\Models\SalesGroup', 'id','sales_group_id');
    }

    public function salesUnit () {
        return $this->hasOne('App\Models\SalesUnit', 'id','sales_unit_id');
    }

    public function customer() {
        return $this->hasOne('App\Models\Customer', 'id','customer_id');
    }

    public function distribReport() {
        return $this->hasMany('App\Models\DistribReports', 'initial_stock_id','id');
    }

    public function items() {
        return $this->hasMany('App\Models\InitialStockItem', 'initial_stock_id');
    }

}
