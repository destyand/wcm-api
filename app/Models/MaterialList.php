<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MaterialList extends Model {

    //
    protected $table = 'wcm_material_list';
    protected $fillable = ['product_id', 'sales_org_id', 'plant_id', 'material_no', 'distrib_channel_id', 'unit', 'mat_desc', 'valid_from', 'valid_to', 'status', 'created_by', 'updated_by'];

    public static function getAll($where = []) {
        $query = DB::table('wcm_material_list as tb1')
                ->leftjoin('wcm_product as tb2', 'tb2.id', '=', 'tb1.product_id')
                ->leftjoin('wcm_sales_org as tb3', 'tb1.sales_org_id', '=', 'tb3.id')
                ->leftjoin('wcm_plant as tb4', 'tb1.plant_id', '=', 'tb4.id')
                ->select('tb1.id', 'tb1.material_no','tb1.mat_desc as name', 'tb1.product_id', 'tb2.name as product_name', 'tb1.sales_org_id', 'tb3.name as sales_org_name', 'tb1.mat_desc', 'tb1.plant_id', 'tb4.name as plant_name', 'tb1.valid_from', 'tb1.valid_to', 'tb1.status')
                ->where($where);


        return $query;
    }

    public static function DistinctProduct($where = []) {
        $query = DB::table('wcm_material_list as tb1')
                ->leftjoin('wcm_product as tb2', 'tb2.id', '=', 'tb1.product_id')
                ->select('tb1.product_id','tb2.name')
                ->distinct('tb1.product_id')
                ->where($where);
        return $query;
    }

}
