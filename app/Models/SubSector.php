<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubSector extends Model {

    protected $table = 'wcm_sub_sector';
    protected $fillable = [
       'name', 'status', 'created_by', 'updated_by',
    ];

    public static function getBulkIDSubSector() {
        $rules = [
            'subsectors' => 'required',
        ];
        return $rules;
    }

}
