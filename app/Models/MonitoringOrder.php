<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MonitoringOrder extends Model
{
    protected $table = 'wcm_orders';

    public static function getAll($where = [],$exported=false) {
        $subQuery = DB::table('wcm_orders AS tb1')
                ->join('wcm_sales_org AS tb2', 'tb1.sales_org_id', '=', 'tb2.id')
                ->join('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
                ->leftjoin('wcm_contract AS tb4', 'tb1.contract_id', '=', 'tb4.id')
                ->leftjoin('wcm_sales_office AS tb5', 'tb1.sales_office_id', '=', 'tb5.id')
                ->leftjoin('wcm_sales_group AS tb6', 'tb1.sales_group_id', '=', 'tb6.id')
                ->leftjoin('wcm_delivery_method AS tb7', 'tb1.delivery_method_id', '=', 'tb7.id')
                ->leftjoin('wcm_bank AS tb8', 'tb1.bank_id', '=', 'tb8.id')
                ->leftjoin('wcm_delivery AS tb9', 'tb1.id', '=', 'tb9.order_id')
                ->leftjoin('view_delivery_item_qty AS tb10', 'tb9.id', '=', 'tb10.delivery_id')
                ->leftjoin('view_order_item_qty AS tb11', 'tb1.id', '=', 'tb11.order_id')
                ->leftjoin('users AS tb12', 'tb1.created_by', '=', 'tb12.id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.number',DB::raw('CONVERT(VARCHAR(10), tb1.good_redemption_due_date, 105) as good_redemption_due_date'), 'tb1.reference_code', 'tb1.booking_code',
                'tb1.so_number', 'tb11.order_item_qty', DB::raw('CONVERT(DECIMAL(10,2),sum([tb10].[delivery_qty])) as delivery_qty'),
                DB::raw('CONVERT(VARCHAR(10), tb1.payment_due_date, 105) as payment_due_date'),'tb1.payment_due_date as payment_due_dates', 'tb1.payment_method',
                DB::raw('CONVERT(VARCHAR(10), tb1.billing_date, 105) as billing_date'),'tb1.billing_date as billing_dates', DB::raw('CONVERT(VARCHAR(10), tb1.df_due_date, 105) as df_due_date'),'tb1.df_due_date as df_due_dates', DB::raw('CONVERT(VARCHAR(10), tb1.df_due_date_plan, 105) as df_due_date_plan'),'tb1.df_due_date_plan as df_due_date_plans', DB::raw('CONVERT(VARCHAR(10), tb1.order_date, 105) as order_date'),'tb1.order_date as order_dates', 'tb1.total_price', 'tb1.ppn', 'tb1.total_price_before_ppn',
                'tb1.upfront_payment', DB::raw('CONVERT(VARCHAR(10), tb1.pickup_due_date, 105) as pickup_due_date'),'tb1.pickup_due_date as pickup_due_dates', 'tb2.id as sales_org_id', 'tb2.name as sales_org_name',
                'tb3.id as customer_id', 'tb3.full_name as customer_name', 'tb4.id as contract_id', 'tb4.number as contract_number',
                'tb5.id as sales_office_id', 'tb5.name as sales_office_name', 'tb6.id as sales_group_id', 'tb6.name as sales_group_name',
                'tb7.id as incoterm_id', 'tb7.ident_name as incoterm', 'tb1.status','tb1.good_redemption_due_date as date_good_redemption_due',
                DB::raw("
                (CASE
                    WHEN tb1.status = 'l' THEN 'PAID'
                    WHEN tb1.status = 'u' THEN 'DPPAID'
                    WHEN tb1.status = 'x' THEN 'CLOSE'
                    WHEN tb1.status = 'r' THEN 'REVIEWED'
                    WHEN tb1.status = 'c' THEN 'COMPLETE'
                    WHEN tb1.status = 'y' THEN 'APPROVE'
                    WHEN tb1.status = 'k' THEN 'GOOD ISSUE'
                    WHEN tb1.status = 'o' THEN 'CANCEL SO'
                    WHEN tb1.status = 's' THEN 'SUBMITED'
                    ELSE '-'
                END) as status_name"), 'tb12.name as created_by', 
                DB::raw("
                    Stuff(
                        (
                        Select '; ' + T2.name
                        From wcm_product As T2
                        Where T2.id IN (SELECT product_id from wcm_order_item WHERE  wcm_order_item.order_id = tb1.id)
                        Order By T2.name
                        For Xml Path(''), type
                        ).value('.', 'nvarchar(max)'), 1,2, '') As product_name
                "),
                DB::raw('CONVERT(VARCHAR(10), tb1.created_at, 105) as created_at'), 
                DB::raw('CONVERT(VARCHAR(10), tb1.updated_at, 105) as updated_at'),
                'tb1.updated_at as updated','tb1.created_at as created',
                DB::raw("CASE WHEN (
                tb1.so_number IS NULL OR
                tb1.sap_booking_code IS NULL OR
                tb1.sap_billing_dp_doc IS NULL OR
                tb1.sap_clearing_doc IS NULL) AND (so_upload IS NULL OR so_upload = 0)
                THEN '-' ELSE tb1.so_number END as so_number_validate"),
                'tb1.so_upload'
                )
                ->where('tb1.status','!=', 'd')
                ->where('tb1.status','!=', 'n')

                ->groupBy('tb1.so_number', 'tb1.id', 'tb1.uuid','tb1.good_redemption_due_date', 'tb1.number', 'tb1.reference_code', 'tb1.booking_code', 'tb1.payment_due_date',
                          'tb1.payment_method', 'tb1.billing_date','tb1.df_due_date', 'tb1.df_due_date_plan', 'tb1.order_date',
                           'tb1.total_price', 'tb1.ppn', 'tb1.total_price_before_ppn', 'tb1.upfront_payment', 'tb1.pickup_due_date',
                           'tb2.id', 'tb2.name', 'tb3.id', 'tb3.full_name', 'tb4.id', 'tb4.number', 'tb5.id', 'tb5.name', 'tb6.id', 'tb6.name',
                           'tb7.id ', 'tb7.ident_name', 'tb11.order_item_qty', 'tb1.status', 'tb12.name', 'tb1.created_at', 'tb1.updated_at',
                           'tb1.sap_booking_code', 'tb1.sap_billing_dp_doc', 'tb1.sap_clearing_doc','tb1.so_upload'
                        );
        $query = DB::table(DB::raw(" ( " . $subQuery->toSql() . " ) as A "))
                ->mergeBindings($subQuery)
                ->select('A.*', DB::raw('(A.order_item_qty - A.delivery_qty) as sisa_tebus'));

        $finalQuery = DB::table(DB::raw(" ( " . $query->toSql() . " ) as B "));
                if($exported){
                    $finalQuery->select('number','reference_code','customer_name','sales_org_name','booking_code','payment_due_date','pickup_due_date','df_due_date_plan','order_date','status_name','so_number','delivery_qty','order_item_qty','incoterm','sales_office_name','sales_group_name','payment_method','total_price_before_ppn','ppn','total_price','upfront_payment','good_redemption_due_date','created_by','created_at');
                }else{
                    $finalQuery->select('*');
                }
                $finalQuery->mergeBindings($query)
                ->where($where);
        // dump($finalQuery->toSql());
        return $finalQuery;
    }

    public static function getBooking($where = []) {
        $query = DB::table('wcm_orders AS tb1')
                ->join('wcm_customer AS tb2', 'tb1.customer_id', '=', 'tb2.id')
                ->leftjoin('wcm_address AS tb3', 'tb2.id', '=', 'tb3.customer_id')
                ->join('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
                ->leftjoin('wcm_village AS tb5', 'tb3.village_id', '=', 'tb5.id')
                ->leftjoin('wcm_sales_unit AS tb6', 'tb3.sales_unit_id', '=', 'tb6.id')
                ->leftjoin('wcm_sales_group AS tb7', 'tb6.sales_group_id', '=', 'tb7.id')
                ->leftjoin('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
                ->select('tb1.number as no_order', 'tb1.total_price', 'tb1.booking_code', 'tb8.name as sales_office_name', 
                        'tb7.name as sales_group_name', 'tb6.name as sales_unit_name', 'tb5.name as village_name', 'tb1.upfront_payment as uang_muka',
                        'tb1.order_date', 'tb1.payment_due_date as batas_akhir_pembayaran', 'tb2.full_name as customer_name', 
                        'tb4.name as produsen', 'tb3.address')
                ->where('tb3.address_type', '=', 'FORMAL')
                ->where($where);

        return $query;
    }

    public static function getSO($where = []) {
        $query = DB::table('wcm_orders AS tb1')
        ->join('wcm_customer AS tb2', 'tb1.customer_id', '=', 'tb2.id')
        ->leftjoin('wcm_address AS tb3', 'tb2.id', '=', 'tb3.customer_id')
        ->join('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftjoin('wcm_village AS tb5', 'tb3.village_id', '=', 'tb5.id')
        ->leftjoin('wcm_sales_unit AS tb6', 'tb3.sales_unit_id', '=', 'tb6.id')
        ->leftjoin('wcm_sales_group AS tb7', 'tb6.sales_group_id', '=', 'tb7.id')
        ->leftjoin('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
        ->join('wcm_contract AS tb9', 'tb1.contract_id', '=', 'tb9.id')
        ->select('tb1.id','tb1.billing_date', 'tb1.so_number', 'tb8.name as sales_office_name', 'tb1.number',
        'tb7.name as sales_group_name', 'tb6.name as sales_unit_name', 'tb5.name as village_name', 'tb1.order_date',
        'tb2.full_name as customer_name', 'tb4.name as produsen', 'tb3.address', 'tb3.tlp_no', 'tb3.fax_no', 'tb9.number as spjb_no',
        'tb1.pickup_due_date', 'tb1.desc as angkutan','tb1.good_redemption_due_date','tb1.so_upload')
        ->where('tb3.address_type', '=', 'FORMAL')
        ->where($where);

        return $query;
    }

    public static function getSOProduct($where = []) {
        $query = DB::table('wcm_order_item AS tb1')
        ->join('wcm_product AS tb2', 'tb1.product_id', '=', 'tb2.id')
        ->join('wcm_plant AS tb3', 'tb1.plant_id', '=', 'tb3.id')
        ->join('wcm_sales_group AS tb4', 'tb3.sales_group_id', '=', 'tb4.id')
        ->leftjoin('wcm_retail AS tb5', 'tb1.retail_id', '=', 'tb5.id')
        ->leftjoin('wcm_sales_unit AS tb6', 'tb5.sales_unit_id', '=', 'tb6.id')
        ->leftjoin('wcm_sales_group AS tb7', 'tb6.sales_group_id', '=', 'tb7.id')
        ->join('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
        ->join('wcm_orders AS tb9', 'tb1.order_id', 'tb9.id')
        ->join('wcm_delivery_method AS tb10', 'tb9.delivery_method_id', 'tb10.id')
        ->select('tb2.name as product_name', DB::raw("sum(tb1.qty) as product_qty"), 'tb3.code as plant_code',
        'tb3.name as plant_name', 'tb4.name as sales_group_name_plant', 'tb7.name as sales_group_name_incoterm',
        'tb8.name as sales_office_name_incoterm', 'tb10.ident_name as delivery_name')
        ->groupBy('tb2.name', 'tb3.code', 'tb3.name', 'tb4.name', 'tb7.name',
        'tb8.name', 'tb10.ident_name')
        ->where($where);

        return $query;
    }

    public static function getSOProductDetail($where = []) {
        $query = DB::table('wcm_order_item AS tb1')
        ->join('wcm_product AS tb2', 'tb1.product_id', '=', 'tb2.id')
        ->join('wcm_retail AS tb3', 'tb1.retail_id', '=', 'tb3.id')
        ->join('wcm_sales_unit AS tb4', 'tb3.sales_unit_id', '=', 'tb4.id')
        ->join('wcm_sales_group AS tb5', 'tb4.sales_group_id', '=', 'tb5.id')
        ->join('wcm_sales_office AS tb6', 'tb5.sales_office_id', '=', 'tb6.id')
        ->select('tb2.id as product_id', 'tb2.name as product_name', 'tb1.qty',
        'tb3.id as retail_id', 'tb3.name as retail_name', 'tb4.id as sales_unit_id', 'tb4.name as sales_unit_name',
        'tb5.id as sales_group_id', 'tb5.name as sales_group_name', 'tb6.id as sales_office_id', 'tb6.name as sales_office_name')
        ->where($where);

        return $query;
    }
}
