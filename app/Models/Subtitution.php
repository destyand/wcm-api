<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Subtitution extends Model
{
    //
    protected $table = "wcm_material_mapping_history";
    protected $fillable = [
        'product_id','sales_org_id','plant_id','material_no','material_no_subtitution','uom',
        'reason','valid_from','valid_to','default','status'
    ];

    public static function ruleCreate() {
        $rules = [
            'product_id' => 'required|exists:wcm_product,id',
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'plant_id' => 'required|exists:wcm_plant,id',
            'material_no' => 'required|exists:wcm_material_list,id',
            'material_no_subtitution' => 'required|exists:wcm_material_list,id',
            'uom' => 'required',
            'reason' => 'required',
            'valid_from' => 'required|date|date_format:Y-m-d',
            'valid_to' => 'required|date|date_format:Y-m-d',
        ];
        return $rules;
    }

    public static function ruleUpdate() {
        $rules = [
            'material_no_subtitution' => 'required|exists:wcm_material_list,id',
            'uom' => 'required',
            'reason' => 'required',
        ];
        return $rules;
    }

    public static function getAll($where=[]){
        $query=DB::table('wcm_material_mapping_history as tb1')
                   ->join('wcm_product as tb2', 'tb1.product_id', '=', 'tb2.id')
                   ->join('wcm_sales_org as tb3','tb1.sales_org_id','=','tb3.id')
                   ->leftjoin('wcm_material_list as tb4','tb1.material_no','=','tb4.id')
                   ->leftjoin('wcm_material_list as tb6','tb1.material_no_subtitution','=','tb6.id')
                   ->join('wcm_plant as tb5','tb1.plant_id','=','tb5.id')
                   ->select('tb1.id','tb1.uuid','tb1.product_id','tb2.name as product_name','tb1.sales_org_id','tb3.name as sales_org_name',
                            'tb1.plant_id','tb5.name as plant_name','tb5.code as plant_code','tb1.material_no','tb4.mat_desc as material_name','tb4.material_no as material_no_code','tb1.material_no_subtitution',
                            'tb6.mat_desc as material_name_subtitusi','tb6.material_no as material_no_subtitution_code','tb1.uom','tb1.reason',
                            DB::raw("CONVERT ( VARCHAR, tb1.valid_from, 105 ) as valid_from"),
                            DB::raw("CONVERT ( VARCHAR, tb1.valid_to, 105 ) as valid_to"),'tb1.default',
                            DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Active'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status"),'tb1.updated_at','tb1.created_at')
                   ->where($where);

        return $query;
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getExportedColumns() {
        return [
            'tb3.name as Produsen',
            'tb1.valid_from as Valid Awal',
            'tb1.valid_to as Valid Akhir',
            'tb5.name as Gudang',
            'tb4.mat_desc as Material',
            'tb2.name as Nama',
            'tb6.mat_desc as Material Subtitusi',
            'tb1.uom as UOM',
            'tb1.reason as Alasan',
            DB::raw("
            (CASE
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status"),
        ];
    }
}
