<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model {
    
    protected $table = 'wcm_bank';

    public $incrementing = false;

    protected $fillable = [
        'name', 'status', 'created_by', 'updated_by','created_at', 'updated_at',
    ];

}
