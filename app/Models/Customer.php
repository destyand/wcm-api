<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{

    protected $table     = 'wcm_customer';
    public $incrementing = false;
    protected $fillable  = [
        'id', 'full_name', 'owner', 'register_date', 'code_cust_group', 'name_cust_group', 'old_number',
        'npwp_no', 'npwp_register', 'recomd_letter_date', 'recomd_letter', 'valid_date_tdp', 'tdp_no', 'situ_no',
        'valid_date_situ', 'valid_date_siup', 'siup_no', 'status', 'created_by', 'updated_by', 'created_at',
        'updated_at', 'category', 'asset', 'revenue', 'account_year', 'sap_status', 'category_type',
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User', 'id_customer');
    }

    public static function getData($where = array(), $type = null)
    {
        switch ($type) {
            case 'address':
                $select = array('b.address', 'b.village_id', 'f.name as village_name', 'b.sales_unit_id', 'c.name as sales_unit_name',
                    'c.sales_group_id', 'd.name as sales_group_name', 'd.sales_office_id', 'e.name as sales_office_name', 'b.tlp_no',
                    'b.fax_no', 'b.address_type');
                $group = array('b.address', 'b.village_id', 'f.name ', 'b.sales_unit_id', 'c.name ',
                    'c.sales_group_id', 'd.name ', 'd.sales_office_id', 'e.name ', 'b.tlp_no',
                    'b.fax_no', 'b.address_type');
                break;
            default:
                $select = array(
                    'a.id', 'a.uuid', 'a.full_name', 'a.owner', 'b.address', 'b.village_id', 'f.name as village_name', 'b.sales_unit_id',
                    'c.name as sales_unit_name', 'c.sales_group_id', 'd.name as sales_group_name', 'd.sales_office_id',
                    'e.name as sales_office_name', 'b.tlp_no', 'b.fax_no',
                    DB::raw("
                    (CASE
                        WHEN a.status = 'y' THEN 'Active'
                        WHEN a.status = 'n' THEN 'Inactive'
                        WHEN a.status = 'p' THEN 'Suspend'
                        WHEN a.status = 'd' THEN 'Draft'
                        WHEN a.status = 's' THEN 'Submited'
                        ELSE '-'
                    END) as status"),
                    'a.category', 'a.asset', 'a.revenue', 'a.account_year',
                    DB::raw("CONCAT(CONVERT ( VARCHAR, a.created_at, 105 ), ' ', CONVERT ( VARCHAR, a.created_at, 108 )) as created_at"),
                    DB::raw("CONCAT(CONVERT ( VARCHAR, a.updated_at, 105 ), ' ', CONVERT ( VARCHAR, a.updated_at, 108 )) as updated_at"),
                );
                $group = array(
                    'a.id', 'a.uuid', 'a.full_name', 'a.owner', 'b.address', 'b.village_id', 'f.name', 'b.sales_unit_id',
                    'c.name', 'c.sales_group_id', 'd.name', 'd.sales_office_id',
                    'e.name', 'b.tlp_no', 'b.fax_no',
                    'a.status', 'a.category', 'a.asset', 'a.revenue', 'a.account_year', 'a.created_at', 'a.updated_at',
                );
                break;
        }

        $query = DB::table('wcm_customer as a')
            ->leftJoin('wcm_address as b', function ($q) {
                $q->on('a.id', '=', 'b.customer_id')->where('b.address_type', '=', 'FORMAL');
            })
            ->leftJoin('wcm_sales_unit as c', function ($q) {
                $q->on('b.sales_unit_id', '=', 'c.id');
            })
            ->leftJoin('wcm_sales_group as d', function ($q) {
                $q->on('b.sales_group_id', '=', 'd.id');
            })
            ->leftJoin('wcm_sales_office as e', function ($q) {
                $q->on('b.sales_office_id', '=', 'e.id');
            })
            ->leftJoin('wcm_village as f', function ($q) {
                $q->on('b.village_id', '=', 'f.id');
            })
            ->leftJoin('wcm_customer_retailer_assg as g', function ($q) {
                $q->on('g.customer_id', '=', 'a.id');
            })
            ->leftJoin("wcm_customer_sales_area as csa", "a.id","csa.customer_id")
            ->leftJoin("wcm_sales_area as sa", "csa.sales_area_id", "sa.id")
            ->leftJoin('wcm_sales_org as h',"h.id", "sa.sales_org_id");
        $query->select($select);
        if (count($where) == 0) {
            $query->distinct('g.customer_id');
        }

        $query->where($where)
            ->whereIn('a.code_cust_group', ['AR01', 'AR02'])
            ->groupBy($group);
        return ($query);

    }

    public static function getDataRelation($where = array())
    {
        $select = array('a.id', 'a.uuid', 'a.full_name as name', 'b.sales_org_id', 'c.name as sales_org_name');

        $query = DB::table('wcm_customer as a')
            ->leftJoin('wcm_customer_retailer_assg as b', function ($q) {
                $q->on('a.id', '=', 'b.customer_id');
            })
            ->leftJoin('wcm_sales_org as c', function ($q) {
                $q->on('b.sales_org_id', '=', 'c.id');
            })
            ->select($select)
            ->where($where)
            ->groupBy('a.id', 'a.uuid', 'a.full_name', 'b.sales_org_id', 'c.name');
        return ($query);
    }

    public static function getAll($where = [])
    {
        $query = DB::table('wcm_customer AS tb1')
            ->leftJoin('wcm_cust_sales_org_assg AS tb2', 'tb1.id', '=', 'tb2.customer_id')
            ->leftjoin('wcm_address as tb3', function ($q) {
                $q->on('tb1.id', '=', 'tb3.customer_id')->where('address_type', 'FORMAL');
            })
            ->leftjoin('wcm_sales_unit AS tb4', 'tb3.sales_unit_id', '=', 'tb4.id')
            ->leftjoin('wcm_sales_group AS tb7', 'tb3.sales_group_id', '=', 'tb7.id')
            ->leftjoin('wcm_sales_office AS tb8', 'tb7.sales_office_id', '=', 'tb8.id')
            ->select('tb1.*', 'tb2.sales_org_id', 'tb4.id as sales_unit_id', 'tb4.name as sales_unit_name', 'tb7.id as sales_group_id', 'tb7.name as sales_group_name', 'tb8.id as sales_office_id', 'tb8.name as sales_office_name')
            ->where($where);

        return $query;
    }

    public function customerRetailsAssign()
    {
        return $this->hasMany("App\Models\CustomerRetailerAssg", "customer_id", "id");
    }

    public function address()
    {
        return $this->hasOne("App\Models\Address", "customer_id", "id");
    }

    public static function getExportedColumns() {
        return [
            'a.id AS ID',
            'a.full_name as Nama',
            'a.owner as Nama pemilik',
            DB::raw("
            (CASE
                WHEN a.status = 'y' THEN 'Active'
                WHEN a.status = 'n' THEN 'Inactive'
                WHEN a.status = 'p' THEN 'Suspend'
                WHEN a.status = 'd' THEN 'Draft'
                WHEN a.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status"),
            'b.address AS Alamat',
            'e.name as Provinsi',
            'd.name as Kabupaten',
            'c.name AS Kecamatan',
            'b.tlp_no AS No. Tlp Kantor',
            'b.fax_no As No. Fax Kantor',
        ];
    }

    public function salesOrg() {
        return $this->hasOne("App\Models\CustSalesOrgAssg",'customer_id', 'id');
    }

    public function paymentMethod() {
        return $this->hasOne("App\Models\CustBankPayment",'customer_id', 'id');
    }
}
