<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportF5Items extends Model
{
    //
    protected $table = 'wcm_report_f5_items';
    protected $fillable = [
       'product_id','report_f5_id','stok_awal','penebusan','penyaluran','stok_akhir','number_f5','status','created_by','updated_by','created_at','updated_at',  
    ];

    public static function getStokReportF5 ( $where = [] )
    {
        $query = DB::table('wcm_report_f5_items as tb1')
                    ->leftjoin('wcm_report_f5 as tb2','tb2.id','=','tb1.report_f5_id')
                    ->select('tb1.*')
                    ->where($where);
        return $query;
    }

    public static function ruleCreates() {
        $rules = [
            'product_id' => 'required|exists:wcm_product,id',
            'report_f5_id' => 'required|exists:wcm_report_f5,id',
            'stok_awal' => 'required',
            'penebusan' => 'required',
            'penyaluran' => 'required',
            'stok_akhir' => 'required',
            'number_f5' => 'required|exists:wcm_report_f5,number',
            'status' => 'required',
        ];
        return $rules;
    }

    public static function ruleCreate() {
        $rules = [
            'distrib_report_id' => 'required|exists:wcm_distrib_reports,id',
            'report_f5_id' => 'required|exists:wcm_report_f5,id',
            'retail_id' => 'required|exists:wcm_retail,id',
            'qty' => 'required',
            'product_id' => 'required|exists:wcm_product,id',
            'status' => 'required',
        ];
        return $rules;
    }
}
