<?php

namespace App\Models;

use App\Rules\RetailCustomerSalesOfficeRule;
use App\Rules\RetailOwnerUniqueRule;
use App\Rules\RetailSalesUnitExistSPJB;
use App\Rules\RetailUniqSalesUnitRule;
use App\Rules\RetailUniqSalesUnitRuleUpdate;
use App\Rules\SalesUnits;
use App\Rules\SiupNoRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Retail extends Model
{

    protected $table = 'wcm_retail';
    protected $fillable = [
        'code', 'name', 'email', 'owner', 'address', 'sales_unit_id', 'village', 'tlp_no', 'hp_no', 'fax_no', 'latitude',
        'longitude', 'npwp_no', 'npwp_register', 'siup_no', 'valid_date_siup', 'situ_no', 'valid_date_situ', 'tdp_no',
        'valid_date_tdp', 'recomd_letter', 'recomd_letter_date', 'old_number', 'status', 'created_by', 'updated_by',
        'created_at', 'updated_at', 'sub_district_default', 'retail_administratif', 'parent_id',
    ];

    // protected $hidden = [
    //     'id'
    // ];

    // public $incrementing = false;

    public static function getData($where = array(), $except = null, $arr = [])
    {
        // return $arr['customer_uuid'];
        $query = DB::table('wcm_retail as a');
        $query->leftJoin('wcm_sales_unit as b', function ($q) {
            $q->on('a.sales_unit_id', '=', 'b.id');
        });
        $query->leftJoin('wcm_sales_group as c', function ($q) {
            $q->on('b.sales_group_id', '=', 'c.id');
        });
        $query->leftJoin('wcm_sales_office as d', function ($q) {
            $q->on('c.sales_office_id', '=', 'd.id');
        });

        /* If where param == retail */
        if (array_key_exists('type', $where)) {
            $query->select('a.id', 'a.code',
                'a.uuid', 'a.name', 'a.email', 'a.owner', 'a.address', 'a.village', 'a.tlp_no', 'a.hp_no', 'a.fax_no',
                'a.latitude', 'a.longitude', 'a.npwp_no', 'a.npwp_register', 'a.siup_no',
                DB::raw('CONVERT ( VARCHAR, a.valid_date_siup, 105 ) as valid_date_siup'),
                'a.situ_no',
                DB::raw('CONVERT ( VARCHAR, a.valid_date_situ, 105 ) as valid_date_situ'),
                'a.tdp_no',
                DB::raw('CONVERT ( VARCHAR, a.valid_date_tdp, 105 ) as valid_date_tdp'),
                'a.recomd_letter',
                DB::raw('CONVERT ( VARCHAR, a.recomd_letter_date, 105 ) as recomd_letter_date'),
                'a.old_number',
                'b.id as sales_unit_id', 'b.name as sales_unit_name', 'c.id as sales_group_id', 'c.name as sales_group_name',
                'd.id as sales_office_id', 'd.name as sales_office_name', DB::raw("
                    (CASE
                        WHEN a.status = 'y' THEN 'Active'
                        WHEN a.status = 'n' THEN 'Inactive'
                        WHEN a.status = 'p' THEN 'Suspend'
                        WHEN a.status = 'd' THEN 'Draft'
                        WHEN a.status = 's' THEN 'Submited'
                        ELSE '-'
                    END) as status"), 'a.created_by', 'a.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, a.created_at, 105 ), ' ', CONVERT ( VARCHAR, a.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, a.updated_at, 105 ), ' ', CONVERT ( VARCHAR, a.updated_at, 108 )) as updated_at"),
                "a.retail_administratif"
            );
            unset($where['type']);
            $query->where($where);

            return $query;
        }

        if ($except == true) {
            $query->leftJoin('wcm_customer_retailer_assg as e', function ($q) use ($arr) {
                $q->on('a.id', '=', 'e.retail_id')->where('e.customer_id', '<>', $arr['customer_uuid'])->where('e.sales_org_id', '<>', $arr['sales_org_uuid']);
            });

        } else {
            $query->leftJoin('wcm_customer_retailer_assg as e', function ($q) {
                $q->on('a.id', '=', 'e.retail_id');
            });
        }

        $query->leftJoin('wcm_customer as f', function ($q) {
            $q->on('e.customer_id', '=', 'f.id');
        });
        $query->leftJoin('wcm_sales_org as g', function ($q) {
            $q->on('e.sales_org_id', '=', 'g.id');
        });
        $query->select('a.id', 'e.sales_org_id', 'g.uuid as sales_org_uuid', 'g.name as sales_org_name', 'e.customer_id', 'f.full_name as customer_name', 'f.uuid as customer_uuid', 'a.code',
            'a.uuid', 'a.name', 'a.email', 'a.owner', 'a.address', 'a.village', 'a.tlp_no', 'a.hp_no', 'a.fax_no',
            'a.latitude', 'a.longitude', 'a.npwp_no', 'a.npwp_register', 'a.siup_no', 'a.sub_district_default',
            DB::raw('CONVERT ( VARCHAR, a.valid_date_siup, 105 ) as valid_date_siup'),
            'a.situ_no',
            DB::raw('CONVERT ( VARCHAR, a.valid_date_situ, 105 ) as valid_date_situ'),
            'a.tdp_no',
            DB::raw('CONVERT ( VARCHAR, a.valid_date_tdp, 105 ) as valid_date_tdp'),
            'a.recomd_letter',
            DB::raw('CONVERT ( VARCHAR, a.recomd_letter_date, 105 ) as recomd_letter_date'),
            'a.old_number',
            'b.id as sales_unit_id', 'b.name as sales_unit_name', 'c.id as sales_group_id', 'c.name as sales_group_name',
            'd.id as sales_office_id', 'd.name as sales_office_name', DB::raw("
                (CASE
                    WHEN a.status = 'y' THEN 'Active'
                    WHEN a.status = 'n' THEN 'Inactive'
                    WHEN a.status = 'p' THEN 'Suspend'
                    WHEN a.status = 'd' THEN 'Draft'
                    WHEN a.status = 's' THEN 'Submited'
                    ELSE '-'
                END) as status"), 'a.created_by', 'a.updated_by',
            DB::raw("CONCAT(CONVERT ( VARCHAR, a.created_at, 105 ), ' ', CONVERT ( VARCHAR, a.created_at, 108 )) as created_at"),
            DB::raw("CONCAT(CONVERT ( VARCHAR, a.updated_at, 105 ), ' ', CONVERT ( VARCHAR, a.updated_at, 108 )) as updated_at"),
            'a.retail_administratif'
        );
        $query->where($where);
        // ->where(DB::raw("(customer_id != '1000000000' or customer_id is null)"));
        return ($query);
    }

    public static function ruleCreate()
    {
        $rules = [
            'name' => ['required', new RetailUniqSalesUnitRule],
            'email' => 'sometimes|nullable|email',
            'owner' => 'required',
            'address' => 'required',
            'sales_unit_id' => ['required', new SalesUnits],
//            'npwp_no' => 'required|unique:wcm_retail',
            'hp_no' => 'required|numeric',
            'siup_no' => ['required', new SiupNoRule],
            'status' => 'required',
        ];
        return $rules;
    }

    public static function ruleCreateUpload()
    {
        $rules = [
            'customer_id' => 'required|exists:wcm_customer,id',
            'name' => ['required', new RetailUniqSalesUnitRule],
            'owner' => 'required',
            'name_and_owner' => [new RetailOwnerUniqueRule],
            'sales_unit_id' => [new RetailCustomerSalesOfficeRule],
            'email' => 'nullable|email|unique:wcm_retail',
            'address' => 'required|max:191',
            'siup_no' => ['required', new SiupNoRule],
            'status' => 'required',
            // 'hp_no' => 'required|unique:wcm_retail,hp_no|max:14',
            'hp_no' => 'required', //|numeric|digits_between:11,14
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'sales_unit_id_spjb' => ['required', new RetailSalesUnitExistSPJB],
        ];
        return $rules;
    }

    public static function ruleUpdate()
    {
        $rules = [
            'name' => ['required', new RetailUniqSalesUnitRuleUpdate],
            'email' => 'sometimes|nullable|email',
            'owner' => 'required',
            'address' => 'required',
            'sales_unit_id' => 'required',
//            'npwp_no' => 'required',
            'hp_no' => 'required|numeric',
            'siup_no' => 'required',
        ];
        return $rules;
    }

    public static function ruleAdministratif()
    {
        $rules = [
            'id' => 'required|exists:wcm_retail',
            'name' => 'required',
            'email' => 'sometimes|nullable|email',
            'owner' => 'required',
            'address' => 'required',
            'sales_unit_id' => ['required'],
            'siup_no' => 'required',
        ];
        return $rules;
    }

    public static function ruleAdministratifupdate()
    {
        $rules = [
            'name' => 'required',
            'email' => 'sometimes|nullable|email',
            'owner' => 'required',
            'address' => 'required',
            'sales_unit_id' => 'required',
            'siup_no' => 'required',
        ];
        return $rules;
    }

    public static function ruleFileImport()
    {
        $rules = [
            'file' => 'required',
        ];
        return $rules;
    }

    public static function ruleImport()
    {
        $rules = [
            'distributor' => 'required|exists:wcm_customer,id',
            'name' => ['required', new RetailUniqSalesUnitRule],
            'owner' => 'required',
            'name_and_owner' => [new RetailOwnerUniqueRule],
            'sales_unit_id' => [new RetailCustomerSalesOfficeRule],
            'email' => 'nullable|email|unique:wcm_retail',
            'address' => 'required',
//            'sales_unit_id'  => 'required|exists:wcm_sales_unit,id',
            //            'npwp_no' => 'required|unique:wcm_retail',
            'siup_no' => ['required', new SiupNoRule],
            'status' => 'required',
            // 'no_handphone' => 'required|unique:wcm_retail,hp_no|max:14',
            'no_handphone' => 'required',//|numeric|digits_between:11,14
            'sales_org' => 'required|exists:wcm_sales_org,id',
            'sales_unit_id_spjb' => ['required', new RetailSalesUnitExistSPJB],
        ];
        return $rules;
    }

    public function customerRetailerAssg()
    {
        return $this->hasMany('App\Models\CustomerRetailerAssg', 'retail_id');
    }

    public static function getAll()
    {
        $query = DB::table('wcm_retail AS tb1')
            ->leftJoin('wcm_customer_retailer_assg AS tb2', 'tb1.id', '=', 'tb2.retail_id')
            ->select('tb1.*', 'tb2.customer_id');

        return $query;
    }

    public static function retailDitributionDO($where = [])
    {
        $query = DB::table('wcm_retail as a')
            ->leftJoin('wcm_sales_unit as b', function ($q) {
                $q->on('a.sales_unit_id', '=', 'b.id');
            })
            ->leftJoin('wcm_sales_group as c', function ($q) {
                $q->on('b.sales_group_id', '=', 'c.id');
            })
            ->leftJoin('wcm_sales_office as d', function ($q) {
                $q->on('c.sales_office_id', '=', 'd.id');
            })
            ->leftJoin('wcm_customer_retailer_assg as e', function ($q) {
                $q->on('a.id', '=', 'e.retail_id');
            })
            ->leftJoin('wcm_customer as f', function ($q) {
                $q->on('e.customer_id', '=', 'f.id');
            })
            ->leftJoin('wcm_sales_org as g', function ($q) {
                $q->on('e.sales_org_id', '=', 'g.id');
            })
            ->select('a.sub_district_default', 'a.id', 'e.sales_org_id', 'g.uuid as sales_org_uuid', 'e.customer_id', 'f.uuid as customer_uuid', 'a.code', 'a.uuid', 'a.name', 'a.email', 'a.owner', 'a.address', 'a.village', 'a.tlp_no', 'a.hp_no', 'a.fax_no', 'a.latitude', 'a.longitude', 'a.npwp_no', 'a.npwp_register', 'a.siup_no', 'a.valid_date_siup', 'a.situ_no', 'a.valid_date_situ', 'a.tdp_no', 'a.valid_date_tdp', 'a.recomd_letter', 'a.recomd_letter_date', 'a.old_number', 'b.id as sales_unit_id', 'b.name as sales_unit_name', 'c.id as sales_group_id', 'c.name as sales_group_name', 'd.id as sales_office_id', 'd.name as sales_office_name', DB::raw("
        (CASE
            WHEN a.status = 'y' THEN 'Active'
            WHEN a.status = 'n' THEN 'Inactive'
            WHEN a.status = 'p' THEN 'Suspend'
            WHEN a.status = 'd' THEN 'Draft'
            WHEN a.status = 's' THEN 'Submited'
            ELSE '-'
        END) as status"), 'a.created_by', 'a.updated_by', 'a.created_at', 'a.updated_at')
            ->where($where);
        // ->WhereRaw("([a].[status] = 'y' OR [a].[status] = 'p')");

        return ($query);
    }

    public static function getSubRetail($where = [], $kecamatan = [])
    {
        $query = DB::table('wcm_retail')->where('sub_district_default', 1)->whereIn('sales_unit_id', $kecamatan);

        $retail = DB::table('wcm_retail as tb1')
            ->join('wcm_customer_retailer_assg as tb2', 'tb1.id', '=', 'tb2.retail_id')
            ->select('tb1.*')
            ->where($where)
            ->union($query);

        return $retail;
    }

    /**
     * Foreign To Sales Unit
     *
     * @return     <Object>  Sales Unit
     */
    public function salesUnit()
    {
        return $this->hasOne("App\Models\SalesUnit", "id", "sales_unit_id");
    }

    public function scopeMasterPengecer($query, $where = [])
    {
        return DB::table(DB::raw("({$query->toSql()}) as a"))
            ->leftJoin('wcm_sales_unit as b', function ($q) {
                $q->on('a.sales_unit_id', '=', 'b.id');
            })
            ->leftJoin('wcm_sales_group as c', function ($q) {
                $q->on('b.sales_group_id', '=', 'c.id');
            })
            ->leftJoin('wcm_sales_office as d', function ($q) {
                $q->on('c.sales_office_id', '=', 'd.id');
            })
        // ->leftJoin('wcm_customer_retailer_assg as e', function ($q) {
        //     $q->on('a.id', '=', 'e.retail_id');
        // })
            ->select('a.sub_district_default', 'a.id', 'a.code', 'a.uuid', 'a.name', 'a.email', 'a.owner', 'a.address', 'a.village', 'a.tlp_no', 'a.hp_no', 'a.fax_no', 'a.latitude', 'a.longitude', 'a.npwp_no', 'a.npwp_register', 'a.siup_no', 'a.valid_date_siup', 'a.situ_no', 'a.valid_date_situ', 'a.tdp_no', 'a.valid_date_tdp', 'a.recomd_letter', 'a.recomd_letter_date', 'a.old_number', 'b.id as sales_unit_id', 'b.name as sales_unit_name', 'c.id as sales_group_id', 'c.name as sales_group_name', 'd.id as sales_office_id', 'd.name as sales_office_name', DB::raw("
        (CASE
            WHEN a.status = 'y' THEN 'Active'
            WHEN a.status = 'n' THEN 'Inactive'
            WHEN a.status = 'p' THEN 'Suspend'
            WHEN a.status = 'd' THEN 'Draft'
            WHEN a.status = 's' THEN 'Submited'
            ELSE '-'
        END) as status"), 'a.created_by', 'a.updated_by', 'a.created_at', 'a.updated_at', 'a.retail_administratif')
            ->where($where);
    }

    public function scopeNotAdministratif($query)
    {
        return $query->where("retail_administratif", "!=", 1)
            ->orWhereRaw("retail_administratif is null");
    }

    public function scopeAdministratif($query)
    {
        return $query->where("retail_administratif", 1);
    }
}
