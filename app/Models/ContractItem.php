<?php

namespace App\models;

use App\Rules\SalesUnits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContractItem extends Model
{
    //
    protected $table    = 'wcm_contract_item';
    protected $fillable = [
        'uuid', 'contract_id', 'product_id', 'sales_office_id',
        'sales_group_id', 'sales_unit_id', 'month', 'year',
        'initial_qty', 'doc_ref_no', 'active_date', 'inactive_date',
        'status', 'update_at', 'created_by', 'updated_by', 'contract_gov_item_id',
    ];

    public static function spjbContractCreateItems()
    {
        $rules = [
            'contract_id'     => 'required|exists:wcm_contract,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => ["required", new SalesUnits],
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'required',
        ];
        return $rules;
    }

    public static function spjbContractItemsImport()
    {
        $rules = [
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'required',
        ];
        return $rules;
    }

    public static function getItemSPJB($where = [])
    {
        $query = DB::table('wcm_contract_item AS tb1')
            ->leftjoin('wcm_contract AS tb8', 'tb8.id', '=', 'tb1.contract_id')
            ->leftjoin('wcm_product as tb2', 'tb2.id', '=', 'tb1.product_id')
            ->leftjoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb8.sales_org_id')
            ->leftjoin('wcm_sales_group AS tb4', 'tb4.id', '=', 'tb1.sales_group_id')
            ->leftjoin('wcm_sales_office AS tb5', 'tb5.id', '=', 'tb1.sales_office_id')
            ->leftjoin('wcm_sales_unit AS tb6', 'tb6.id', '=', 'tb1.sales_unit_id')
            ->leftjoin('wcm_customer AS tb7', 'tb7.id', '=', 'tb8.customer_id')
            ->select('tb1.id',
                'tb1.uuid',
                'tb1.contract_id', 'tb8.number as no_doc',
                'tb1.sales_office_id', 'tb5.name as sales_office_name',
                'tb1.sales_group_id', 'tb4.name as sales_group_name',
                'tb1.sales_unit_id', 'tb6.name as sales_unit_name',
                'tb8.sales_org_id', 'tb3.name as sales_org_name',
                'tb8.customer_id', 'tb7.full_name as customer_name',
                'tb1.product_id', 'tb2.name as product_name',
                'tb1.month',
                'tb1.initial_qty',
                'tb1.year', 'tb1.status', DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"),
                DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at")
            )
            ->where($where);
        // ->where('tb8.contract_type', 'asli');

        return $query;
    }

    public static function getItemSPJBDownload($where = [])
    {
        $query = DB::table('wcm_contract_item AS tb1')
            ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
            ->select(
                'tb1.contract_id',
                'tb2.sales_org_id',
                'tb2.number',
                'tb2.customer_id',
                'tb1.sales_office_id',
                'tb1.sales_group_id',
                'tb1.sales_unit_id',
                'tb1.year',
                'tb1.month',
                'tb1.product_id',
                'tb1.initial_qty'
            )
            ->where($where);

        return $query;
    }

    public function contract()
    {
        return $this->hasOne('App\Models\Contract', 'id', 'contract_id');
    }

    public function scopeOperationalRetail($query,$order, $salesUnitID, $year)
    {
        $query->whereIn("month", currentTriwulan(true))
            ->whereIn("sales_unit_id", $salesUnitID)
            ->whereHas("contract", function($q) use ($order, $year){
                $q->where("contract_type", "operational")
                    ->where("sales_org_id", $order->sales_org_id)
                    ->where("year", $year);
            })
            ->whereHas("contract", function($q) use ($order){
                $q->where("id", $order->contract_id);
            })
            ->where("status", "y")
            ->get();
    }    
}
