<?php

namespace App\models;

use App\Rules\RayonisasiSPJB;
use App\Rules\SPJBDistributorInSalesUnitRule;
use App\Rules\SPJBHeaderRule;
use App\Rules\SPJBItemRule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Rules\SalesOrgAssgRules;

class Contract extends Model
{
    //
    protected $table    = 'wcm_contract';
    protected $fillable = [
        'number', 'customer_id', 'sales_org_id', 'status',
        'contract_type', 'year', 'from_date', 'thru_date',
        'commodity', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by',
    ];

    public static function spjbHeaderCreate()
    {
        $rules = [
            'number'                => 'required',
            'sales_org_id'          => 'required|exists:wcm_sales_org,id',
            'customer_id'           => 'required|exists:wcm_customer,id',
            'year'                  => 'required',
            'rayonisasi'            => ["required", new RayonisasiSPJB],
            'distributor_salesunit' => ["required", new SPJBDistributorInSalesUnitRule],
        ];
        return $rules;
    }

    public static function spjbUploadDetail()
    {
        $rules = [
            'rayonisasi'            => ["required", new RayonisasiSPJB],
            'distributor_salesunit' => ["required", new SPJBDistributorInSalesUnitRule],
            'data_header'           => ["required", new SPJBHeaderRule],
            'initial_qty'           => 'numeric|min:0',
            'sales_org_id'          => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'customer_id'           => 'required|exists:wcm_customer,id',
        ];
        return $rules;
    }

    public static function spjbOperationalUpload()
    {
        $rules = [
            'number'                  => 'required',
            'sales_org_id'            => ['required','exists:wcm_sales_org,id', new SalesOrgAssgRules],
            'customer_id'             => 'required|exists:wcm_customer,id',
            'year'                    => 'required',
            'rayonisasi'              => ["required", new RayonisasiSPJB],
            // 'distributor_salesunit' => ["required", new SPJBDistributorInSalesUnitRule],
            'data_header_operational' => ["required", new SPJBHeaderRule],
            'data_header_asal'        => ["required", new SPJBHeaderRule],
            'initial_qty'           => 'numeric|min:0',
        ];
        return $rules;
    }

    public static function spjbOperationalUploadItem()
    {
        $rules = [
            'item_spjb_operational' => ["required", new SPJBItemRule],
            'item_spjb_asal'        => ["required", new SPJBItemRule],
        ];
        return $rules;
    }

    public static function getgroupbyspjb()
    {
        $rules = [
            'uuid'            => 'required|exists:wcm_contract,uuid',
            'sales_office_id' => 'required|exists:wcm_contract_item,sales_office_id',
        ];
        return $rules;
    }

    public static function getHeaderCoontract($where = [])
    {
        # code...
        $query = DB::table('wcm_contract AS tb1')
            ->leftjoin(DB::raw('( SELECT DISTINCT contract_id, sales_office_id FROM wcm_contract_item ) AS tb2'), 'tb2.contract_id', '=', 'tb1.id')
            ->leftjoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb1.sales_org_id')
            ->leftjoin('wcm_sales_office AS tb5', 'tb5.id', '=', 'tb2.sales_office_id')
            ->leftjoin('wcm_customer AS tb7', 'tb7.id', '=', 'tb1.customer_id')
            ->leftJoin("wcm_address AS tb8", function($join){
                $join->on("tb8.customer_id", "tb1.customer_id")
                ->where("tb8.address_type","FORMAL");
            })
            ->select('tb1.id', 'tb1.uuid', 'tb1.year',
                'tb1.number as no_doc',
                'tb5.id as sales_office_id', 'tb5.name as sales_office_name',
                'tb1.customer_id', 'tb7.full_name as customer_name',
                'tb1.sales_org_id', 'tb3.name as sales_org_name',
                'tb1.status',
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"), DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"), DB::raw("CONVERT ( VARCHAR, tb1.updated_at, 105 ) as updated_at"))
        // ->where('tb1.contract_type', 'asli')
            ->where($where);

        return $query;
    }

    public static function getHeaderCoontractDatatable($where = [])
    {
        # code...
        $query = DB::table('wcm_contract AS tb1')
            // ->leftjoin(DB::raw('( SELECT DISTINCT contract_id, sales_office_id FROM wcm_contract_item ) AS tb2'), 'tb2.contract_id', '=', 'tb1.id')
            ->leftjoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb1.sales_org_id')
            // ->leftjoin('wcm_sales_office AS tb5', 'tb5.id', '=', 'tb2.sales_office_id')
            ->leftjoin('wcm_customer AS tb7', 'tb7.id', '=', 'tb1.customer_id')
            ->leftJoin("wcm_address AS tb8", function($join){
                $join->on("tb8.customer_id", "tb1.customer_id")
                ->where("tb8.address_type","FORMAL");
            })
            ->select('tb1.id', 'tb1.uuid', 'tb1.year',
                'tb1.number as no_doc',
                // 'tb5.id as sales_office_id', 'tb5.name as sales_office_name',
                'tb1.customer_id', 'tb7.full_name as customer_name',
                'tb1.sales_org_id', 'tb3.name as sales_org_name',
                'tb1.status',
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"), DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"), DB::raw("CONVERT ( VARCHAR, tb1.updated_at, 105 ) as updated_at"))
        // ->where('tb1.contract_type', 'asli')
            ->where($where);

        return $query;
    }

    public static function MasterContrract($where = [])
    {
        # code...
        $query = DB::table('wcm_contract AS tb1')
            ->leftjoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb1.sales_org_id')
            ->leftjoin('wcm_customer AS tb7', 'tb7.id', '=', 'tb1.customer_id')
            ->leftJoin("wcm_address AS tb8", function($join){
                $join->on("tb8.customer_id", "tb1.customer_id")
                ->where("tb8.address_type","FORMAL");
            })
            ->select('tb1.id', 'tb1.uuid', 'tb1.year',
                'tb1.number as no_doc',
                'tb1.customer_id', 'tb7.full_name as customer_name',
                'tb1.sales_org_id', 'tb3.name as sales_org_name',
                'tb1.status',
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'z' THEN 'Partial Active'
                            ELSE '-'
                        END) as status_name"), DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"), DB::raw("CONVERT ( VARCHAR, tb1.updated_at, 105 ) as updated_at"))
        // ->where('tb1.contract_type', 'asli')
            ->where($where);

        return $query;
    }

    public static function getOperationalContract($where = [])
    {
        # code...
        $query = DB::table('wcm_contract AS tb1')
            ->join(DB::raw('( SELECT DISTINCT contract_id, sales_office_id, sales_group_id FROM wcm_contract_item ) AS tb2'), 'tb2.contract_id', '=', 'tb1.id')
            ->leftjoin('wcm_sales_org AS tb3', 'tb3.id', '=', 'tb1.sales_org_id')
            ->leftjoin('wcm_sales_group AS tb4', 'tb4.id', '=', 'tb2.sales_group_id')
            ->leftjoin('wcm_sales_office AS tb5', 'tb5.id', '=', 'tb2.sales_office_id')
            ->leftjoin('wcm_customer AS tb7', 'tb7.id', '=', 'tb1.customer_id')
            ->select('tb1.id', 'tb1.uuid', 'tb1.year',
                'tb1.number as no_doc',
                'tb5.id as sales_office_id', 'tb5.name as sales_office_name',
                'tb4.id as sales_group_id', 'tb4.name as sales_group_name',
                'tb1.customer_id', 'tb7.full_name as customer_name',
                'tb1.sales_org_id', 'tb3.name as sales_org_name',
                'tb1.status',
                DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at"))
            ->where($where);

        return $query;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function contractItems()
    {
        return $this->hasMany("App\Models\ContractItem", "contract_id", "id");
    }

    public function customer()
    {
        return $this->belongsTo("App\Models\Customer","customer_id","id");
    }

    public function scopeSelect2($query)
    {
        return DB::table("wcm_contract as wc")->where("contract_type", "operational")
            ->whereIn("wc.status", ["y", "z"])
            ->select(
                "wc.id",
                "wc.uuid",
                "wc.number",
                DB::raw(
                "stuff(
                    (select distinct '|'+ wci.sales_office_id + '-' + wso.name from wcm_contract_item wci join wcm_sales_office wso on  wci.sales_office_id = wso.id 
                    where wci.contract_id = wc.id 
                    for xml path('')), 1,1,''
                ) as sales_offices")
            );
    }
}
