<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\SalesArea;
use App\Models\CustomerSalesArea;

class Syncronize extends Model {
    //
    public function scopeMaterialLists() {
        $columns = ["tbl1.sales_org_id", "tbl3.name as sales_org_name", "tbl4.code", "tbl4.sales_group_id",  "tbl4.name as plant_name", "tbl2.name as product_name", "tbl1.material_no", "tbl1.unit", "tbl1.mat_desc"];

        $query = DB::table("wcm_material_list as tbl1")
                        ->leftJoin("wcm_product as tbl2", "tbl1.product_id", "tbl2.id")
                        ->leftJoin("wcm_sales_org as tbl3", "tbl1.sales_org_id", "tbl3.id")
                        ->leftjoin("wcm_plant as tbl4", "tbl1.plant_id", "tbl4.id")
                        ->select($columns);

        return $query;
    }

    public function scopeRegionals() {
        $columns = ["prop.id as sales_office_id", "prop.name as sales_office_name", "kab.id as sales_group_id", "kab.name as sales_group_name", "kec.id as sales_unit_id", "kec.name as sales_unit_name"];

        $query = DB::table("wcm_sales_office as prop")
                        ->leftJoin("wcm_sales_group as kab", "kab.sales_office_id", "prop.id")
                        ->leftJoin("wcm_sales_unit as kec", "kec.sales_group_id", "kab.id")
                        ->select($columns);

        
        return $query;
    }

    public function scopeSalesDistributions() {
        $columns = ["id", "name"];
        return \DB::table("wcm_distrib_channel")
                        ->select($columns);
    }

    public function scopeSalesDivisions() {
        $columns = ["id", "name"];

        return \DB::table("wcm_sales_division")
                        ->select($columns);
    }

    public function scopeSalesAreas() {
        $columns = ["sa.sales_org_id", "so.name as sales_org_name", "sa.distrib_channel_id", "dc.name as distrib_channel_name", "sa.sales_division_id ", "sd.name as sales_division_name"];

        return \DB::table("wcm_sales_area as sa")
                        ->leftJoin("wcm_sales_division as sd", "sa.sales_division_id", "sd.id")
                        ->leftJoin("wcm_distrib_channel as dc", "sa.distrib_channel_id", "dc.id")
                        ->leftJoin("wcm_sales_org as so", "sa.sales_org_id", "so.id")
                        ->select($columns);
    }

    public function scopeCustomers() {
        // $columns = ["cus.id", "cus.full_name", "cus.owner", "addr.address", "addr.tlp_no", "addr.fax_no"];

        // $query = $this->regionals()
        //                 ->rightJoin("wcm_address as addr", "kec.id", "addr.sales_unit_id")
        //                 ->rightJoin("wcm_customer as cus", function ($join) {
        //                     $join->on("cus.id", "addr.customer_id")->where("addr.address_type", "FORMAL"); #
        //                 })
        //                 ->addSelect($columns)
        //                 ->addSelect(\DB::raw("
        //             (CASE WHEN cus.status = 'y' THEN 'Active' WHEN cus.status = 'n' THEN 'Inactive' WHEN cus.status = 'p' THEN 'Suspend' WHEN cus.status = 'd' THEN 'Draft' WHEN cus.status = 's' THEN 'Submited' ELSE '-' END) as status"));
        
        $columns = ['addr.sales_office_id','tb4.name as sales_office_name','addr.sales_group_id','tb3.name as sales_group_name','addr.sales_unit_id','tb2.name as sales_unit_name','wc.id','wc.full_name','wc.owner','addr.address','addr.tlp_no','addr.fax_no','wc.status',];

        $query = DB::table('wcm_customer as wc')
                    ->join("wcm_address as addr", function ($join) {
                        $join->on("wc.id", "addr.customer_id")->where("addr.address_type", "FORMAL"); #
                    })
                    ->leftjoin('wcm_sales_unit AS tb2', 'addr.sales_unit_id', '=', 'tb2.id')
                    ->leftjoin('wcm_sales_group AS tb3', 'addr.sales_group_id', '=', 'tb3.id')
                    ->leftjoin('wcm_sales_office AS tb4', 'addr.sales_office_id', '=', 'tb4.id')
                    ->addselect($columns)
                    ->addSelect(\DB::raw("
                    (CASE WHEN wc.status = 'y' THEN 'Active' WHEN wc.status = 'n' THEN 'Inactive' WHEN wc.status = 'p' THEN 'Suspend' WHEN wc.status = 'd' THEN 'Draft' WHEN wc.status = 's' THEN 'Submited' ELSE '-' END) as status_name"));
        return $query;
        
    }

    public function scopePlants() {
        $columns = ["p.uuid","p.code", "p.name", "p.address", "p.sales_group_id", "kab.name as sales_group_name", "kab.sales_office_id", "prop.name as sales_office_name", "pa.sales_org_id", "so.name as sales_org_name","p.status"];

        $query = DB::table("wcm_plant as p")
                        ->leftJoin("wcm_sales_group as kab", "p.sales_group_id", "kab.id")
                        ->leftJoin("wcm_sales_office as prop", "kab.sales_office_id", "prop.id")
                        ->leftJoin("wcm_plant_assg as pa", function ($join) {
                            $join->on("p.id", "pa.plant_id");
                            #->where("p.created_at","=","pa.created_at");
                            /* ->where("p.from_date","pa.from_date")
                              ->where("p.thru_date","pa.thru_date"); */
                        })
                        ->leftJoin("wcm_sales_org as so", "pa.sales_org_id", "so.id")
                        ->addSelect($columns)
                        ->addSelect(\DB::raw("
                    (CASE WHEN p.status = 'y' THEN 'Active' WHEN p.status = 'n' THEN 'Inactive' WHEN p.status = 'p' THEN 'Suspend' WHEN p.status = 'd' THEN 'Draft' WHEN p.status = 's' THEN 'Submited' ELSE '-' END) as status_name"))
                        ->distinct();
        return $query;
    }

    public function scopeMergeBindings($query, $param) {
        $method = Str::plural($param);
        $scope = $this->{$method}();

        $query = \DB::table(\DB::raw("(" . $scope->toSql() . ") as table_"))
                        ->mergeBindings($scope)
                        ->select("*");
        $user = Auth::user();
        $filters = $user->filterRegional;          
        
        if (@$filters) {
            switch ($param) {
                case "customer":
                    if(@$filters["sales_org_id"]){
                        $salesArea = DB::table("wcm_sales_area")->whereIn("sales_org_id", @$filters["sales_org_id"])
                            ->select("id");
            
                        $cusSalesArea = DB::table("wcm_customer_sales_area")
                            ->whereRaw("sales_area_id in ({$salesArea->toSql()})")
                            ->select("customer_id");

                        $query->whereRaw("id in ({$cusSalesArea->toSql()})")
                            ->mergeBindings($salesArea)
                            ->mergeBindings($cusSalesArea);
                    }
                    break;
                case "materialList":
                case "plant":
                    if(@$filters["sales_org_id"]){  
                        $query->whereIn("sales_org_id", $filters["sales_org_id"]);
                    }
                    break;
                default:
                    break;
            }
        }

       



        return $query;
    }

}
