<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;

class RoleHasMenu extends Model
{
    //
    protected $table = 'wcm_role_has_menus';
    protected $fillable = [
        'role_id', 'menu_id','permission_id'
    ];

    public function menus(){
        return $this->hasMany(Menu::class);
    }
}
