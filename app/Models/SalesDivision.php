<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesDivision extends Model {

    protected $table = 'wcm_sales_division';
    protected $fillable = [
        'id', 'name', 'code', 'desc', 'status', 'created_by', 'updated_by',
    ];

    public $incrementing = false;
}
