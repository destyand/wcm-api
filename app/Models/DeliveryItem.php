<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DeliveryItem extends Model
{

    protected $table    = 'wcm_delivery_item';
    protected $fillable = [
        'delivery_id', 'plant_id', 'product_id', 'shipping_point_id', 'storage_location_id', 'delivery_qty', 'delivery_date', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

    public static function getDeliveryReportF5($where = [])
    {
        $query = DB::table('wcm_delivery_item as tb1')
            ->leftjoin('wcm_delivery as tb2', 'tb2.id', '=', 'tb1.delivery_id')
            ->select('tb1.*', 'tb2.uuid as uuid_delivery', 'tb2.number', 'tb2.order_id')
            ->where($where);
        return $query;
    }

    public function delivery()
    {
        return $this->belongsTo("App\Models\Delivery", "delivery_id", "id");
    }

}
