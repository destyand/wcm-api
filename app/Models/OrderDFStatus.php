<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDFStatus extends Model
{
    //
    protected $table    = "wcm_order_df_status";
    protected $fillable = ['order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
