<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MAction extends Model
{
    protected $table = 'wcm_m_actions';

    public $incrementing = false;

    public static function ruleCreate() {
        $rules = [
            'route_id' => 'required|numeric',
            'action_id' => 'required',
            'guard_name' => 'required',
            'method' => 'required',
            'url' => 'required',
            'path' => 'required'
        ];
    
        return $rules;
    }

}
