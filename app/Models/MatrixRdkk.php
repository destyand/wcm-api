<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MatrixRdkk extends Model
{
    protected $table    = "wcm_matrix_rdkk";
    protected $fillable = [
        "sales_org_id", "sales_office_id", "sales_group_id", "product_id",
        "created_by", "updated_by",
    ];
    protected $hidden  = ["salesOrg", "salesOffice", "salesGroup", "product"];
    protected $appends = [
        "sales_org_name", "sales_office_name", "sales_group_name",
        "product_name",
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (request()->user()) {
                $model->created_by = request()->user()->id;
            }
        });

        static::updating(function ($model) {
            if (request()->user()) {
                $model->updated_by = request()->user()->id;
            }
        });
    }

    public function scopeDataTable($query)
    {
        $query = DB::table("wcm_matrix_rdkk as mr")
            ->leftJoin("wcm_sales_org as so ", "mr.sales_org_id", "so.id")
            ->leftJoin("wcm_sales_office as of", "mr.sales_office_id", "of.id")
            ->leftJoin("wcm_sales_group as sg", "mr.sales_group_id", "sg.id")
            ->leftJoin("wcm_product as p", "mr.product_id", "p.id")
            ->select("mr.*", "so.name as sales_org_name", "sg.name as sales_group_name")
            ->addSelect("of.name as sales_office_name", "p.name as product_name");

        return DB::table(DB::raw("({$query->toSql()}) as tbl"))->mergeBindings($query);
    }

    public function salesOrg()
    {
        return $this->hasOne("App\Models\SalesOrg", "id", "sales_org_id");
    }

    public function salesOffice()
    {
        return $this->hasOne("App\Models\SalesOffice", "id", "sales_office_id");
    }

    public function salesGroup()
    {
        return $this->hasOne("App\Models\SalesGroup", "id", "sales_group_id");
    }

    public function product()
    {
        return $this->hasOne("App\Models\Product", "id", "product_id");
    }

    public function getSalesOrgNameAttribute()
    {
        return $this->salesOrg ? $this->salesOrg->name : "";
    }

    public function getSalesOfficeNameAttribute()
    {
        return $this->salesOffice ? $this->salesOffice->name : "";
    }

    public function getSalesGroupNameAttribute()
    {
        return $this->salesGroup ? $this->salesGroup->name : "";
    }

    public function getProductNameAttribute()
    {
        return $this->product ? $this->product->name : "";
    }

    public static function getExportedColumns() {
        return [
            'sales_org_id as Kode Produsen',
            'sales_org_name as Nama Produsen',
            'sales_office_id as Kode Provinsi',
            'sales_office_name as Nama Provinsi',
            'sales_group_id as Kode Kota/Kabupaten',
            'sales_group_name as Nama Kota/Kabupaten',
            'product_id as Kode Produk',
            'product_name as Nama Produk',
        ];
    }
}
