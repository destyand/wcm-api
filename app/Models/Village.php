<?php

namespace App\Models;

use App\Rules\VillageInRegion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Village extends Model
{

    protected $table    = 'wcm_village';
    protected $fillable = [
        'code', 'name', 'sales_unit_id', 'created_by', 'updated_by',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $userId = @request()->user()->id;
            if (!is_null($userId) && is_null($model->created_by)) {
                $model->created_by = $userId;
                $model->updated_by = $userId;
                $model->save();
            }
        });
    }

    public function scopeGetAll($query)
    {
        $sql = DB::table(DB::raw("(" . $query->toSql() . ") as tb_village"))
            ->leftJoin("wcm_sales_unit as tb_su", "tb_su.id", "tb_village.sales_unit_id")
            ->leftJoin("wcm_sales_group as tb_sg", "tb_sg.id", "tb_su.sales_group_id")
            ->leftJoin("wcm_sales_office as tb_so", "tb_so.id", "tb_sg.sales_office_id")
            ->select(
                "tb_so.id as sales_office_id", "tb_so.name as sales_office_name",
                "tb_sg.id as sales_group_id", "tb_sg.name as sales_group_name",
                "tb_su.id as sales_unit_id", "tb_su.name as sales_unit_name",
                "tb_village.code as village_code", "tb_village.id as village_id",
                "tb_village.name as village_name"
            );

        return DB::table(DB::raw("(" . $sql->toSql() . ") as tbl"))
            ->mergeBindings($sql)
            ->select("*");
    }

    public static function validator($type = "created")
    {
        $validators = [
            "created"  => [
                "code"            => "required|unique:wcm_village",
                "name"            => "required|string|min:3",
                "sales_office_id" => "required",
                "sales_group_id"  => "required",
                "sales_unit_id"   => "required",
                "village"         => ["required", new VillageInRegion],
            ],
            "updated"  => [
                "name"            => "required|string|min:3",
                "sales_office_id" => "required",
                "sales_group_id"  => "required",
                "sales_unit_id"   => "required",
                "village"         => ["required", new VillageInRegion],
            ],
            "uploaded" => [
                "code"            => "required|unique:wcm_village",
                "name"            => "required|string|min:3",
                "sales_office_id" => "required",
                "sales_group_id"  => "required",
                "sales_unit_id"   => "required",
                "village"         => ["required", new VillageInRegion],
            ],
        ];

        return isset($validators[$type]) ? $validators[$type] : [];
    }

    public function getSalesOfficeAttribute()
    {
        return SalesOffice::find((int) substr($this->sales_unit_id,0,2));
    }

    public function getSalesGroupAttribute()
    {
        return SalesGroup::find((int) substr($this->sales_unit_id,0,4));
    }

    public function getSalesUnitAttribute()
    {
        return SalesUnit::find($this->sales_unit_id);
    }
}
