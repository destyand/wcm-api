<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportF5 extends Model
{
    //

    protected $table = 'wcm_report_f5';
    protected $fillable = [
        'number','sales_org_id','customer_id','sales_group_id','month','year','submited_date',
        'status',
        'created_by','updated_by','created_at','updated_at',
    ];

    public function items()
    {
        return $this->hasMany(ReportF5Items::class);
    }

    public static function createf5rule() {
        $rules = [
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id' => 'required|exists:wcm_sales_group,id',
            'sales_org_id' =>'required|exists:wcm_sales_org,id',
            'month' => 'required',
            'year' => 'required',
        ];
        return $rules;
    }

    public static function MasterF5Report ( $where=[] , $exported=false)
    {
        $query = DB::table("wcm_report_f5 as tb1")
                    ->leftJoin('wcm_sales_org as tb2','tb2.id','=','tb1.sales_org_id')
                    ->leftJoin('wcm_customer as tb3','tb3.id','=','tb1.customer_id')
                    ->leftJoin('wcm_sales_group as tb4','tb4.id','=','tb1.sales_group_id')
                    ->leftJoin('wcm_sales_office as tb5','tb5.id','=','tb4.sales_office_id')
                    ->leftJoin('wcm_months as tb6','tb6.id','=','tb1.month');
                if($exported){
                    $query->leftJoin('wcm_report_f5_items as tb7','tb1.id','=','tb7.report_f5_id');
                    $query->leftJoin('wcm_product as tb8','tb8.id','=','tb7.product_id');
                    $query->select('tb1.sales_org_id','tb2.name as sales_org_name',
                        'tb1.number','tb1.customer_id','tb3.full_name as customer_name','tb1.year','tb1.month','tb5.id as sales_office_id',
                        'tb5.name as sales_office_name','tb1.sales_group_id','tb4.name as sales_group_name',DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Approve'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name"),'tb7.product_id','tb8.name as product_name','tb7.stok_awal','tb7.penebusan','tb7.penyaluran','tb7.stok_akhir'

                    );
                    
                }else{
                    $query->select('tb1.id','tb1.uuid','tb1.sales_org_id','tb2.name as sales_org_name',
                             'tb1.number','tb1.customer_id','tb3.full_name as customer_name','tb3.owner as customer_owner',
                             'tb1.month','tb1.year',DB::raw('CONVERT(VARCHAR(10), tb1.submited_date, 105) as submited_date'),'tb1.submited_date as submited_dates',
                             'tb5.id as sales_office_id','tb5.name as sales_office_name',
                             'tb1.sales_group_id','tb4.name as sales_group_name',
                             'tb1.status',
                             DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Approve'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name")
                            );
                }
                    
                    $query->where($where);
         return $query;
    }

    public static function find_so ( $where=[] )
    {
         $query = DB::table("wcm_report_f5 as tb1")
                     ->leftJoin('wcm_distrib_reports as tb2','tb2.report_f5_id','=','tb1.id')
                     ->leftJoin('wcm_orders as tb3','tb2.order_id','=','tb3.id')
                     ->select('tb3.uuid','tb3.so_number')
                     ->distinct()
                     ->where($where);
        return $query;
    }

    public static function find_pkp ( $where=[] )
    {
        $query = DB::table("wcm_report_f5 as tb1")
                     ->leftJoin('wcm_distrib_reports as tb2','tb2.report_f5_id','=','tb1.id')
                     ->select('tb2.*')
                     ->distinct()
                     ->where($where);
        return $query;
    }


    public static function HirarkiReportF5 ($where =[])
    {
        $query = DB::table("wcm_report_f5_items as tb1")
                    // ->leftjoin(DB::raw('( SELECT DISTINCT (distrib_report_id)  FROM wcm_report_f5_items ) AS tb2'),'tb2.distrib_report_id','=','tb1.id')
                    ->where($where);
        return $query;
    }
}
