<?php

namespace App\Models;

use App\Rules\FarmerGroupLandAreaRule;
use App\Rules\FarmerGroupNameRule;
use App\Rules\FarmerGroupProdusenRetailRule;
use App\Rules\FarmerGroupSalesOfficeRule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FarmerGroup extends Model
{

    protected $table    = 'wcm_farmer_groups';
    protected $fillable = [
        'code', 'name', 'retail_id', 'address', 'sales_unit_id', 'customer_id', 'sales_org_id',
        'telp_no', 'fax_no', 'village', 'sub_sector_id', 'commodity_id', 'latitude', 'longitude',
        'qty_group', 'land_area', 'status', 'created_by', 'updated_by',
    ];
//    protected $hidden = ['id'];

    public static function ruleCreate()
    {
        $rules = [
            //'name'          => 'required|unique:wcm_farmer_groups',
            // 'retail_id'     => 'required|exists:wcm_retail,id',
            // 'sales_org_id'  => 'required|exists:wcm_sales_org,id',
            'sales_unit_id' => ['required', new FarmerGroupSalesOfficeRule],
            'group_name'    => [
                'required',
                new FarmerGroupNameRule,
            ],
            // 'village'       => 'required',
            // 'address'       => 'required',
            // 'status'        => 'required',
            // 'telp_no'       => 'numeric',
            // 'fax_no'        => 'numeric',
            // 'latitude'      => 'numeric',
            // 'longitude'     => 'numeric',
            // 'qty_group'     => 'numeric',
            // 'land_area'     => 'numeric',
        ];
        return $rules;
    }

    public static function ruleCreateBulk()
    {
        $rules = [
            // 'name'                      => 'required|unique:wcm_farmer_groups',
            // 'retail_code'               => 'required|exists:wcm_retail,code',
            // 'sales_org_id'              => 'required|exists:wcm_sales_org,id',
            // 'sales_unit_id' => ["required", new SalesUnits],
            // 'village'                   => 'required',
            // 'address'                   => 'required',
            // 'status'                    => 'required',
            // 'telp_no'                   => 'numeric',
            // 'fax_no'                    => 'numeric',
            // 'latitude'                  => 'numeric',
            // 'longitude'                 => 'numeric',
            'land_area_size' => [
                'required',
                new FarmerGroupLandAreaRule,
            ],
            'group_name'     => [
                'required',
                new FarmerGroupNameRule,
            ],
            'sales_office'   => [
                'required',
                new FarmerGroupSalesOfficeRule,
            ],
            'sales_org_id'   => [
                'required',
                new FarmerGroupProdusenRetailRule,
            ],
        ];

        return $rules;
    }

    public static function ruleUpdate()
    {
        $rules = [
            'telp_no'   => 'numeric',
            'fax_no'    => 'numeric',
            'latitude'  => 'numeric',
            'longitude' => 'numeric',
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function getAll($where = [])
    {
        $query = DB::table('wcm_farmer_groups AS tb1')
            ->join('wcm_retail AS tb2', 'tb1.retail_id', '=', 'tb2.id')
            ->join('wcm_sales_unit AS tb3', 'tb1.sales_unit_id', '=', 'tb3.id')
            ->leftJoin('wcm_customer AS tb4', 'tb1.customer_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_org AS tb5', 'tb1.sales_org_id', '=', 'tb5.id')
            ->join('wcm_sales_group AS tb6', 'tb3.sales_group_id', '=', 'tb6.id')
            ->join('wcm_sales_office AS tb7', 'tb6.sales_office_id', '=', 'tb7.id');
//                ->leftJoin('wcm_farmer AS tb8', 'tb1.id', '=', 'tb8.farmer_group_id');
        // ->leftJoin('view_farmer_count AS tb9', 'tb1.id', '=', 'tb9.farmer_group_id')
        if (array_key_exists('tb9.customer_id', $where) || array_key_exists('tb10.uuid', $where)) {
            $query->leftJoin('wcm_customer_retailer_assg AS tb9', 'tb2.id', '=', 'tb9.retail_id')
                ->leftJoin('wcm_customer AS tb10', 'tb9.customer_id', '=', 'tb10.id');
        }
        $query->select('tb1.id', 'tb1.uuid', 'tb1.name', 'tb1.code', 'tb1.retail_id',
            'tb1.address', 'tb1.sales_unit_id', 'tb1.telp_no', 'tb1.fax_no',
            'tb1.village', 'tb1.sub_sector_id', 'tb1.commodity_id', 'tb1.latitude',
            'tb1.longitude', 'tb2.code AS retail_code', 'tb2.name AS retail_name', 'tb3.name AS sales_unit_name',
            'tb1.customer_id', 'tb4.full_name as customer_name', 'tb4.uuid as customer_uuid',
            'tb1.sales_org_id', 'tb5.name AS sales_org_name', 'tb3.sales_group_id',
            'tb6.name AS sales_group_name', 'tb6.sales_office_id', 'tb7.name AS sales_office_name',
            'tb1.qty_group', 'tb1.land_area', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status"), 'tb1.created_by', 'tb1.updated_by',
            DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
            DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
        )
            ->where($where);

        return $query;
    }
    public static function getExportedColumns(){
        return [
            'tb5.name as Produsen',
            'tb1.code as Kode',
            'tb1.name as Nama',
            'tb2.code as Kode Pengecer',
            'tb2.name as Nama Pengecer',
            DB::raw("
            (CASE
                WHEN tb1.status = 'y' THEN 'Active'
                WHEN tb1.status = 'n' THEN 'Inactive'
                WHEN tb1.status = 'p' THEN 'Suspend'
                WHEN tb1.status = 'd' THEN 'Draft'
                WHEN tb1.status = 's' THEN 'Submited'
                ELSE '-'
            END) as status"),
            'tb1.address as Alamat',
            'tb7.name as Provinsi',
            'tb6.name as Kota/Kabupaten',
            'tb3.name as Kecamatan',
            'tb1.village as Desa/Kelurahan',
            'tb1.qty_group as Total Anggota',
            'tb1.land_area as Luas Lahan',
            'tb1.created_at as Dibuat Tanggal',
            'tb1.updated_at as Diperbarui',
        ];
    }
}
