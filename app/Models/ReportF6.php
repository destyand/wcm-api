<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportF6 extends Model
{
    //
    protected $table = 'wcm_report_f6';
    protected $fillable = [
        'number','sales_org_id','customer_id','sales_group_id','month','year','submited_date',
        'status','report_f5_id',
        'created_by','updated_by','created_at','updated_at',
    ];

    public static function MasterReportF6( $where=[] )
    {
        $query = DB::table("wcm_report_f6 as tb1")
                    ->leftJoin('wcm_sales_org as tb2','tb2.id','=','tb1.sales_org_id')
                    ->leftJoin('wcm_customer as tb3','tb3.id','=','tb1.customer_id')
                    ->leftJoin('wcm_sales_group as tb4','tb4.id','=','tb1.sales_group_id')
                    ->leftJoin('wcm_sales_office as tb5','tb5.id','=','tb4.sales_office_id')
                    ->leftJoin('wcm_months as tb6','tb6.id','=','tb1.month')
                    ->select('tb1.id','tb1.uuid','tb1.report_f5_id','tb1.number','tb1.sales_org_id','tb2.name as sales_org_name','tb1.customer_id','tb3.full_name as customer_name','tb1.month','tb6.name as month_name','tb1.year','tb1.submited_date',
                             'tb5.id as sales_office_id','tb5.name as sales_office_name',
                             'tb1.sales_group_id','tb4.name as sales_group_name',
                             'tb1.status',
                            DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Approve'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name"
                            ),
                             DB::raw("CONVERT (VARCHAR, tb1.submited_date, 105 ) as date_submited")

                    )
                    ->where($where);
        return $query;
    }

    public static function find_so ( $where=[] )
    {
         $query = DB::table("wcm_report_f5 as tb1")
                     ->leftJoin('wcm_distrib_reports as tb2','tb2.report_f5_id','=','tb1.id')
                     ->leftJoin('wcm_orders as tb3','tb2.order_id','=','tb3.id')
                     ->select('tb3.uuid','tb2.so_number')
                     ->distinct()
                     ->where($where);
        return $query;
    }

    public function items()
    {
        return $this->hasMany("App\Models\ReportF6Items", "report_f6_id", "id");
    }
}
