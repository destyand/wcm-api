<?php

namespace App\models;

use App\models\ContractGoverment;
use App\Rules\RayonisasiRule;
use App\Rules\RegionRule;
use App\Rules\SalesOrgAssInPermentanRule;
use App\Rules\SalesOrgAssInPergubPerbup;
use App\Rules\SalesGroupRule;
use App\Rules\SalesOrgAssgRules;
use App\Rules\SalesOrgAssPermentanManual;
use App\Rules\PermentanRule;
use App\Rules\SalesOrgAlokasiOneAktive;
use App\Rules\PerbupSalesUnitOneActive;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContractGovItem extends Model
{

    protected $table    = 'wcm_contract_gov_item';
    protected $fillable = [
        'contract_gov_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id',
        'month', 'year', 'initial_qty', 'status', 'active_date', 'inactive_date', 'create_at', 'update_at', 'created_by', 'updated_by',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $parent = ContractGoverment::find($model->contract_gov_id);
            $parent->touchStatus();
        });
    }

    public static function PermentanRuleCreate()
    {
        $rules = [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'numeric|min:0',
            'rayonisasi'      => [new RayonisasiRule],
            'permentan'       => [new PermentanRule],
        ];
        return $rules;
    }

    public static function PermentanStoreRule()
    {
        $rules = [
            'sales_org_id' => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'rayonisasi'      => [new RayonisasiRule],
            // 'sales_org_rule'  =>['required', new SalesOrgAssInPermentanRule ]
        ];
        return $rules;
    }

    public static function rayonisasi_sales_org()
    {
        $rules = [
            'sales_org_id' => ['required','exists:wcm_sales_org,id',new SalesOrgAssgRules],
            'rayonisasi'      => [new RayonisasiRule],
            // 'sales_org_rule'  =>['required', new SalesOrgAssInPermentanRule ]
        ];
        return $rules;
    }

    public static function PermentanRuleUpdate()
    {
        $rules = [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'numeric|min:0',
            //'rayonisasi'      => [new RayonisasiRule],
        ];
        return $rules;
    }

    public static function headerUpdateStatus()
    {
        $rules =[
            'permentan'       => [new PermentanRule],
        ];
        return $rules;
    }

    public static function headerSalesOrgPergubStatus()
    {
        $rules =[
            'pergub'       => [new SalesOrgAlokasiOneAktive],
        ];
        return $rules;
    }

    public static function headerSalesOrgPerbupStatus()
    {
        $rules =[
            'perbup'       => [new SalesOrgAlokasiOneAktive],
            'one_aktive'   => [new PerbupSalesUnitOneActive],
        ];
        return $rules;
    }

    public static function perbupRuleCreate()
    {
        $rules = [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'numeric|min:0',
            'region'          => [
                new RegionRule,
            ],
            'rayonisasi' => [
                new RayonisasiRule,
            ],
        ];
        return $rules;
    }

    public static function perbupRuleUpdate()
    {
        $rules = [
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'numeric|min:0',
        ];
        return $rules;
    }

    public static function pergupRuleCreateItems($headerData)
    {
        $rules = [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'month'           => 'required',
            'year'            => 'required',
            'initial_qty'     => 'numeric|min:0',
            'sales_group_id'  => new SalesGroupRule($headerData['sales_office_id']),
            // 'sales_org_rule'  => [new SalesOrgAssInPergubPerbup]
        ];
        return $rules;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public static function ruleFileImport()
    {
        $rules = [
            'file' => 'required',
        ];
        return $rules;
    }
    public static function getDetailPermentan($where = [])
    {
        $query = DB::table('wcm_contract_goverment AS tb1')
            ->join('wcm_contract_gov_item AS tb2', 'tb2.contract_gov_id', '=', 'tb1.id')
            ->join('wcm_sales_org AS tb3', 'tb1.sales_org_id', '=', 'tb3.id')
            ->join('wcm_product AS tb4', 'tb2.product_id', '=', 'tb4.id')
            ->join('wcm_sales_office AS tb5', 'tb2.sales_office_id', '=', 'tb5.id')
            ->select('tb1.id as header_id', 'tb2.id as item_id', 'tb1.uuid as uuid_gov', 'tb2.uuid as uuid_item', 'tb1.contract_type', 'tb3.id as sales_org_id', 'tb3.name as sales_name',
                'tb5.id as sales_office_id', 'tb5.name as sales_office_name',
                'tb2.month', 'tb2.year', 'tb4.id as product_id', 'tb4.name as name_product',
                'tb2.initial_qty', 'tb2.status', DB::raw("
                        (CASE
                            WHEN tb2.status = 'y' THEN 'Active'
                            WHEN tb2.status = 'n' THEN 'Inactive'
                            WHEN tb2.status = 'p' THEN 'Suspend'
                            WHEN tb2.status = 'd' THEN 'Draft'
                            WHEN tb2.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb2.created_by', 'tb2.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb2.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb2.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb2.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb2.updated_at, 108 )) as updated_at")
            )
            ->where($where);

        return $query;
    }

    public static function getItemPermentan($where = [])
    {
        $query = DB::table('wcm_contract_goverment AS tb1')
            ->join('wcm_contract_gov_item AS tb2', 'tb2.contract_gov_id', '=', 'tb1.id')
            ->join('wcm_sales_org AS tb3', 'tb1.sales_org_id', '=', 'tb3.id')
            ->join('wcm_product AS tb4', 'tb2.product_id', '=', 'tb4.id')
            ->join('wcm_sales_office AS tb5', 'tb2.sales_office_id', '=', 'tb5.id')
            ->select('tb1.id as header_id', 'tb2.id as item_id', 'tb1.uuid as uuid_gov', 'tb2.uuid as uuid_item',
                'tb1.number', 'tb2.contract_gov_id', 'tb1.contract_type', 'tb3.id as sales_org_id', 'tb3.name as sales_name',
                'tb2.month', 'tb2.year', 'tb4.id as product_id', 'tb4.name as product_name', 'tb2.sales_office_id', 'tb5.name as provinsi',
                'tb2.initial_qty', 'tb2.status', DB::raw("
                        (CASE
                            WHEN tb2.status = 'y' THEN 'Active'
                            WHEN tb2.status = 'n' THEN 'Inactive'
                            WHEN tb2.status = 'p' THEN 'Suspend'
                            WHEN tb2.status = 'd' THEN 'Draft'
                            WHEN tb2.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb2.created_by', 'tb2.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb2.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb2.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb2.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb2.updated_at, 108 )) as updated_at")
            )
            ->where($where);

        return $query;
    }

    public static function getItemPergub($where = [])
    {
        $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment as tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->leftjoin('wcm_sales_office as tb3', 'tb3.id', '=', 'tb1.sales_office_id')
            ->leftjoin('wcm_sales_group as tb4', 'tb4.id', '=', 'tb1.sales_group_id')
            ->leftjoin('wcm_product as tb5', 'tb5.id', '=', 'tb1.product_id')
            ->leftjoin('wcm_sales_org AS tb6', 'tb2.sales_org_id', '=', 'tb6.id')
            ->select('tb1.id',
                'tb1.uuid',
                'tb1.contract_gov_id', 'tb2.number as pergubno',
                'tb1.sales_office_id', 'tb3.name as provinsi',
                'tb1.sales_group_id', 'tb4.name as kabupaten',
                'tb1.product_id', 'tb5.name as produk',
                'tb5.name as product_name',
                'tb2.sales_org_id',
                'tb6.name as sales_org_name',
                'tb1.month',
                'tb1.initial_qty',
                'tb1.year', 'tb1.status', DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), DB::raw("CONVERT ( VARCHAR, tb1.created_at, 105 ) as created_at")
            )
            ->where($where)
            ->where('tb2.contract_type', 'pergub');

        return $query;
    }
    public static function getItemPerbup($where = [])
    {
        $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->leftJoin('wcm_sales_org AS tb3', 'tb2.sales_org_id', '=', 'tb3.id')
            ->leftJoin('wcm_sales_office AS tb4', 'tb1.sales_office_id', '=', 'tb4.id')
            ->leftJoin('wcm_sales_group AS tb5', 'tb1.sales_group_id', '=', 'tb5.id')
            ->leftJoin('wcm_product AS tb6', 'tb1.product_id', '=', 'tb6.id')
            ->leftJoin('wcm_sales_unit AS tb7', 'tb1.sales_unit_id', '=', 'tb7.id')
            ->select('tb1.id', 'tb1.uuid', 'tb2.uuid AS contract_gov_uuid',
                'tb1.contract_gov_id', 'tb2.number', 'tb2.sales_org_id', 'tb3.name AS sales_org_name',
                'tb2.year', 'tb1.month', 'tb1.sales_office_id', 'tb4.name AS sales_office_name', 'tb1.sales_group_id',
                'tb5.name AS sales_group_name', 'tb5.district_code', 'tb1.sales_unit_id', 'tb7.name as sales_unit_name',
                'tb1.product_id', 'tb6.name AS product_name', 'tb1.initial_qty',
                'tb1.status', DB::raw("(CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
            )
            ->where('tb2.contract_type', 'perbup')
            ->where($where);

        return $query;
    }

    public static function cekDataForSpjb($where = [])
    {
        # code...
        $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->select('tb1.*')
            // ->where('tb2.contract_type', 'perbup')
            // ->where('tb1.status', 'y')
            ->where($where);

        return $query;
    }

    public function contractGoverment()
    {
        return $this->belongsTo("App\Models\ContractGoverment", "contract_gov_id", "id");
    }

    public function scopePerbupByRetail($query, $salesOrgID)
    {
        $now = Carbon::now();
        $query->whereHas("contractGoverment", function($q) use ($salesOrgID) {
                $q->where("contract_type", "perbup");
                if ($salesOrgID) {
                    $q->where("sales_org_id", $salesOrgID);
                } 
            })
            ->where("status", "y")
            ->where("year", $now->format("Y"))
            ->select("sales_unit_id","product_id", DB::raw("sum(initial_qty) as qty"))
            ->groupBy("product_id")
            ->groupBy("sales_unit_id");
    }

    public function scopePerbupRetialItemPenyaluran($query, $salesOrgID,$year)
    {
        $query->whereHas("contractGoverment", function($q) use ($salesOrgID,$year) {
                $q->where("contract_type", "perbup")
                ->where("sales_org_id", $salesOrgID)            
                ->where("year", $year);
            })
            ->where("year", $year)
            ->where('status','y')
            ->select("sales_unit_id","product_id", DB::raw("sum(initial_qty) as qty"))
            ->groupBy("product_id")
            ->groupBy("sales_unit_id");
    }
}
