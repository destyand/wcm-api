<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Contract;
use App\Rules\CustomerExistsRule;

class DistribReports extends Model {

    //
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($model){
            $model->distribItems()->delete();
        });

        static::updated(function ($model){
            $model->distribItems()->update(['status'=> $model->status]);
        });
    }


    protected $table = 'wcm_distrib_reports';
    protected $fillable = [
        'report_f5_id', 'number', 'customer_id', 'order_id', 'so_number', 'month', 'year',
        'sales_org_id', 'initial_stock_id', 'sales_group_id', 'distribution_date',
        'distrib_resportable_type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];


    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function setMonthAttribute($value) {
        $this->attributes['month'] = str_pad($value, 2, '0', STR_PAD_LEFT);
    }

    /*public function getDistributionDateAttribute($value) {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }*/

    public static function ruleCreate() {
        $rules = [
            'order_id' => 'required|exists:wcm_orders,id',
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'customer_id' => 'required|exists:wcm_customer,id',
            'sales_group_id' => 'required|exists:wcm_sales_group,id',
            'so_number' => 'required',
            'delivery_method_id' => 'required',
            'distribution_date' => 'required|date',
            'order_date' => 'required',
        ];
        return $rules;
    }

    public static function ruleCreateDistribInitStockF5() {
        $rules = [
            'sales_org_id' => 'required|exists:wcm_sales_org,id',
            'initial_stock_id' => 'required|exists:wcm_initial_stocks,id',
            'customer_id' => ["required", new CustomerExistsRule],
            'sales_group_id' => 'required|exists:wcm_sales_group,id',
            'distribution_date' => 'required|date',
            'month' => 'required',
            'year' => 'required',
            'status' => 'required'
        ];
        return $rules;
    }

    public static function getInitF5($where) {
        $query = DB::table('wcm_distrib_reports AS tb1')
                ->leftJoin('wcm_report_f5 AS tb2', 'tb1.report_f5_id', '=', 'tb2.id')
                ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
                ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
                ->leftJoin('wcm_initial_stocks AS tb5', 'tb1.initial_stock_id', '=', 'tb5.id')
                ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
                ->leftJoin('wcm_months as tb7', 'tb7.id', '=', 'tb1.month')
                ->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.report_f5_id', 'tb2.number AS report_f5_number', 'tb1.customer_id',
                        'tb3.full_name AS customer_name', 'tb1.month','tb7.name as month_name', 'tb1.year', 'tb1.sales_org_id', 'tb4.name AS sales_org_name',
                        'tb1.status','tb6.id as sales_group_id', 'tb6.name as sales_group_name', DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"), 'tb1.created_by', 'tb1.updated_by',
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.created_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.created_at, 108 )) as created_at"),
                        DB::raw("CONCAT(CONVERT ( VARCHAR, tb1.updated_at, 105 ), ' ', CONVERT ( VARCHAR, tb1.updated_at, 108 )) as updated_at")
                )
                ->where($where);

        return $query;
    }

    public static function getDistribInitF5Item($where){
        $query = DB::table('wcm_distrib_reports AS tb1')
                ->leftjoin('view_initial_stock_f5 AS tb2', 'tb1.initial_stock_id', '=', 'tb2.id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.number', 'tb1.report_f5_id', 'tb1.sales_org_id', 'tb2.sales_org_name', 'tb1.customer_id',
                        'tb2.customer_name', 'tb1.sales_group_id', 'tb2.sales_group_name', 'tb1.month', 'tb1.year',
                        'tb1.distribution_date', 'tb2.p01_qty', 'tb2.p02_qty', 'tb2.p03_qty', 'tb2.p04_qty', 'tb2.p05_qty','tb2.p51_qty', 'tb2.p54_qty',
                        'tb2.p01_reduce', 'tb2.p02_reduce', 'tb2.p03_reduce', 'tb2.p04_reduce', 'tb2.p05_reduce', 'tb2.p51_reduce', 'tb2.p54_reduce' , 'tb1.status',
                        DB::raw("
                        (CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name")
                )
                ->where($where);

        return $query;
    }

    public static function detailheader($where = []) {
        $query = DB::table('wcm_distrib_reports as tb1')
                ->join('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
                ->leftjoin('wcm_customer as tb3', 'tb3.id', '=', 'tb1.customer_id')
                ->leftJoin('wcm_sales_group as tb4', 'tb4.id', '=', 'tb1.sales_group_id')
                ->leftJoin('wcm_sales_org as tb5', 'tb5.id', '=', 'tb1.sales_org_id')
                ->leftJoin('wcm_delivery_method as tb6', 'tb6.id', '=', 'tb2.delivery_method_id')
                ->select('tb1.id', 'tb1.uuid', 'tb1.report_f5_id', 'tb1.number', 'tb2.so_number',
                        DB::raw("CONVERT ( VARCHAR, tb2.order_date, 105 ) as order_date"), 'tb1.customer_id',
                        'tb3.full_name as customer_name', 'tb1.sales_group_id', 'tb4.name as sales_group_name',
                        'tb1.sales_org_id', 'tb5.name as sales_org_name', 'tb2.delivery_method_id as inconterm_id',
                        'tb6.name as inconterm_name', DB::raw("CONVERT ( VARCHAR, tb1.distribution_date, 105 ) as distribution_date"),'tb1.distribution_date as distribution_dates',
                        'tb1.status', DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Active'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name"), 'tb2.contract_id', 'tb1.month', 'tb1.year', 'tb1.order_id',
                        'tb2.order_date as order_dates')
                ->where($where);

        return $query;
    }

    public static function listDistripReportByOrder($where = []) {
        $query = DB::table('wcm_distrib_reports as tb1')
                ->join('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
                ->leftjoin('wcm_customer as tb3', 'tb3.id', '=', 'tb1.customer_id')
                ->leftJoin('wcm_sales_group as tb4', 'tb4.id', '=', 'tb1.sales_group_id')
                ->leftJoin('wcm_sales_org as tb5', 'tb5.id', '=', 'tb1.sales_org_id')
                ->leftJoin('wcm_delivery_method as tb6', 'tb6.id', '=', 'tb2.delivery_method_id')
                ->leftJoin('wcm_report_f5 as tb7', 'tb7.id', '=', 'tb1.report_f5_id')
                ->leftJoin('wcm_months as tb8', 'tb8.id', '=', 'tb1.month')
                ->select('tb1.id', 'tb1.uuid', 'tb1.report_f5_id', 'tb1.number', 'tb1.so_number', 'tb1.order_id',
                        DB::raw("CONVERT ( VARCHAR, tb2.order_date, 105 ) as order_date"), 'tb1.customer_id',
                        'tb3.full_name as customer_name', 'tb1.sales_group_id', 'tb4.name as sales_group_name',
                        'tb1.sales_org_id', 'tb5.name as sales_org_name', 'tb2.delivery_method_id as inconterm_id',
                        'tb6.name as inconterm_name', DB::raw("CONVERT ( VARCHAR, tb1.distribution_date, 105 ) as distribution_date"),
                        'tb1.status', DB::raw("
                                (CASE
                                    WHEN tb1.status = 'y' THEN 'Active'
                                    WHEN tb1.status = 'n' THEN 'Inactive'
                                    WHEN tb1.status = 'p' THEN 'Suspend'
                                    WHEN tb1.status = 'd' THEN 'Draft'
                                    WHEN tb1.status = 's' THEN 'Submited'
                                    ELSE '-'
                                END) as status_name"),'tb2.contract_id',
                        'tb1.month','tb8.name as month_name','tb1.year',
                        'tb7.number as f5_number',
                        'tb7.uuid as f5_uuid' )
                ->where ($where);

        return $query;
    }

    public static function hirarkiF5($where = []) {
        $query = DB::table('wcm_distrib_reports as tb1')
                ->join('wcm_orders as tb2', 'tb2.id', '=', 'tb1.order_id')
                ->where($where);

        return $query;
    }


    /**
     * Gets the status name attribute.
     *
     * @return     <String>  The status name attribute.
     */
    public function getStatusNameAttribute()
    {
        $status = collect(["y"=> "Active", "n" => "Inactive","p" => "Suspend","d"=>"Draf","s"=>"Submited"]);

        return $status->get($this->attributes["status"]);
    }

    /**
     * Gets the distrib report items f6 attribute.
     *
     * @return     array  The distrib report items f6 attribute.
     */
    public function getDistribReportItemsF6Attribute()
    {
        if (is_null($this->contract) || is_null($this->contract->contractItems) ) return [];

        $customerAssign = $this->customer
                        ->customerRetailsAssign()
                        ->where("sales_org_id", $this->attributes["sales_org_id"])
                        ->whereHas("retail", function ($query){
                            $query->whereIn(
                                "sales_unit_id",
                                $this->contract->contractItems->unique("sales_unit_id")->pluck("sales_unit_id")
                            );
                        })
                        ->pluck("retail_id");

        $distribItems   = $this->distribItems()->whereIn("retail_id", $customerAssign)->get();

        $initialStocks  = $this->initialStock;


        $products       = Product::all();

        $items          = [];

        $products->map(function($product) use ($distribItems, $initialStocks, &$items){

            $qty     = (float) $initialStocks->items()->where("product_id",$product->id)->sum("qty");
            $distrib = (float) $distribItems->where("product_id",$product->id)->sum("qty");

            $items[$product->id] = [
                "id" => $product->id,
                "name" => $product->name,
                "qty" => $qty,
                "distrib" => $distrib,
                "qty_sisa" => (float) ($qty-$distrib),
            ];

        });

        return array_values($items);

    }

    /**
     * Gets the retails penyaluran f6 attribute.
     *
     * @return     array  The retails penyaluran f 6 attribute.
     */
    public function getRetailsPenyaluranF6Attribute()
    {

        // return null if contract or Contract items not Found
        if (is_null($this->contract) || is_null($this->contract->contractItems) ) return [];

        //return $this->attributes;

        $cusRetailAssg  = $this->customer->customerRetailsAssign()
                                // ->whereRaw("MONTH(created_at) >= ". (int) $this->attributes["month"])
                                // ->whereRaw("YEAR(created_at) = ". $this->attributes["year"])
                                //
                                ->where("created_at","<=",$this->attributes["created_at"])
                                ->whereIn("status",["y","p"])
                                ->where("sales_org_id", $this->attributes["sales_org_id"])
                                ->whereHas("retail", function ($query){
                                    $query->whereIn(
                                        "sales_unit_id",
                                        $this->contract->contractItems->unique("sales_unit_id")->pluck("sales_unit_id")
                                    );
                                })
                                ->get();
        //
        $distribItems   = $this->distribItems;


        $units          = [];

        $cusRetailAssg->map(function ($assign, $index) use (&$units, $distribItems) {

            $retail     = $assign->retail;

            $unit_name  = $retail->salesUnit->name;

            $units[$index] = [
                "id" => $retail->salesUnit->id,
                "name" => $retail->salesUnit->name,
                "retail" => collect($retail)->only(["id","uuid","name","code","created_at"])->toArray(),
            ];

            Product::all()->map(function ($product) use (&$units, $retail, &$products, $distribItems, $index, &$totalProductByUnits){

                $item  = $distribItems->where("retail_id", $retail->id)
                            ->where("product_id", $product->id)
                            ->first();

                $units[$index]["retail"]["data"][]= [
                    "id" => $product->id,
                    "name" => $product->name,
                    "sales_unit_id" => $retail->sales_unit_id,
                    "qty" => (float) @$item->qty,
                    "uuid" => @$item->uuid,
                ];


            });



        });

        return $this->formatToResponse($units);

    }

    public function getContractAttribute()
    {
        /*
        * Check SPJB
        * with customer, sales org and year
        */
        return Contract::where([
                                    "customer_id"  => $this->attributes["customer_id"],
                                    "sales_org_id" => $this->attributes["sales_org_id"],
                                    "year"         => $this->attributes["year"],
                                    "contract_type" => "operational",
                                ])->first();
    }

    /**
     * { function_description }
     *
     * @param      array  $array  The array
     *
     * @return     array  ( description_of_the_return_value )
     */
    private function formatToResponse(array $array)
    {
        $return = [];

        $totalProductByUnits = [];

        foreach ($array as $index => $row) {
            foreach ($row["retail"]["data"] as $key => $value) {
                $totalProductByUnits[$value["sales_unit_id"]][$value["id"]][] = $value["qty"];
            }
        }

        foreach ($array as $index => $value) {
            $array[$index]["total"][$value["id"]] = [
                [
                    "id" => "P01",
                    "qty" => array_sum($totalProductByUnits[$value["id"]]["P01"]),
                ],
                [
                    "id" => "P02",
                    "qty" => array_sum($totalProductByUnits[$value["id"]]["P02"]),
                ],
                [
                    "id" => "P03",
                    "qty" => array_sum($totalProductByUnits[$value["id"]]["P03"]),
                ],
                [
                    "id" => "P04",
                    "qty" => array_sum($totalProductByUnits[$value["id"]]["P04"]),
                ],
                [
                    "id" => "P05",
                    "qty" => array_sum($totalProductByUnits[$value["id"]]["P05"]),
                ],
            ];
        }

        foreach ($array as $key => $value) {
            $return[$value["id"]]= [
                "id" => $value["id"],
                "name" => $value["name"],
                "total" => $value["total"][$value["id"]],
            ];

        }

        foreach ($array as $key => $value) {
            $return[$value["id"]]["data"][] = $value["retail"];
        }

        return array_values($return);
    }

    /**
     * Relation To Sales Org
     *
     * @return     <Object> Sales Org
     */
    public function salesOrg()
    {
        return $this->hasOne("App\Models\SalesOrg", "id","sales_org_id");
    }

    /**
     * Relation To Sales Group
     *
     * @return     <Object>  Sales Group
     */

    public function salesGroup()
    {
        return $this->hasOne("App\Models\SalesGroup","id", "sales_group_id");
    }

    /**
     * Relation To Customer
     *
     * @return     <Object>  Customer
     */
    public function getCustomerAttribute()
    {
        $cusSalesAssg = CustSalesOrgAssg::where("customer_id", $this->attributes["customer_id"])
                            ->where("sales_org_id", $this->attributes["sales_org_id"])
                            #->whereRaw("MONTH(created_at) = ". $this->attributes["month"])
                            #->whereRaw("YEAR(created_at) = ". $this->attributes["year"])
                            ->first();

        return $cusSalesAssg ? Customer::where("id", $cusSalesAssg->customer_id)->first() : null;
    }

    public function initialStock()
    {
        return $this->belongsTo("App\Models\InitialStocks","initial_stock_id","id");
    }

    /**
     * Relation OneToOne With Distrib Items
     *
     * @return    Collection  Distrib Items
     */
    public function distribItems()
    {
        return $this->hasMany("App\Models\DistribReportItems", "distrib_report_id", "id");
    }

    public  function ReportF5()
    {
        return $this->belongsTo("App\Models\ReportF5","report_f5_id","id");
    }

}
