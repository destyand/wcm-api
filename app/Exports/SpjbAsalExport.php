<?php

namespace App\Exports;

use App\models\Contract;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SpjbAsalExport implements FromCollection, WithHeadings, WithColumnFormatting, ShouldAutoSize
{

    private $spjbAsal;

    public function __construct(Contract $spjbAsal)
    {
        $this->spjbAsal = $spjbAsal;
    }
    /**
     * @return Collection
     */
    public function collection()
    {
        $items = $this->spjbAsal->contractItems()
            ->leftjoin('wcm_sales_office as z','z.id','=','sales_office_id')
            ->leftjoin('wcm_sales_group as y','y.id','=','sales_group_id')
            ->leftjoin('wcm_sales_unit as x','x.id','=','sales_unit_id')
            ->leftjoin('wcm_product as w','w.id','=','product_id')
            ->orderBy('wcm_contract_item.sales_office_id', 'asc')
            ->orderBy('wcm_contract_item.sales_group_id', 'asc')
            ->orderBy('wcm_contract_item.sales_unit_id', 'asc')
            ->orderBy('wcm_contract_item.product_id', 'asc')
            ->orderBy(DB::raw('CONVERT(INT,wcm_contract_item.month)'), 'asc')
            ->select('wcm_contract_item.*','z.name as sales_office_name','y.district_code','y.name as sales_group_name','x.name as sales_unit_name','w.name as product_name')
            ->get();

        return $items->map(function ($item) {
            return [
                $item->contract->sales_org_id,
                $item->contract->number,
                $item->contract->customer_id,
                $item->contract->customer->full_name,
                $item->sales_office_id,
                $item->sales_office_name,
                $item->district_code,
                $item->sales_group_name,
                $item->sales_unit_id,
                $item->sales_unit_name,
                $item->year,
                (int) $item->month,
                $item->product_id,
                $item->product_name,
                (int) $item->initial_qty,
            ];
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Sales Organization',
            'No Doc',
            'Kode Distributor',
            'Deskripsi Distributor',
            'Kode Provinsi',
            'Deskripsi Provinsi',
            'Kode Kabupaten',
            'Deskripsi Kabupaten',
            'Kode Kecamatan',
            'Deskripsi Kecamatan',
            'Tahun',
            'Bulan',
            'Kode Jenis Produk',
            'Deskripsi Jenis Produk',
            'Amount Alokasi',
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            "C" => NumberFormat::FORMAT_TEXT,
            "H" => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
