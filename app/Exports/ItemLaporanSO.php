<?php

namespace App\Exports;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Sheet;

class ItemLaporanSO implements FromCollection,WithHeadings, ShouldAutoSize
{
    public function __construct(Collection $items,$columns=[])
	{
		$this->items=$items;
		$this->columns=$columns;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->items->map(function($item){
            return [
              $item->nomor_do,
              $item->nomor_sales_order,
              '-',
              $item->nomor_material,
              $item->deskripsi_material,
              $item->material_group,
              $item->sales_organization,
              $item->distribution_channel,
              $item->division,
              $item->sales_office,
              $item->deskripsi_sales_office,
              $item->sales_group,
              $item->deskripsi_sales_group,
              $item->so_legacy,
              $item->sales_unit_id,
              $item->kecamatan_so_desc,
              $item->distributor,
              $item->nama_distributor,
              $item->provinsi_distributor,
              $item->kabupaten_distributor,
              '-',
              '-',
              '-',
              '-',
              $item->nomor_kontrak,
              '-',
              '-',
              $item->tanggal_so_dibuat,
              $item->so_release,
              $item->payment_term,
              '-',
              $item->paymentMethod,
              $item->payment_method,
              $item->batas_akhir_pengambilan,
              $item->quantity_so,
              $item->unit_of_measure,
              $item->kode_gudang,
              $item->gudang_pengambilan,
              $item->quantity_do,
              $item->tanggal_dokumen,
              $item->tanggal_pgi,
              '-',
              $item->pgi_qty,
              $item->sektor,
              $item->no_billing,
              $item->billing_date,
              '-',
              $item->status,
              '-',
              '-',
              '-',
              '-',
              '-',
              '-',
              '-',
              '-',
              $item->so_type,
              $item->so_type_description,
              $item->nomor_material,
              $item->nomor_material,
              $item->provinsi_gudang,
              $item->kabupaten_gudang,
              $item->pengecer,
              $item->no_po,
              $item->tanggal_po,
              $item->so_created_by,
              '-',
              '-',
              '-',
              '-',
              $item->incoterm_1,
              $item->incoterm_2,
              '-',
              '-',
              '-',
              $item->mata_uang,
              '-',
              '-',
              '-',
              '-',
              $item->alokasi_operasional,
            ];
        });
    }

    public function headings(): array
    {
        if ($this->columns) return $this->columns;

        $firstRow = $this->items->first();

        if ($firstRow instanceof Arrayable || \is_object($firstRow)) {
            return array_keys(Sheet::mapArraybleRow($firstRow));
        }

        return $this->items->collapse()->keys()->all();
    }
}
