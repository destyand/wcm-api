<?php

namespace App\Exports;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Sheet;

class HeaderLaporanSO implements FromCollection,WithHeadings, ShouldAutoSize
{
    public function __construct(Collection $items,$columns=[])
	{
		$this->items=$items;
		$this->columns=$columns;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->items->map(function($item){
            return [
  			$item->nomor_sales_order,
  			$item->so_Item,
  			$item->sales_organization,
  			$item->distribution_channel,
  			$item->division,
  			$item->sales_office,
  			$item->deskripsi_sales_office,
  			$item->sales_group,
  			$item->deskripsi_sales_group,
  			$item->so_legacy,
  			$item->sales_unit_id,
  			$item->kecamatan_so_desc,
  			$item->provinsi_distributor,
  			$item->kabupaten_distributor,
  			$item->distributor,
  			$item->nama_distributor,
  			$item->pengecer,
  			'-',
  			'-',
  			'-',
  			'-',
  			$item->nomor_kontrak,
  			'-',
  			'-',
  			$item->tanggal_so_dibuat,
  			$item->tanggal_dokumen,
  			$item->so_release,
  			$item->payment_term,
  			$item->payment_method,
  			$item->nomor_material,
  			$item->deskripsi_material,
  			$item->material_group,
  			$item->alokasi_asal,
  			$item->alokasi_operasional,
  			$item->quantity_so,
  			$item->unit_of_measure,
  			$item->mata_uang,
  			$item->harga_jual_exc_ppn,
  			$item->ppn,
  			$item->total,
  			$item->harga_per_ton,
  			$item->harga_total,
  			$item->nomor_do,
  			$item->tanggal_pgi,
  			$item->plant_so,
  			$item->gudang_so,
  			$item->gudang_so_deskripsi,
  			$item->kode_gudang,
  			$item->gudang_pengambilan,
  			$item->quantity_do,
  			$item->quantity_so_min_do,
  			$item->status,
  			'-',
  			'-',
  			'-',
  			'-',
  			$item->pgi_qty,
  			$item->total_harga_tonase_pgi,
  			$item->outstanding_so,
  			$item->so_type,
  			$item->so_type_description,
  			$item->provinsi_gudang,
  			$item->kabupaten_gudang,
  			$item->payment_term,
  			$item->paymentMethod,
  			'-',
  			$item->no_po,
  			$item->tanggal_po,
  			$item->so_created_by,
  			'-',
  			'-',
  			'-',
  			'-',
  			'-',
  			$item->description,
  			$item->sektor,
  			$item->no_billing,
  			$item->billing_date,
  			$item->incoterm_1,
  			$item->incoterm_2,
  			'-',
  			'-',
  			'-',
  			'-',
  			'-',
  			'-',
  			'-',
  			'-',
            ];
        });
    }

    public function headings(): array
    {
        if ($this->columns) return $this->columns;

        $firstRow = $this->items->first();

        if ($firstRow instanceof Arrayable || \is_object($firstRow)) {
            return array_keys(Sheet::mapArraybleRow($firstRow));
        }

        return $this->items->collapse()->keys()->all();
    }
}
