<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;


class RekapF6Export implements FromCollection, WithHeadings, ShouldAutoSize
{
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
        return [
            "Produsen",
            "Nomor",
            "Kode Distributor",
            "Nama Distributor",
            "Kode Provinsi",
            "Nama Provinsi",
            "Kode Kota/Kabupaten",
            "Nama Kota/Kab",
            "Kode Kecamatan",
            "Nama Kecamatan",
            "Bulan",
            "Tahun",
            "Kode Pengecer",
            "Nama Pengecer",
            "Produk",
            "Stok Awal",
            "Penebusan",
            "Penyaluran",
            "Stok Akhir",
            "Status",
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
       
        // TODO: Implement collection() method.
        return $this->model
                ->leftJoin("wcm_product as tb_product", "product_id","tb_product.id")
                ->addSelect(
                    "tb_product.name as product_name",
                    \DB::raw("
                        (CASE
                            WHEN tb_f6.status = 'y' THEN 'Active'
                            WHEN tb_f6.status = 'n' THEN 'Inactive'
                            WHEN tb_f6.status = 'p' THEN 'Suspend'
                            WHEN tb_f6.status = 'd' THEN 'Draft'
                            WHEN tb_f6.status = 's' THEN 'Submited'
                            ELSE '-'
                        END) as status_name"
                    )
                )
                ->get()
                ->map(function($item){
                        return [
                            @$item->sales_org_name,
                            @$item->number,
                            @$item->customer_id,
                            @$item->customer_name,
                            @$item->sales_office_id,
                            @$item->sales_office_name,
                            @$item->district_code,
                            @$item->sales_group_name,
                            @$item->sales_unit_id,
                            @$item->sales_unit_name,
                            @$item->month,
                            @$item->year,
                            @$item->retail_code,
                            @$item->retail_name,
                            @$item->product_name,
                            number_format((float) @$item->stok_awal,3,",","."),
                            number_format((float) @$item->penebusan,3,",","."),
                            number_format((float) @$item->penyaluran,3,",","."),
                            number_format((float) @$item->stok_akhir,3,",","."),
                            @$item->status_name,
                        ];
                });
                
    }
    
    


}
