<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportSPJBAsalHeader implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function __construct(Collection $items)
    {
        $this->items = $items;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
   
    public function collection()
    {
        //
        return $this->items->map(function($item){
            return [
                $item->no_doc,
                $item->year,
                $item->customer_name,
                $item->sales_org_name,
                $item->status_name,            
            ];
        });

    }

    public function headings(): array
    {
        return [
            "No Dokumen",
            "Tahun",
            "Distributor",
            "Produsen",
            "status",
        ];
    }
}
