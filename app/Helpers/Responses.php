<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function responseLoginSuccess($message) {
    $response = [
        'status' => 1,
        'status_txt' => "success",
        'message' => $message,
    ];

    return $response;
}

function responseFail($message) {
    $response = [
        'status' => 0,
        'status_txt' => "errors",
        'message' => $message,
    ];

    return $response;
}

function responseSuccess($message, $data = array()) {
    $response = [
        'status' => 1,
        'status_txt' => "success",
        'message' => $message,
        'data' => $data
    ];

    return $response;
}

function responseDatatableSuccess($message, $data = array()) {
    $response = [
        'status' => 1,
        'status_txt' => "success",
        'message' => $message,
    ];
    $return = array_merge($response, $data);

    return $return;
}

