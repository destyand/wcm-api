<?php

namespace App\Helpers;

use SoapClient;
use SoapFault;

class SAPConnect {

    public static function connect($xml) {
        $options = array(
            'login' => env("SAP_USERNAME", ""),
            'password' => env("SAP_PASSWORD", ""),
        );

        $wsdl = resource_path() . '/wsdl/' . env("SAP_SERVER", "") . '/' . $xml;

        try {
            $soap = new SoapClient($wsdl, $options);
            return $soap;
        } catch (SoapFault $ex) {
            $response['faultcode'] = $ex->faultcode();
            $response['faultstring'] = $ex->faultstring();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
