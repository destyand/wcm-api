<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            // throw UnauthorizedException::notLoggedIn();
            $response = responseFail(trans('messages.spatie-not-login'));
            
            return response()->json($response, 401, [], JSON_PRETTY_PRINT);
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if (app('auth')->user()->can($permission)) {
                return $next($request);
            }
        }
        $response = responseFail(trans('messages.spatie-not-permission'));
        return response()->json($response, 401, [], JSON_PRETTY_PRINT);

        // throw UnauthorizedException::forPermissions($permissions);
    }
}
