<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\CreateSalesOrder;
use App\Jobs\SendInvoice;
use App\Libraries\PaymentGateway;
use App\Models\Bank;
use App\Models\ManualSO;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManualSOController extends Controller {

    public function store(Request $request) {
        \LogActivity::addToLog('create manual so');

        $attributes = $request->only(['booking_code', 'sender', 'bank_id', 'payment_date', 'amount']);

        $this->validate($attributes, ManualSO::ruleCreate());

        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {
            $model = ManualSO::create($attributes);
            $order = Order::where('booking_code', $model->booking_code)->first();

            if ($order) {
                if ($order->payment_method === PaymentGateway::CASH) {
                    $order->bank_id = $attributes['bank_id'];
                }

                $order->bank_payment = $attributes["bank_id"];
                $order->billing_date = $attributes['payment_date'];

                if ($order->status != "y") {
                    DB::rollback();
                    $msg = ['booking_code' => [trans('messages.booking-paid')]];
                    $str = trans('messages.booking-paid');
                    $response = responseFail($msg);
                    $response['errors'] = $str;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                if (
                    $attributes['amount'] != $order->upfront_payment || 
                    $attributes['payment_date'] > $order->payment_due_date ||
                    $attributes['payment_date'] < $order->order_date
                ) {
                    DB::rollback();
                    $msg = [];
                    $str = trans('messages.amount-unmatch');
                    $msg += ['amount' => [trans('messages.amount-unmatch')]];

                    if (
                        $attributes['payment_date'] > $order->payment_due_date || 
                        $attributes['payment_date'] < $order->order_date
                    ) {
                        $msg += ['payment_date' => [trans('messages.payment-expired')]];
                        $str .= ", " . trans('messages.payment-expired');
                    }

                    $response = responseFail($msg);
                    $response['errors'] = $str;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
                if ($order->payment_method == 'Cash') {
                    $order->status = 'l';
                } elseif ($order->payment_method == 'Distributor Financing') {
                    $order->status = 'u';
                    SendInvoice::dispatch($order->bank_id, $order->id);
                }

                $order->save();
                CreateSalesOrder::dispatch($order->booking_code);
            }

            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
