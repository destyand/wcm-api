<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\MenuURLMAPS;
use App\Models\PermissionHasRoutes;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Models\MAction;
use App\Models\RoleHasMenu;

class MenuController extends Controller
{

    public function index(Request $request) {
        \LogActivity::addToLog('get all menu');
        $query = Menu::getMenu();
        $user = Datatables::of($query)
            ->filter(function($query) use ($request) {
                $columns = [
                    'a.name_ID' => 'name_ID', 'a.name_EN' => 'name_EN', 'a.url' => 'url', 'a.order_no' => 'order_no', 'a.icon' => 'icon', 
                    'a.created_at' => 'created_at', 'a.updated_at' => 'updated_at', 'b.name_ID' => 'parent_name_ID', 'b.name_EN' => 'parent_name_EN'
                ];
                $this->filterColumn($columns, $request, $query);
            })
            ->orderColumn('name_ID', 'a.name_ID $1')
            ->orderColumn('name_EN', 'a.name_EN $1')
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $user->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query) {
        foreach ($columns as $key => $value) {
            if ($request->get($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query) {
        if ($value === 'created_at' || $value === 'updated_at') {
            $arr = explode(';', $request->get($value));
            if (count($arr) == 1) {
                $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
            } else {
                $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
            }
        } else {
            $query->where($key, 'like', "%{$request->get($value)}%");
        }
    }


    public function sidebar(Request $request)
    {
        \LogActivity::addToLog('get menu sidebar');
        $sidebarKey = sprintf("%s-%s", base64_encode(json_encode($request->user())), "sidebar");
        $tree = Cache::remember($sidebarKey, 3600 * 1000, function() use ($request){
            $language = strtoupper($request->header('Language'));
            if(!$language){
                $language = 'EN';
            }
            $user_id = Auth::user()->id;
            $user = User::find($user_id);
            $role_id = $user->roles->pluck('id')[0];
            $menus = Menu::byRole($role_id, $language);

            $menus = json_decode(json_encode($menus), true);

            return $this->buildTree($menus, 0, $role_id);
        });

        $response = responseSuccess(trans('messages.read-success'), $tree);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        
    }

    function buildTree(array $elements, $parentId = 0, $role_id) {
        $branch = array();
    
        foreach ($elements as $element) {
            $roleHasMenus = RoleHasMenu::where(['role_id' => $role_id, 'menu_id' => $element['id']])->select('action_id')->get();
            $access = [];
            foreach($roleHasMenus as $i => $iv){
                $access[] = trim($iv['action_id']); 
            }
            $element['access'] = $access;
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id'], $role_id);
                if ($children) {
                    $element['children'] = $children;
                }else{
                    $element['children'] = [];
                }
                $branch[] = $element;
            }
        }
    
        return $branch;
    }
    
    public function store(Request $request) {
        \LogActivity::addToLog('create menu');

        $attributes = $request->all();
        $this->validate($attributes, Menu::ruleCreate());

        if(!$attributes['url']){
            $attributes['url'] = '#';
        }

        /*
        if(Menu::where('order_no', $attributes['order_no'])->count() > 0){
            $response = responseFail(trans('messages.order_no-fail'));
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }*/
        
        DB::beginTransaction();
        try {
            $model = Menu::create($attributes);
            DB::commit();
            Cache::flush();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    } 

    public function show($id) {
        \LogActivity::addToLog('get menu by id');

        $model = $this->findData(Menu::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    
    public function update(Request $request, $id) {

        \LogActivity::addToLog('update menu');

        $attributes = $request->all();
        $this->validate($attributes, Menu::ruleCreate());

        $model = $this->findData(Menu::class, $id);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            Cache::flush();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id) {
        \LogActivity::addToLog('delete menu');

        $model = $this->findData(\App\Models\Menu::class, $id);
        $attributes = ['status' => 'n'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            Cache::flush();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function permission(Request $request, $role_id) {
        \LogActivity::addToLog('get all permission menu');
        $language = strtoupper($request->header('Language'));
        if(!$language){
            $language = 'EN';
        }

        $menus = Menu::select('id', 'name_'.$language.' as name')->where('status','y')->get()->toArray();      
        $actions = MAction::all()->toArray();      
        foreach($menus as $i => $iv){
            foreach($actions as $j => $jv){
                $actions[$j]['id'] = trim($jv['id']); 
                $actions[$j]['action_name'] = $jv['action_name']; 
                $menus[$i][trim($jv['id'])] = (RoleHasMenu::where(['role_id' => $role_id, 'menu_id' => $iv['id'], 'action_id' => $jv['id']])->count() ? 'Y' : 'N');
            }
        }
        $response = responseSuccess(trans('messages.read-success'), $menus);
        $response['dataAction'] = $actions;

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function permission_new(Request $request, $role_id) {
        \LogActivity::addToLog('get all permission menu');
        $language = strtoupper($request->header('Language'));
        if(!$language){
            $language = 'EN';
        }
        $arrAction =MAction::all(); $actions = $arrAction->pluck('id');
        $menus = Menu::select('id', 'name_'.$language.' as name','parent_id')->where('status','y');
        if(!$this->isAdmin){
            $menus = $menus->whereNotIn('id',[22,23,27,30,50]);
        }
        $menus = $menus->get(); 

        foreach($menus as $i => $iv){
            foreach($actions as $j => $jv){
                $menus[$i][trim($jv)] = ($iv->roleHasMenu()->where(['role_id' => $role_id, 'action_id' => $jv])->count() ? 'Y' : 'N');
            }
        }

        $menus = $this->hirarki($menus->toArray());

        $response = responseSuccess(trans('messages.read-success'), $menus);
        $response['dataAction'] = $arrAction;
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);    
        
    }

    public function setPermission(Request $request, $role_id) {
        \LogActivity::addToLog('set permission menu');
        
        $attributes = ['data'=>$request->all()];
        $this->validate($attributes, Menu::rulePermission());

        // $attributes = json_decode($attributes['data'], true);
        $roleHasMenu = $this->findRoleHasMenu($role_id);
        $role = $this->findData(Role::class, $role_id);

        $arr = collect([['method'=> 'GET','url'=>'sidebar'],
                ['method'=> 'GET', 'url'=>'region/provinsi'],
                ['method'=> 'GET', 'url'=>'user/profile'],
                ['method'=> 'GET', 'url'=>'region/kabupaten'],
                ['method'=> 'GET', 'url'=>'region/kecamatan'],
                ['method'=> 'GET', 'url'=>'region/salesorg/provinsi/assg'],
                ['method'=> 'GET', 'url'=>'region/provinsi/salesgroup/assg']]);

        $collection = collect([]);
        $permissionsArr  = PermissionHasRoutes::whereIn('method',$arr->pluck('method')->unique())
                            ->whereIn('url',$arr->pluck('url'))->get();
        foreach ($permissionsArr->pluck('permission_id')->unique() as $value) {
            # code...
            $collection->push((int)@$value);
        }

        foreach ($attributes['data'] as $attr ) {
            # code...
            $permission_maps= DB::table('wcm_menu_permission_maps')->where('menu_id',$attr['menu_id'])
                            ->where('action_id',$attr['action_id'])->get();
            $permissions  = PermissionHasRoutes::whereIn('method',$permission_maps->pluck('method')->unique())
                            ->whereIn('url',$permission_maps->pluck('url'))->get();

            foreach ($permissions->pluck('permission_id')->unique() as $value) {
                # code...
                if(!$collection->contains($value)) $collection->push((int)@$value);
            }
        }

        DB::beginTransaction();
        try {
            $role->syncPermissions($collection);
            $roleHasMenu->delete();
            RoleHasMenu::insert($attributes['data']);
            DB::commit();
            Cache::flush();
            $response = responseSuccess(trans('messages.create-success'),$collection);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function findRoleHasMenu($role_id) {
        try {
            $data = RoleHasMenu::where('role_id', $role_id);
        } catch (ModelNotFoundException $e) {
            $response = responseFail(   );
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }

        return $data;
    }

    private function hirarki(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->hirarki($elements, $element['id']);
                if ($children) {
                    $element['menu_childs'] = $children;
                }
                $branch[] = $element+=['menu_childs'=>[]];
            }
        }

        return $branch;
    }

    public function menuUrlMaps(Request $request)
    {
        \LogActivity::addToLog('Menu Has URL ACSESS');

        $attributes = $request->only(['menu_id','action_id','method','url']);


        $this->validate($attributes, ['menu_id'=>'required|exists:wcm_m_menus,id','action_id'=>'required|exists:wcm_m_actions,id', 'method'=>'required|in:GET,POST,PUT,DELETE,OPTIONS', 'url'=>'required|array' ]);

        $arr=[];
        foreach ($attributes['url'] as $key => $value) {
            # code...
            $where=['menu_id'=>$attributes['menu_id'], 'action_id' => $attributes['action_id'], 'method' => $attributes['method'] , 'url'=> $value];
            $detail = MenuURLMAPS::where($where)->first();
            
            if(!$detail){
                array_push($arr, $where);
            }
        }

        DB::beginTransaction();
        try {
            MenuURLMAPS::insert($arr);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'));
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

}
