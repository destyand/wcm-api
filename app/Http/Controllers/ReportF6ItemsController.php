<?php

namespace App\Http\Controllers;

use App\Exports\RekapF6Export;
use App\Models\ContractItem;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\Order;
use App\Models\Product;
use App\Models\ReportF5;
use App\Models\ReportF5Items;
use App\Models\ReportF6;
use App\Models\ReportF6Items;
use App\Models\Retail;
use App\Models\ProductLimit;
use App\Models\SalesUnit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ReportF6ItemsController extends Controller
{
    //
    public function index($uuid)
    {
        # code...
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Detail Report F6');
        is_uuid($uuid);

        $print = $this->newHirarkiArr($uuid);

        $response = responseSuccess(trans('messages.read-success'), $print);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    private function newHirarkiArr($uuid)
    {
        is_uuid($uuid);
        $where_f6['tb1.uuid'] = $uuid;
        $reportf6             = ReportF6::MasterReportF6($where_f6)->first();

        // Report F5 Item
        $reportf6item = ReportF6Items::where('report_f6_id', $reportf6->id)->get();

        $whereDR['tb1.report_f5_id']   = $reportf6->report_f5_id;
        $whereDR['tb1.customer_id']    = $reportf6->customer_id;
        $whereDR['tb1.sales_org_id']   = $reportf6->sales_org_id;
        $whereDR['tb1.month']          = intval($reportf6->month);
        $whereDR['tb1.year']           = $reportf6->year;
        $whereDR['tb1.sales_group_id'] = $reportf6->sales_group_id;
        $distrib_reports               = DistribReports::where("report_f5_id", $reportf6->report_f5_id)
                                            ->where("status", "s")
                                            ->whereIn("distrib_resportable_type", ["order","InitialStockF5"])
                                            ->get();
        // Find Produks
        $produks     = Product::all();
        // Find uniq pkp
        $uniq_pkp = $distrib_reports->pluck("number")->unique()->toArray();
        // Uniq Distrib ID
        $uniq_id = $distrib_reports->pluck("id")->unique()->toArray();

        $spjb = DB::table('wcm_contract_item AS tb1')
        ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
        ->select('tb1.sales_unit_id')
        ->where('tb2.customer_id',$reportf6->customer_id)
        ->where('tb2.sales_org_id', $reportf6->sales_org_id)
        ->where('tb1.month', $reportf6->month)
        ->where('tb1.year', $reportf6->year)
        ->where('tb1.status', 'y')
        ->where('tb1.sales_group_id', $reportf6->sales_group_id)
        ->where('tb2.contract_type','asal')
        ->get()
        ->pluck('sales_unit_id')->unique();

        // Retail
        if($reportf6->status=="d"){
        $where_r['c.id'] = $reportf6->sales_group_id;
        $where_r['f.id'] = $reportf6->customer_id;
        $where_r['g.id'] = $reportf6->sales_org_id;
        $last            = date('Y-m-t', strtotime($reportf6->year . "-" . $reportf6->month));
        $retails         = Retail::retailDitributionDO($where_r)->whereIn('a.sales_unit_id', $spjb)
                ->whereRaw("e.created_at <= '" . $last . "'")
                ->whereIn("e.status",['y','p'])
                ->whereRaw("a.sub_district_default = '0'")
                ->orderBy('a.name', 'ASC')
                ->get();
        // Find uniq Kecamatan | Sales_unit_id In SPJB
        $salesUnits = SalesUnit::whereIn('id', $spjb)->orderby('name', 'ASC')->get();
        }else{
            $inRetails = ReportF6Items::where('report_f6_id',$reportf6->id)->pluck('retail_id')->unique();
            $retails = Retail::whereIn('id',$inRetails)->orderBy('name', 'ASC')->get();
            $salesUnits = SalesUnit::whereIn('id', $retails->pluck('sales_unit_id'))->orderby('name', 'ASC')->get();
        }

        // Distrib Order (Penyaluran DO)
        $distribitems = DistribReportItems::wherein('distrib_report_id', $uniq_id)
        ->where('qty', '!=', 0)->get();

        // // Find uniq Kecamatan | Sales_unit_id In SPJB
        // $salesUnits = SalesUnit::whereIn('id', $spjb)->orderby('name', 'ASC')->get();

        // Find Stok Awal (bulan Lalu)
        $where_stoks['tb2.customer_id']  = $reportf6->customer_id;
        $where_stoks['tb2.sales_org_id'] = $reportf6->sales_org_id;
        $years                           = $reportf6->year;
        $months                          = $reportf6->month - 1;
        if ($reportf6->month == "01" || $reportf6->month == 1) {
            $years  = $reportf6->year - 1;
            $months = 12;
        }
        $where_stoks['tb2.month']          = $months;
        $where_stoks['tb2.year']           = $years;
        $where_stoks['tb2.sales_group_id'] = $reportf6->sales_group_id;
        $stoks                             = ReportF6Items::getStokReportF6($where_stoks)->get();

        // Cek Stoks Initials stok f5
        $where_stok_f6['customer_id']              = $reportf6->customer_id;
        $where_stok_f6['sales_org_id']             = $reportf6->sales_org_id;
        $where_stok_f6['month']                    = intval($reportf6->month);
        $where_stok_f6['year']                     = $reportf6->year;
        $where_stok_f6['distrib_resportable_type'] = 'InitialStockF6';
        $InitialStokF6ID                           = DistribReports::where($where_stok_f6)->where('status', 's')->latest()->first();

        $initial_stocks = DB::table('wcm_initial_stock_item AS tb1')
        ->leftjoin('wcm_initial_stocks AS tb2', 'tb2.id', '=', 'tb1.initial_stock_id')
        ->where('tb2.customer_id',$reportf6->customer_id)
        ->where('tb2.sales_org_id', $reportf6->sales_org_id)
        ->where('tb2.month', intval($reportf6->month))
        ->where('tb2.year', $reportf6->year)
        ->where('tb2.type', 'InitialStockF6')
        ->get();

        if($InitialStokF6ID){
            $stokitems = DistribReportItems::where('distrib_report_id', $InitialStokF6ID->id)->get();
        }

        

        $print['data']   = [];
        $print['header'] = [
            'uuid'              => $reportf6->uuid,
            'id'                => $reportf6->id,
            'f5_id'             => $reportf6->report_f5_id,
            'number'            => $reportf6->number,
            'sales_org_name'    => $reportf6->sales_org_name,
            'customer_id'       => $reportf6->customer_id,
            'customer_name'     => $reportf6->customer_name,
            'sales_group_id'    => $reportf6->sales_group_id,
            'sales_group_name'  => $reportf6->sales_group_name,
            'submited_date'     => $reportf6->submited_date,
            'sales_office_name' => $reportf6->sales_office_name,
            'month'             => $this->monthName(intval($reportf6->month)),
            'month_angka'       =>intval($reportf6->month),
            'year'              => $reportf6->year,
            'status'            => $reportf6->status,
            'status_name'       => $reportf6->status_name,
            'product_limit'     => ProductLimit::select('product_id',DB::raw("CAST(CONVERT(DECIMAL(10,3),qty) as nvarchar) as qty"))->get()
        ];
        $print['report_f6_id'] = $reportf6->id;
        $child_template = $this->makeHeaderKec($produks);
        $template       = array('awal' => $child_template , 'penebusan' => $child_template, 'penyaluran' => $child_template, 'akhir' => $child_template );
        $print['total'] = $template;
        $jumlahdata = 0;
        // Looping Kecamatan
        foreach ($salesUnits as $key => $valUnit) {
            // $salesUnit = $salesUnits->where('id', $valUnit)->first();
            $kecamatanArr = array('id' => $valUnit->id, 'name' => $valUnit->name, 'data' => array(),'total'=> $template);
            
            // Find Retail Int This Sales unit / Kecamatan
            $retails_sales_unit    = $retails->where('sales_unit_id',$valUnit->id);

            foreach ($retails_sales_unit as $key => $retail_sales_unit) {
                # code...
                $retailArr['id']          = $retail_sales_unit->id;
                $retailArr['retail_code'] = $retail_sales_unit->code;
                $retailArr['name']        = $retail_sales_unit->name;
                $retailArr['data']        = array();
                $produkArr = array('awal' => array(), 'penebusan' => array(), 'penyaluran' => array(), 'akhir' => array(),'total'=> $template);
                $jumlahdata += 1;
                // Looping Produks
                $flags = 0;
                foreach ($produks as $key => $valP) {
                    # code...
                    /*penebusan*/
                    $ditribitems_produks_arr = $distribitems->where('retail_id',$retail_sales_unit->id)->where('product_id',$valP->id)->sum('qty');
                    $reportf6item_p_arr = $reportf6item->where('retail_id',$retail_sales_unit->id)->where('product_id',$valP->id);
                    $stoks_p_arr = $stoks->where('retail_id',$retail_sales_unit->id)->where('product_id',$valP->id)->sum('stok_akhir');
                                       
                    $latest_initial_stock = $initial_stocks->where('retail_id',$retail_sales_unit->id)
                    ->where('product_id',$valP->id)
                    ->last();

                    if($latest_initial_stock){
                        if($latest_initial_stock->qty!=null){
                            $awal=round(floatval($latest_initial_stock->qty),3);
                        }else{
                            $awal = $stoks_p_arr;
                        }
                    }else{
                        $awal = $stoks_p_arr;
                    }
                    // Penebusan
                    $qty = $ditribitems_produks_arr;
                    // Penyaluran
                    $distrib    =  $reportf6item_p_arr->sum('penyaluran');
                    $penebusanx = $ditribitems_produks_arr;

                    if (count($reportf6item_p_arr) != 0) {
                        $uuids = $reportf6item_p_arr->first()->uuid;
                        $akhir = ($awal + $penebusanx) - $distrib;
                    } else {
                        $uuids = "";
                        $akhir = ($awal + $penebusanx) - $distrib;
                    }
                    // Akhir
                    $awal=round($awal,3);
                    $penebusanx=round($penebusanx,3);
                    $distrib=round($distrib,3);
                    $akhir=round($akhir,3);
                    array_push($produkArr['awal'], array('uuid' => $uuids, 'id' => $valP->id, 'qty' => $awal, 'sales_unit_id' => $valUnit->id, 'retail_id' => $retail_sales_unit->id));
                    array_push($produkArr['penebusan'], array('uuid' => $uuids, 'id' => $valP->id, 'qty' => $penebusanx, 'sales_unit_id' => $valUnit->id, 'retail_id' => $retail_sales_unit->id));
                    array_push($produkArr['penyaluran'], array('uuid' => $uuids, 'id' => $valP->id, 'qty' => $distrib, 'sales_unit_id' => $valUnit->id, 'retail_id' => $retail_sales_unit->id));
                    array_push($produkArr['akhir'], array('uuid' => $uuids, 'id' => $valP->id, 'qty' => $akhir, 'sales_unit_id' => $valUnit->id, 'retail_id' => $retail_sales_unit->id));

                    $kecamatanArr['total']['awal'][$key]['qty'] = round(($kecamatanArr['total']['awal'][$key]['qty']+$awal),3);
                    $kecamatanArr['total']['akhir'][$key]['qty'] = round($kecamatanArr['total']['akhir'][$key]['qty']+($akhir),3);
                    $kecamatanArr['total']['penebusan'][$key]['qty'] = round($kecamatanArr['total']['penebusan'][$key]['qty'] + $penebusanx,3);
                    $kecamatanArr['total']['penyaluran'][$key]['qty'] = round(($kecamatanArr['total']['penyaluran'][$key]['qty']+$distrib),3);

                    $print['total']['awal'][$key]['qty'] = round(($print['total']['awal'][$key]['qty']+ $awal),3);
                    $print['total']['akhir'][$key]['qty'] = round(($print['total']['akhir'][$key]['qty']+ $akhir),3);
                    $print['total']['penebusan'][$key]['qty'] = round(($print['total']['penebusan'][$key]['qty']+ $penebusanx),3);
                    $print['total']['penyaluran'][$key]['qty'] = round(($print['total']['penyaluran'][$key]['qty']+ $distrib),3);
                }
                $retailArr['data'] = $produkArr;
                array_push($kecamatanArr['data'], $retailArr);
            }
            if (count($kecamatanArr['data']) != 0) {
                array_push($print['data'], $kecamatanArr);
            }
        }
        $print['produks']             = $produks;
        $print['jumlah_page']         = $jumlahdata;
        return $print;
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('Store Report F6');

        $attributes = $request->all();
        $this->validate($attributes, ReportF6Items::createf6ItemsHead());

        $datasInsert = [];
        DB::beginTransaction();
        try {
            //code...

            $modelf6 = ReportF6::where('id', $attributes['report_f6_id'])->first();

            /*Submit Kondisi*/
            if ($attributes['status'] == 's' && $modelf6->status == 'd') {
                $wheref5['id'] = $modelf6->report_f5_id;
                $modelF5       = $this->findDataWhere(ReportF5::class, $wheref5);
                if ($modelF5->status != 's' && $modelF5->status != 'y') {
                    DB::rollback();
                    $response             = responseFail(trans('messages.update-fail'), $attributes);
                    $response['messages'] = "Report F5 Status Belum Approve Atau Submit";
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            }

            /*Approve*/
            if ($attributes['status'] == 'y' && $modelf6->status == 's') {
                $wheref5['id'] = $modelf6->report_f5_id;
                $modelF5       = $this->findDataWhere(ReportF5::class, $wheref5);
                if ($modelF5->status != 'y') {
                    DB::rollback();
                    $response             = responseFail(trans('messages.update-fail'), $attributes);
                    $response['messages'] = "Report F5 Status Belum Approve";
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            }

            /*UnApprove*/
            if ($attributes['status'] == 's' && $modelf6->status == 'y') {
                $wheref5['id'] = $modelf6->report_f5_id;
                $modelF5       = ReportF5::where($wheref5)->first();

                $whereNextF5['customer_id'] = $modelF5->customer_id;
                $whereNextF5['month']       = $modelF5->month + 1;
                $whereNextF5['year']        = $modelF5->year;
                if ($modelF5->month == 12) {
                    $whereNextF5['month'] = 1;
                    $whereNextF5['year']  = $modelF5->year + 1;
                }
                $whereNextF5['sales_group_id'] = $modelF5->sales_group_id;
                $whereNextF5['sales_org_id']   = $modelF5->sales_org_id;

                $modelNextF5 = ReportF5::where($whereNextF5)->first();
                if ($modelNextF5) {
                    if ($modelNextF5->status != "d" && $modelNextF5->status != "s") {
                        DB::rollback();
                        $response             = responseFail(trans('messages.update-fail'), $attributes);
                        $response['messages'] = "Report F5 Bulan Selanjutnya Status Belum Draft Atau Submit";
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            /*Un Submit*/
            if ($attributes['status'] == 'd' && $modelf6->status == 's') {
                $wheref5['id'] = $modelf6->report_f5_id;
                $modelF5       = ReportF5::where($wheref5)->first();

                $whereNextF5['customer_id'] = $modelF5->customer_id;
                $whereNextF5['month']       = $modelF5->month + 1;
                $whereNextF5['year']        = $modelF5->year;
                if ($modelF5->month == 12) {
                    $whereNextF5['month'] = 1;
                    $whereNextF5['year']  = $modelF5->year + 1;
                }
                $whereNextF5['sales_group_id'] = $modelF5->sales_group_id;
                $whereNextF5['sales_org_id']   = $modelF5->sales_org_id;

                $modelNextF5 = ReportF5::where($whereNextF5)->first();
                if ($modelNextF5) {
                    if ($modelNextF5->status != "d") {
                        DB::rollback();
                        $response             = responseFail(trans('messages.update-fail'));
                        $response['messages'] = "Report F5 Bulan Selanjutnya Status Belum Draft";
                        $response['data']     = $modelNextF5;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            $val_status = array('status' => $attributes['status']);

            /* Find Data On Distrip report*/
            $whereDistribz['report_f5_id']             = $modelf6->report_f5_id;
            $whereDistribz['customer_id']              = $modelf6->customer_id;
            $whereDistribz['month']                    = intval($modelf6->month);
            $whereDistribz['year']                     = $modelf6->year;
            $whereDistribz['sales_org_id']             = $modelf6->sales_org_id;
            $whereDistribz['sales_group_id']           = $modelf6->sales_group_id;
            $whereDistribz['distrib_resportable_type'] = "DistribReportGroup";

            $distrib = DistribReports::where($whereDistribz)->first();
            if (!$distrib) {
                $whereDistribz['number']            = $this->genUniqPKP();
                $whereDistribz['created_by']        = Auth::user()->id;
                $whereDistribz['distribution_date'] = now();
                $whereDistribz['status']            = $attributes['status'];
                $distribId                          = DistribReports::insertGetId($whereDistribz);
                $distrib = DistribReports::where('id',$distribId)->first();
            } else {
                $whereDistrib['updated_by'] = Auth::user()->id;
                $whereDistrib['status']     = $attributes['status'];
                $distrib->update($whereDistrib);
            }

            /* END Finda Data*/
            $temp_retail = [];

            foreach ($attributes['data'] as $data) {
                foreach ($data['data'] as $val) {
                    // Insert Or update
                    if(!in_array($val['retail_id'], $temp_retail)){
                        array_push($temp_retail,$val['retail_id']);
                    }

                    $whereDistribItems['distrib_report_id'] = $distrib->id;
                    $whereDistribItems['retail_id']         = $val['retail_id'];
                    $whereDistribItems['product_id']        = $val['product_id'];
                    $whereDistribItems['report_f5_id']      = $modelf6->report_f5_id;
                    $distribitems                           = DistribReportItems::where($whereDistribItems)->first();

                    if (!$distribitems and floatval($val['penyaluran']) != 0) {
                        $whereDistribItems['qty']        = floatval($val['penyaluran']);
                        $whereDistribItems['status']     = $attributes['status'];
                        $whereDistribItems['created_by'] = Auth::user()->id;
                        $whereDistribItems['created_at'] = now();
                        $distribitems                    = DistribReportItems::create($whereDistribItems);
                        unset($whereDistribItems['created_by']);
                        unset($whereDistribItems['created_at']);
                    } elseif ($distribitems && floatval($val['penyaluran']) != 0) {
                        $whereDistribItems['qty']        = floatval($val['penyaluran']);
                        $whereDistribItems['status']     = $attributes['status'];
                        $whereDistribItems['updated_by'] = Auth::user()->id;
                        $whereDistribItems['updated_at'] = now();
                        $distribitems->update($whereDistribItems);
                        unset($whereDistribItems['updated_by']);
                        unset($whereDistribItems['updated_at']);
                    } elseif ($distribitems && floatval($val['penyaluran']) == 0) {
                        $distribitems->delete();
                    }
                    unset($whereDistribItems['qty']);
                    unset($whereDistribItems['status']);

                    $val['report_f6_id'] = $attributes['report_f6_id'];
                    $val['penyaluran']   = floatval($val['penyaluran']);
                    $val['penebusan']    = floatval($val['penebusan']);
                    $val['stok_awal']    = floatval($val['awal']);
                    $val['stok_akhir']   = floatval($val['akhir']);
                    unset($val['awal']);unset($val['akhir']);
                    $this->validate($val, ReportF6Items::createf6ItemsRule());

                    if ($val['uuid'] == null or $val['uuid'] == "") {
                        unset($val['uuid']);
                        $val['created_by'] = Auth::user()->id;
                        $val['updated_by'] = Auth::user()->id;
                        $val['created_at'] = now();
                        $val['updated_at'] = now();
                        array_push($datasInsert, $val);
                    } else {
                        // Update
                        $model             = $this->findDataUuid(ReportF6Items::class, $val['uuid']);
                        $val['updated_by'] = Auth::user()->id;
                        $val['updated_at'] = now();
                        $model->update($val);
                    }

                }
            }

            $perInsertLength = 100;
            $length = count($datasInsert);
            // Insert Data Baru
            for ($i=0; $i<ceil($length/$perInsertLength); $i++) {
                $slices_arr = array_slice($datasInsert, $i*$perInsertLength, $perInsertLength);
                ReportF6Items::insert($slices_arr);
            }
            // ReportF6Items::insert($datasInsert);

            // Update Status On report F6
            $modelf6->update(['status' => $attributes['status'],'updated_by'=>Auth::user()->id]);

            ReportF6Items::where('report_f6_id',$modelf6->id)->whereNotIn('retail_id', $temp_retail)->delete();

            DB::commit();

            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'), $attributes);
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function download($uuid)
    {
        \LogActivity::addToLog('download Report F6');
        is_uuid($uuid);
        $datas['data'] = $this->newHirarkiArr($uuid);
        $pdf       = PDF::loadView('reportf6.download', $datas);
        $nama_file = 'REPORT-F6' . $datas['data']['header']['number'] . "-" . $datas['data']['header']['sales_group_name'] . "-" . $datas['data']['header']['month'] . "-" . $datas['data']['header']['year'];
        return $pdf->setPaper('a4', 'potrait')->download($nama_file);
    }

    public function rekap($uuid, $fileType = null)
    {
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('download Rekap F6');
        if (in_array($fileType, ['xls', 'xlsx'])) {
            $columns   = ['sales_org_name', 'number', 'customer_name', 'month', 'year','sales_group_id','sales_office_id', 'sales_office_name', 'sales_group_name','status'];
            $filtering = request()->only($columns);

           
            $alias = [  
                'sales_org_name' => 'tb_sales_org.name', 
                'number' => 'tb_f6.number', 
                'customer_name' => 'tb_f6.customer_name', 
                'month' => 'tb_f6.month', 
                'year' => 'tb_f6.year', 
                'sales_office_name' => 'tb_sales_office.name', 
                'sales_group_name' => 'tb_sales_group.name',
                'sales_group_id' => 'tb_sales_group.id',
                'sales_office_id' => 'tb_sales_office.id',
                'status' => 'tb_f6.status',
                'sales_org_id' => 'tb_sales_org.id'
            ];

            $query     = ReportF6Items::rekap()->where(function ($q) use ($filtering,$alias) {
                foreach ($filtering as $key => $value) {
                    if($key=="month" && $value!=""){
                        if(strpos($value, ';') !== false){
                            $listMonth = explode(';', $value);
                            $q->whereIn($alias[$key], $listMonth);
                        }else{
                            $q->where($alias[$key], '=', "{$value}" );
                        }
                    }elseif(strpos($value, ';')){
                        $listStatus = explode(';', $value);
                        $q->whereIn($alias[$key], $listStatus);
                    }else{
                        $q->where($alias[$key], "like", "%{$value}%");
                    }
                }
            });

            $user = Auth::user();
            $filters = $user->filterRegional;

            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb_sales_org.id", $filters["sales_org_id"]);
                }
                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb_sales_group.id", $filters["sales_group_id"]);
                }
            }

            if ($this->isAdminAnper) {
                $query = $query->where('tb_sales_org.id',$this->salesOrgId);
            }

            return Excel::download(new RekapF6Export($query), "rekapf6.{$fileType}");
        } else if ($fileType === 'pdf') {
            is_uuid($uuid);
            // $query = ReportF6Items::rekap($uuid)
                // ->get();

            // if (!$query) {
            //     abort(404);
            // }
            $details = $this->newHirarkiArr($uuid);
            $header["periode"]  = $details['header']['month'] ." ".$details['header']['year'];
            $header['date_ttd'] = Carbon::createFromDate($details['header']['year'],$details['header']['month_angka'])->format("t F Y");
           

            $aes                = new \Legierski\AES\AES;
            // $headsser             = (array) $query->first();
            $route              = DB::table('wcm_m_routes')->where('name', 'reportf5')->first();
            $access             = DB::table('wcm_role_has_menus')
                ->where('role_id', @Auth::user()->role_id)
                ->where('menu_id', $route->id)
                ->get();

            $acceses = array();
            foreach ($access as $key) {
                array_push($acceses, $key->action_id);
            }
            $akses         = implode(", ", $acceses);
            $akses         = str_replace(' ', '', $akses);
            $passphrase    = "sy4hr!lb3g!tut4mp4n";
            $decrypted     = $aes->encrypt($akses, $passphrase);
            $header['url'] = \URL::to('/#/' . urlencode($decrypted) . '/laporan/laporan-bulanan-pengecer/detail/' . $uuid);
            
            // return response()->json($header);
            $render = compact('header', 'details');
            return PDF::loadView('reportf6.rekap', $render)->setPaper([0, 0, 841.89, 1900], 'landscape')
                ->download("rekapf6.pdf");
        }

        abort(404);
    }

    public function bast($uuid)
    {
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Download BAST Report F6');
        is_uuid($uuid);
        $aes = new \Legierski\AES\AES;

        $datas['data'] = $this->newHirarkiArr($uuid);

        $user   = Auth::user();
        $f5uuid = DB::table('wcm_report_f5')->where('id', $datas['data']['header']['f5_id'])->first();
        // user role
        $route = DB::table('wcm_m_routes')->where('name', 'reportf5')->first();

        $access = DB::table('wcm_role_has_menus')
            ->where('role_id', $user->role_id)
            ->where('menu_id', $route->id)
            ->get();

        $acceses = array();
        foreach ($access as $key) {
            array_push($acceses, $key->action_id);
        }

        $akses = implode(", ", $acceses);
        $akses = str_replace(' ', '', $akses);

        $passphrase = "sy4hr!lb3g!tut4mp4n";

        $decrypted = $aes->encrypt($akses, $passphrase);

        $datas['url'] = URL::to('/#/' . urlencode($decrypted) . '/laporan/laporan-bulanan-distributor/detail/' . $f5uuid->uuid);

        // return view('reportf6.bast',$datas);
        $pdf       = PDF::loadView('reportf6.bast', $datas);
        $nama_file = 'BAST-F6' . $datas['data']['header']['number'] . "-" . $datas['data']['header']['sales_group_name'] . "-" . $datas['data']['header']['month'] . "-" . $datas['data']['header']['year'];
        return $pdf->setPaper([0, 0, 841.89, 1250], 'landscape')->download($nama_file);
    }
    public function findso($uuid)
    {
        \LogActivity::addToLog('Find SO Report F6');
        is_uuid($uuid);
        $where_f6['tb1.uuid'] = $uuid;
        $reportf6             = ReportF6::MasterReportF6($where_f6)->first();

        // Find SO Number By Report F5 id
        $where_f5['tb1.id'] = $reportf6->report_f5_id;
        $data               = ReportF5::where('id',$reportf6->report_f5_id)->first();

        $m= (strlen($data->month) == 1) ? "0".$data->month : $data->month;

        $return = Order::leftJoin('wcm_delivery AS tb2', 'wcm_orders.id', '=', 'tb2.order_id')
                ->where('wcm_orders.sales_org_id',$data->sales_org_id)
                ->where('wcm_orders.sales_group_id',$data->sales_group_id)
                ->where('wcm_orders.customer_id',$data->customer_id)
                ->whereIn('wcm_orders.status',['k','c'])
                ->whereMonth('tb2.delivery_date', $m)
                ->whereYear('tb2.delivery_date',$data->year)
                ->select('wcm_orders.uuid','wcm_orders.so_number')
                ->distinct()
                ->get();  

        $response = responseSuccess(trans('messages.read-success'), $return);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    private function makeHeaderKec($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'qty' => 0,
                'id'  => $val->id,
            );
            array_push($datas, $temps);
        }
        return $datas;
    }

    private function makeTotal($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'id'  => $val['id'],
                'qty' => $val['qty'],
            );
            // $datas[$val- > id]=$temps;
            array_push($datas, $temps);
        }

        return $datas;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }
    private function genUniqPKP()
    {
        $maxNumber = DB::table('wcm_distrib_reports')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "PKP" . $number;
    }
}
