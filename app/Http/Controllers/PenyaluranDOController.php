<?php

namespace App\Http\Controllers;

use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\Order;
use App\Models\ReportF5;
use App\Models\Retail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use Yajra\DataTables\DataTables;
use Excel;
use App\Exports\Download;
use App\Models\SalesOrg;


class PenyaluranDOController extends Controller
{
    //
    public function index(Request $request,$exported=false)
    {
        # code...
        \LogActivity::addToLog('get Penyaluran DO');
        $where = [];
        // if ($this->isAdminAnper) {
        //     $where["tb1.sales_org_id"] = $this->salesOrgId;
        // }
        $query   = Order::masterPenyaluranDO($where, $exported)->whereRaw('do_qty > 0');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'sales_org_id'         => 'sales_org_id',
            'sales_org_name'       => 'sales_org_name',
            'so_number'            => 'so_number',
            'number'               => 'number',
            'customer_id'          => 'customer_id',
            'customer_name'        => 'customer_name',
            'so_qty'               => 'so_qty',
            'do_qty'               => 'do_qty',
            'totals'               => 'totals',
            'delivery_method_id'   => 'delivery_method_id',
            'delivery_method_name' => 'delivery_method_name',
            'sales_office_id'      => 'sales_office_id',
            'sales_office_name'    => 'sales_office_name',
            'sales_group_id'       => 'sales_group_id',
            'sales_group_name'     => 'sales_group_name',
            'number_order'         => 'number_order',
            'sisa_final'           => 'sisa_final',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if($exported)  return $model->getFilteredQuery()->get();

        $model=$model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function getAtrributesEntry($number)
    {
        # code...
        \LogActivity::addToLog('get header perbup');

        $where['tb1.number'] = $number;
        $model               = Order::getEntryDo($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        # code...
        \LogActivity::addToLog('create Penyaluran DO');
        // Validate Atrributs
        $attributes = $request->all();
        // Validate
        $this->validate($attributes, DistribReports::ruleCreate());

        $date          = explode('-', $attributes['distribution_date']);
        $ceks          = $request->only(['sales_org_id', 'customer_id', 'sales_group_id']);
        $ceks['month'] = intval($date[1]);
        $ceks['year']  = $date[0];

        // Cek Data On Report F5;
        $cekOnItem                 = $this->cekReportF5(ReportF5::class, $ceks);
        $ceks['so_number']         = $attributes['so_number'];
        $ceks['order_id']          = $attributes['order_id'];
        $ceks['distribution_date'] = $date[0] . '-' . $date[1] . '-' . $date[2];
        $ceks['month']  = strval($ceks['month']);
        // New Initial
        $attributes = $ceks;        
        DB::beginTransaction();
        if ($cekOnItem) {
            if($cekOnItem->status != 'd')
            {
                $response = responseFail(trans('messages.f5-status-draft-allow'));
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $attributes['report_f5_id'] = $cekOnItem->id;

            // Cek pada tanggal Tanggal tersebut  Ada maka Akan Edit Buka Insert Data Baru
            $dataInsert = $this->cekReportEdit(DistribReports::class, $attributes);
            if ($dataInsert) {
                $response = responseSuccess(trans('messages.create-update'), $dataInsert);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            }

            $attributes['status']                   = 'd';
            // $attributes['number']                   = $this->genUniqPKP();
            $attributes['distrib_resportable_type'] = 'order';
            $attributes['created_by']               = Auth::user()->id;
            $attributes['created_at']               = date('Y-m-d H:i:s');

            try {
                // return response()->json($attributes);
                $datas = DistribReports::insertGetId($attributes);
                DB::commit();
                $resp            = $this->findDataWhere(DistribReports::class, ['id'=>$datas]);
                $response         = responseSuccess(trans('messages.create-success'), $resp);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        } else {
            DB::rollback();
            $response['status']     = 0;
            $response['status_txt'] = "errors";
            $response['message']    = "Data Report F5 Not Found";
            $response['errors']     = 'Data Report F5 Not Found';
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function ListDistripReport($uuid, Request $request)
    {
        \LogActivity::addToLog('Master List Report By UUID ORDER');

        is_uuid($uuid);

        $where['tb2.uuid'] = $uuid;
        $query             = DistribReports::listDistripReportByOrder($where);

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                    if (count($filters["sales_org_id"]) > 0) {
                        $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                    }

                    if (count($filters["sales_group_id"]) > 0) {
                        $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                    }
                }
        }


        $columns           = [
            'tb1.id'                 => 'id',
            'tb1.uuid'               => 'uuid',
            'tb1.report_f5_id'       => 'report_f5_id',
            'tb1.number'             => 'number',
            'tb1.so_number'          => 'so_number',
            'tb2.order_date'         => 'order_date',
            'tb1.customer_id'        => 'customer_id',
            'tb3.full_name'          => 'customer_name',
            'tb1.sales_group_id'     => 'sales_group_id',
            'tb4.name'               => 'sales_group_name',
            'tb1.sales_org_id'       => 'sales_org_id',
            'tb5.name'               => 'sales_org_name',
            'tb2.delivery_method_id' => 'inconterm_id',
            'tb6.name'               => 'inconterm_name',
            'tb1.status'             => 'status',
            'tb1.month'              => 'month',
            'tb1.year'               => 'year',
            'tb7.number'             => 'f5_number',
            'tb1.distribution_date'  => 'distribution_date',
            'tb7.uuid'               => 'f5_uuid',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Produsen","Kode SO","No. Penebusan","Distributor","Total Kuantitas SO","Total Kuantitas DO","Total Penyaluran","Sisa Kuantitan DO","Incoterm","Provinsi","Kabupaten"];
        return Excel::download((new Download($data,$columns)), "Download Penyaluran DO.xls");
    }

    public function destroy($uuid)
    {
        \LogActivity::addToLog('Delete Distrip Report / Penyaluran DO');

        is_uuid($uuid);
        $where['uuid'] = $uuid;
        $datas         = $this->findDataWhere(DistribReports::class, $where);
        DB::beginTransaction();
        try {
            DB::table('wcm_distrib_report_item')->where('distrib_report_id', '=', $datas->id)->delete();
            DB::table('wcm_distrib_reports')->where('uuid', '=', $uuid)->delete();
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $datas);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function bast($uuid, Request $request)
    {
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Download BAST Penyaluran DO');
        is_uuid($uuid);
        $distribs                          = $this->findDataUuid(DistribReports::class, $uuid);
        $orders                            = $this->findDataWhere(Order::class, array('id' => $distribs->order_id));
        $where_di['tb1.distrib_report_id'] = $distribs->id;
        $distrib_items                     = DistribReportItems::bastPrint($where_di)->get();

        $sales_org_name = SalesOrg::where('id',$distribs->sales_org_id)->first();
        if($sales_org_name){
            $org_name=$sales_org_name->name;
        }else{
            $org_name="";
        }

        if (count($distrib_items) == 0) {
            $error = [
                "message" => 'Data DO Items not Found ',
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $retails = $distrib_items->pluck('retail_id')->unique(); //array_unique(array_column($distrib_items, 'retail_id'));
        $print['data'] = [];
        foreach ($retails as $keyR => $valR) {
            # code...
            $arr_retail = $distrib_items->where('retail_id',$valR);
            $retail =  $arr_retail->first();
           
            $datas['number']    = 'BAST/' . $distribs->number . '/' . $distribs->month . '-' . $distribs->year . '/' . $retail->code;
            $datas['number_so'] = $orders->so_number;
            
            $today          = Carbon::parse($retail->distribution_date)->format('l');
            $date           = explode('-', Carbon::parse($retail->distribution_date)->format('d-m-Y'));
            $datas['day']   = $this->convNameDate($today);
            $datas['date']  = $date[0];
            $datas['month'] = $this->monthName($date[1]);
            $datas['year']  = $date[2];
            $datas['owner'] = $retail->owner;
            $datas['data']  = array();
           
            foreach ($arr_retail as $key => $val) {
                # code...
                if ($val->qty != 0) {
                    $temps = array(
                        'product_name' => $val->product_name,
                        'qty'          => number_format((float)$val->qty,3,',','.'),
                        'retail_name'  => $val->retail_name,
                        'address'      => $retail->address . ' ' . $val->sales_unit_name,
                    );
                    array_push($datas['data'], $temps);
                }
            }
           
            if (count($datas['data']) != 0) {
                array_push($print['data'], $datas);
            }
        }
        $print['sales_org_name'] = $org_name;

        $pdf       = PDF::loadView('distrib_do.bast', $print);
        $nama_file = 'BAST ' . $distribs->number . '-' . $distribs->customer_id;
        return $pdf->setPaper('a4', 'potrait')->download($nama_file);

    }

    private function cekReportF5($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }
    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }

    private function convNameDate($today)
    {
        switch ($today) {
            case "Sunday":
                return 'Minggu';
                break;
            case "Monday":
                return 'Senin';
                break;
            case "Tuesday":
                return 'Selesa';
                break;
            case "Wednesday":
                return 'Rabu';
                break;
            case "Thursday":
                return 'Kamis';
                break;
            case "Friday":
                return 'Jumat';
                break;
            case "Saturday":
                return 'Sabtu';
                break;
            default:
                return '-';
        }
    }

    private function cekReportEdit($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    private function genUniqPKP()
    {
        $maxNumber = DB::table('wcm_distrib_reports')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "PKP" . $number;
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'year' || $value === "distribution_date") {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }
    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

}
