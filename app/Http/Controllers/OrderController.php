<?php

namespace App\Http\Controllers;

use App\Models\SalesArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderPartnerFunc;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\ApprovalSetting;
use App\Helpers\SAPConnect;
use App\Libraries\PaymentGateway;
use App\Models\CustomerSalesArea;
use App\Models\LogOrder;
use App\Models\LimitDate;
use App\Models\OrderDFStatus;
use App\Models\SalesGroup;
use App\Exports\Download;
use App\Imports\SalesOrderImport;
use App\Models\Plant;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class OrderController extends Controller {

    public function index(Request $request, $exported=false) {
        \LogActivity::addToLog('get all permintaan penebusan');

        $columns = [
            'tb1.number' => 'number',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb2.name' => 'sales_org_name',
            'tb1.contract_id' => 'contract_id',
            'tb1.customer_id' => 'customer_id',
            'tb3.full_name' => 'customer_name',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb4.name' => 'sales_office_name',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb5.name' => 'sales_group_name',
            'tb1.delivery_method_id' => 'delivery_method_id',
            'tb6.name' => 'delivery_method_name',
            'tb1.order_date' => 'order_date',
            'tb1.payment_method' => 'payment_method',
            'tb1.[desc]' => 'desc',
            'tb1.booking_code' => 'booking_code',
            'tb1.total_price' => 'total_price',
            'tb1.total_price_before_ppn' => 'total_price_before_ppn',
            'tb1.upfront_payment' => 'upfront_payment',
            'tb1.ppn' => 'ppn',
            'tb1.pph22' => 'pph22',
            'tb1.billing_date' => 'billing_date',
            'tb1.payment_due_date' => 'payment_due_date',
            'tb1.bank_id' => 'bank_id',
            'tb1.good_redemption_due_date' => 'good_redemption_due_date',
            'tb1.df_due_date' => 'df_due_date',
            'tb1.df_due_date_plan' => 'df_due_date_plan',
            'tb1.pickup_due_date' => 'pickup_due_date',
            'tb1.delivery_location' => 'delivery_location',
            'tb1.so_number' => 'so_number',
            'tb1.sap_status' => 'sap_status',
            'tb1.billing_fulldate' => 'billing_fulldate',
            'tb1.sap_product_desc' => 'sap_product_desc',
            'tb1.sap_miscs' => 'sap_miscs',
            'tb1.reference_code' => 'reference_code',
            'tb1.flag' => 'flag',
            'tb1.seq' => 'seq',
            'tb1.approve_date' => 'approve_date',
            'tb1.approve_type' => 'approve_type',
            'tb1.status' => 'status',
            'tb1.created_by' => 'created_by',
            'tb8.name' => 'created_name',
            'tb1.updated_by' => 'updated_by',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
        ];

        $where['tb1.status'] = 'd';
        // if ($this->isAdminAnper) {
        //     $where["tb1.sales_org_id"] = $this->salesOrgId;
        // }
        $query = Order::getAll($where,$exported);

        $user = $request->user();
        $filters = $user->filterRegional;

        // return response()->json($user->customer_id);
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }
        
        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });

        if ($exported) return $model->getFilteredQuery()->get();

        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request) {
        \LogActivity::addToLog('create permintaan penebusan');

        $attributes = $request->all();
        $attributes['number'] = $this->generateCode(Order::class, 'number', 'ORD', 9);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;
        $attributes['approve_type'] = $this->cekApprovalType($attributes['sales_org_id'], $attributes['sales_group_id']);

        $this->validate($attributes, Order::ruleCreate());

        DB::beginTransaction();
        try {
            $order = Order::create($attributes);
            $model = Order::find($order->id);
            $attr['order_id'] = $order->id;
            $attr['customer_id'] = $attributes['customer_id'];
            $attr['partner_id'] = $attributes['partner_id'];
            $attr['status'] = $attributes['status'];
            $this->storePartnerFunction($attr);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid) {
        \LogActivity::addToLog('get permintaan penebusan by id');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        try {
            $model = Order::getAllAndPartner($where)->first();
            $model->today = Carbon::now()->format('Y-m-d');
        } catch (ModelNotFoundException $e) {
            $response = responseFail(trans('messages.read-fail'));
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $uuid) {
        \LogActivity::addToLog('update header permintaan penebusan');

        $attributes = $request->all();
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, Order::ruleCreate());

        $model = $this->findDataUuid(Order::class, $uuid);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            $attr['order_id'] = $model->id;
            $attr['customer_id'] = $attributes['customer_id'];
            $attr['partner_id'] = $attributes['partner_id'];
            $attr['status'] = $attributes['status'];
            $this->updatePartnerFunction($attr);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function status(Request $request, $uuid) {
        $attributes = $request->only(['status', 'upfront_payment', 'total_price']);
        $status = $request->get("status");
        $upfront_payment = 0;
        $total_price = 0;
        if ($request->has('upfront_payment')) {
            $upfront_payment = $attributes['upfront_payment'];
        }
        if ($request->has('total_price')) {
            $total_price = $attributes['total_price'];
        }

        if ($status == 's') {
            \LogActivity::addToLog('checkout permintaan penebusan');
        } else {
            \LogActivity::addToLog('ganti status permintaan penebusan');
        }

        $model = $this->findDataUuid(Order::class, $uuid);
        $updatedTotalPrice = false;
        DB::beginTransaction();
        try {
            if ($status == 's') {
                $wapu = $this->checkTaxAndPph22($model->customer_id, $model->sales_org_id);
                if ($model->payment_method === PaymentGateway::CASH) {
                    $dpp = (float) $model->total_price_before_ppn;
                    $ppn = (float) $model->ppn;
                    $pph22 = (float) $model->pph22;
                    $wapuInClassification = false;
                    
                    if($wapu && in_array($wapu->tax_classification, [2,3])){
                        $pph05 = $wapu->pph22 === "05";
                        if ( $dpp > 10000000.0) {
                            if ($pph05) {
                                $upfront_payment = (float) $model->total_price_before_ppn - $pph22;
                                $total_price = ($dpp + $ppn) - $pph22;
                                $updatedTotalPrice = true;
                            } else {
                                $upfront_payment = $dpp;
                            }
                        }  elseif (($dpp + $ppn) > 10000000) {
                            $upfront_payment = $dpp;
                        } elseif (($dpp + $ppn) <= 10000000) {
                            $upfront_payment = $dpp + $ppn;
                        }

                        // throw new Exception("Disable For Wapu distributor");
                        $wapuInClassification = true;
                        
                    } elseif ($wapu && $wapu->tax_classification == 1 && $wapu->pph22 === "05") { 
                        $total_price = $dpp + $ppn;
                        if ($dpp > 10000000) {
                            $upfront_payment = (float) ($dpp + $ppn) - $pph22;
                        } else {
                            $upfront_payment = (float) $total_price;
                        }
                        $wapuInClassification = true;
                    }
                    
                    if ($wapuInClassification) {
                        if ((float) $request->get("total_price") !== (float) $total_price) {
                            throw new Exception("Invalid Total Price");
                        }

                        if ((float) $request->get("upfront_payment") !== (float) $upfront_payment) {
                            throw new Exception("Invalid Upfront Payment");
                        }
                        
                    } else if ( (float) @$model->total_price !== (float) $total_price || (float) @$model->total_price !== (float) $upfront_payment ){
                        throw new Exception("Invalid Total Price");
                    }
                    
                }  else {
                    if ( (float) @$model->ppn !== (float) $upfront_payment || (float) @$model->total_price !== (float) $total_price) {
                        throw new Exception("Invalid Upfront Payment Price");
                    }
                }

                if ($model->payment_method == PaymentGateway::DF) {
                    if($wapu && (in_array($wapu->tax_classification, [2,3]) || $wapu->tax_classification == 1 && $wapu->pph22 === "05")) {
                        throw new Exception("Disable For Wapu distributor");
                    }
                    $model->df_due_date = $this->getDFDueDate($model->only("customer_id", "sales_org_id"));
                    $model->save();

                    $checkLimit = $this->validateCheckLimit($model);
                    $expiredDate = $checkLimit ? $checkLimit->expired_date : null;

                    if (!$expiredDate) {
                        throw new Exception("Expired date is Null");
                    }

                    $dfDueDate = parseDate($model->df_due_date);
                    $expiredDate = parseDate(preg_replace("@[^0-9\-]@", "-", $expiredDate));
                    if ($dfDueDate > $expiredDate) {
                        throw new Exception("DF Due Date Melebihi Expired Date");
                    }
                }
                $this->createBooking($uuid);
                $updatedParams = [
                    'updated_by' => Auth::user()->id,
                    'submit_date' => Carbon::now(),
                    "upfront_payment" => $upfront_payment,
                ];

                if ($updatedTotalPrice) {
                    $updatedParams["total_price"] = $total_price;
                }

                $model->update($updatedParams);
            } elseif ($status == 'o') {
                if ($model->status != 'y' && $model->status != 's' && $model->status != 'r') {
                    $so = $this->getStatusSO($model->so_number);
                    if (!$so) {
                        DB::rollback();
                        $response = responseFail(trans('messages.update-fail'));
                        $response['errors'] = "Status SO DI SAP Belum CLOSE";
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                    $model->update(['status' => $status, 'updated_by' => Auth::user()->id]);
                }elseif($model->status=='y' || $model->status=='r' || $model->status == "s"){
                    $model->update(['status' => $status, 'updated_by' => Auth::user()->id]);
                }
            } else {
                $model->update(['status' => $status, 'updated_by' => Auth::user()->id]);
            }

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function hariToday(Request $request)
    {
        $model['today']=Carbon::now()->format('Y-m-d');
        $response = responseSuccess(trans('messages.update-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No. Penebusan","Kode Referensi","Nama Distributor","Nama Produsen","Tgl. Order","Status","Incoterm","Provinsi","Kabupaten","Tipe Pembayaran","Tipe Kredit","Total Harga","PPN","Jumlah Setelah Pajak","Uang Muka","Batas Akhir Pengambilan Barang","Dibuat Oleh","Dibuat Pada"];
        return Excel::download((new Download($data,$columns)), "Download Permintaan Penebusan.xls");
    }

    public function getPaymentDueDate($attributes) {
        $salesArea = CustomerSalesArea::getSalesAreaOrder($attributes['customer_id'], $attributes['sales_org_id']);
        $dateAwal = date('Y-m-d H:i:s');
        $dateAkhir = date('Y-m-d H:i:s');
        if ($salesArea->top_dp_uom == 'H') {
            $date = strtotime($dateAwal);
            $date = strtotime("+" . intval($salesArea->top_dp) . " hour", $date);
            $dateAkhir = date('Y-m-d H:i:s', $date);
        } else if ($salesArea->top_dp_uom == 'D') {
            $date = strtotime($dateAwal);
            $date = strtotime("+" . intval($salesArea->top_dp) . " day", $date);
            $dateAkhir = date('Y-m-d H:i:s', $date);
        }
        return $dateAkhir;
    }

    private function getGRDueDate($order) {
        $orderItem = OrderItem::where('order_id', $order->id)->sum('qty');

        $duration = LimitDate::where([
                    ['sales_org_id', '=', $order->sales_org_id],
                    ['min_qty', '<=', $orderItem],
                    ['max_qty', '>=', $orderItem]
                ])->first();

        $dateAwal = date('Y-m-d H:m:s');
        if ($duration) {
            $date = strtotime($dateAwal);
            $date = strtotime("+" . intval($duration->duration_date) . " day", $date);
            $dateAkhir = date('Y-m-d H:m:s', $date);
            return $dateAkhir;
        }
        return $dateAwal;
    }

    private function getDFDueDate($attributes) {
        $salesArea = CustomerSalesArea::getSalesAreaOrder($attributes['customer_id'], $attributes['sales_org_id']);
        $top = intval(substr($salesArea->term_of_payment, -2));
        $increment = $top == 0 ? 1 : $top;
        $dateInit = strtotime(date('Y-m-d H:m:s'));
        $date = strtotime("+" . $increment . " day", $dateInit);

        return date('Y-m-d', $date);
    }

    private function storePartnerFunction($attributes) {
        $attributes['ident_name_funct'] = "WE";
        $attributes['name_funct'] = "Ship to Party";
        OrderPartnerFunc::create($attributes);
        $tipe = [
            "AG" => "Sold to Party",
            "RE" => "Bill to Party",
            "RG" => "Payer"
        ];
        foreach ($tipe as $key => $value) {
            $attributes['partner_id'] = $attributes['customer_id'];
            $attributes['ident_name_funct'] = $key;
            $attributes['name_funct'] = $value;
            OrderPartnerFunc::create($attributes);
        }
    }

    private function updatePartnerFunction($attributes) {
        OrderPartnerFunc::where('order_id', $attributes['order_id'])->delete();
        $attributes['ident_name_funct'] = "WE";
        $attributes['name_funct'] = "Ship to Party";
        OrderPartnerFunc::create($attributes);
        $tipe = [
            "AG" => "Sold to Party",
            "RE" => "Bill to Party",
            "RG" => "Payer"
        ];
        foreach ($tipe as $key => $value) {
            $attributes['partner_id'] = $attributes['customer_id'];
            $attributes['ident_name_funct'] = $key;
            $attributes['name_funct'] = $value;
            OrderPartnerFunc::create($attributes);
        }
    }

    private function cekApprovalType($sales_org_id, $sales_group_id) {
        $cek = ApprovalSetting::where(['sales_org_id' => $sales_org_id, 'sales_group_id' => $sales_group_id, 'status' => 'y'])->first();

        if ($cek) {
            return 'm';
        } else {
            return 's';
        }
    }

    public function createBooking($order_uuid) {
        $data = $this->cekTypeApproveOrder($order_uuid);
        $paymentDueDate = $this->getPaymentDueDate(['customer_id' => $data->customer_id, 'sales_org_id' => $data->sales_org_id]);
        //$data->good_redemption_due_date = $this->getGRDueDate($data);
        $data->save();

        if ($data->delivery_method_id == 2) {
            $route = $this->cekRoute($data->id, $data->sales_group_id);
            if (!$route) {
                $data->update(['status' => 'r', 'updated_by' => Auth::user()->id]);
                $this->logOrder($data->id, 'Cek Route', 5, 'Route tidak tersedia');
                return;
            }
        }
        if ($data->approve_type == 's') {
            // dd("s",$data);
            $cekStock = $this->cekStok($data->id);
            $cekTaxClass = $this->cekTaxClass($data->customer_id, $data->sales_org_id);
            if ($cekTaxClass && $cekStock) {
                 $cekSsp = $this->cekSspDate($data->id, $data->customer_id, $data->sales_org_id);
                // Set cek SSP to True For Testing
//                $cekSsp = true;
                if ($cekSsp) {
                    $kode_booking = $this->generateKodeBooking($data->id);
                    $data->update([
                        'status' => 'y',
                        'booking_code' => $kode_booking,
                        'updated_by' => Auth::user()->id,
                        "payment_due_date" => $paymentDueDate,
                    ]);
                    $this->writeLogDF($data);
                } else {
                    $data->update(['status' => 'r', 'updated_by' => Auth::user()->id]);
                }
            } else if ($cekStock) {
                $kode_booking = $this->generateKodeBooking($data->id);
                $this->writeLogDF($data);
                $data->update([
                    'status'           => 'y',
                    'booking_code'     => $kode_booking,
                    'updated_by'       => Auth::user()->id,
                    "payment_due_date" => $paymentDueDate,
                ]);
            } else {
                $data->update(['status' => 'r', 'updated_by' => Auth::user()->id]);
            }
        } else {
            // dd("No S", $data);
            $data->update(['status' => 's', 'updated_by' => Auth::user()->id]);
        }
    }

    private function logOrder($id, $status, $kode, $message) {
        LogOrder::create([
            'order_id' => $id,
            'status' => $status,
            'kode' => $kode,
            'message' => $message
        ]);
    }

    private function cekTypeApproveOrder($order_uuid) {
        $model = $this->findDataUuid(Order::class, $order_uuid);

        return $model;
    }

    private function getQtyProduk($material_list_id, $plant_id) {
        $where = ['tb1.material_list_id' => $material_list_id, 'tb1.plant_id' => $plant_id,];
        $data = OrderItem::getItemApproved($where)->get();
        $qty = 0;
        foreach ($data as $value) {
            $qty += floatval($value->qty);
        }
        return $qty;
    }

    private function cekStok($order_id) {
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order_id])
            ->select(
                DB::raw("sum(qty) as qty"), "tb1.plant_id", "material_list_id",  "material_no",
                "tb3.code as plant_code", "tb1.product_id"
            )
            ->groupBy(["material_no", "tb3.code", "tb1.plant_id", "material_list_id", "tb1.product_id"])
            ->get();

        $approve = true;

        foreach ($orderItem as $value) {
            $otherQty = $this->getQtyProduk($value->material_list_id, $value->plant_id);
            $forCek = $value->qty + $otherQty;
            $param['R_PLANT'] = $value->plant_code;
            $param['R_MATERIAL'] = $value->material_no;
            $stok = $this->getStok(
                $value->plant_code,
                $value->material_no,
                getRLock($value->plant_code, $value->product_id),
                getUOM($value->product_id)
            );

            if ($stok < $forCek) {
                $approve = false;
            }
        }
        if (!$approve) {
            $this->logOrder($order_id, 'Cek Stok', 1, 'Stok tidak tersedia');
        }

        return $approve;
    }

    private function getStok($plant_code, $material_no, $rlock, $uom) {
        $soap = SAPConnect::connect('cekstok.xml');
        
    
        $param = [
            'R_MATERIAL' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $material_no]
            ],
            'R_PLANT' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $plant_code]
            ],
            'R_SLOC' => [
                'item' => ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => $rlock ]
            ],
            'P_DATUM' => date('Y-m-d'),
            'P_UNIT' => $uom,
            "RETURN" => [],
        ];
        $result = $soap->SI_CheckStock($param);
        $check = checkSAPResponse($result);

        if (!$check["status"]) return 0;

        if (key_exists('item', $result->T_DATA)) {
            return (float) $result->T_DATA->item->STOCK;
        }

        return 0;
    }

    private function generateKodeBooking($id) {
        return sprintf("8%09d", $id);
    }

    private function cekTaxClass($customer_id, $sales_org_id) {
        $data = CustomerSalesArea::getSalesAreaOrder($customer_id, $sales_org_id);

        if ($data->tax_classification == 2 || $data->tax_classification == 3) {
            return true;
        }

        return false;
    }

    private function cekSspDate($order_id, $customer_id, $sales_org_id) {
        $soap = SAPConnect::connect('cek_ssp_date.xml');

        $param = [
            'P_CUSTOMER' => $customer_id,
            'P_SALESORG' => $sales_org_id
        ];
        try {
            $result = $soap->SI_CheckSSPDate($param);
            if (!property_exists($result, "KODE")) throw new Exception();
        }catch (Exception $e) {
            return false;
        }

        $code = $result->KODE;
        if (is_null($code) || empty($code) || strtoupper($code) === "F") {
            return true;
        }

        $this->logOrder($order_id, 'Cek SSP Date', 2, 'SSP Date tidak tersedia');
        return false;
    }

    private function cekRoute($order_id, $sales_group_id) {
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order_id])->get();
        $sales_group = SalesGroup::find($sales_group_id);
        $approve = true;

        foreach ($orderItem as $value) {
            $route = $this->getRoute($value->plant_code, $sales_group->district_code, $value->retail_sales_unit_id);

            if ($route == 'E') {
                $approve = false;
            }
        }
        return $approve;
    }

    private function getRoute($plant_code, $sales_group_id, $sales_unit_id) {
        $soap = SAPConnect::connect('cek_route.xml');

        $param = [
            'R_ROUTE' => [
                'item' => [
                    'IDPLANT' => $plant_code,
                    'SGROUP' => $sales_group_id,
                    'SUNIT' => $sales_unit_id
                ]
            ]
        ];
        $result = $soap->SI_Check_Route($param);
        if (key_exists('item', $result->T_DATA)) {
            return $result->T_DATA->item->TYPE;
        }
        return 'E';
    }

    private function getStatusSO($so_number) {
        $params = [
            "R_SALESORDER" => [
                'item' => [
                    "LOW" => $so_number,
                    "SIGN" => "I",
                    "OPTION" => "EQ",
                ]
            ],
        ];

        $sap = SAPConnect::connect("check_status_so.xml");
        $res = $sap->SI_CheckStatusSO($params);

        if (array_key_exists('item', $res->T_DATA)) {
            if ($res->T_DATA->item->GBSTK != "Y6") {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    private function writeLogDF($order)
    {
        if (@$order->payment_method === PaymentGateway::DF) {
            $df = new OrderDFStatus();
            $df->order_id = $order->id;
            $df->is_sent = 0;
            $df->send_order_count = 0;
            $df->status = "s";
            $df->save();
        }
    }

    public function upload(Request $request)
    {
        $this->validate($request->all(),["file" => "required|file:xls,xlsx"]);

        $import = new SalesOrderImport;

        try {
            Excel::import($import, $request->file("file"));
        }
        catch(ValidationException $ve) {
            return $ve->getMessage();
        }
        $data = $import->getData();
        // return $data[0]["header"];
        $response = [];
        if ($import->getStatus()) {
            DB::beginTransaction();

            $insertStatus = true;
            $insertMessage = null;
            $inserteds = [];

            foreach($data as $row) {
                try {
                    $header = $row["header"];
                    $header["number"] = $this->generateCode(Order::class, 'number', 'ORD', 9);
                    $header["created_by"] = @$request->user()->id;
                    $header["updated_by"] = @$request->user()->id;
                    $order = Order::create($header);
                    $order->orderItems()->createMany($row["childs"]);
                    $inserteds[] = $header;
                }
                catch(Exception $e) {
                    $insertStatus = false;
                    $insertMessage = $e->getMessage();
                }
            }

            if ($insertStatus) {
                DB::commit();
                return responseSuccess(trans("messages.import-success"), $inserteds);
            } else {
                DB::rollback();
                return response()->json(responseFail($insertMessage), 500);
            }
        }

        $response = responseFail(trans("messages.import-fail"));
        $response["data"] = $data;
        return response()->json($response, 403);
    }

    public function changePlant(Request $request, $uuid)
    {
        $order = Order::where("uuid", $uuid)->first();

        if (!$order) return responseFail("invalid order");
        $items = [];

        $newPlants = collect($request->all())->groupBy("new_plant_id");
        $qtyPlants = $newPlants->map(function($item, $key){
            return $item->sum("qty");
        });

        foreach($request->all() as $req) {

            $validator = Validator::make($req, [
                "retail_id" => "required|exists:wcm_retail,id",
                "plant_id" => "required|exists:wcm_plant,id",
                "new_plant_id" => "required|exists:wcm_plant,id",
                "product_id" => "required|exists:wcm_product,id",
            ]);
            if ($validator->fails())  {
                $items["invalid"][] =[
                    "data" => $req,
                    "message" => $validator->errors(),
                ];
                continue;
            }
            $item = $order->orderItems()->where(Arr::only($req, "retail_id","plant_id","product_id"))->first();

            if (!$item || !$item->item_order_sap) {
                $items["invalid"][] =[
                    "data" => $req,
                    "message" => "Invalid item number."
                ];
                continue;
            }


            $plant = Plant::find($req["new_plant_id"]);

            $stok = $this->getStok($plant->code, $item->material->material_no, getRLock($plant->code, $item->product_id), getUOM($item->product_id));
            if ($stok < $qtyPlants->get($req["new_plant_id"])) {
                $items["invalid"][] =[
                    "data" => $req,
                    "message" => "stock not available."
                ];
                continue;
            }
            $item->plant = $plant;
            $items["valid"][] = $item;
        }

        if (@$items["invalid"]) {

            return response()->json([
                "status" => "error",
                "message" => "Something wrong!",
                "data" => $items["invalid"]
            ], 403);

        } else {
            $checkStatus = $this->checkStatusSO($order->so_number, $items["valid"]);
            if ($checkStatus !== false) {
                $itemValidStatus = collect($items["valid"])->whereIn("item_order_sap", $checkStatus)->all();
                $updateds = $this->doChangePlant($order->so_number, $itemValidStatus);

                if ($updateds !== false ) {
                    $response = [];
                    if (is_array($updateds)) {
                        foreach($updateds as $update) {
                            $item = collect($items["valid"])
                                ->where("item_order_sap", @$update->MESSAGE_V2)
                                ->first();
                            if (@$update->TYPE == "E") {
                                $response["error"][] = [
                                    "item_number" => @$update->MESSAGE_V2,
                                    "message" => @$update->MESSAGE,
                                    "item" => $item,
                                ];
                            }
                        }
                        if (isset($response["error"])) {
                            return response()->json([
                                "status" => "error",
                                "message" => "Something wrong !",
                                "data" => $response["error"]
                            ]);
                        }

                        foreach($itemValidStatus as $item) {
                            $item->plant_id = $item->plant->id;
                            unset($item->plant);
                            $item->save();
                        }

                        return responseSuccess("Plant has been processed successfully");
                    }
                    else if (is_object($updateds)) {
                        if (@$updateds->TYPE == "E") {
                            return response()->json([
                                "status" => "error",
                                "message" => "Something wrong !",
                                "data" => [
                                    "item_number" => @$updateds->MESSAGE_V2,
                                    "message" => @$updateds->MESSAGE,
                                ],
                            ]);
                        } else {
                            $item = OrderItem::where("item_order_sap",@$updateds->MESSAGE_V2)->first();
                            $item->save();
                            return responseSuccess(@$updateds->MESSAGE);
                        }
                    }
                }
            }
            return response()->json(responseFail("Invalid SO Status"), 500);
        }

        return response()->json(responseFail(""), 500);

    }

    private function doChangePlant($SONumber, $items)
    {
        if (count($items) == 0) return false;
        $soap = SAPConnect::connect('change_so.xml');
        $orderItems = [];
        foreach($items as $item) {
            $orderItems["IN"][] = [
                "ITM_NUMBER" => sprintf("%05s",$item->item_order_sap),
                "PLANT" => $item->plant->code,
            ];

            $orderItems["INX"][] =  [
                "ITM_NUMBER" => sprintf("%05s",$item->item_order_sap),
                "PLANT" => "X",
                "UPDATEFLAG" => "U"
            ];
        }

        $param = [
            "SALESDOCUMENT" => $SONumber,
            "ORDER_HEADER_INX" => [
                "UPDATEFLAG" => "U"
            ],
            "ORDER_ITEM_IN" => [
                "item" => $orderItems["IN"]
            ],
            "ORDER_ITEM_INX" => [
                "item" => $orderItems["INX"]
            ],
            "RETURN" => []

        ];


        try {
            $result = $soap->SI_ChangeSO($param);
        }catch(Exception $e) {
            return false;
        }

        if (property_exists($result, "RETURN") && array_key_exists("item", $result->RETURN)) {
            return $result->RETURN->item;
        }

        return false;
    }

    private function checkStatusSO($SONumber, $items)
    {
        $orderItems = [];

        if (count($items) == 0) return false;
        // return ["00010","00020"];
        foreach($items as $item) {
            $orderItems[] = [
                "LOW" => sprintf("%05s",$item->item_order_sap),
                "SIGN" => "I",
                "OPTION" => "EQ"
            ];
        }

        $param = [
            "LR_SALES_DOC" => [
                "item" => [
                    "SIGN" => "I",
                    "OPTION" => "EQ",
                    "LOW" => $SONumber,
                ]
            ],
            "LR_LINE_ITEM" => [
                "item" => $orderItems
            ]
        ];


        try {
            $sap = SAPConnect::connect("status_so.xml");
            $result = $sap->SI_Status_SalesDoc($param);
        }
        catch(Exception $e) {
            return false;
        }

        if (
            property_exists($result, "T_STATUS_ITEM") &&
            array_key_exists("item", $result->T_STATUS_ITEM)
        ) {
            $items = $result->T_STATUS_ITEM->item;
            $itemValids = [];
            if (is_array($items)) {
                foreach ($items as $item) {
                    if ($item->LFSTA == "A") {
                        $itemValids[] = $item->POSNR;
                    }
                }
            }
            else if (is_object($items)) {
                if ($items->LFSTA == "A") {
                    $itemValids[] = $items->POSNR;
                }
            }

            return $itemValids;
        }

        return false;

    }

    public function checkTaxAndPph22($customerId, $salesOrgId)
    {
        $area = SalesArea::select("id")->where("sales_org_id", $salesOrgId)->toSql();
        return CustomerSalesArea::selectRaw("tax_classification, pph22")
            ->whereRaw("sales_area_id in ({$area})")
            ->where("customer_id",$customerId)
            ->setBindings(compact("salesOrgId","customerId"))
            ->first();
    }

}