<?php

namespace App\Http\Controllers;

use App\Libraries\PaymentGateway;
use App\Models\MonitoringDF;
use App\Models\Order;
use App\Models\OrderDFStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Excel;
use App\Exports\Download;


class MonitoringDFController extends Controller
{
    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get all monitoring df');
        $where = [];
        // if ($this->isAdminAnper) {
        //     $where["tb1.sales_org_id"] = $this->salesOrgId;
        // }
        $data    = MonitoringDF::getAll($where,$exported);
        $user    = $request->user();
        $filters = @$user->filterRegional;

        if ($user->customer_id != "") {
            $data->where("tbl.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $data->whereIn("tbl.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $data->whereIn("tbl.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tbl.number'                     => 'no_penebusan',
            'tbl.reference_code'             => 'kode_referensi',
            'tbl.sales_org_name'             => 'produsen',
            'tbl.customer_name'              => 'distributor',
            'tbl.bank_name'                  => 'bank',
            'tbl.billing_fulldate'           => 'billing_fulldate',
            'tbl.df_due_date'                => 'df_due_date',
            'tbl.order_date'                 => 'order_date',
            'tbl.status'                     => 'status_order',
            'tbl.so_number'                  => 'kode_so',
            'tbl.total_price_before_ppn'     => 'total_price_before_ppn',
            'tbl.total_price'                => 'total_price',
            'tbl.sales_office_name'          => 'provinsi',
            'tbl.sales_group_name'           => 'kabupaten',
            'tbl.payment_method'             => 'tipe_pembayaran',
            'tbl.disbursement_status'        => 'disbursement_status',
            'tbl.disbursement_status_name'   => 'disbursement_status_name',
            'tbl.status_sent'                => 'status_sent',
            'tbl.created_at'                 => 'created_at',
            'tbl.updated_at'                 => 'updated_at',
            'tbl.error_message_disbursement' => 'error_message_disbursement',
            'tbl.send_order_count'           => 'send_order_count'
        ];
        $model = DataTables::of($data)
            ->filter(function ($data) use ($request, $columns) {
                $this->filterColumn($columns, $request, $data);
            })
            ->order(function ($data) use ($request, $columns) {
                $this->orderColumn($columns, $request, $data);
            });

        if ($exported) return $model->getFilteredQuery()->get();

        $model = $model->make(true);

            
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'tgl_order' || $value === 'tgl_pelunasan' || $value === 'tgl_rencana' || $value === 'billing_fulldate' || $value === 'disbursement_status') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status_order' || $value === 'status_sent') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function orderColumn($columns, $request, $query)
    {
        $order     = $request->get('order');
        $kolom     = $request->get('columns');
        $field     = $kolom[$order[0]['column']]['name'];
        $direction = $order[0]['dir'];
        foreach ($columns as $key => $value) {
            if ($field == $value) {
                $query->orderBy($key, $direction);
            }
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No Penebusan","Kode Referensi","Distributor","Produsen","Bank","Tgl. Pelunasan","Tgl. Rencana","Tgl. Order","Status","Status Disbursment","Error Message","Kode SO","Provinsi","Kabupaten","Tipe Pembayaran","Total Harga","Status Send","Error Message","Jumlah Percobaan"];
        return Excel::download((new Download($data,$columns)), "Download monitoring DF.xls");
    }

    public function changeStatus(Request $request, $uuid)
    {
        $rules = [
            "df_due_date" => "required|date|after:yesterday",
            "status" => "required|in:y,n"
        ];

        $this->validate($request->all(), $rules);
        $user = $request->user();
        if (!$user || $this->isDistributor) abort(401);

        $order = Order::where("uuid", $uuid)
            ->where("payment_method", PaymentGateway::DF)
            ->firstOrFail();

        $df = OrderDFStatus::where("order_id", $order->id)
            ->where("status", "n")
            ->firstOrFail();

        DB::beginTransaction();
        try {
            $order->df_due_date = parseDate($request->get("df_due_date"));
            $order->updated_by = @$user->id;
            $order->save();

            $df->status = $request->get("status");
            $df->updated_by = @$user->id;
            $df->save();
        }
        catch (\Exception $exception) {
            DB::rollback();
            return response()->json(responseFail($exception->getMessage()), 500);
        }

        DB::commit();
        return response()->json(responseSuccess(__("messages.update-success"),$request->all()));

    }

}
