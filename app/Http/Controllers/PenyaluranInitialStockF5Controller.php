<?php

namespace App\Http\Controllers;

use App\models\ContractItem;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\Product;
use App\Models\ProductLimit;
use App\Models\ReportF5;
use App\Models\Retail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class PenyaluranInitialStockF5Controller extends Controller
{

    public function index(Request $request, $uuid = null)
    {
        \LogActivity::addToLog('get header penyaluran initial stock f5');

        isset($uuid) ? is_uuid($uuid) : '';

        $where['tb1.distrib_resportable_type'] = 'InitialStockF5';
        if ($uuid) {
            $where['tb5.uuid'] = $uuid;
        }

        $query = DistribReports::getInitF5($where);
        $columns = [
            'tb1.number'       => 'number',
            'tb1.report_f5_id' => 'report_f5_id',
            'tb2.number'       => 'report_f5_number',
            'tb1.customer_id'  => 'customer_id',
            'tb3.full_name'    => 'customer_name',
            'tb1.month'        => 'month',
            'tb1.year'         => 'year',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb4.name'         => 'sales_org_name',
            'tb1.status'       => 'status',
            'tb1.created_by'   => 'created_by',
            'tb1.updated_by'   => 'updated_by',
            'tb1.created_at'   => 'created_at',
            'tb1.updated_at'   => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create penyaluran initial stock F5');

        $attributes = $request->only(['sales_org_id', 'initial_stock_id', 'customer_id', 'sales_group_id', 'distribution_date', 'month', 'year', 'status',]);

        $this->validate($attributes, DistribReports::ruleCreateDistribInitStockF5());
        $attributes['month'] = intval(Carbon::parse($attributes['distribution_date'])->format('m'));
        $attributes['year'] = Carbon::parse($attributes['distribution_date'])->format('Y');
        $report = $this->cekReportF5($attributes);
        $attributes['report_f5_id'] = $report->id;
        $attributes['number'] = $this->generateCode(DistribReports::class, 'number', 'PKP', 10);
        $attributes['distrib_resportable_type'] = 'InitialStockF5';
        $attributes['created_by'] = Auth::user()->id;
        $attributes['created_at'] = date('Y-m-d H:i:s');
       

        // return response()->json($attributes);

        DB::beginTransaction();
        try {
            $id = DistribReports::insertGetId($attributes);
            DB::commit();
            $distrib = DistribReports::find($id);
            $response = responseSuccess(trans('messages.create-success'), $distrib);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        }
        catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekReportF5($params)
    {
        $report = ReportF5::where([
            'sales_org_id'   => $params['sales_org_id'],
            'customer_id'    => $params['customer_id'],
            'sales_group_id' => $params['sales_group_id'],
            'month'          => (int)@$params['month'],
            'year'           => $params['year'],
        ])->first();
        if ($report) {
            if($report->status!='d'){
                $response = responseFail("Status F5 belum Draft");
                $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
                $return->throwResponse();
            }
            return $report;
        } else {
            $response = responseFail(trans('messages.read-fail'));
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function getItemPenyaluran($uuid)
    {
        \LogActivity::addToLog('get header penyaluran initial stock f5');

        isset($uuid) ? is_uuid($uuid) : '';

        if ($uuid) {
            $where['tb1.uuid'] = $uuid;
        }

        $distrib = DistribReports::getDistribInitF5Item($where)->first();
        // return response()->json($distrib);
        $reportf5 = ReportF5::where('id', $distrib->report_f5_id)->first();

        $arr['header'] = $this->getMainHeader($distrib);
        $arr['data']['header'] = $this->getSubHeader($distrib);

        if ($distrib->status == "d") {
            // Retail
            $where_r['c.id'] = $reportf5->sales_group_id;
            $where_r['f.id'] = $reportf5->customer_id;
            $where_r['g.id'] = $reportf5->sales_org_id;
            $last = date('Y-m-t', strtotime($reportf5->year . "-" . $reportf5->month));

            $spjb = DB::table('wcm_contract_item AS tb1')
            ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
            ->select('tb1.sales_unit_id')
            ->where('tb2.customer_id',$reportf5->customer_id)
            ->where('tb2.sales_org_id', $reportf5->sales_org_id)
            ->where('tb1.month', $reportf5->month)
            ->where('tb1.year', $reportf5->year)
            ->where('tb1.sales_group_id', $reportf5->sales_group_id)
            ->where('tb2.contract_type','asal')
            ->get()
            ->pluck('sales_unit_id')->unique();

            $retails = Retail::retailDitributionDO($where_r)
                ->whereIn('a.sales_unit_id', $spjb)
                ->whereRaw("e.created_at <= '" . $last . "'")
                ->whereIn("e.status",['y','p'])
                ->get();
                
        } else {
            $inRetails = DistribReportItems::where('wcm_distrib_report_item.distrib_report_id', $distrib->id)
                ->rightjoin('wcm_distrib_reports', function ($join) use ($reportf5) {
                    $join->on('wcm_distrib_reports.id', '=', 'wcm_distrib_report_item.distrib_report_id')
                        ->where('wcm_distrib_reports.distrib_resportable_type', 'InitialStockF5');
                })->pluck('retail_id')->unique();

            $retails = Retail::whereIn('wcm_retail.id', $inRetails)->where('sub_district_default', '!=', 1)
                ->leftjoin('wcm_sales_unit as wsu', 'wsu.id', '=', 'wcm_retail.sales_unit_id')
                ->select('wcm_retail.*', 'wsu.name as sales_unit_name')->get();
        }
        $kecamatan = $retails->pluck('sales_unit_id')->unique();
        $distribItem = DistribReportItems::DitribItems(['tb1.distrib_report_id' => $distrib->id])->get();
        $product = Product::all();
        $totalKecamatan = $this->getTotalQtyKecamatan($kecamatan, $product, $distribItem);

        $arr['data']['kecamatan'] = [];

        foreach ($kecamatan as $key => $value) {
            $retail = $retails->where('sales_unit_id', $value);
            $data['id'] = $value;
            $data['name'] = $retail->first()->sales_unit_name;
            $data['retail'] = [];
            foreach ($retail as $valRetail) {
                $dataRetail['id'] = $valRetail->id;
                $dataRetail['uuid'] = $valRetail->uuid;
                $dataRetail['code'] = $valRetail->code;
                $dataRetail['name'] = $valRetail->name;
                $dataRetail['product'] = [];
                foreach ($product as $valProduct) {
                    $dataProduct['id'] = $valProduct->id;
                    $dataProduct['name'] = $valProduct->name;
                    $dataProduct['sales_unit_id'] = $valRetail->sales_unit_id;
                    $dataProduct['qty'] = round($distribItem->where('retail_id',$valRetail->id)->where('product_id',$valProduct->id)->sum('qty'),3);
                    array_push($dataRetail['product'], $dataProduct);
                }

                array_push($data['retail'], $dataRetail);
            }
            $data['product'] = $totalKecamatan[ $value ]['product'];
            array_push($arr['data']['kecamatan'], $data);
        }

        $response = responseSuccess(trans('messages.read-success'), $arr);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    private function getMainHeader($arr)
    {
        $array = [
            'id'                => $arr->id,
            'uuid'              => $arr->uuid,
            'number'            => $arr->number,
            'report_f5_id'      => $arr->report_f5_id,
            'sales_org_id'      => $arr->sales_org_id,
            'sales_org_name'    => $arr->sales_org_name,
            'customer_id'       => $arr->customer_id,
            'customer_name'     => $arr->customer_name,
            'sales_group_id'    => $arr->sales_group_id,
            'sales_group_name'  => $arr->sales_group_name,
            'month'             => $arr->month,
            'year'              => $arr->year,
            'distribution_date' => $arr->distribution_date,
            'status'            => $arr->status,
            'status_name'       => $arr->status_name,
            'products_limit'    => ProductLimit::select('product_id', DB::raw("CAST(CONVERT(DECIMAL(10,3),qty) as nvarchar) as qty"))->get(),
        ];
        return $array;
    }

    private function getSubHeader($arr)
    {
        $product = Product::all();
        $ret = [];
        foreach ($product as $key => $value) {
            $qty = strtolower($value->id) . "_qty";
            $reduce = strtolower($value->id) . "_reduce";
            $qtyfloat = round(floatval($arr->$qty),3);
            $reducefloat = round(floatval($arr->$reduce),3);
            array_push($ret, [
                'id'      => $value->id,
                'name'    => $value->name,
                'qty'     => $qtyfloat,
                'reduce'  => $reducefloat,
                'balance' => round(($qtyfloat - $reducefloat),3),
            ]);
        }
        return $ret;
    }

    private function getTotalQtyKecamatan($kecamatan, $product, $item)
    {
        $dataKecamatan = [];
        foreach ($kecamatan as $valKecamatan) {
            $dataKecamatan[ $valKecamatan ]['product'] = [];
            foreach ($product as $valProduct) {
                $dataProduct['id'] = $valProduct->id;
                $dataProduct['name'] = $valProduct->name;
                $dataProduct['qty'] = round($item->where('sales_unit_id', $valKecamatan)->where('product_id',$valProduct->id)->sum('qty'),3);
                array_push($dataKecamatan[ $valKecamatan ]['product'], $dataProduct);
            }
        }
        return $dataKecamatan;
    }

    public function storeItem(Request $request)
    {
        \LogActivity::addToLog('create item penyaluran initial stock F5');

        DB::beginTransaction();
        $collect = collect($request->all());
        $first =$collect->first();

        $loopData = $collect->where('qty','!=', 0);
        
        DistribReportItems::where('distrib_report_id', $first['distrib_report_id'])->delete();
        try {
            $insert = [];
            foreach ($loopData as $iv) {

                $this->validate($iv, DistribReportItems::ruleCreate());

                if ($iv['status'] == 's' || $iv['status']=='d') {
                    $model_f5 = ReportF5::where('id', $iv['report_f5_id'])->first();
                    if ($model_f5->status != 'd') {
                        DB::rollback();
                        $response = responseFail('Status F5 harus Submited');
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
                
                $iv['created_by'] = Auth::user()->id;
                $iv['updated_by'] = Auth::user()->id;
                DistribReportItems::create($iv);
            }

            $model = DistribReports::where('id', $first['distrib_report_id'])->first();
            $model->update(['status' => $first['status']]);
            
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        }
        catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            // Because Only Unsubmit & draf
            "status" => 'required|in:d,s',
        ]);

        $distrib = $this->findDataUuid(DistribReports::class, $id);
        DB::beginTransaction();
        try {

            if (
                !in_array($request->get('status'), ['s', 'd']) ||
                $distrib->distrib_resportable_type != "InitialStockF5"
            ) {
                throw new \Exception("Invalid status");
            }

            $distrib->update(['status' => $request->get('status')]);
            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()
            ->json(
                responseSuccess(
                    trans("messages.update-success"),
                    $distrib
                )
            );
    }

    public function destroy($id)
    {

        is_uuid($id);

        $distrib = $this->findDataUuid(DistribReports::class, $id);
        DB::beginTransaction();
        try {

            if ($distrib->status !== 'd' || $distrib->distrib_resportable_type != "InitialStockF5") {
                throw new \Exception("Status initial penyaluran f5 harus draft dahulu");
            }

            DB::table('wcm_distrib_report_item')->where('distrib_report_id', '=', $distrib->id)->delete();
            $distrib->delete();
            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()
            ->json(
                responseSuccess(
                    trans("messages.deleted-success"),
                    $distrib
                )
            );
    }

}
