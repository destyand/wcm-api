<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use Illuminate\Http\Request;
use App\Models\Gudang;
use App\Models\Customer;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class GudangController extends Controller {

    public function index(Request $request, $exported = false) {
        \LogActivity::addToLog('get all gudang');
        
        $where = array();
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        // return response()->json($request->get('id_customerd'));

        if($request->has('id_customer'))
        {
            $where['tb1.customer_id'] = $request->get('id_customer');
        }

        $query = Gudang::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0 && !$this->isDistributor) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb5.sales_org_id", $filters["sales_org_id"]);
            }

            if($request->has('sub_distributor')!==true){
                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb8.id", $filters["sales_group_id"]);
                }
            }
        }
       
        $columns = [
            'tb1.customer_id' => 'customer_id',
            'tb1.sales_area_id' => 'sales_area_id',
            'tb5.sales_org_id' => 'sales_org_id',
            'tb1.term_of_payment' => 'term_of_payment',
            'tb1.top_dp' => 'top_dp',
            'tb1.tax_classification' => 'tax_classification',
            'tb1.pph22' => 'pph22',
            'tb1.top_up_uom' => 'top_up_uom',
            'tb4.uuid' => 'customer_uuid',
            'tb4.full_name' => 'customer_name',
            'tb4.owner' => 'owner',
            'tb6.address' => 'address',
            'tb6.tlp_no' => 'tlp_no',
            'tb6.fax_no' => 'fax_no',
            'tb7.name' => 'sales_unit_name',
            'tb8.name' => 'sales_group_name',
            'tb9.name' => 'sales_office_name',
            'tb8.id' => 'sales_group_id',
            'tb9.id' => 'sales_office_id',
            'tb3.status' => 'status',
            'tb3.created_by' => 'created_by',
            'tb3.updated_by' => 'updated_by',
            'tb3.created_at' => 'created_at',
            'tb3.updated_at' => 'updated_at',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if ($exported) return $model->getFilteredQuery();
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($id) {
        \LogActivity::addToLog('get gudang by id');

        $model = $this->findDataUuid(Customer::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function shipToParty(Request $request) {
        \LogActivity::addToLog('get gudang ship to party');

        $attrib = $request->only('customer_id', 'sales_org_id');

        $model = Gudang::getGudangShipToParty($attrib['sales_org_id'], $attrib['customer_id']);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function byDistributor($uuid) {
        \LogActivity::addToLog('get gudang by uuid distributor');

        isset($uuid) ? is_uuid($uuid) : '';
        
        $where['tb4.uuid'] = $uuid;
        $data = Gudang::getAll($where)->get()->toArray();
        if (count($data) > 0) {
            $response = responseSuccess(trans('messages.read-success'), $data);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } else {
            $response = responseFail(trans('messages.read-fail'));
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request, true);
        $data = $q->select(Gudang::getExportedColumns())->get();
        return Excel::download((new Download($data)), "gudang.xls");
    }

}
