<?php

namespace App\Http\Controllers\SAP;

use Illuminate\Http\Request;
use App\Models\Plant;
use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\SalesOrg;
use App\Models\PlantAssg;
use Illuminate\Support\Facades\Log;
use App\Models\DistribChannel;

class PlantController extends Controller {

    public function store() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('plant.xml');
            $result = $soap->SI_Plant(['P_MASTER' => 'X']);

            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_DATA->item as $key => $value) {
                if ($key == 0 || $value->ID_GROUP == '') {
                    continue;
                }
                $data = array();
                $data['code'] = $value->WERKS;
                //$data['name'] = $value->NAME1;
                $data['sales_group_id'] = substr($value->ID_GROUP, -4);
                $data['name'] = $value->NAME1;
                $data['thru_date'] = $value->TO_DATE;
                

                $so = Plant::where('code', $data['code'])->first();
                if (!$so) {
                    $data['status']='n';
                    $so = Plant::create($data);
                    $create++;
                } else {
                    unset($data["code"]);
                    $so->update($data);
                    $update++;
                }
                array_push($hasil, $so);
            }
            DB::commit();
            Log::info('SAP Sync plant master', [
                'status' => 'success',
                'sap' => count($result->T_DATA->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync plant master', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function storeAssign() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('plant.xml');
            $result = $soap->SI_Plant(['P_ASSIGNMENT' => 'X']);

            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_ASSIGNMENT->item as $key => $value) {
                if (!$this->cekRelasi($value->WERKS, $value->VKORG, $value->VTWEG)) {
                    continue;
                }
                $plant = Plant::where('code', $value->WERKS)->first();

                $data['plant_id'] = $plant->id;
                $data['sales_org_id'] = $value->VKORG;
                $data['distrib_channel_id'] = $value->VTWEG;

                $so = PlantAssg::updateOrCreate($data, $data);
                if ($so->wasRecentlyCreated) {
                    $create++;
                } else if ($so->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $so);
            }
            DB::commit();
            Log::info('SAP Sync plant assign', [
                'status' => 'success',
                'sap' => count($result->T_ASSIGNMENT->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync plant assign', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekRelasi($plant_id, $sales_org_id, $distrib_id) {
        $plant = Plant::where('code', $plant_id)->first();
        $sales_org = SalesOrg::find($sales_org_id);
        $distrib = DistribChannel::find($distrib_id);
        if (!$plant || !$sales_org || !$distrib) {
            return false;
        }
        return true;
    }

}
