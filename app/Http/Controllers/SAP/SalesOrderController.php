<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\CustomerSalesArea;
use App\Models\DeliveryMethod;
use App\Models\LogOrder;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderPartnerFunc;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SalesOrderController extends Controller
{

    public function store($orderBooking, $bank = null)
    {
        $param = $this->setParam($orderBooking);
        //dd($param);
        //return response()->json($param, 200, [], JSON_PRETTY_PRINT);
        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('create_so.xml');
            $result = $soap->SI_CreateSO($param);
            $check = checkSAPResponse($result);

            if ($check["status"]) {
                $order = Order::where('booking_code', $orderBooking)->firstOrFail();
                $so_release = Carbon::now();
                $incoterm = DeliveryMethod::find($order->delivery_method_id);
                // $order->orderitems->each(function($item) use (&$so_release, $order, $incoterm) {
                //     $qty = $item->qty;
                //     $limitDueDate = LimitDate::where("sales_org_id", $order->sales_org_id)
                //         ->where('sales_office_id', $order->sales_office_id)
                //         ->where('product_id', $item->product_id)
                //         ->where('incoterm', $incoterm->name)
                //         ->where('min_qty', '<=', $qty)
                //         ->where('max_qty', '>=', $qty)
                //         ->first();

                //     if (!$limitDueDate) {
                //         $limitDueDate = LimitDate::where("sales_org_id", $order->sales_org_id)
                //             ->where('sales_office_id', $order->sales_office_id)
                //             ->where('incoterm', $incoterm->name)
                //             ->where('min_qty', '<=', $qty)
                //             ->where('max_qty', '>=', $qty)
                //             ->first();
                //     }

                //     $so_release->addDays(@$limitDueDate->duration_date);
                // });

                // $order->good_redemption_due_date = $so_release;
                $order->so_number = $result->E_SALESDOCUMENT;
                $order->save();

                $param['SALES_ORDER_NUMBER'] = $result->E_SALESDOCUMENT;
                DB::commit();
                $this->createDP($order, $bank);
                \LogActivity::addToLog('SAP create sales order', 1, 'success');
                return true;
            } else {
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $check["data"];
                throw new \Exception(@$check["data"][0]["error_message"], 1);
            }
        }
        catch (\Exception $ex) {
            DB::rollback();
            \LogActivity::addToLog('SAP create sales order', 0, 'fail');
            $this->logOrder($orderBooking, 'Create SO', 3, $ex->getMessage(), null);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            throw new \Exception($ex->getMessage(), 1);
        }
    }

    private function logOrder($orderBooking, $status, $kode, $message, $items = null)
    {
        $order = Order::where('booking_code', $orderBooking)->first();
        DB::beginTransaction();
        try {
            $logOrder = LogOrder::create([
                'order_id' => $order->id,
                'status'   => $status,
                'kode'     => $kode,
                'message'  => $message,
            ]);
            if ($items) {
                if ($items instanceof Collection) {
                    $items = $items->toArray();
                }
                $logOrder->items()->createMany($items);
            }
            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
        }

    }

    private function setParam($kode)
    {
        $order = Order::getAll(['tb1.booking_code' => $kode])->first();
        $orderItem = OrderItem::getQtyItem(['tb1.order_id' => $order->id])->get();
        $salesArea = CustomerSalesArea::getSalesAreaOrder($order->customer_id, $order->sales_org_id);
        $orderHeaderParam = $this->setOrderHeader($order, $salesArea);
        $orderItemParam = $this->setOrderItem($orderItem, $order->delivery_method_ident, $order->sales_group_name);
        $partnerFunction = $this->setPartnerFunction($orderItem, $order->id);
        $orderTextParam = $this->setOrderText($orderItem, $order->desc);
        $pricingParam = $this->setPricing($order, $orderItem);
        $param = [
            "P_ORDER_HEADER_IN"     => $orderHeaderParam,
            "P_ORDER_HEADER_INX"    => [
                "UPDATEFLAG" => "I",
                "DOC_TYPE"   => "X",
                "SALES_ORG"  => "X",
                "DISTR_CHAN" => "X",
                "DIVISION"   => "X",
                "SALES_GRP"  => "X",
                "SALES_OFF"  => "X",
                "REQ_DATE_H" => "X",
                "PURCH_DATE" => "X",
                "NAME"       => "X",
                "INCOTERMS1" => "X",
                "INCOTERMS2" => "X",
                "PMNTTRMS"   => "X",
                "PRICE_DATE" => "X",
                "PURCH_NO_C" => "X",
                "DOC_DATE"   => "X",
                "PYMT_METH"  => "X",
                "PP_SEARCH"  => "X",
                "DUN_DATE"   => "X",
                "DLV_BLOCK"  => "X",
            ],
            "P_DISMODE"             => "N",
            "T_ORDER_ITEMS_IN"      => [
                "item" => $orderItemParam['IN'],
            ],
            "T_ORDER_ITEMS_INX"     => [
                "item" => $orderItemParam['INX'],
            ],
            "T_ORDER_PARTNERS"      => [
                "item" => $partnerFunction,
            ],
            "T_ORDER_SCHEDULES_IN"  => [
                "item" => $orderItemParam['SCH'],
            ],
            "T_ORDER_SCHEDULES_INX" => [
                "item" => $orderItemParam['SCHINX'],
            ],
            "T_ORDER_TEXT"          => [
                "item" => $orderTextParam,
            ],
            "T_ITEM_PRICING"        => [
                "item" => $pricingParam,
            ],
            "RETURN"                => [],
        ];

        return $param;
    }

    private function setOrderHeader($order, $salesArea)
    {
        $billingDate = $order->billing_date == "" ? date('Ymd') : str_replace('-', '', date('Ymd', strtotime($order->billing_date)));
        $param = [
            "DOC_TYPE"   => "Z3SU",
            "SALES_ORG"  => $order->sales_org_id,
            "DISTR_CHAN" => $salesArea->distrib_channel_id,
            "DIVISION"   => "00",
            "SALES_GRP"  => $order->district_code,
            "SALES_OFF"  => $order->sales_office_id,
            "REQ_DATE_H" => str_replace('-', '', date('Ymd', strtotime($order->order_date))),
            "PURCH_DATE" => $billingDate,
            "NAME"       => $order->contract_number,
            "INCOTERMS1" => $order->delivery_method_ident,
            "INCOTERMS2" => $order->sales_group_name,
            "PMNTTRMS"   => $salesArea->term_of_payment,
            "PRICE_DATE" => $billingDate,
            "PURCH_NO_C" => $order->number,
            "DOC_DATE"   => str_replace('-', '', date('Ymd', strtotime($order->order_date))),
            "PYMT_METH"  => strtolower($order->payment_method) == "cash" ? "E" : "D",
            "PP_SEARCH"  => "",
            "DUN_DATE"   => @$order->good_redemption_due_date ? parseDate($order->good_redemption_due_date)->format("Ymd") : "",
        ];

        return $param;
    }

    private function setOrderItem($orderItem, $delivMethod, $sales_group)
    {
        $params = [];
        $orderItem->groupBy("item_order_sap")->each(function ($item, $key) use (
            &$params, $delivMethod, $sales_group
        ) {
            
            $orderNumber = str_pad($key, 6, "0", STR_PAD_LEFT);
            $firstItem = $item->first();
            $data = [
                "ITM_NUMBER" => $orderNumber,
                "MATERIAL"   => $firstItem->material_no,
                "PLANT"      => $firstItem->plant_code,
                "STORE_LOC"  => getRLock($firstItem->plant_code, $firstItem->product_id),
                "TARGET_QTY" => round($item->sum("qty"), 2),
                "TARGET_QU"  => $this->setUOM($firstItem->product_id),
                "SALES_DIST" => $firstItem->retail_sales_unit_id,
                "INCOTERMS1" => $delivMethod,
                "INCOTERMS2" => $sales_group,
                "SALES_UNIT" => $this->setUOM($firstItem->product_id),
            ];

            $dataInx = [
                "UPDATEFLAG" => "I",
                "ITM_NUMBER" => $orderNumber,
                "MATERIAL"   => "X",
                "PLANT"      => "X",
                "STORE_LOC"  => "X",
                "TARGET_QTY" => "X",
                "TARGET_QU"  => "X",
                "SALES_DIST" => "X",
                "INCOTERMS1" => "X",
                "INCOTERMS2" => "X",
                "SALES_UNIT" => "X",
            ];

            $dataSchedules = [
                "ITM_NUMBER" => $orderNumber,
                "SCHED_LINE" => "0001",
                "REQ_QTY"    => round($item->sum("qty"), 2),
            ];
            $dataSchedulesInx = [
                "UPDATEFLAG" => "I",
                "ITM_NUMBER" => $orderNumber,
                "SCHED_LINE" => "0001",
                "REQ_QTY"    => "X",
            ];

            $params['IN'][] = $data;
            $params['INX'][] = $dataInx;
            $params['SCH'][] = $dataSchedules;
            $params['SCHINX'][] = $dataSchedulesInx;

        });

        return $params;
    }

    private function setOrderText($orderItem, $desc)
    {
        $dataText[0] = [
            "DOC_NUMBER" => '10',
            "ITM_NUMBER" => '000000',
            "TEXT_ID"    => 'Z016',
            "LANGU"      => "EN",
            "TEXT_LINE"  => $desc,
        ];
        // $number = 10;
        $i = 1;
        $orderItem->groupBy("item_order_sap")->each(function ($item, $key) use (&$i, &$dataText) {
            $orderNumber = str_pad($key, 6, "0", STR_PAD_LEFT);
            $data = [
                "DOC_NUMBER" => '',
                "ITM_NUMBER" => $orderNumber,
                "TEXT_ID"    => 'Z002',
                "LANGU"      => "EN",
                "TEXT_LINE"  => $item->implode("retail_name", ", "),
            ];
            $dataText[ $i ] = $data;
            // $number += 10;
            $i++;
        });

        return $dataText;
    }

    private function setPartnerFunction($orderItem, $orderID)
    {
        $partner = OrderPartnerFunc::where('order_id', $orderID)->get();

        $param = [];
        foreach ($partner as $value) {
            $data = [
                "ITM_NUMBER" => "000000",
                "PARTN_ROLE" => $value->ident_name_funct,
                "PARTN_NUMB" => $value->partner_id,
            ];
            array_push($param, $data);
        }

        // $number         = 10;
        $paramSalesUnit = [];
        $orderItem->groupBy("item_order_sap")->each(function ($item, $key) use (
            &$paramSalesUnit
        ) {
            $orderNumber = str_pad($key, 6, "0", STR_PAD_LEFT);
            $salesUnitId = $item->first()->retail_sales_unit_id;
            $data = [
                "ITM_NUMBER" => $orderNumber,
                "PARTN_ROLE" => "ZC",
                "PARTN_NUMB" => preg_match("/[a-zA-Z]/", $salesUnitId) > 0 ? sprintf("%010s", $salesUnitId) : $salesUnitId,
            ];

            array_push($paramSalesUnit, $data);
        });

        return array_merge($param, $paramSalesUnit);
    }

    private function setPricing($order, $orderItem)
    {
        $param = [];
        $i = 0;

        $tipeTax = $this->cekTaxClass($order->customer_id, $order->sales_org_id);
        $orderItem->groupBy("item_order_sap")->each(function ($item, $key) use (
            $order, $tipeTax, &$param, &$i
        ) {
            $firstItem = $item->first();
            $orderNumber = str_pad($key, 6, "0", STR_PAD_LEFT);
            $param[ $i ] = $this->paramPricing($orderNumber, 'ZHET', round($firstItem->zhet), 'IDR', round($firstItem->zhet * $item->sum("qty")), $firstItem);
            $param[ $i + 1 ] = $this->paramPricing($orderNumber, 'ZBKI', round($firstItem->zbki), 'IDR', round($firstItem->zbki * $item->sum("qty")), $firstItem);
            $param[ $i + 2 ] = $this->paramPricing($orderNumber, 'ZBDI', round($firstItem->zbdi), 'IDR', round($firstItem->zbdi * $item->sum("qty")), $firstItem);
            $param[ $i + 3 ] = $this->paramPricing($orderNumber, 'ZHTR', round($order->total_price_before_ppn), 'IDR', round($item->sum("total_price_before_ppn")), $firstItem);
            if ($tipeTax == 'wapu') {
                $wapu = $this->setWapuPricing($order->total_price_before_ppn, $order->ppn, $orderNumber, $firstItem->zhet, $order->ppn, $firstItem);
                $keyWapu = 4;
                foreach ($wapu as $valWapu) {
                    $param[ $i + $keyWapu ] = $valWapu;
                    $keyWapu++;
                }
            } else if ($tipeTax == 'nonwapu') {
                $wapu = $this->setNonWapuPricing($order->total_price_before_ppn, $order->ppn, $orderNumber, $order->ppn, $firstItem);
                $keyWapu = 4;
                foreach ($wapu as $valWapu) {
                    $param[ $i + $keyWapu ] = $valWapu;
                    $keyWapu++;
                }
            } else {
                $keyWapu = 4;
                $param[ $i + $keyWapu ] = $this->paramPricing($orderNumber, 'YMWS', round($order->ppn), 'IDR', round($item->sum("ppn")), $firstItem);
                $keyWapu++;
            }

            $i += $keyWapu;
        });

        return $param;
    }

    public function createDP($order, $bank_id)
    {
        $tipeTax = $this->cekTaxClass($order->customer_id, $order->sales_org_id);
        $bilVal = $order->total_price_before_ppn;
        if ($tipeTax == 'wapu') {
            $rulePricing = $this->cekRulePricing($order->total_price_before_ppn, $order->ppn);
            if ($rulePricing == 3) {
                $bilVal = $order->upfront_payment;
            }
        } elseif ($tipeTax === "nonwapu") {
            if ($order->total_price_before_ppn > 10000000) {
                $bilVal = $order->total_price_before_ppn - $order->pph22;
            }
        }

        $billingDate = $order->billing_date == "" ? date('Ymd') : str_replace('-', '', date('Ymd', strtotime($order->billing_date)));
        $param = [
            'P_SO'         => $order->so_number,
            'START_DATE'   => $billingDate,
            'BILLING_PLAN' => [
                'item' => ['BIL_DATE' => $billingDate, 'BIL_VALUE' => $bilVal],
            ],
            "RETURN"       => [],
        ];

        if ($order->payment_method == 'Distributor Financing') {
            $nilaiDF = $this->getDFCondPricing($order->ppn, $order->total_price_before_ppn);
            $param['BILLING_PLAN']['item']['BIL_VALUE'] = strval($nilaiDF);
            $param['COND_PRICING']['item']['COND_TYPE'] = 'YMWS';
            $param['COND_PRICING']['item']['COND_VALUE'] = strval($order->ppn - $nilaiDF);
        }

        $soap = SAPConnect::connect('create_dp.xml');
        $result = $soap->SI_DPRequest($param);
        $bankId = is_null($bank_id) ? $order->bank_payment : $bank_id;
        $bank = str_replace('Bank ', '', Bank::find($bankId)->name);
        $bank = strtolower($bank) == "bsb" ? "SUMSEL" : $bank;

        $check = checkSAPResponse($result);

        if ($check["status"]) {
            DB::beginTransaction();
            try {
                Order::find($order->id)->update(
                    [
                        'sap_booking_code'   => $result->DP_REQ,
                        'sap_billing_dp_doc' => $result->BILLING_NUMBER,
                    ]);
                DB::commit();
                $this->clearence($order->id, $result->BILLING_NUMBER, $bank, $order->sales_org_id);
            }
            catch (\Exception $e) {
                DB::rollback();
            }
        } else {
            $this->logOrder($order->booking_code, 'Create DP & Billing DP', 4, 'Gagal create dp request', $check["data"]);
        }
    }

    public function clearence($order_id, $billing, $bank, $sales_org)
    {
        $param = ['BILLING_NUMBER' => $billing, 'BANK_NAME' => $bank, 'COMPANY' => $sales_org, 'RETURN' => []];

        $soap = SAPConnect::connect('clearing.xml');
        $result = $soap->SI_ClearingDoc_Payment($param);
        $check = checkSAPResponse($result);
        $order = Order::find($order_id);
        if ($check["status"]) {

            $order->sap_clearing_doc = $result->CLEARING_NUMBER;
            $order->save();
        } else {
            $this->logOrder($order->booking_code, 'Create Clearing', 6, 'Gagal create clearing', $check["data"]);
        }
    }

    private function getDFCondPricing($ymws, $zhtr)
    {
        $hasil = $zhtr * ($ymws / ($zhtr + $ymws));

        return round($hasil);
    }

    private function cekTaxClass($customer_id, $sales_org_id)
    {
        $data = CustomerSalesArea::getSalesAreaOrder($customer_id, $sales_org_id);

        if ((($data->tax_classification == 2 || $data->tax_classification == 3) && $data->pph22 == '05') || ($data->tax_classification == 2 || $data->tax_classification == 3)) {
            return 'wapu';
        } else if ($data->tax_classification == 1 && $data->pph22 == '05') {
            return 'nonwapu';
        }

        return 'default';
    }

    private function paramPricing($number, $type, $amount, $currency, $value, $item)
    {
        $arr = [
            "COND_ITEM"      => $number,
            "COND_TYPE"      => $type,
            "COND_AMOUNT"    => strval($amount),
            "COND_CURRENCY"  => $currency,
            "COND_PRIC_UNIT" => "1",
            "COND_UOM"       => $this->setUOM($item->product_id),
            "COND_VALUE"     => strval($value),
        ];

        return $arr;
    }

    private function cekRulePricing($dpp, $ppn)
    {
        if ($dpp <= 10000000 && ($dpp + $ppn) >= 10000000) {
            return 1;
        } else if ($dpp <= 10000000 && ($dpp + $ppn) <= 10000000) {
            return 2;
        } else if ($dpp >= 10000000 && ($dpp + $ppn) >= 10000000) {
            return 3;
        }
    }

    private function setWapuPricing($dpp, $ppn, $orderNumber, $zhet, $orderPpn, $item)
    {
        $rulePricing = $this->cekRulePricing($dpp, $ppn);
        if ($rulePricing == 1) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', '0', '', '0', $item);
        } else if ($rulePricing == 2) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', strval($orderPpn), 'IDR', strval($ppn), $item);
        } else if ($rulePricing == 3) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', '0', '', '0', $item);
        }

        return $arr;
    }

    private function setNonWapuPricing($dpp, $ppn, $orderNumber, $orderPpn, $item)
    {
        $rulePricing = $this->cekRulePricing($dpp, $ppn);
        if ($rulePricing == 1) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', strval($orderPpn), 'IDR', strval($ppn), $item);
        } else if ($rulePricing == 2) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', strval($orderPpn), 'IDR', strval($ppn), $item);
        } else if ($rulePricing == 3) {
            $arr[] = $this->paramPricing($orderNumber, 'YMWS', strval($orderPpn), 'IDR', strval($ppn), $item);
        }

        return $arr;
    }

    private function setUOM($product_id)
    {
        return substr(getUOM($product_id), 0,2);
    }

}
