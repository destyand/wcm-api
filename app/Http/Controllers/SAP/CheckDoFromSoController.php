<?php

namespace App\Http\Controllers\SAP;

use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use App\Models\Delivery;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CheckDoFromSoController extends Controller
{
    /**
     * Verified Delivery Order From Sales Order Number
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verified(Request $request)
    {

        $this->validate($request->all(), [
            "so_number" => "required|string",
        ]);
        $params = [
            "R_VGBEL" => [
                "item" => [
                    "LOW"    => "",
                    'SIGN'   => 'I',
                    'OPTION' => 'EQ',
                ],
            ],
            "RETURN" => []
        ];

        $so_number = $request->input("so_number");
        $order = Order::where('so_number', $so_number)->first();

        if (!$order || $order->so_upload == 1) {
            return response()->json(responseFail("Order Is Invalid !"), 400);
        }

        Arr::set($params, "R_VGBEL.item.LOW", $so_number);

        try {
            $sap = SAPConnect::connect("check_do_from_so.xml");

            $results = $sap->SI_CheckDO($params);
            //var_dump($results);exit;
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        $response = collect($results->T_DATA)->get("item");
        if (is_array($response)) {

            $data = collect($response)->map(function ($row) {
                return [
                    "number"        => $row->VBELN,
                    "product_id"    => Str::limit($row->MATKL, 3, null),
                    "delivery_date" => $row->ERDAT,
                    "delivery_qty"  => $row->LFIMG,
                ];
            })->groupBy("number");
        }
        elseif (property_exists($results, "T_DATA") && property_exists($results->T_DATA, "item")) {
            $row = $results->T_DATA->item;

            $data =  collect([[
                "number"        => @$row->VBELN,
                "product_id"    => Str::limit(@$row->MATKL, 3, null),
                "delivery_date" => @$row->ERDAT,
                "delivery_qty"  => @$row->LFIMG,
            ]])
                ->groupBy("number");
        }
        else {
            return response()->json(responseFail(__("messages.read-fail")), 500);
        }

        $verified = $this->doVerified($so_number, $data);
        $deleted  = $this->doDeleteNonExistsDo($so_number,$data->keys()->toArray());


        if ($order && $order->validationQtySoAndDo()) {
            $order->update(['status' => 'c']);
        }

        return response()->json(
            responseSuccess(
                trans("messages.validate-check-success"),
                [
                    "inserted_or_updated" => $verified,
                    "deleting_data"       => $deleted,
                ]
            )
        );

    }

    /**
     * Do Verified Existing table wcm_delivery
     * with data from SAP
     *
     * @param Collection $collect  The collect
     *
     * @return int
     */
    private function doVerified($so_number, Collection $collect)
    {
        $order = Order::where("so_number", $so_number)->first();
        $verified = 0;
        foreach ($collect as $key => $item) {

            if ($order) {
                $data = Arr::only($item->first(), ["delivery_date"]);

                $header = ["order_id" => $order->id, "number" => Arr::get($item->first(), "number")];

                DB::beginTransaction();
                try {
                    $deliveryOrder = Delivery::updateOrCreate($header, $data);

                    $deliveryOrder->items()->delete();

                    $doItems = $item->map(function ($row) use (&$deliveryOrder) {
                        return Arr::only($row, ["product_id", "delivery_qty", "delivery_date"]);
                    });

                    $deliveryOrder->items()->createMany($doItems->toArray());;
                }
                catch (\Exception $e) {
                    DB::rollback();
                    continue;
                }
                DB::commit();
                $verified++;
            }
        }

        return $verified;
    }

    /**
     * Do Deleting from wcm_delivery
     * where not in SAP data
     *
     * @param      array    $numbers  The numbers
     *
     * @return     integer  ( description_of_the_return_value )
     */
    private function doDeleteNonExistsDo($so_number, array $numbers)
    {
        $orderId  = Order::where('so_number', "$so_number")->pluck('id')->first();
        $notInSap = Delivery::whereNotIn("number",
            collect($numbers)->map(function ($value) {
                return "{$value}";
            })
        )->where('order_id', $orderId);

        if ($notInSap->exists()) {
            return $notInSap->delete();
        }

        return 0;
    }
}
