<?php

namespace App\Http\Controllers\SAP;

use Illuminate\Http\Request;
use App\Models\SalesOffice;
use App\Helpers\SAPConnect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesOfficeController extends Controller {

    public function store() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('sales_office.xml');
            $result = $soap->SI_SalesOffice();

            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_DATA->item as $value) {
                if (!is_numeric(@$value->VKBUR)) {
                    continue;
                }

                $id['id'] = $value->VKBUR;
                $data['name'] = $value->VKBURX;

                $so = SalesOffice::updateOrCreate($id, $data);
                if ($so->wasRecentlyCreated) {
                    $create++;
                } else if ($so->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $so);
            }
            DB::commit();
            Log::info('SAP Sync sales office', [
                'status' => 'success',
                'sap' => count($result->T_DATA->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync sales office', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
