<?php

namespace App\Http\Controllers\SAP;

use App\Models\MaterialList;
use App\Http\Controllers\Controller;
use App\Helpers\SAPConnect;
use Illuminate\Http\Request;
use App\Models\Plant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MaterialListController extends Controller {

    public function store(Request $request) {

        $param = [
            'R_COMPANY' => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'B000'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'C000'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'D000'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'E000'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => 'F000'],
                ]
            ],
            'R_DISTRB_CHANNEL' => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '10'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '20'],
                ]
            ],
            'R_MATERIAL' => [
                'item' => [
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000036'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1001192'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000147'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000137'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000138'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000076'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000093'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000361'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000283'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1000057'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1001838'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1001865'],
                    ['SIGN' => 'I', 'OPTION' => 'EQ', 'LOW' => '1001751'],
                ]
            ]
        ];

        
        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('material_list.xml');
            $result = $soap->SI_Material_Product($param);

            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_DATA->item as $value) {
                $plant = Plant::where('code', $value->WERKS)->first();
                if (!$plant) {
                    continue;
                }
                $id['product_id'] = $value->CDPRD;
                $id['material_no'] = substr($value->MATNR, -7);
                $id['plant_id'] = $plant->id;
                $id['sales_org_id'] = $value->BUKRS;
                $data['mat_desc'] = $value->MAKTX;
                $data['unit'] = $value->MEINS;
                $data['distrib_channel_id'] = $value->VTWEG;
                $cust = MaterialList::updateOrCreate($id, $data);
                if ($cust->wasRecentlyCreated) {
                    $create++;
                } else if ($cust->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $cust);
            }
            DB::commit();
            Log::info('SAP Sync material list', [
                'status' => 'success',
                'sap' => count($result->T_DATA->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync material list', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
