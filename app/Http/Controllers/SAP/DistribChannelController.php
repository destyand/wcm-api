<?php

namespace App\Http\Controllers\SAP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DistribChannel;
use App\Helpers\SAPConnect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DistribChannelController extends Controller {

    //
    public function store() {

        DB::beginTransaction();
        try {
            $soap = SAPConnect::connect('distrib_channel.xml');
            $result = $soap->SI_Distribution_Channel();
            
            $create = 0;
            $update = 0;
            $hasil = [];
            foreach ($result->T_DATA->item as $value) {
                if($value->VTWEG != '00' && $value->VTWEG != '10' && $value->VTWEG != '20'){
                    continue;
                }
                $id['id'] = $value->VTWEG;
                $data['name'] = $value->VTEXT;
                $data['desc'] = $value->VTEXT;

                $dc = DistribChannel::updateOrCreate($id, $data);
                if ($dc->wasRecentlyCreated) {
                    $create++;
                } else if ($dc->wasChanged()) {
                    $update++;
                }
                array_push($hasil, $dc);
            }
            DB::commit();
            Log::info('SAP Sync distribution channel', [
                'status' => 'success',
                'sap' => count($result->T_DATA->item),
                'data' => count($hasil),
                'create' => $create,
                'update' => $update
            ]);
            $response = responseSuccess(trans('messages.create-success'), $hasil);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('SAP Sync distribution channel', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
