<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\DeliveryItem;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Exports\Download;
use Excel;

class MonitoringDOController extends Controller
{
    //
    public function index(Request $request,$exported=false)
    {
        \LogActivity::addToLog('get header perbup');
        $where = [];
        if ($this->isAdminAnper) {
            $where["tb2.sales_org_id"] = $this->salesOrgId;
        }

        $query   = Delivery::getHeader($where,$exported);

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb2.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if ( isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb2.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb2.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb2.number'        => 'number',
            'tb2.sales_org_id'  => 'sales_org_id',
            'tb4.name'          => 'sales_org_name',
            'tb2.customer_id'   => 'customer_id',
            'tb5.full_name'     => 'customer_name',
            'tb2.order_date'    => 'order_date',
            'tb9.qty'           => 'quantity_qty',
            'tb2.so_number'     => 'code_so',
            'tb1.number'        => 'no_doc',
            'tb3.delivery_id'   => 'delivery_id',
            'tb8.id'            => 'product_id',
            'tb8.name'          => 'product_name',
            'tb3.delivery_qty'  => 'delivery_qty',
            'tb1.delivery_date' => 'delivery_date',
            'tb1.created_by'    => 'created_by',
            'tb1.created_at'    => 'created_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });

        if ($exported) return $model->getFilteredQuery()->get();

        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Create Manual Monitoring DO
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     JSON
     */
    public function store(Request $request)
    {
        \LogActivity::addToLog("Creating Monitoring DO");

        $auth       = $request->header('Authorization');
        $dateCreate = strtotime("2019-04-04");
        $t2         = strtotime('+5 year', $dateCreate);
        $date5Year  = date("Y-m-d", $t2);

        if (($auth != 'YVO9J+feNZN8BIKo9O7OeW1GR/PMjwZ+zgG/50L/6yW4lW243bIdpaCOszRKTruZFam04pfOpP4SNuir7unQgNZmUIb7QaJq//6WTVNjpZm6kAfCAZvzo9l+IfDf0WYU') || ($date5Year < date("Y-m-d"))) {
            return response()->json(responseFail(trans("messages.create-fail")), 401);
        }

        $params = collect();

        foreach ($request->all() as $req) {
            if (!is_array($req)) {
                break;
            }

            $param = Arr::only($req,
                [
                    "number", "kode_so", "delivery_date",
                    "product_id", "delivery_qty",
                ]
            );

            $this->validate($param, Delivery::getRulesCreate());

            $params->prepend(collect($param));
        }

        $params = $params->groupBy("number")->toArray();
        //dd($params);
        try {
            $data = $this->doStore($params);
        } catch (\Exception $e) {
            return response()
                ->json(responseFail(trans("messages.create-fail")), 500);
        }

        return responseSuccess(trans("messages.create-success"), $data);
    }

    /**
     * Begin Storing Delivery & Delivery Items
     *
     * @param      array       $params  The parameters
     *
     * @throws     \Exception  (description)
     *
     * @return     array
     */
    private function doStore(array $params)
    {
        $orderIds = $inserted = $failed = [];

        foreach ($params as $key => $param) {
            try {

                $header = Arr::first($param);

                if (!Arr::exists($header, 'kode_so')) {
                    throw new \Exception("", 1);
                }

                // Find Order ID with SO Number
                $order    = Order::where("so_number", "{$header["kode_so"]}")->first();
                $id_order = $order ? @$order->id : 0;

                // Throw is Order not exists
                if (is_null($id_order)) {
                    throw new \Exception("ID Order : " . trans("messages.read-fail"), 1);
                }

                // Replacing SO Number with Order ID
                Arr::forget($header, "kode_so");
                $header = Arr::add($header, "order_id", $id_order);

                $delivery = $deliveryItems = null;

                // Begin Automatic Transaction
                DB::transaction(function () use ($header, $param, &$delivery, &$deliveryItems) {
                    $delivery = Delivery::updateOrCreate(Arr::except(
                        $header,
                        ["product_id", "delivery_qty"])
                    );

                    $deliveryItems = collect($param)->map(function ($items) use (&$delivery) {
                        return Arr::only($items, [
                            "product_id", "delivery_date", "delivery_qty", "created_by", "updated_by",
                        ]);

                    })->toArray();

                    return $delivery->items()->createMany($deliveryItems);
                });

                $inserted[] = [
                    'order_id' => $id_order,
                    'delvery'  => $delivery,
                ];

            } catch (\Exception $e) {
                $failed[$key] = $e->getMessage();

            }
        }

        foreach ($inserted as $key => $row) {
            $order = Order::find($row['order_id']);
            if ($order && $order->validationQtySoAndDo()) {
                $order->update(['status' => 'c']);
            } else {
                $orderIds[] = $row['order_id'];
            }
        }

        Order::whereIn('id', $orderIds)->update(['status' => 'k']);

        return response()->json(responseSuccess(trans('messages.create-success'), compact("inserted", "failed")));

    }

    /**
     * Reverse From APG
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     Illuminate\Http\Response the Response
     */
    public function reverse(Request $request)
    {

        $auth       = $request->header('Authorization');
        $dateCreate = strtotime("2019-04-04");
        $t2         = strtotime('+5 year', $dateCreate);
        $date5Year  = date("Y-m-d", $t2);

        if (($auth != 'YVO9J+feNZN8BIKo9O7OeW1GR/PMjwZ+zgG/50L/6yW4lW243bIdpaCOszRKTruZFam04pfOpP4SNuir7unQgNZmUIb7QaJq//6WTVNjpZm6kAfCAZvzo9l+IfDf0WYU') || ($date5Year < date("Y-m-d"))) {
            return response()->json(responseFail(trans("messages.create-fail")), 401);
        }

        $params = $request->only(['kode_so', 'number_do']);
        $rules  = [
            "kode_so"   => 'required',
            "number_do" => 'required',
        ];

        $this->validate($params, $rules);

        $order    = Order::where('so_number', "{$params['kode_so']}")->first();
        $response = responseFail(trans('messages.delete-fail'));

        if ($order) {
            $orderId = $order->id;
            DB::transaction(function () use (&$response, $orderId, $params) {
                $delivery = Delivery::where('order_id', $orderId)
                    ->where('number', $params['number_do']);
                $deliveryIds     = $delivery->pluck('id')->unique();
                $deletedItems    = DeliveryItem::whereIn('delivery_id', $deliveryIds)->delete();
                $deletedDelivery = Delivery::where('order_id', $orderId)->delete();

                if ($deletedDelivery) {
                    $response = responseSuccess(trans('messages.delete-success'), ['count' => $deletedDelivery]);
                }

            });
        }

        if ($order && $order->status == 'c') {
            $order->update(['status' => 'k']);
        }

        return response()->json($response);
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No Penembusan","Nama Produsen","Distributor","Kode Distributor","Kode SO","Tgl Order","Total Kuantitas","Nomor DO","Nama Produk","QTY","Tanggal DO","Dibuat Oleh","Dibuat Pada"];
        return Excel::download((new Download($data,$columns)), "Download Monitoring DO.xls");
    }

}
