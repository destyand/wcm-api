<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Imports\PricingImport;
use App\Models\DeliveryMethod;
use App\Models\PricingCondition;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Yajra\DataTables\DataTables;

class PricingController extends Controller
{

    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get pricing');
        $where = [];
        $query   = PricingCondition::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb1.product_desc'           => 'product_desc',
            'tb1.condition_type'         => 'condition_type',
            'tb1.calculate_type'         => 'calculate_type',
            'tb1.amount'                 => 'amount',
            'tb1.current'                => 'current',
            'tb1.uom'                    => 'uom',
            'tb1.per_uom'                => 'per_uom',
            'tb1.level'                  => 'level',
            'tb1.sap_tax_classification' => 'sap_tax_classification',
            'tb1.sales_org_id'           => 'sales_org_id',
            'tb2.name'                   => 'sales_org_name',
            'tb1.sales_group_id'         => 'sales_group_id',
            'tb3.name'                   => 'sales_group_name',
            'tb1.sales_unit_id'          => 'sales_unit_id',
            'tb4.name'                   => 'sales_unit_name',
            'tb1.product_id'             => 'product_id',
            'tb5.name'                   => 'product_name',
            'tb1.delivery_method_id'     => 'delivery_method_id',
            'tb6.name'                   => 'delivery_method_name',
            'tb1.sales_office_id'        => 'sales_office_id',
            'tb7.name'                   => 'sales_office_name',
            'tb1.sales_division_id'      => 'sales_division_id',
            'tb8.name'                   => 'sales_division_name',
            'tb1.status'                 => 'status',
            'tb1.valid_from'             => 'valid_from',
            'tb1.valid_to'               => 'valid_to',
            'tb1.created_by'             => 'created_by',
            'tb1.updated_by'             => 'updated_by',
            'tb1.created_at'             => 'created_at',
            'tb1.updated_at'             => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
            if ($exported) return $model->getFilteredQuery();
        $model= $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {
            if ($value === 'created_at' || $value === 'updated_at' || $value === 'valid_from' || $value === 'valid_to') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } else if ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function orderColumn($columns, $request, $query)
    {
        $order     = $request->get('order');
        $kolom     = $request->get('columns');
        $field     = $kolom[$order[0]['column']]['name'];
        $direction = $order[0]['dir'];
        foreach ($columns as $key => $value) {
            if ($field == $value) {
                $query->orderBy($key, $direction);
            }
        }
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create pricing');

        $attributes = $request->all();

        $this->validate($attributes, PricingCondition::ruleCreate(@$attributes['level']));

        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

//        $query = $this->cekDuplikasi($request, PricingCondition::class);

//        if (is_null($query)) {
        DB::beginTransaction();
        try {
            // if ($this->isAdminAnper && $this->salesOrgId != $attributes["sales_org_id"]) {
            //     throw new \Exception("Error Processing Request", 1);
            // }
            $duplicate = $this->cekDuplikasiUpload($attributes, PricingCondition::class, $attributes["level"]);
            $message   = trans('messages.update-success');
            if (!$duplicate) {
                PricingCondition::create($attributes);
                $message   = trans('messages.create-success');
            }

            DB::commit();
            $response = responseSuccess($message, $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
//        } else {
        //            $result = $this->cekValidDate($query, $attributes);
        //            return response()->json($result, 201, [], JSON_PRETTY_PRINT);
        //        }
    }

    public function show($uuid)
    {
        \LogActivity::addToLog('get pricing by id');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model             = PricingCondition::getAll($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        \LogActivity::addToLog('update pricing');

        $attributes               = $request->only(['amount', 'valid_from', 'valid_to', 'status', 'per_uom']);
        $attributes['updated_by'] = Auth::user()->id;

        $model = $this->findDataUuid(PricingCondition::class, $id);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update pricing bulk');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $iv) {
                is_uuid($iv['uuid']);

                $model = $this->findDataUuid(PricingCondition::class, $iv['uuid']);
                unset($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete pricing');

        $model = $this->findDataUuid(PricingCondition::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'n'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function duplicateRule()
    {
        $field = ['sales_org_id', 'sales_group_id', 'sales_unit_id', 'product_id', 'delivery_method_id', 'sales_office_id', 'sales_division_id', 'distrib_channel_id'];
        return $field;
    }

    private function keyRule($key)
    {
        $keyCombination = [
            1  => ['sales_org_id', 'sales_group_id', 'sales_unit_id', 'product_id', 'delivery_method_id'],
            2  => ['sales_org_id', 'sales_group_id', 'product_id', 'delivery_method_id'],
            3  => ['sales_org_id', 'sales_group_id', 'product_id'],
            4  => ['sales_org_id', 'sales_group_id'],
            5  => ['sales_org_id', 'sales_office_id', 'product_id', 'delivery_method_id'],
            6  => ['sales_org_id', 'sales_office_id', 'product_id'],
            7  => ['sales_org_id', 'sales_office_id'],
            8  => ['sales_org_id', 'sales_division_id'],
            9  => ['sales_org_id'],
            10 => ['sales_org_id', 'product_id', 'distrib_channel_id'],
        ];
        return $keyCombination[$key];
    }

    private function keyRuleUpload($key)
    {
        $keyCombination = [
            1  => ['sales_org_id' => 'sales_org', 'sales_group_id' => 'sales_group', 'sales_unit_id' => 'district', 'product_id' => 'material', 'delivery_method_id' => 'incoterm'],
            2  => ['sales_org_id' => 'sales_org', 'sales_group_id' => 'sales_group', 'product_id' => 'material', 'delivery_method_id' => 'incoterm'],
            10 => ['sales_org_id' => 'sales_org', 'product_id' => 'material', 'distrib_channel_id' => 'distribution_channel'],
        ];
        return $keyCombination[$key];
    }

    public function cekDuplikasi($request, $model)
    {
        $key = $request->level;

        $keyRule = $this->keyRule($key);
        $field   = $this->duplicateRule();

        $forCheck = [];
        foreach ($keyRule as $value) {
            $forCheck[$value] = $request->input($value);

            if (($key = array_search($value, $field)) !== true) {
                unset($field[$key]);
            }
        }

        $query = $model::where($forCheck);

        foreach ($field as $value) {
            $query->whereNull($value);
        }

        $check = $query
            ->orderBy('status', 'desc')
            ->orderBy('updated_at', 'desc')
            ->first();

        return $check;
    }

    private function cekValidDate($model, $attributes)
    {
        if ($attributes['valid_from'] <= date('Y-m-d') && $attributes['valid_to'] >= date('Y-m-d')) {
            DB::beginTransaction();
            try {
                if ($model->status == 'y') {
                    $model->status = 'n';
                    $model->save();
                }
                $pricing = PricingCondition::create($attributes);
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $pricing);
                return $response;
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
            }
        } else {
            $response = responseFail($this->errorDuplicate($attributes['level']));
            response()->json($response, 400, [], JSON_PRETTY_PRINT)->throwResponse();
        }
    }

    public function errorDuplicate($level)
    {
        $keyRule   = $this->keyRule($level);
        $duplikasi = [];
        foreach ($keyRule as $value) {
            $duplikasi[$value] = [trans('messages.duplicate')];
        }
        return $duplikasi;
    }

    public function errorDuplicateUpload($level)
    {
        $keyRule = $this->keyRuleUpload($level);
//        $duplikasi = [];
        //        foreach ($keyRule as $key => $value) {
        //            $duplikasi[$key] = [trans('messages.duplicate')];
        //        }
        $duplikasi['valid_from'] = [trans('messages.pricing-date')];
        $duplikasi['valid_to']   = [trans('messages.pricing-date')];
        return $duplikasi;
    }

    public function upload(Request $request)
    {
        $validAttrib = $request->all();
        $flag        = true;
        $this->validate($validAttrib, [
            'file'           => 'required|mimes:xls,xlsx',
            'level'          => 'required|integer',
            'condition_type' => 'required',
        ]);
        $level         = $validAttrib['level'];
        $conditionType = strtoupper(trim($validAttrib['condition_type']));
        if ($request->hasFile('file')) {

            $file       = $request->file('file');
            $attributes = Excel::toArray(new PricingImport(), $file)[0];
            $inserteds = $updateds = [];
            foreach ($attributes as $i => $iv) {
                
                if ($level == 2 && (isset($iv["district"]) || isset($iv["distrik"]))) {
                    $flag = false;
                    $attributes[$i]["validate"] = ["message" => "Invalid Template"];
                    break;
                }

                $attribNormal = $this->setValue($iv, $level);
                if (array_key_exists('delivery_method_id', $attribNormal)) {
                    $delivMethod                        = $this->getDelivMethod($attribNormal['delivery_method_id']);
                    $attribNormal['delivery_method_id'] = $delivMethod;
                    $iv['incoterm']                     = $delivMethod;
                }
                $attribNormal['level']          = $level;
                $attribNormal['condition_type'] = $conditionType;
                $attribNormal['status']         = 'y';
                $error                          = $this->validateImport($attribNormal, PricingCondition::ruleCreate($level));

                $isValidUOM = validateUOM($attribNormal["uom"], $attribNormal["product_id"]);

                $duplicate = false;
                if ($error['status'] == 0 || !$isValidUOM) {
                    $flag                     = false;
                    if (!$isValidUOM) {
                        $attribNormal['validate'] =  [
                            'status'     => 0,
                            'status_txt' => "errors",
                            "message" => [
                                "uom" => [
                                    sprintf("UOM: %s untuk produk %s tidak benar.", $attribNormal["uom"], $attribNormal["product_id"]),
                                ],
                            ],
                        ];
                    } else {
                        $attribNormal['validate'] = $error;
                    }
                }else {
                    $attribNormal['created_by'] = Auth::user()->id;
                    $attribNormal['updated_by'] = Auth::user()->id;
                    $attribNormal['created_at'] = now();
                    $attribNormal['updated_at'] = now();
                    $duplicate = $this->cekDuplikasiUpload($attribNormal, PricingCondition::class, $level);
                }

                if (!$duplicate && $flag) {
                    $inserteds[$i] = $attribNormal;
                } elseif ($duplicate && $flag) {
                    $updateds[$i] = $attribNormal;
                }

                $attributes[$i] = $attribNormal;
            }
            $perInsert = 100;
            $length = count($inserteds);

            $error = false;
            $errorMsg = "";
            
            if ($length > 0) {
                DB::beginTransaction();
                for ($i=0; $i<ceil($length/$perInsert); $i++) {
                    $slices = array_slice($inserteds, $i*$perInsert, $perInsert);
                    try {
                        PricingCondition::insert($slices);
                    } catch(Exception $e) {
                        $error = true;
                        $errorMsg = $e->getMessage();
                    }
                }
                
                if ($error) {
                    DB::rollback();
                    return response()->json(responseFail($errorMsg), 500)
                        ->throwResponse();
                    
                } else {
                    DB::commit();
                    return response()->json(responseSuccess("messages.import-success", $inserteds), 200);
                }
            } elseif (count($updateds) > 0) {
                return response()->json(responseSuccess("messages.update-success", $updateds), 200);
            } else {
                return response()->json(responseSuccess("messages.import-fail", $attributes), 400);
            }
        }
        
        $response = responseFail(trans('messages.import-fail'));
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'date'     => trans('messages.date'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function cekDuplikasiUpload($attrib, $model, $level)
    {

        $levels  = [
            1 => ['sales_org_id','sales_group_id','sales_unit_id','product_id','delivery_method_id'],
            2 => ['sales_org_id','sales_group_id','product_id','delivery_method_id'],
            10 => ['sales_org_id','product_id','distrib_channel_id'],
        ];

        if (!@$levels[$level]) return false;

        $valicationColumns = Arr::only($attrib, array_merge($levels[$level], ["valid_from","condition_type"]));
        $query = $model::where(array_merge($valicationColumns, compact('level')));

        if ($query->exists()) {
            $updateData = Arr::except($attrib, $levels[$level]);
            $query->update($updateData);
            return true;
        }

        return false;
    }

    public function setValue($attrib, $level)
    {
        $fieldFirst  = $this->keyRuleUpload($level);
        $fieldSecond = [
            'amount'     => 'amount',
            'uom'        => 'uom',
            'per_uom'    => 'per_uom',
            'valid_from' => 'valid_date',
            'valid_to'   => 'valid_to',
            'current'    => 'currency',
        ];

        $field = array_merge($fieldFirst, $fieldSecond);

        $getValue = [];
        foreach ($field as $key => $value) {
            $getValue[$key] = $this->cekKeyExist($value, $attrib);
        }

        return $getValue;
    }

    private function cekKeyExist($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        } else {
            return null;
        }
    }

    private function getDelivMethod($dm)
    {
        $modelDm = DeliveryMethod::where('name', $dm)->first();

        if ($modelDm) {
            return $modelDm->id;
        }

        return null;
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,true);
        $data = $q->select(PricingCondition::getExportedColumns())->get();
        return Excel::download((new Download($data)), "pricing.xls");
    }

}
