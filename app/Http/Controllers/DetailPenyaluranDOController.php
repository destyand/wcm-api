<?php

namespace App\Http\Controllers;

use App\Models\ContractGovItem;
use App\Models\ContractItem;
use App\Models\Delivery;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\OrderItem;
use App\Models\PlantingPeriod;
use App\Models\Product;
use App\Models\ProductLimit;
use App\Models\Rdkk;
use App\Models\ReportF5;
use App\Models\Retail;
use App\Models\SalesOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DetailPenyaluranDOController extends Controller
{
    //
    public function index($uuid)
    {
        ini_set("max_execution_time",180);
        # code...
        \LogActivity::addToLog('Detail Penyaluran Delivery Order');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $data['header']    = DistribReports::detailheader($where)->first();
        $order_date_for_spjb= date_create($data['header']->order_dates);

        if (!@$data["header"]) {
            return response()->json(responseFail(__("messages.read-fail")), 404);
        }

        $F5Arr                     = ReportF5::where('id', $data['header']->report_f5_id)->first();
        $data['header']->f5_status = $F5Arr->status;

        $date                              = explode('-', $data['header']->distribution_date);
        $month                             = $this->monthName($date[1]);
        $data['header']->distribution_date = $date[0] . '-' . $month . '-' . $date[2];
        $data['header']->periode           = $month . '-' . $date[2];

        $sales_org_Arr = SalesOrg::where('id', $data['header']->sales_org_id)->first();
        $data['header']->config_lintas_alur   = $sales_org_Arr->config_lintas_alur =="0" ? false : true;
        $data["header"]->products_limit = ProductLimit::select('id','product_id', DB::raw("CAST(CONVERT(DECIMAL(10,3),qty) as nvarchar) as qty"))->get();

        // Where Condition
        $wheres['tb8.contract_type']  = 'operational';
        $wheres['tb1.sales_group_id'] = $data['header']->sales_group_id;
        $wheres['tb8.sales_org_id']   = $data['header']->sales_org_id;
        $wheres['tb8.customer_id']    = $data['header']->customer_id;
        $wheres['tb1.month']          = intval($data['header']->month);
        $wheres['tb1.year']           = $data['header']->year;
        $model                        = Contractitem::getItemSPJB($wheres)->where('tb1.status', 'y')->get();
       
        // Find Uniq Kecamatan
        $sales_units = $model->pluck("sales_unit_id")->unique();
        // Find Produks
        $produks         = Product::all();
        $print['header'] = [];//$totals;
        $produks_keys    = $produks->pluck("id")->unique();

        if($data['header']->status=="s" || $data['header']->status=="y")
        {
            $inRetails= DistribReportItems::whereRaw("distrib_report_id = '{$data['header']->id}'")->selectRaw('distinct retail_id')->toSql();
            $retails= Retail::whereRaw("id in ({$inRetails})");//->get();
        }else{
            // Find Retail
            $where_r['c.id'] = $data['header']->sales_group_id;
            $where_r['f.id'] = $data['header']->customer_id;
            $where_r['g.id'] = $data['header']->sales_org_id;
            $last            = date('Y-m-t', strtotime($data['header']->year . "-" . $data['header']->month));
            $retails         = Retail::retailDitributionDO($where_r)->whereIn("e.status",['y','p']);
            $retails->whereRaw("e.created_at <= '" . $last . "'");
            $retails->whereRaw("a.sub_district_default = '0'");
            // $retails = $retails->get();
        }    

        if ($sales_org_Arr->status === "true" && ($data['header']->status !== "s" && $data['header']->status !== "y")) {
            $retails = $retails->orwhereRaw("sub_district_default = '1'")->whereIn('sales_unit_id', $sales_units);//->get();
        }
        $retails=$retails->orderBy('sub_district_default', 'asc')->get();
        
        // Find So_Items
        $where_so['tb2.so_number']      = $data['header']->so_number;
        $where_so['tb2.contract_id']    = $data['header']->contract_id;
        $where_so['tb2.sales_group_id'] = $data['header']->sales_group_id;
        $where_so['tb2.customer_id']    = $data['header']->customer_id;
        $where_so['tb2.sales_org_id']   = $data['header']->sales_org_id;
        $so_items                       = OrderItem::getForCalculationSO($where_so)->get();

        // FIND Delivery
        $where_delivery['tb2.order_id'] = $data['header']->order_id;
        // $where_delivery_date=$data['header']->order_dates;
        $last            = date('Y-m-t', strtotime($data['header']->year . "-" . $data['header']->month));
        $cal_do_items = Delivery::getForCalculationDO($where_delivery)
            ->whereRaw("tb2.delivery_date <= '{$last}' ")
            ->get();

        // Find DO Items
        $year = $data['header']->year ;$today = date('Y-m-d');
        $allreportdistrib= DistribReports::whereRaw("sales_org_id = '{$data['header']->sales_org_id}'")
                            ->whereRaw("customer_id = '{$data['header']->customer_id}'")
                            ->whereRaw("distribution_date <= '{$today}' ")
                            ->whereRaw("year = '{$year}' ")
                            ->whereRaw("distrib_resportable_type in ('order')")
                            ->selectRaw('distinct id')->toSql();

        $itemDistrib=DistribReportItems::whereRaw("distrib_report_id in ({$allreportdistrib})")
        ->whereRaw("(status = 'd' OR status = 's')")->get();

        $where_do['tb3.order_id']          = $data['header']->order_id;
        $do_items                          = DistribReportItems::getForCalculationDO($where_do)
            ->whereRaw("(tb1.status = 'd' OR tb1.status = 's')")
            ->get();
        // Planting Periode
        $periode = PlantingPeriod::whereRaw("[desc] = '{$data['header']->year}'")->select('id')->toSql();

        $rdkkitem = Rdkk::whereRaw("planting_period_id in ($periode)")
            ->leftjoin('wcm_rdkk_item AS tb2', 'tb2.rdkk_id', '=', 'wcm_rdkks.id')
            ->where('wcm_rdkks.customer_id', $data['header']->customer_id)
            ->whereIn('wcm_rdkks.sales_unit_id', $sales_units)
            ->where('tb2.status', 'y')
            ->select('wcm_rdkks.sales_unit_id', 'wcm_rdkks.retail_id', 'tb2.approved_qty', 'tb2.product_id')
            ->get();  
    
        $perbups = ContractGovItem::PerbupRetialItemPenyaluran($data['header']->sales_org_id,$data['header']->year)
            ->whereIn('sales_unit_id', $sales_units)
            ->get();     
        // Calculation Header "distribution": 6, "draft_distribution": 0,
        foreach ($produks as $keyH => $valH) {

            $productTSO = $so_items->where("product_id", $valH->id);
            $productTDO = $cal_do_items->where("product_id", $valH->id);
            $productH = $do_items->where("product_id", $valH->id);
            $productHD = $productH->where("status", 'd');
            $productHY = $productH->where("status", "s");

            $distrib_submited = round($productHY->sum("qty"),3);
            $distrib_draft    = round($productHD->sum("qty"),3);
            $total_do         = round($productTDO->sum("delivery_qty"),3);
            $total_so         = round($productTSO->sum("qty"),3);

            $arr['id']                 = $valH->id;
            $arr['name']               = $valH->name;
            $arr['total_so']           = $total_so;
            $arr['total_do']           = $total_do;
            $arr['distribution']       = $distrib_submited;
            $arr['draft_distribution'] = $distrib_draft;
            $arr['available']          = round($total_do - ($distrib_draft + $distrib_submited),3);
            array_push($print['header'],$arr);
        }
        // Print Data
        $print['data'] = [];
        /// Looping Sesuai Kecamatan
        foreach ($sales_units as $keyU => $val) {
            # code...
            $retailKec = $retails->where("sales_unit_id", $val);
            $spjb_kec_Arr = $model->where("sales_unit_id", $val)->first();

            // make Arr Kecamtan
            $kecArr['id']    = $spjb_kec_Arr->sales_unit_id;
            $kecArr['name']  = $spjb_kec_Arr->sales_unit_name;
            $kecArr['data']  = array();

            $retails_unit = $retailKec->pluck("id")->unique();
            // Retial In Kecamatan
            foreach ($retails_unit as $key => $value) {
                # code...
                $unitRetail = $retailKec->where("id", $value)->first();
                $retailArr = array(
                    'id'        => $unitRetail->id,
                    'code'      => $unitRetail->code,
                    'owner'     => $unitRetail->owner,
                    'name'      => $unitRetail->name,
                    'isVirtual' => $unitRetail->sub_district_default,
                    'data'      => array(),
                );
                // Loop Produks
                foreach ($produks_keys as $keyP => $valP) {
                    # code...
                    // Make Arr For SO Item Where Retail Kecamatan Ini / Filter retail On Kecamatan
                    $so_prod =  round($so_items->where("sales_unit_id", $val)->where("retail_id", $value)->where("product_id", $valP)->sum("qty"),3);

                    // Make Arr For SO Item Where Retail Kecamatan Ini / Filter retail On Kecamatan
                    $do_items_p_Arr = $do_items->where("sales_unit_id", $val)->where("retail_id", $value)->where("product_id", $valP)->where('distrib_report_id',$data['header']->id);
                    $do_prod        = round($do_items_p_Arr->sum("qty"),3);

                    $uuid_do = $do_items_p_Arr->count() > 0 ? $do_items_p_Arr->first()->uuid : "";
                    
                    $calc = array('sales_unit_id' => $kecArr['id'] ,
                        'distrib_report_id'           => $data['header']->id,
                        'report_f5_id'                => $data['header']->report_f5_id,
                        'retail_id'                   => $unitRetail->id,
                        'so'                          => $so_prod,
                        'uuid'                        => $uuid_do,
                        'do'                          => $do_prod,
                        'rddk'                        => 0,
                        'perbup'                      => 0,
                        'sisa_rdkk'                   => 0,
                        'sisa_perbub'                 => 0,
                        'id'                          => $valP,
                        'product_name'                => $keyP);

                    array_push($retailArr['data'], $calc);
                }
                array_push($kecArr['data'], $retailArr);
            }
            
            if (count($retailKec) != 0) {
                $kecArr['total'] = $this->makeTotalKec($rdkkitem,
                $produks,
                $perbups,
                $spjb_kec_Arr->sales_unit_id,
                $so_items,
                $do_items->whereIn('retail_id', $retails_unit)->where('month', $data['header']->month),
                $itemDistrib->whereIn('retail_id', $retails_unit), 
                $do_items->whereIn('retail_id', $retails_unit)->where('month', $data['header']->month)->where('distrib_report_id',$data['header']->id));
                array_push($print['data'], $kecArr);
            }

        }
        
        $out         = $data;
        $out['data'] = $print;

        $response = responseSuccess(trans('messages.read-success'), $out);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function newreport($uuid)
    {
        is_uuid($uuid);

        $where['tb1.uuid'] = $uuid;
        $report            = $this->findDataUuid(DistribReports::class, $uuid);

        $data['header']                    = DistribReports::detailheader($where)->first();
        $data['header']->f5_status         = $report->ReportF5->status;
        $data['header']->distribution_date = Carbon::parse($data['header']->distribution_date)->format("d-M-Y");

        $produks = Product::get();

        $sales_org_Arr = SalesOrg::where('id', $data['header']->sales_org_id)->first();

        // Where Condition
        $wheres['tb8.contract_type']  = 'operational';
        $wheres['tb1.contract_id']    = $data['header']->contract_id;
        $wheres['tb1.sales_group_id'] = $data['header']->sales_group_id;
        $wheres['tb8.sales_org_id']   = $data['header']->sales_org_id;
        $wheres['tb8.customer_id']    = $data['header']->customer_id;
        $wheres['tb1.month']          = intval($data['header']->month);
        $wheres['tb1.year']           = $data['header']->year;
        $spjb                         = Contractitem::getItemSPJB($wheres)->get();

        $sales_units = $spjb->pluck('sales_unit_id')->unique();

        $items = $report->distribItems;
        // $data['data']['header'] = $this->makeHeaderTotal(json_decode($produks));
        // Find Retail
        if($data['header']->status=="s" || $data['header']->status=="y")
        {
            $inRetails=DistribReportItems::where('distrib_report_id',$data['header']->id)->pluck('retail_id')->unique();
            $retails=json_decode(Retail::whereIn('id',$inRetails)->get()); 
        }else{
            // Find Retail
            $where_r['c.id'] = $data['header']->sales_group_id;
            $where_r['f.id'] = $data['header']->customer_id;
            $where_r['g.id'] = $data['header']->sales_org_id;
            $last            = date('Y-m-t', strtotime($data['header']->year . "-" . $data['header']->month));
            $retails         = Retail::retailDitributionDO($where_r)->where('e.status','y');
            $retails->whereRaw("e.created_at <= '" . $last . "'");
            $retails->whereRaw("a.sub_district_default = '0'");
            $retails = json_decode($retails->get());
        }
    

        if ($sales_org_Arr->status == "true" && ($data['header']->status!="s" && $data['header']->status!="y")) {
            $retailsDefault = Retail::whereRaw("sub_district_default = '1'")->whereIn('sales_unit_id', $sales_units)->get();
            foreach ($retailsDefault as $key => $value) {
                # code...
                array_push($retails, $value);
            }
        }

        // Find So_Items
        $where_so['tb2.so_number']      = $data['header']->so_number;
        $where_so['tb2.contract_id']    = $data['header']->contract_id;
        $where_so['tb2.sales_group_id'] = $data['header']->sales_group_id;
        $where_so['tb2.customer_id']    = $data['header']->customer_id;
        $where_so['tb2.sales_org_id']   = $data['header']->sales_org_id;
        $so_items                       = OrderItem::getForCalculationSO($where_so)->get();

        // FIND Delivery
        $last            = date('Y-m-t', strtotime($data['header']->year . "-" . $data['header']->month));
        $cal_do_items = Delivery::getForCalculationDO($where_delivery)
            ->whereRaw("tb2.delivery_date <= '{$last}' ")
            ->get();

        // Find DO Items
        $where_do['tb3.order_id'] = $data['header']->order_id;
        $do_itemss                = DistribReportItems::getForCalculationDO($where_do)
            ->whereRaw("(tb1.status = 'd' OR tb1.status = 's')")
            ->get();

        $where_do['tb3.order_id']          = $data['header']->order_id;
        $where_do['tb1.distrib_report_id'] = $data['header']->id;
        $do_items                          = DistribReportItems::getForCalculationDO($where_do)
            ->whereRaw("(tb1.status = 'd' OR tb1.status = 's')")
            ->get();

        // Planting Periode
        $periode = PlantingPeriod::where('desc', date('Y'))->get()->pluck('id');

        $rdkk = Rdkk::whereIn('planting_period_id', $periode)
            ->leftjoin('wcm_rdkk_item AS tb2', 'tb2.rdkk_id', '=', 'wcm_rdkks.id')
            ->where('wcm_rdkks.customer_id', $data['header']->customer_id)
            ->where('wcm_rdkks.sales_unit_id', $sales_units)
            ->where('tb2.status', 'y')
            ->select('wcm_rdkks.sales_unit_id', 'wcm_rdkks.retail_id', 'tb2.approved_qty')
            ->get();

        $perbups = ContractGovItem::PerbupRetialItemPenyaluran($data['header']->sales_org_id)
            ->whereIn('sales_unit_id', $sales_units)
            ->get();




        return $perbups;



    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('Create Penyaluran Delivery Order');
        $attributes = $request->all();

        $updateArr  = [];
        $insertArr  = [];
        $flag       = 0;
        $status     = '';
        $continuef5 = false;
        $collect=collect($attributes);

        $loopData = $collect->where('qty','!=', 0);
        $updateRemoveZero = $collect->where('qty',0)->where('uuid','!=','')->pluck('uuid');
        
        DB::beginTransaction();
        $delete = DistribReportItems::whereIn('uuid', $updateRemoveZero)->delete();
        $subRetails= Retail::whereIn('id',$loopData->pluck("retail_id")->unique())->where('sub_district_default',1)->get()->pluck('id');
        foreach ($loopData as $key => $val) {
            # code...
            if($val['qty']!==0){
                if($subRetails->contains($val['retail_id']))
                {
                    continue;
                }
                $this->validate($val, DistribReportItems::ruleCreate());
                // Cek Initial F5  Or Initial F6
                if ($continuef5 == false && $val['status'] == 'd') {
                    $wheref5['id'] = $val['report_f5_id'];
                    $f5            = ReportF5::where($wheref5)->first();

                    $distrib = DistribReports::where('id', $val['distrib_report_id'])->first();

                    $checkf5 = DB::select("SELECT * FROM (SELECT CONVERT(DATE, CONCAT([year],'-',[month],'-','01') ) as TGL FROM [dbo].[wcm_report_f5] where status='s' and sales_org_id='" . $f5->sales_org_id . "' and customer_id='" . $f5->customer_id . "' and sales_group_id='" . $f5->sales_group_id . "') AS X WHERE TGL >= '" . $distrib->year . "-" . $distrib->month . "-01' ");

                    if ($checkf5) {
                        $response             = responseFail(trans('messages.create-fail'));
                        $response['data']     = $checkf5;
                        $response['messages'] = 'Data ReportF5 yang dipakai Status Submited';
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }

                    $checkf6 = DB::select("SELECT * FROM (SELECT CONVERT(DATE, CONCAT([year],'-',[month],'-','01') ) as TGL FROM [dbo].[wcm_report_f6] where status='s' and report_f5_id='" . $f5->id . "'  and sales_org_id='" . $f5->sales_org_id . "' and customer_id='" . $f5->customer_id . "' and sales_group_id='" . $f5->sales_group_id . "') AS X WHERE TGL >= '" . $distrib->year . "-" . $distrib->month . "-01' ");

                    if ($checkf6) {
                        $response             = responseFail(trans('messages.create-fail'));
                        $response['data']     = $checkf6;
                        $response['messages'] = 'Data ReportF6 yang dipakai Status Submited';
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }

                    $continuef5 = true;
                }
                
                    if ($val["uuid"] == "" or is_null($val["uuid"]) ) {
                        unset($val['uuid']);
                        $val['created_by'] = Auth::user()->id;
                        $val['qty']=floatval($val['qty']);
                        array_push($insertArr, $val);
                    } elseif (is_null($val['uuid']) === false or $val["uuid"] != "") {
                        $val['qty']=floatval($val['qty']);
                        array_push($updateArr, $val);
                    }
            }
            $flag += 1;
        }
       

        try {
            $distrib_report_id="";$status="";
            foreach ($updateArr as $i => $iv) {
                is_uuid($iv['uuid']);
                $iv['updated_by']  = Auth::user()->id;
                $model             = $this->findDataUuid(DistribReportItems::class, $iv['uuid']);
                unset($iv['uuid']);
                if($distrib_report_id==""){
                    $distrib_report_id=$val['distrib_report_id'];
                    $status=$val['status'];
                }
                $model->update($iv);
            }
            foreach ($insertArr as $key => $value) {
                # code...
                $value['created_by'] = Auth::user()->id;
                if($distrib_report_id==""){
                    $distrib_report_id=$value['distrib_report_id'];
                    $status=$value['status'];
                }
                DistribReportItems::create($value);
            }           
            if ($flag != 0) {
                $where_u['id']              = $distrib_report_id;
                $model                      = $this->findDataWhere(DistribReports::class, $where_u);
                $updateStatus['updated_by'] = Auth::user()->id;
                $updateStatus['status']     = $status;
                $model->update($updateStatus);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'));
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    private function makeHeaderTotal($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'id'                 => $val->id,
                'name'               => $val->name,
                'total_so'           => 0,
                'total_do'           => 0,
                'distribution'       => 0,
                'draft_distribution' => 0,
                'available'          => 0,
            );
            $datas[$val->id] = $temps;
        }

        return $datas;
    }

    private function makeHeaderKec($produks,$perbups,$kec)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'so'          => 0,
                'do'          => 0,
                'id'          => $val->id,
                'product_name'=> $val->name,
                'rdkk'        => 0,
                'perbup'      => 0,
                'sisa_rdkk'   => 0,
                'sisa_perbub' => 0,
            );
            $datas[$val->id] = $temps;
        }

        return $datas;
    }

    private function makeTotalKec($rdkkitem,$produks,$perbups,$kec,$so_items,$do_items,$itemDistrib,$kecamatan)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $rdkk= $rdkkitem->where("sales_unit_id",$kec)->where('product_id',$val['id'])->sum("approved_qty");
            $perbup=round($perbups->where('sales_unit_id',$kec)->where('product_id',$val['id'])->sum('qty'),3);
            $so=round($so_items->where('sales_unit_id',$kec)->where('product_id',$val['id'])->sum('qty'),3);
            $do_lama=round($do_items->where('product_id',$val['id'])->sum('qty'),3);
            $do=round($kecamatan->where('product_id',$val['id'])->sum('qty'),3);
            $penyaluran=round($itemDistrib->where('product_id',$val['id'])->sum('qty'),3);
            $temps = array(
                'so'          => $so,
                'do'          => $do,
                'do_lama'     => $do_lama,
                'id'          => $val['id'],
                'product_name'=> $key,
                'rdkk'        => ($rdkk) == 0 ? "undefined" : ($val['id']=="P51" ? round($rdkk,3) : round(($rdkk/1000),3)),
                'perbup'      => $perbup,
                'sisa_rdkk'   => ($rdkk) == 0 ? "undefined" : ($val['id']=="P51" ? round($rdkk,3) : round((($rdkk/1000)-$penyaluran),3)),
                'sisa_perbub' => round(($perbup-$penyaluran),3),
            );
            array_push($datas, $temps);
        }

        return $datas;
    }

    private function templateTotalSales($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'so'          => 0,
                'do'          => 0,
                'id'          => $val['id'],
                'product_name'=> $val['product_name'],
                'rdkk'        => 0,
                'perbup'      => 0,
                'sisa_rdkk'   => 0,
                'sisa_perbub' => 0,
            );
            array_push($datas, $temps);
        }

        return $datas;
    }

    private function makeTotalHeader($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'id'                 => $val['id'],
                'name'               => $val['name'],
                'total_so'           => $val['total_so'],
                'total_do'           => $val['total_do'],
                'distribution'       => $val['distribution'],
                'draft_distribution' => $val['draft_distribution'],
                'available'          => $val['available'],
            );
            array_push($datas, $temps);
        }

        return $datas;
    }

}
