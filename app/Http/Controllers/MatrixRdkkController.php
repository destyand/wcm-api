<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Imports\MatrixRdkkImport;
use App\Models\MatrixRdkk;
use App\Models\Product;
use App\Rules\OwnedSalesOrg;
use App\Rules\SalesGroupRule;
use App\Traits\ValidationTemplateImport;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class MatrixRdkkController extends Controller
{
    use ValidationTemplateImport;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $exported=false)
    {
        //
        $columns = [
            'name'              => 'name',
            'sales_org_id'      => 'sales_org_id',
            'sales_org_name'    => 'sales_org_name',
            'sales_office_id'   => 'sales_office_id',
            'sales_office_name' => 'sales_office_name',
            'sales_group_id'    => 'sales_group_id',
            'sales_group_name'  => 'sales_group_name',
            'product_id'        => 'product_id',
            'product_name'      => 'product_name',
        ];
        $query = MatrixRdkk::datatable();
        $user = request()->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("sales_group_id",$filters["sales_group_id"]);
            }
        }

        $model =  DataTables::of($query)
            ->filter(function ($query) use ($columns) {
                $this->filterColumn($columns, request(), $query);
            })
            ->order(function ($query) use ($columns) {
                $this->orderColumn($columns, request(), $query);
            });
        if ($exported) return $model->getFilteredQuery();

        return $model->make();
        
    }

    /**
     * importing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        if ($request->has('file')) {
            $row      = Excel::toArray(new MatrixRdkkImport, $request->file('file'))[0];
            if(!$this->compareTemplate("matrix", array_keys($row[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }
            $inserted = $errors = [];
            $line     = 1;

            DB::beginTransaction();
            foreach ($row as $r) {
                $validation = Validator::make($r, [
                    'kode_produsen'  => ["required", new OwnedSalesOrg],
                    'kode_provinsi'  => "required|exists:wcm_sales_office,id",
                    'kode_kabupaten' => [
                        'required', new SalesGroupRule($r['kode_provinsi']),
                    ],
                    'kode_produk'    => 'required|exists:wcm_product,id',
                ]);

                if ($validation->fails()) {
                    $errors = [
                        "line"   => $line,
                        "errors" => $validation->errors(),
                    ];

                    break;
                }

                $params = [
                    "sales_org_id"    => $r['kode_produsen'],
                    "sales_office_id" => $r['kode_provinsi'],
                    "sales_group_id"  => $r['kode_kabupaten'],
                    "product_id"      => $r['kode_produk'],
                ];

                if (MatrixRdkk::where($params)->exists()) {
                    $errors = [
                        "line"   => $line,
                        "errors" => trans("messages.duplicate-error"),
                    ];

                    break;
                } else {
                    $matrix     = MatrixRdkk::create($params);
                    $inserted[] = $matrix;
                }

            }

            if (count($errors) > 0) {
                DB::rollback();
                return response()->json([
                    "status" => "error",
                    "message" => trans("messages.duplicate-error"),
                    "data" => $errors,
                ], 500);
            }

            DB::commit();
            return response()->json(
                responseSuccess(trans('messages.import-success'), $inserted)
            );

        }

        abort(400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request->all(), [
            "sales_org_id"    => "required|exists:wcm_sales_org,id",
            "sales_office_id" => "required|exists:wcm_sales_office,id",
            "sales_group_id"  => [
                "required",
                new SalesGroupRule($request->get("sales_office_id")),
            ],
            "products"      => "required|array",
        ]);

        $params = $request->only([
            "sales_office_id", "sales_group_id",
        ]);
        
        $inserts = [];
        $errors = null;
        DB::beginTransaction();
        try {
            $products = $request->get("products");
            foreach($products as $p) {
                if (!Product::find($p)) {
                    $errors = "Invalid Product ID";
                } 
                if (MatrixRdkk::where($params)->where('product_id',$p)->exists()) {
                    $errors = "RDKK Sudah Tersedia";
                }

                if ($errors) continue;
                $params["sales_org_id"]=$request->get("sales_org_id");
                $params["product_id"] = $p;
                $params["created_by"] = Auth::user()->id;
                $inserts[] = $params;
            }
            if ($errors) {
                throw new Exception($errors);
            }
            $matrix = MatrixRdkk::insert($inserts);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(responseFail($e->getMessage()), 500);
        }
        DB::commit();
        return response()->json(
            responseSuccess(trans('messages.create-success'), $matrix)
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matrix = MatrixRdkk::findOrFail($id);

        return response()->json(
            responseSuccess(trans("message.read-success"), $matrix)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request->all(), [
            "sales_org_id"    => "required|exists:wcm_sales_org,id",
            "sales_office_id" => "required|exists:wcm_sales_office,id",
            "sales_group_id"  => [
                "required",
                new SalesGroupRule($request->get("sales_office_id")),
            ],
            "product_id"      => "required|exists:wcm_product,id",
        ]);

        $params = $request->only([
            "sales_org_id", "sales_office_id", "sales_group_id", "product_id",
        ]);

        try {
            $matrixExists = MatrixRdkk::where($params)->where("id", "!=", $id)->exists();
            if ($matrixExists) {
                throw new \Exception(trans("message.duplicate-date"), 1);
            }

            $matrix = MatrixRdkk::findOrFail($id);
            $params["updated_by"] = Auth::user()->id;
            $matrix->update($params);
        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return response()->json(responseSuccess(trans("message.update-success"), $matrix));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $matrix = MatrixRdkk::findOrFail($id);

        try {
            $matrix->delete();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(
            responseSuccess(trans("message.delete-success"), $matrix)
        );
    }
    
    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,true);
        $data = $q->select(MatrixRdkk::getExportedColumns())->get();
        return Excel::download((new Download($data)), "matrixrdkk.xls");
    }
}
