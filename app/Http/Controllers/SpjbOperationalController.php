<?php

namespace App\Http\Controllers;

use App\Exports\ExportSPJBAsalHeader;
use App\Exports\SpjbOprExport;
use App\Imports\SpjbImport;
use App\Models\Contract;
use App\Models\ContractItem;
use App\Models\Order;
use App\Models\SalesGroup;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class SpjbOperationalController extends Controller
{
    //

    public function index(Request $request,$exported=false)
    {
        # code...
        \LogActivity::addToLog('Show Data Master Data table Header');

        $columns = [
            'tb1.id'          => 'id',
            'tb1.uuid'        => 'uuid',
            'tb1.year'        => 'year',
            'tb1.number'      => 'no_doc',
            'tb1.customer_id' => 'customer_id',
            'tb7.full_name'   => 'customer_name',
            'tb1.sales_org_id'=> 'sales_org_id',
            'tb3.name'        => 'sales_org_name',
            'tb1.status'      => 'status',
            'tb1.created_at'  => 'created_at',
            'tb1.updated_at'  => 'updated_at',
        ];

        $where['tb1.contract_type'] = 'operational';
        
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $query = Contract::getHeaderCoontract($where);
        $user = $request->user();
        $filters = $user->filterRegional;

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0 && !$this->isDistributor) {
                    $query->whereIn("tb8.sales_group_id",$filters["sales_group_id"]);
                }
            }
        }
        
        if ($request->has("select2")) {
            $query = Contract::select2()
                ->where($request->only("sales_org_id", "customer_id", "year"));
        }

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                if (!$request->has("select2")) {
                    $this->filterColumn($columns, $request, $query);
                }
            })
            ->order(function ($query) use ($request, $columns) {
                if (!$request->has("select2")) {
                    $this->orderColumn($columns, $request, $query);
                }
            });

        if ($exported) return $model->getFilteredQuery()->get();
        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function datatable(Request $request,$exported=false)
    {
        # code...
        \LogActivity::addToLog('Show Data Master Data table Header');

        $columns = [
            'tb1.id'          => 'id',
            'tb1.uuid'        => 'uuid',
            'tb1.year'        => 'year',
            'tb1.number'      => 'no_doc',
            'tb1.customer_id' => 'customer_id',
            'tb7.full_name'   => 'customer_name',
            'tb1.sales_org_id'=> 'sales_org_id',
            'tb3.name'        => 'sales_org_name',
            'tb1.status'      => 'status',
            'tb1.created_at'  => 'created_at',
            'tb1.updated_at'  => 'updated_at',
        ];

        $where['tb1.contract_type'] = 'operational';
        
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $query = Contract::getHeaderCoontractDatatable($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0 && !$this->isDistributor) {
                $query->whereIn("tb8.sales_group_id",$filters["sales_group_id"]);
            }
        }
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });

        if ($exported) return $model->getFilteredQuery()->get();
        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function dataheader(Request $request, $uuid)
    {
        # code...
        \LogActivity::addToLog('Get SPJB ITEM detail Header');
        is_uuid($uuid);
        $where['tb8.uuid'] = $uuid;

        $columns = [
            'tb1.contract_id'     => 'contract_id',
            'tb8.number'          => 'no_doc',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb5.name'            => 'sales_office_name',
            'tb1.sales_group_id'  => 'sales_group_id',
            'tb4.name'            => 'sales_group_name',
            'tb1.sales_unit_id'   => 'sales_unit_id',
            'tb6.name'            => 'sales_unit_name',
            'tb1.sales_org_id'    => 'sales_org_id',
            'tb3.name'            => 'sales_org_name',
            'tb8.customer_id'     => 'customer_id',
            'tb7.full_name'       => 'customer_name',
            'tb1.product_id'      => 'product_id',
            'tb2.name'            => 'product_name',
            'tb1.month'           => 'month',
            'tb1.initial_qty'     => 'initial_qty',
            'tb1.year'            => 'year',
            'tb1.status'          => 'status',
            'tb1.created_at'      => 'created_at',
        ];

        $query = Contractitem::getItemSPJB($where);
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function detailitem($uuid)
    {
        # code...
        \LogActivity::addToLog('GET Detail Item By UUID');
        is_uuid($uuid);

        $where['tb1.uuid'] = $uuid;
        $model             = Contractitem::getItemSPJB($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function update(Request $request, $uuid)
    {
        # code...
        \LogActivity::addToLog('Update SPJB Operational');
        is_uuid($uuid);

        $model = $this->findDataUuid(Contractitem::class, $uuid);

        if ($model->status == "n") {
            DB::rollback();
            $errors             = ['status' => "Status Item SPJB Operational Tidak Active"];
            $response           = responseFail($errors);
            $response['errors'] = 'Status Item SPJB Operational Tidak Active';
            $response['data']   = $model;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $modelOpr  = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
        $modelAsal = $this->findDataWhere(Contract::class, ['number' => $modelOpr->number, 'customer_id' => $modelOpr->customer_id, 'sales_org_id' => $modelOpr->sales_org_id, 'contract_type' => 'asal', 'year' => $modelOpr->year]);

        // return response()->json($modelOpr);

        // Data Untuk mencari Item yang Kembar
        $findItem["contract_id"]     = $modelOpr->id;
        $findItem["product_id"]      = $model["product_id"];
        $findItem["sales_office_id"] = $model["sales_office_id"];
        $findItem["sales_group_id"]  = $model["sales_group_id"];
        $findItem["sales_unit_id"]   = $model["sales_unit_id"];
        $findItem["month"]           = $model["month"];
        $findItem["year"]            = $model["year"];

        // $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);

        /*SUM Calc SPJB OPR TAHUN*/
        $qtyspjbOprYear          = $this->spjbOprYear($findItem) - $model->initial_qty;
        $findItem["contract_id"] = $modelAsal['id'];
        $qtyspjbAsalYear         = $this->spjbOprYear($findItem);

        $attributes = $request->all();

        if (($qtyspjbOprYear + $attributes["initial_qty"]) > $qtyspjbAsalYear) {
            DB::rollback();
            $error = [
                "initial_qty" => ["Limit Spjb ASAL " . $qtyspjbAsalYear],
            ];
            $response           = responseFail($error);
            $response['errors'] = 'Qty SPJB Operational : ' . ($qtyspjbOprYear + $attributes["initial_qty"]) . ' Melebihi SPJB Asal, Spjb Asal Tersedia : ' . $qtyspjbAsalYear;
            $response['data']   = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $this->validate($attributes, Contractitem::spjbContractCreateItems());

        // Cek Pasti Atributs;
        $ceks = $request->only(['contract_id', 'product_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id', 'month', 'year']);

        // Cek Duplicate
        $cekOnItem = $this->cekDuplikasiItemUpdate(Contractitem::class, $ceks, $uuid);

        // return response()->json($attributes);
        DB::beginTransaction();
        if ($cekOnItem) {
            DB::rollback();
            $error = [
                "sales_group_id" => [trans('messages.duplicate')],
                "sales_unit_id"  => [trans('messages.duplicate')],
                "month"          => [trans('messages.duplicate')],
                "year"           => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        // Request All Atributs
        $ceks['initial_qty'] = $attributes['initial_qty'];
        $ceks['updated_by']  = Auth::user()->id;

        // Validate Atrribute
        try {
            $model->update($ceks);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $ceks);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function updates(Request $request)
    {
        # code...
        \LogActivity::addToLog('Update Status Bulk SPJB Operational');
        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);

                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(Contractitem::class, $iv['id']);
                unset($iv['id']);

                $modelOpr  = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);
                $modelAsal = $this->findDataWhere(Contract::class, ['number' => $modelOpr->number, 'customer_id' => $modelOpr->customer_id, 'sales_org_id' => $modelOpr->sales_org_id, 'contract_type' => 'asal', 'year' => $modelOpr->year]);

                // Pengecualian Id Item

                // Data Untuk mencari Item yang Kembar
                $findItem["contract_id"]     = $modelAsal["id"];
                $findItem["product_id"]      = $model["product_id"];
                $findItem["sales_office_id"] = $model["sales_office_id"];
                $findItem["sales_group_id"]  = $model["sales_group_id"];
                $findItem["sales_unit_id"]   = $model["sales_unit_id"];
                $findItem["month"]           = $model["month"];
                $findItem["year"]            = $model["year"];

                // Mencari UUID Duplicate untuk update Yang data yang diduplicate
                $modelDuplicate = $this->findDataWhere(Contractitem::class, $findItem);

                if ($iv['status'] == 'y' && $modelDuplicate->status == 'n') {
                    DB::rollback();
                    $response           = responseFail('SPJB ASAL Status masih Inactive');
                    $response['errors'] = 'SPJB ASAL Status masih Inactive';
                    $response['data']   = $model;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                if ($iv['status'] == 'n') {
                    $modelDuplicate->update($iv);
                }

                $model->update($iv);
            }

            /* Update Header ke z */
            $num         = $this->getuniqStatusForHeader(Contractitem::class, $model->contract_id);
            $modelheader = Contract::where('id', $model->contract_id)->first();
            if ($num == 2) {
                $updateHeader['status']     = 'z';
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            } else {
                $updateHeader['status']     = $model->status;
                $updateHeader['updated_by'] = Auth::user()->id;
                $modelheader->update($updateHeader);
            }
            if ($iv['status'] == 'n') {
                $num             = $this->getuniqStatusForHeader(Contractitem::class, $modelDuplicate->contract_id);
                $modelheaderAsal = Contract::where('id', $modelDuplicate->contract_id)->first();
                if ($num == 2) {
                    $updateHeader['status']     = 'z';
                    $updateHeader['updated_by'] = Auth::user()->id;
                    $modelheaderAsal->update($updateHeader);
                } else {
                    $updateHeader['status']     = $model->status;
                    $updateHeader['updated_by'] = Auth::user()->id;
                    $modelheaderAsal->update($updateHeader);
                }
            }
            /*End*/
            /*Cek Partisial Active Status*/

            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function exports($uuid)
    {
        \LogActivity::addToLog('Export SPJB Operational');
        is_uuid($uuid);

        return (new SpjbOprExport($uuid))->download('spjboprdownloaditem.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function hirarki($uuid)
    {
        \LogActivity::addToLog('Get Hirarki Spjb ');

        is_uuid($uuid);

        // $where['tb8.uuid'] = $uuid;
        $where['tb8.contract_type'] = 'operational';
        $where['tb8.uuid']          = $uuid;

        $model = Contractitem::getItemSPJB($where)->get();
        $datas = json_decode($model);

        // Data From Order By Contract Id
        $order_where['tb2.contract_id'] = $datas[0]->contract_id;
        $orders                         = Order::getOrderDetailItem($order_where)
            ->where("tb2.so_upload", "!=", 1)
            ->get();
        $orders                         = json_decode($orders);

        // return response()->json($orders);

        $provinces = array_unique(array_column($datas, 'sales_office_id'));

        $uniq_produk_global = array_unique(array_column($datas, 'product_id'));

        $totals = [];

        foreach ($uniq_produk_global as $key => $value) {
            # code...
            $totals[$value]['id']             = $value;
            $totals[$value]['name']           = $datas[$key]->product_name;
            $totals[$value]['total_alokasi']  = 0;
            $totals[$value]['total_so']       = 0;
            $totals[$value]['total_approved'] = 0;
            $totals[$value]['total_sisa']     = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $totals[$value]['month'][$j]['alokasi']  = 0;
                $totals[$value]['month'][$j]['so']       = 0;
                $totals[$value]['month'][$j]['approved'] = 0;
                $totals[$value]['month'][$j]['sisa']     = 0;
            }
        }

        // Print Provinsi Arr
        $printArr = array('province' => array(), 'total' => array());

        foreach ($provinces as $iprov => $vprov) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_office_id'), $vprov);
            $datasprovArr = $this->makeArr($datas, $keyItemsProv);

            // Seacrh Kabupatens In provinsi
            $kabupatens = array_unique(array_column($datasprovArr, 'sales_group_id'));

            $nameProvArr = array('id' => $datas[$iprov]->sales_office_id, 'name' => $datas[$iprov]->sales_office_name, 'kabupaten' => array(), 'total' => array());

            // Total In Provinsi
            $produksProv = array_unique(array_column($datasprovArr, 'product_id'));
            $sumprovinsi = $this->gettotal($produksProv, $datasprovArr);

            // Mencari Jumlah Kabupaten
            foreach ($kabupatens as $ikab => $vkab) {
                # code...
                $keyItemsKab = array_keys(array_column($datasprovArr, 'sales_group_id'), $vkab);
                $dataskabArr = $this->makeArr($datasprovArr, $keyItemsKab);

                $kacamatans = array_unique(array_column($dataskabArr, 'sales_unit_id'));

                $printKabArr = array('id' => $datasprovArr[$ikab]->sales_group_id, 'name' => $datasprovArr[$ikab]->sales_group_name, 'kecamatan' => array(), 'total' => array());

                $produksKab   = array_unique(array_column($dataskabArr, 'product_id'));
                $sumkabupaten = $this->gettotal($produksKab, $dataskabArr);
                // Cari Kecamatan di kabupaten

                foreach ($kacamatans as $ikec => $vkec) {
                    # code...
                    // Orders Filter By Kecamatan
                    $keyItemsOrderKec = array_keys(array_column($orders, 'sales_unit_id'), $vkec);
                    $newOrderArr      = $this->makeArr($orders, $keyItemsOrderKec);

                    $orderFilterContractId = array_unique(array_column($newOrderArr, 'id'));
                    $keys                  = array_keys($orderFilterContractId);

                    $newOrderArrFilterId = $this->makeArr($newOrderArr, $keys);

                    // Make New Arr For Data Kecamatan
                    $keyItemsKec = array_keys(array_column($dataskabArr, 'sales_unit_id'), $vkec);
                    $dataskecArr = $this->makeArr($dataskabArr, $keyItemsKec);

                    // Cari produk Yang Dipunyai kecamatan
                    $produks = array_unique(array_column($dataskecArr, 'product_id'));

                    // create
                    $printKecArr = array('id' => $dataskabArr[$ikec]->sales_unit_id, 'name' => $dataskabArr[$ikec]->sales_unit_name, 'data' => array(), 'total' => array());

                    $sumkecamatan = $this->gettotal($produks, $dataskecArr);

                    foreach ($produks as $iprod => $vprod) {
                        # code...
                        // Filter Order SO, Aprroves
                        $keyOrders      = array_keys(array_column($newOrderArrFilterId, 'product_id'), $vprod);
                        $orderByProdArr = $this->makeArr($newOrderArrFilterId, $keyOrders);

                        // Filter Data Sesuai Dengan Produks
                        //
                        $keyItemsProd = array_keys(array_column($dataskecArr, 'product_id'), $vprod);
                        $dataspodArr  = $this->makeArr($dataskecArr, $keyItemsProd);

                        // Print Product Kabupaten Punya
                        $prodArr = array('id' => $dataskecArr[$iprod]->product_id, 'name' => $dataskecArr[$iprod]->product_name, 'month' => array(), 'status' => array(), 'total_alokasi' => 0, 'total_so' => 0, 'total_approved' => 0, 'total_sisa' => 0);

                        for ($i = 1; $i <= 12; $i++) {
                            # code...
                            // SO Order Approves
                            $monKeyItemss     = array_keys(array_column($orderByProdArr, 'order_date'), $i);
                            $datasOrdrProdArr = $this->makeArr($orderByProdArr, $monKeyItemss);

                            // Find Status Complete
                            $yKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'y');
                            $yArr  = $this->makeArr($datasOrdrProdArr, $yKeys);
                            // Find Status D
                            $sKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 's');
                            $sArr  = $this->makeArr($datasOrdrProdArr, $sKeys);

                            $dKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'd');
                            $dArr  = $this->makeArr($datasOrdrProdArr, $dKeys);

                            // Find Status Approve
                            $rKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'r');
                            $rArr  = $this->makeArr($datasOrdrProdArr, $rKeys);
                            // Find Status Approve
                            $iKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'l');
                            $iArr  = $this->makeArr($datasOrdrProdArr, $iKeys);
                            // Find Status Approve
                            $uKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'u');
                            $uArr  = $this->makeArr($datasOrdrProdArr, $uKeys);
                            // Find Status Approve
                            $cKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'c');
                            $cArr  = $this->makeArr($datasOrdrProdArr, $cKeys);

                            $kKeys = array_keys(array_column($datasOrdrProdArr, 'status'), 'k');
                            $kArr  = $this->makeArr($datasOrdrProdArr, $kKeys);

                            // Calculation Approve and Complate Sum
                            $approveSUM = array_sum(array_column($kArr, 'qty')) + array_sum(array_column($yArr, 'qty')) + array_sum(array_column($iArr, 'qty')) + array_sum(array_column($uArr, 'qty')) + array_sum(array_column($cArr, 'qty'));
                            $soSUM      = array_sum(array_column($rArr, 'qty')) + array_sum(array_column($dArr, 'qty')) + array_sum(array_column($sArr, 'qty'));

                            // SO
                            $monKeyItems             = array_keys(array_column($dataspodArr, 'month'), $i);
                            ($monKeyItems) ? $qty    = is_null($dataspodArr[$monKeyItems[0]]->initial_qty) ? 0 : $dataspodArr[$monKeyItems[0]]->initial_qty : $qty    = 0;
                            ($monKeyItems) ? $status = is_null($dataspodArr[$monKeyItems[0]]->status) ? "-" : $dataspodArr[$monKeyItems[0]]->status : $status = "";

                            array_push($prodArr['month'], array('alokasi' => $qty, 'approved' => $approveSUM, 'so' => $soSUM, 'sisa' => $qty - $approveSUM));
                            array_push($prodArr['status'], $status);
                            $prodArr['total_alokasi'] += $qty;
                            $prodArr['total_so'] += $soSUM;
                            $prodArr['total_approved'] += $approveSUM;
                            $prodArr['total_sisa'] += $qty-$approveSUM;

                            // Sum Kecamatai
                            $sumkecamatan[$vprod]['month'][$i]['alokasi'] += $qty;
                            $sumkecamatan[$vprod]['month'][$i]['so'] += $soSUM;
                            $sumkecamatan[$vprod]['month'][$i]['approved'] += $approveSUM;
                            $sumkecamatan[$vprod]['month'][$i]['sisa'] += $qty - $approveSUM;
                            $sumkecamatan[$vprod]['total_alokasi'] += $qty;
                            $sumkecamatan[$vprod]['total_so'] += $soSUM;
                            $sumkecamatan[$vprod]['total_approved'] += $approveSUM;
                            $sumkecamatan[$vprod]['total_sisa'] += $qty - $approveSUM;
                            // Sum Kabupaten
                            $sumkabupaten[$vprod]['month'][$i]['alokasi'] += $qty;
                            $sumkabupaten[$vprod]['month'][$i]['so'] += $soSUM;
                            $sumkabupaten[$vprod]['month'][$i]['approved'] += $approveSUM;
                            $sumkabupaten[$vprod]['month'][$i]['sisa'] += $qty - $approveSUM;
                            $sumkabupaten[$vprod]['total_alokasi'] += $qty;
                            $sumkabupaten[$vprod]['total_so'] += $soSUM;
                            $sumkabupaten[$vprod]['total_approved'] += $approveSUM;
                            $sumkabupaten[$vprod]['total_sisa'] += $qty - $approveSUM;
                            // Sum Provinsi
                            $sumprovinsi[$vprod]['month'][$i]['alokasi'] += $qty;
                            $sumprovinsi[$vprod]['month'][$i]['so'] += $soSUM;
                            $sumprovinsi[$vprod]['month'][$i]['approved'] += $approveSUM;
                            $sumprovinsi[$vprod]['month'][$i]['sisa'] += $qty - $approveSUM;
                            $sumprovinsi[$vprod]['total_alokasi'] += $qty;
                            $sumprovinsi[$vprod]['total_so'] += $soSUM;
                            $sumprovinsi[$vprod]['total_approved'] += $approveSUM;
                            $sumprovinsi[$vprod]['total_sisa'] += $qty - $approveSUM;
                            // $totals[$vprod]['month'][$i]+=$qty;
                            // Qty Total
                            $totals[$vprod]['month'][$i]['alokasi'] += $qty;
                            $totals[$vprod]['month'][$i]['so']=$soSUM;
                            $totals[$vprod]['month'][$i]['approved'] += $approveSUM;
                            $totals[$vprod]['month'][$i]['sisa'] += $qty - $approveSUM;
                            $totals[$vprod]['total_alokasi'] += $qty;
                            $totals[$vprod]['total_so'] += $soSUM;
                            $totals[$vprod]['total_approved'] += $approveSUM;
                            $totals[$vprod]['total_sisa'] += $qty - $approveSUM;
                        }

                        array_push($printKecArr['data'], $prodArr);
                        // return response()->json($printKecArr['data']);

                    }

                    // Mengeluarkan perhitungan Total kecamatan
                    $sumkecamatan         = $this->pushtotal($sumkecamatan);
                    $printKecArr['total'] = $sumkecamatan;

                    array_push($printKabArr['kecamatan'], $printKecArr);

                }

                $sumkabupaten         = $this->pushtotal($sumkabupaten);
                $printKabArr['total'] = $sumkabupaten;

                array_push($nameProvArr['kabupaten'], $printKabArr);

            }

            $sumprovinsi          = $this->pushtotal($sumprovinsi);
            $nameProvArr['total'] = $sumprovinsi;

            array_push($printArr['province'], $nameProvArr);

        }
        // Mengeluarkan perhitungan Total kecamatan
        $sumprovinsi                = $this->pushtotal($totals);
        $printArr['total']          = $sumprovinsi;
        $printArr['no_spjb']        = $datas[0]->no_doc;
        $printArr['sales_org_id']   = $datas[0]->sales_org_id;
        $printArr['sales_org_name'] = $datas[0]->sales_org_name;
        $printArr['customer_id']    = $datas[0]->customer_id;
        $printArr['customer_name']  = $datas[0]->customer_name;
        $printArr['year']           = $datas[0]->year;

        // array_push($printKabArr['data'],$printKecArr);

        $response = responseDatatableSuccess(trans('messages.read-success'), $printArr);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function getImport(Request $request)
    {
        # code...
        \LogActivity::addToLog('Import Excel SPJB Operational');

        $attributes = $request->all();

        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file  = $request->file('file');
            $datas = Excel::toArray(new SpjbImport, $file)[0];

            // Cek template
            $cekKeys = array_keys(@$datas[0]);
            if($cekKeys[0]!="sales_organization" or $cekKeys[1]!="no_doc" or $cekKeys[2]!="kode_distributor" or $cekKeys[3]!="kode_provinsi" or $cekKeys[4]!="kode_kabupaten" or $cekKeys[5]!="kode_kecamatan"  or  $cekKeys[6]!="tahun"  or $cekKeys[7]!="bulan" or $cekKeys[8]!="kode_jenis_produk" or $cekKeys[9]!="amount_alokasi"){
                $response           = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template'=>['Format template Tidak Sesuai'],'file'=>['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            // Loop Data Excell
            $return = [];
            foreach ($datas as $key => $val) {
                # code...

                /*Month Tidak Sesuai*/
                if (intval($val['bulan']) < 1 || intval($val['bulan']) > 12) {
                    DB::rollback();
                    $response           = responseFail(trans('messages.update-fail'));
                    $response['errors'] = 'Hitungan Bulan Tidak Sesuai. baris ke-' . (intval($key) + 2);
                    $response['data']   = $val;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                // $sumExcel = collect($datas)->where('no_doc', $val['no_doc'])
                //     ->where('sales_organization', $val['sales_organization'])
                //     ->where('kode_distributor', $val['kode_distributor'])
                //     ->where('kode_jenis_produk', $val['kode_jenis_produk'])
                //     ->where('kode_provinsi', $val['kode_provinsi'])
                //     ->where('kode_kabupaten', $val['kode_kabupaten'])
                //     ->where('kode_kecamatan', $val['kode_kecamatan'])
                //     ->whereIn('bulan', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
                //     ->where('tahun', $val['tahun'])
                //     ->sum('amount_alokasi');

                $val['kode_provinsi']  = $this->cekSalesOfficeCode($val['kode_provinsi']);
                $val['kode_kabupaten'] = $this->cekGroupCode($val['kode_kabupaten']);

                $dataValidate = [
                    'number'                  => $val['no_doc'],
                    'sales_org_id'            => $val['sales_organization'],
                    'customer_id'             => $val['kode_distributor'],
                    'year'                    => $val['tahun'],
                    'rayonisasi'              => [
                        'sales_org_id'    => $val['sales_organization'],
                        'sales_office_id' => $val['kode_provinsi'],
                        'sales_group_id'  => $val['kode_kabupaten'],
                        'sales_unit_id'   => $val['kode_kecamatan'],
                    ],
                    'distributor_salesunit'   => $val,
                    'data_header_operational' => [
                        'number'        => $val['no_doc'],
                        'sales_org_id'  => $val['sales_organization'],
                        'customer_id'   => $val['kode_distributor'],
                        'year'          => $val['tahun'],
                        'contract_type' => 'operational',
                    ],
                    'data_header_asal'        => [
                        'number'        => $val['no_doc'],
                        'sales_org_id'  => $val['sales_organization'],
                        'customer_id'   => $val['kode_distributor'],
                        'year'          => $val['tahun'],
                        'contract_type' => 'asal',
                    ],
                    'data'                    => $val,
                    'initial_qty'  => $val['amount_alokasi'],

                ];
                $this->validateImportXls($dataValidate, Contract::spjbOperationalUpload(), (intval($key) + 2));

                $headerOperational = $this->getHeaderExist($dataValidate['data_header_operational']);
                $headerAsal        = $this->getHeaderExist($dataValidate['data_header_asal']);

                /*Find Detail*/
                $dataItem = [
                    'item_spjb_operational' => [
                        'contract_id'     => $headerOperational->id,
                        'product_id'      => $val['kode_jenis_produk'],
                        'sales_office_id' => $val['kode_provinsi'],
                        'sales_group_id'  => $val['kode_kabupaten'],
                        'sales_unit_id'   => $val['kode_kecamatan'],
                        'month'           => intval($val['bulan']),
                        'year'            => $val['tahun'],
                    ],
                    'item_spjb_asal'        => [
                        'contract_id'     => $headerAsal->id,
                        'product_id'      => $val['kode_jenis_produk'],
                        'sales_office_id' => $val['kode_provinsi'],
                        'sales_group_id'  => $val['kode_kabupaten'],
                        'sales_unit_id'   => $val['kode_kecamatan'],
                        'month'           => intval($val['bulan']),
                        'year'            => $val['tahun'],
                    ],
                    'data'                  => $headerAsal->id,
                ];
                $this->validateImportXls($dataItem, Contract::spjbOperationalUploadItem(), (intval($key) + 2));

                $itemOperational = $this->findDataWhere(Contractitem::class, $dataItem['item_spjb_operational']);

                if ($itemOperational->status == "n") {
                    DB::rollback();
                    $response           = responseFail(trans('messages.update-fail'));
                    $response['errors'] = 'Data SPJB InActive pada baris ke-' . (intval($key) + 2);
                    $response['data']   = $val;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
                /* Update Attributes */
                $UpdatesAttributes = [
                    'initial_qty' => $val['amount_alokasi'],
                    'updated_by'  => Auth::user()->id,
                ];

                $itemOperational->update($UpdatesAttributes);
                array_push($return, $val);
            }

            // Try To Commit
            try {
                DB::commit();
                $sukses   = ['msg' => 'Sukses Input Data'];
                $response = responseSuccess(trans('messages.update-success'), $val);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.update-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $data = $this->datatable($request,true);
        return Excel::download((new ExportSPJBAsalHeader($data)),"Download SPJB Operational.xls");
    }

    public function statusBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk SPJB operational Header');
        $attributes = $request->all();

        $validator = Validator::make($attributes, [
            "*.uuid"    => "required",
            "*.status"     => "in:d,y,n,s",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 402);
        }

        $collect = collect($attributes)->pluck('uuid');
        // check status active inactive
        $data = Contract::whereIn('uuid',$collect)->WhereIn('status',['y','n'])->pluck('status')->unique()->count();
        if($data>1)
        {
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = ['status' => [trans('messages.status-only-one')]];
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['uuid']);
                $iv['updated_by'] = Auth::user()->id;
                $iv['status']     = $iv['status'];
                $model            = $this->findDataUuid(Contract::class, $iv['uuid']);

                $spjbitem         = Contractitem::where('contract_id', $model->id)->get();
                $checkspjbitem    = $this->checkOneItemActive($spjbitem,$iv['status'],'operational');
                if($checkspjbitem)
                {
                    DB::rollback();
                    $response           = responseFail("Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}");
                    $response['errors'] = ['Gagal Update' => "Data Detail SPJB sudah ada yang aktif. Product : {$checkspjbitem->product_name}, kecamatan : {$checkspjbitem->sales_unit_name}"];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $item             = Contractitem::where('contract_id', $model->id)->update($iv);
                $model->update($iv);

                /* Change Status SPJB ASAL */
                $spjbAsal = Contract::where('number', $model->number)
                ->where('customer_id', $model->customer_id)
                ->where('sales_org_id', $model->sales_org_id)
                ->where('contract_type', 'asal')
                ->where('year', $model->year)
                ->first();
                $itemAsal = Contractitem::where('contract_id', $spjbAsal->id)->update($iv);
                $spjbAsal->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    // Component Pendukung On Controller Ini

    private function cekDuplikasiItemUpdate($model, $attrib, $uuid)
    {

        $query = $model::where($attrib)
            ->where('uuid', '!=', $uuid);

        $check = $query->first();

        return $check;
    }

    private function duplicateItemUpdate($attrib, $uuid)
    {

        $query = Contractitem::getItemSPJBDownload($attrib)
            ->where('tb2.contract_type', '=', 'operational')
            ->where('tb1.status', '=', 'y')
            ->where('tb1.uuid', '!=', $uuid);

        $check = $query->first();

        return $check;
    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
                'errors'     => 'Unvalidate Data in Line ' . $line,
                'data'       => $request['data'],
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    private function calculateddiferentQty($old, $new, $available)
    {
        if ($old < $new) {
            // Case Old Qty Kurang Dari
            $selisih = $available - ($new - $old);
        } elseif ($old == $new) {
            // Case Old Qty Sama dengan Data baru
            $selisih = $available;
        } else {
            // Case Qty Lebih Dari
            $selisih = $available + ($old - $new);
        }

        return $selisih;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    private function gettotal($produks, $datasArr)
    {
        # code...
        foreach ($produks as $key => $value) {
            # code...
            $sumkecamatan[$value]['id']             = $value;
            $sumkecamatan[$value]['name']           = $datasArr[$key]->product_name;
            $sumkecamatan[$value]['total_alokasi']  = 0;
            $sumkecamatan[$value]['total_so']       = 0;
            $sumkecamatan[$value]['total_approved'] = 0;
            $sumkecamatan[$value]['total_sisa']     = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $sumkecamatan[$value]['month'][$j]['alokasi']  = 0;
                $sumkecamatan[$value]['month'][$j]['so']       = 0;
                $sumkecamatan[$value]['month'][$j]['approved'] = 0;
                $sumkecamatan[$value]['month'][$j]['sisa']     = 0;
            }
        }

        return $sumkecamatan;
    }

    private function pushtotal($totals)
    {
        $newTotals = [];
        foreach ($totals as $key => $value) {
            # code...
            array_push($newTotals, $value);
        }
        return $newTotals;
    }

    private function cekRayonisasi($produsen, $sales_office, $sales_group, $sales_unit)
    {
        $sales_office = $this->cekSalesOfficeCode($sales_office);
        $sales_group  = $this->cekGroupCode($sales_group);
        $query        = DB::table('wcm_sales_org as a')
            ->join('wcm_sales_area as b', function ($q) {
                $q->on('a.id', '=', 'b.sales_org_id')
                    ->where('distrib_channel_id', '10')
                    ->where('sales_division_id', '00');
            })
            ->join('wcm_sales_office_assg as c', function ($q) use ($sales_office) {
                $q->on('c.sales_area_id', '=', 'b.id')->where('c.sales_office_id', $sales_office);
            })
            ->join('wcm_sales_group as d', function ($q) use ($sales_group) {
                $q->on('d.sales_office_id', '=', 'c.sales_office_id')->where('d.id', $sales_group);
            })
            ->join('wcm_sales_unit as e', function ($q) use ($sales_unit) {
                $q->on('e.sales_group_id', '=', 'd.id')->where('e.id', $sales_unit);
            })
            ->where('a.id', $produsen)
            ->first();
        return $query;
    }
    private function getDuplicateItem($notId, $where)
    {
        # code...
        $query = Contractitem::where($where)
            ->where('id', '!=', $notId);
        $check = $query->first();

        return $check['uuid'];
    }

    private function cekGroupCode($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length < 4) {
            $kabupaten = SalesGroup::where('district_code', $sales_group_id)->first();
            return @$kabupaten->id;
        }
        return $sales_group_id;
    }

    private function cekSalesOfficeCode($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }

    private function GetDetailSPJBAsal($model)
    {
        $contractOpr = $this->findDataWhere(Contract::class, ['id' => $model->contract_id]);

        /*Find Header SPJB Asal*/
        $contractAsal = $this->findDataWhere(Contract::class,
            [
                'number'        => $contractOpr->number,
                'customer_id'   => $contractOpr->customer_id,
                'sales_org_id'  => $contractOpr->sales_org_id,
                'year'          => $contractOpr->year,
                'contract_type' => 'asal',
            ]);

        $findItem["contract_id"]     = $contractAsal->id;
        $findItem["product_id"]      = $model->product_id;
        $findItem["sales_office_id"] = $model->sales_office_id;
        $findItem["sales_group_id"]  = $model->sales_group_id;
        $findItem["sales_unit_id"]   = $model->sales_unit_id;
        $findItem["month"]           = $model->month;
        $findItem["year"]            = $model->year;

        $SPJBAsalItem = $this->findDataWhere(Contractitem::class, $findItem);

        return $SPJBAsalItem;
    }

    private function getuniqStatusForHeader($model, $idheader)
    {
        $allStatus = $model::select('status')
            ->distinct()
            ->where('contract_id', $idheader)
            ->get()
            ->toArray();
        return count($allStatus);
    }

    private function getHeaderExist($value)
    {
        $query = Contract::where($value);
        $check = $query->first();
        return $check;
    }

    private function spjbOprYear($data)
    {
        $year = $data['month'];unset($data['month']);

        $sum = DB::table('wcm_contract_item')
            ->whereIn('month', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            ->where($data)
            ->select(DB::raw('SUM(initial_qty) as qty'))
            ->get();
        return (is_null($sum[0]->qty)) ? 0 : $sum[0]->qty;

    }
    private function checkOneItemActive($data,$status,$type)
    {
        foreach ($data as $key => $value) {
            # code...
            if($status=='y' &&  $value['status'] =='n')
            {
                $checkitem=$this->checkOneActiveArea($value,$type);
                if($checkitem)
                {
                    return $checkitem;
                }
            }
        }

        return false;
    }

    private function checkOneActiveArea($where,$type)
    {

        $dataitem = Contractitem::getItemSPJB(['tb1.month'=>intval($where['month']),'tb1.product_id'=>$where['product_id'],
                'tb1.sales_group_id' => $where['sales_group_id'],
                'tb1.sales_office_id' => $where['sales_office_id'],
                'tb1.sales_unit_id' => $where['sales_unit_id'],'tb1.year'=>$where['year'],'tb1.status'=>'y' , 'tb8.contract_type'=>$type
                        ])->first();
        return $dataitem;
    }
}
