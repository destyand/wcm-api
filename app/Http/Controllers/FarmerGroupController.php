<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Imports\FarmerGroupImport;
use App\Models\Commodity;
use App\Models\FarmerGroup;
use App\Models\Retail;
use App\Models\SalesUnit;
use App\Models\SubSector;
use App\Rules\FarmerGroupLandAreaRule;
use App\Rules\FarmerGroupNameRule;
use App\Rules\FarmerGroupProdusenRetailRule;
use App\Rules\FarmerGroupSalesOfficeRule;
use App\Traits\ValidationTemplateImport;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class FarmerGroupController extends Controller
{
    use ValidationTemplateImport;

    public function index(Request $request, $cust_uuid = null, $exported=false)
    {
        \LogActivity::addToLog('get all farmer group');
        isset($cust_uuid) ? is_uuid($cust_uuid) : '';

        $where = array();
        if ($cust_uuid) {
            $where['tb10.uuid'] = $cust_uuid;
        }
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $query   = FarmerGroup::getAll($where);
        $user    = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb3.sales_group_id", $filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb1.name'          => 'name',
            'tb1.code'          => 'code',
            'tb1.retail_id'     => 'retail_id',
            'tb1.qty_group'     => 'qty_group',
            'tb1.land_area'     => 'land_area',
            'tb1.address'       => 'address',
            'tb1.sales_unit_id' => 'sales_unit_id',
            'tb1.telp_no'       => 'telp_no',
            'tb1.fax_no'        => 'fax_no',
            'tb1.village'       => 'village',
            'tb1.sub_sector_id' => 'sub_sector_id',
            'tb1.latitude'      => 'latitude',
            'tb1.longitude'     => 'longitude',
            'tb2.code'          => 'retail_code',
            'tb2.name'          => 'retail_name',
            'tb3.name'          => 'sales_unit_name',
            'tb4.full_name'     => 'customer_name',
            'tb5.name'          => 'sales_org_name',
            'tb5.id'            => 'sales_org_id',
            'tb6.name'          => 'sales_group_name',
            'tb6.id'            => 'sales_group_id',
            'tb7.name'          => 'sales_office_name',
            'tb7.id'            => 'sales_office_id',
            'tb1.status'        => 'status',
            'tb1.created_by'    => 'created_by',
            'tb1.updated_by'    => 'updated_by',
            'tb1.created_at'    => 'created_at',
            'tb1.updated_at'    => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if ($exported) return $model->getFilteredQuery();
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function orderColumn($columns, $request, $query)
    {
        $order     = $request->get('order');
        $kolom     = $request->get('columns');
        $field     = $kolom[$order[0]['column']]['name'];
        $direction = $order[0]['dir'];
        foreach ($columns as $key => $value) {
            if ($field == $value) {
                $query->orderBy($key, $direction);
            }
        }
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create farmer group');

        $attributes               = $request->all();
        $attributes['group_name'] = [
            'name'          => $request->get('name'),
            'sales_unit_id' => $request->get('sales_unit_id'),
        ];

        $this->validate($attributes, FarmerGroup::ruleCreate());
        unset($attributes['group_name']);
        $this->cekDuplikasiStore(FarmerGroup::class, ['sales_unit_id' => $attributes['sales_unit_id'], 'village' => $attributes['village']]);

        $attributes['code']       = $this->generateCode(FarmerGroup::class, 'code', 'FG', 10);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {
            $model = FarmerGroup::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {
        \LogActivity::addToLog('get farmer group by id');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model             = FarmerGroup::getAll($where)->get();

        if (!$model->isEmpty()) {
            $commodities     = explode(";", $model->first()->commodity_id);
            $commoditiesName = [];
            if ($commodities) {
                $commoditiesName = Commodity::whereIn('id', $commodities)->pluck('name')->toArray();
            }
        }

        $model = $model->map(function ($value) use ($commoditiesName) {
            $row                   = (array) $value;
            $row['commodity_name'] = implode(',', $commoditiesName);
            return $row;
        });

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        \LogActivity::addToLog('update farmer group');

        $attributes               = $request->except(['uuid', 'code']);
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, FarmerGroup::ruleUpdate());

        $model = $this->findDataUuid(FarmerGroup::class, $id);

        DB::beginTransaction();
        try {

            if (!$this->cekStatus(@$model['status'], @$attributes['status'])) {
                throw new \Exception(trans('messages.status-check-fail'), 1);
            }

            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk farmer group');

        $attributes = $request->all();

        DB::beginTransaction();
        $model = [];
        foreach ($attributes as $iv) {
            is_uuid($iv['uuid']);

            $model = $this->findDataUuid(FarmerGroup::class, $iv['uuid']);
            $this->validateStatusChange($model->uuid, $model->status, $iv['status']);
            unset($iv['id']);
            $iv['updated_by'] = Auth::user()->id;

            $model->update($iv);
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete farmer group');

        $model = $this->findDataUuid(FarmerGroup::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'n'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getImport(Request $request)
    {
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new FarmerGroupImport, $file)[0];
            if(!$this->compareTemplate("farmer_group", array_keys($data[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }

            $rules = [
                'land_area'    => [
                    'required',
                    new FarmerGroupLandAreaRule,
                ],
                'group_name'   => [
                    'required',
                    new FarmerGroupNameRule,
                ],
                'sales_office' => [
                    'required',
                    new FarmerGroupSalesOfficeRule,
                ],
                //'village'      => 'required|unique:wcm_farmer_groups',
                'retail_code'  => 'exists:wcm_retail,code',
                'sales_org_id' => [
                    'required',
                    new FarmerGroupProdusenRetailRule,
                ],
            ];

            $newData = [];
            foreach ($data as $i => $iv) {
                if (is_null($iv['kode_kecamatan'])) {
                    continue;
                }

                //$forValidate['sales_unit_id'] = $iv['kode_kecamatan'];
                $forValidate['village']     = $iv['desa'];
                $forValidate['retail_code'] = $iv['kode_pengecer'];
                $forValidate['group_name']  = [
                    "name"          => $iv['nama_kelompok_tani'],
                    "sales_unit_id" => $iv['kode_kecamatan'],
                ];

                $forValidate['land_area'] = [
                    "jumlah_anggota" => $iv['jumlah_anggota'],
                    "luas_lahan"     => $iv['luas_lahan'],
                ];

                $forValidate['sales_office'] = [
                    "retail_code"   => $iv['kode_pengecer'],
                    "sales_unit_id" => $iv['kode_kecamatan'],
                ];

                $forValidate['sales_org_id'] = [
                    "retail_code"  => $iv['kode_pengecer'],
                    "sales_org_id" => $iv['sales_org'],
                ];

                $error = $this->validateImport($forValidate, $rules);

                $retail                         = Retail::select('id as retail_id', 'name as retail_name')->where('code', $iv['kode_pengecer'])->first();
                $newData[$i]['retail_id']       = $retail['retail_id'];
                $newData[$i]['retail_name']     = $retail['retail_name'];
                $newData[$i]['retail_code']     = $iv['kode_pengecer'];
                $newData[$i]['name']            = $iv['nama_kelompok_tani'];
                $newData[$i]['address']         = $iv['alamat_kelompok_tani'];
                $newData[$i]['sales_org_id']    = $iv['sales_org'];
                $newData[$i]['sales_unit_id']   = $iv['kode_kecamatan'];
                $newData[$i]['sales_unit_name'] = SalesUnit::select('name')->where('id', $iv['kode_kecamatan'])->first()['name'];
                $newData[$i]['village']         = $iv['desa'];
                $newData[$i]['land_area']       = $iv['luas_lahan'];
                $newData[$i]['qty_group']       = $iv['jumlah_anggota'];
                $subsector                      = $this->getValue($iv, 'sub_sektor', SubSector::class);
                $newData[$i]['sub_sector']      = $subsector;
                $commodity                      = $this->getValue($iv, 'komoditas', Commodity::class);
                $newData[$i]['commodity']       = $commodity;

                $newData[$i]['validate'] = $error;
            }

            $response = responseSuccess(trans('messages.import-success'), $newData);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        $response = responseFail(trans('messages.import-fail'));
        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    }

    public function getKomidities(Request $request)
    {
        $attributes = $request->all();
        $this->validate($attributes, Commodity::getBulkIDCommodities());
        $inArr = explode(';', $attributes['commodities']);

        $datas = Commodity::wherein('id', $inArr)->get();

        $response = responseSuccess(trans('messages.read-success'), $datas);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function getSubsector(Request $request)
    {
        $attributes = $request->all();
        $this->validate($attributes, SubSector::getBulkIDSubSector());
        $inArr = explode(';', $attributes['subsectors']);

        $datas = SubSector::wherein('id', $inArr)->get();

        $response = responseSuccess(trans('messages.read-success'), $datas);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    private function getValue($array, $string, $model)
    {
        $param = [];
        foreach ($array as $key => $val) {
            if (strpos($key, $string) !== false && strtolower($val) == "ya") {
                $kata  = str_replace($string . '_', '', $key);
                $nilai = str_replace('_', ' ', $kata);
                array_push($param, $nilai);
            }
        }
        $result = $model::select('id', 'name')->whereIn('name', $param)->get();
        return $result;
    }

    public function saveImport(Request $request)
    {
        DB::beginTransaction();
        $flag       = true;
        $attributes = $request->all();

        $newData    = [];
        $valMax     = $this->getMaxCode(FarmerGroup::class);
        $attribSave = [];
        foreach ($attributes as $i => $iv) {
            $forValidate['group_name'] = [
                "name"          => $iv['name'],
                "sales_unit_id" => $iv['sales_unit_id'],
            ];

            $forValidate['land_area_size'] = [
                "jumlah_anggota" => $iv['qty_group'],
                "luas_lahan"     => $iv['land_area'],
            ];

            $forValidate['sales_office'] = [
                "retail_code"   => $iv['retail_id'],
                "sales_unit_id" => $iv['sales_unit_id'],
            ];
            $forValidate['sales_org_id'] = [
                "retail_code"  => $iv['retail_code'],
                "sales_org_id" => $iv['sales_org_id'],
            ];

            $error = $this->validateImport($forValidate, FarmerGroup::ruleCreateBulk());

            // Arr::forget($iv, ['group_name', 'land_area_size', 'sales_office', 'relation_sales_org_retail']);

            $duplicate = false; //$this->cekDuplikasiBulk(FarmerGroup::class, ['sales_unit_id' => $iv['sales_unit_id'], 'village' => $iv['village']]);

            $retail = Retail::where('code', $iv['retail_code'])->first();

            $attribSave[$i]['retail_id']     = @$retail->id;
            $attribSave[$i]['retail_name']   = @$retail->name;
            $attribSave[$i]['retail_code']   = @$retail->code;
            $attribSave[$i]['name']          = $this->cekKeyExist('name', $iv);
            $attribSave[$i]['customer_id']   = $this->cekKeyExist('customer_id', $iv);
            $attribSave[$i]['sales_org_id']  = $this->cekKeyExist('sales_org_id', $iv);
            $attribSave[$i]['address']       = $this->cekKeyExist('address', $iv);
            $attribSave[$i]['sales_unit_id'] = $this->cekKeyExist('sales_unit_id', $iv);
            $attribSave[$i]['telp_no']       = $this->cekKeyExist('telp_no', $iv);
            $attribSave[$i]['fax_no']        = $this->cekKeyExist('fax_no', $iv);
            $attribSave[$i]['village']       = $this->cekKeyExist('village', $iv);
            $attribSave[$i]['sub_sector_id'] = $this->cekKeyExist('sub_sector_id', $iv);
            $attribSave[$i]['commodity_id']  = $this->cekKeyExist('commodity_id', $iv);
            $attribSave[$i]['land_area']     = $this->cekKeyExist('land_area', $iv);
            $attribSave[$i]['qty_group']     = $this->cekKeyExist('qty_group', $iv);
            $attribSave[$i]['latitude']      = $this->cekKeyExist('latitude', $iv);
            $attribSave[$i]['longitude']     = $this->cekKeyExist('longitude', $iv);
            $attribSave[$i]['status']        = $this->cekKeyExist('status', $iv);
            if ($error['status'] == 0) {
                $flag                       = false;
                $attribSave[$i]['validate'] = $error;
            } else if ($duplicate) {
                $flag                       = false;
                $attribSave[$i]['validate'] = $duplicate;
            } else {
                $attribSave[$i]['created_by'] = Auth::user()->id;
                $attribSave[$i]['updated_by'] = Auth::user()->id;
                $attribSave[$i]['created_at'] = now();
                $attribSave[$i]['updated_at'] = now();
                $attribSave[$i]['code']       = $this->generateCodeBulk($valMax, 'FG', 10);
                $newData[]                    = $attribSave[$i];
                $valMax++;
                Arr::forget($attribSave[$i], ['retail_name', 'retail_code']);
            }
        }
        if ($flag) {

            FarmerGroup::insert($attribSave);
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $newData);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            DB::rollback();
            $response         = responseFail(trans('messages.create-fail'));
            $response['data'] = $attribSave;
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekKeyExist($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        } else {
            return null;
        }
    }

    private function cekDuplikasiBulk($model, $attrib)
    {

        $cek = $this->cekDuplikasi($model, $attrib);
        if ($cek) {
            $response = responseFail($this->errorDuplicate($attrib));
            return $response;
        }
        return false;
    }

    private function cekDuplikasiStore($model, $attrib)
    {
        $cek = $this->cekDuplikasi($model, $attrib);
        if ($cek) {
            $response = responseFail($this->errorDuplicate($attrib));
            $return   = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
        return false;
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,null, true);
        $data = $q->select(FarmerGroup::getExportedColumns())->get();
        return Excel::download((new Download($data)), "farmer-group.xls");
    }

}
