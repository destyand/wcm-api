<?php

namespace App\Http\Controllers;

use App\Models\CertificateRelease;
use App\Models\CertificateReleaseItems;
use Illuminate\Http\Request;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Rules\CreateCertificateReleaseRules;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CertificateReleaseController extends Controller
{
    //

    public function index(Request $request){
        $query = DB::table('wcm_certificate_release as tb1')
        ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
        ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
        ->select('tb1.*', 'tb3.full_name as customer_name', 'tb4.name as sales_org_name','tb6.name as sales_group_name');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb1.number' => 'number',
            'tb1.customer_id' => 'customer_id',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb3.full_name'=>' customer_name', 
            'tb4.name '=>' sales_org_name',
            'tb6.name '=>' sales_group_name',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at'
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);        
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function listDataCreate(Request $request)
    {
        # code...
        \LogActivity::addToLog('List PKP Surat Jalan');

        $query   =  DistribReports::detailheader(['tb1.distrib_resportable_type'=>'order','tb1.status'=>'s'])
                    ->whereNull('status_certificate_release')
                    ->addSelect('status_certificate_release');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }

        $columns = [
            'tb1.id' => 'id', 
            'tb1.uuid' => 'uuid', 
            'tb1.report_f5_id' => 'report_f5_id', 
            'tb1.number' => 'number', 
            'tb2.so_number' => 'so_number',
            'tb1.customer_id' => 'customer_id',
            'tb3.full_name' => 'customer_name', 
            'tb1.sales_group_id' => 'sales_group_id', 
            'tb4.name' => 'sales_group_name',
            'tb1.sales_org_id' => 'sales_org_id', 
            'tb5.name'=>'sales_org_name', 
            'tb2.delivery_method_id'=>'inconterm_id',
            'tb6.name' => 'inconterm_name', 
            'tb1.distribution_date' => 'distribution_dates',
            'tb1.status' => 'status',
            'tb2.contract_id' => 'contract_id', 
            'tb1.month' => 'month', 
            'tb1.year' => 'year', 
            'tb1.order_id' => 'order_id',
            'tb2.order_date' => 'order_date'
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);        
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }


    public function store(Request $request)
    {
        # code...
        \LogActivity::addToLog('Create PKP Surat Jalan');

        $attributes               = $request->only('data');
       
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
            'data'  => [new CreateCertificateReleaseRules]
        ];

        $this->validate($attributes, $rules);

        $data = DistribReports::whereIn('uuid',$attributes['data'])->get();
        $customer_id=$data->pluck('customer_id')->first();
        $sales_org_id =$data->pluck('sales_org_id')->first();
        $sales_group_id=$data->pluck('sales_group_id')->first();      

        DB::beginTransaction();
        try {
            $certificate = CertificateRelease::create([
                'status'=>'s',
                'created_by'=> Auth::user()->id,
                'sales_org_id' => $sales_org_id,
                'sales_group_id' =>$sales_group_id,
                'customer_id' => $customer_id
            ]);
            $createItemData = [];
            foreach ($data as $key => $value) {
                # code...
                $createItemData[] = [
                    'certificate_release_id'=> $certificate->id,
                    'distrib_report_id' => $value['id'],
                    'created_by' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
           
            $model = CertificateReleaseItems::insert($createItemData);
            $update = DistribReports::whereIn('uuid',$attributes['data'])->update(['status_certificate_release'=>'s','updated_by'=>Auth::user()->id]);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {
        # code...
        $model['header'] = DB::table('wcm_certificate_release as tb1')
        ->leftJoin('wcm_customer AS tb3', 'tb1.customer_id', '=', 'tb3.id')
        ->leftJoin('wcm_sales_org AS tb4', 'tb1.sales_org_id', '=', 'tb4.id')
        ->leftJoin('wcm_sales_group as tb6', 'tb1.sales_group_id', '=','tb6.id')
        ->select('tb1.*', 'tb3.full_name as customer_name', 'tb4.name as sales_org_name','tb6.name as sales_group_name')
        ->where('tb1.uuid', $uuid)->first();
        $model['data'] = CertificateRelease::detail()
        ->where('wcr.uuid', $uuid)
        ->get();
        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update($uuid, Request $request)
    {
        # code...
        $certificate = CertificateRelease::where('uuid', $uuid)->firstOrFail();
        $attributes  = $request->only('data');
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
            'data'  => [new CreateCertificateReleaseRules]
        ];
        $this->validate($attributes, $rules);
        $data = DistribReports::whereIn('uuid',$attributes['data'])->get();
        $pass = false; $errors=[];
        if($data->pluck('sales_org_id')->first()!=$certificate->sales_org_id){
            $errors['sales_org_id'] = ["sales_org_id tidak sama dengan data header surat jalan yang ada"];
            $pass = true;
        }
        if($data->pluck('customer_id')->first()!=$certificate->customer_id){
            $errors['customer_id'] = ["customer_id tidak sama dengan data header surat jalan yang ada"];
            $pass = true;
        }
        if($data->pluck('sales_group_id')->first()!=$certificate->sales_group_id){
            $errors['sales_group_id'] = ["sales_group_id tidak sama dengan data header surat jalan yang ada"];
            $pass = true;
        }
        if($pass){
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $errors,
            ];
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();
        try {
            $createItemData = [];
            foreach ($data as $key => $value) {
                # code...
                $createItemData[]= [
                    'certificate_release_id'=> $certificate->id,
                    'distrib_report_id' => $value['id'],
                    'updated_by' => Auth::user()->id,
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            CertificateRelease::where('uuid', $uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            CertificateReleaseItems::insert($createItemData);
            DistribReports::whereIn('uuid',$attributes['data'])->update(['status_certificate_release'=>'s','updated_by'=>Auth::user()->id, 'updated_at'=> date('Y-m-d H:i:s')]);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function destroyItems($uuid, Request $request)
    {
        # code...
        $attributes = ['uuid'=> $uuid];
        $rules = [
            'uuid' => 'required|exists:wcm_certificate_release,uuid', 
        ];
        $this->validate($attributes, $rules);

        $certificate = CertificateRelease::where('uuid',$uuid)->first();

        $req               = $request->only('data');
        $rules = [
            'data' => 'required|exists:wcm_distrib_reports,uuid', 
        ];
        $this->validate($req, $rules);
        $distrib = DistribReports::where('uuid',$req['data'])->first();

        DB::beginTransaction();
        try {
            CertificateReleaseItems::where(['distrib_report_id'=>$distrib->id,'certificate_release_id'=>$certificate->id])->delete();
            CertificateRelease::where('uuid',$uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            DistribReports::where('uuid',$req['data'])->update(['status_certificate_release'=>null,'updated_by'=>Auth::user()->id]);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroyBulkItems($uuid, Request $request)
    {
        # code...
        $attributes = ['uuid'=> $uuid];
        $rules = [
            'uuid' => 'required|exists:wcm_certificate_release,uuid', 
        ];
        $this->validate($attributes, $rules);

        $certificate = CertificateRelease::where('uuid',$uuid)->first();

        $items = $request->only('data');
        $rules = [
            'data' => 'required|array',
            'data.*' => 'exists:wcm_distrib_reports,uuid', 
        ];
        $this->validate($items, $rules);    
        $distrib = DistribReports::whereIn('uuid', $items['data'])->get();
        

        DB::beginTransaction();
        try {
            CertificateReleaseItems::whereIn('distrib_report_id',$distrib->pluck('id')->toArray())->where('certificate_release_id',$certificate->id)->delete();
            CertificateRelease::where('uuid',$uuid)->update(['updated_by'=> Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            DistribReports::whereIn('uuid', $items['data'])->update(['status_certificate_release'=>null,'updated_by'=>Auth::user()->id,'updated_at'=> date('Y-m-d H:i:s')]);
            // return response()->json($distrib->pluck('id')->toArray());
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), []);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }
}
