<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class CommodityController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all commodity');

        $query = Commodity::all();
        $model = Datatables::of($query)
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
