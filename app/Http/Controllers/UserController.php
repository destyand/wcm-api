<?php

namespace App\Http\Controllers;

use App\Models\CustSalesOrgAssg;
use App\Models\SalesArea;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SalesGroupUserAssg;
use App\Models\SalesOrgUserAssg;
use App\Models\SalesOrg;
use App\Models\SalesOffice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Rules\OnlyOneRules;
use Exception;

class UserController extends Controller
{

    public function index(Request $request)
    {
        \LogActivity::addToLog('get all user');

        $query = User::getUser();
        $users = $request->user();
        $filters = $users->filterRegional;
        
        if (@$filters["sales_org_id"]) {
            //  CoDing Lawaas
            // $query->leftJoin("wcm_customer_sales_area as csa", 'csa.customer_id', 'tb1.customer_id');
            // $query->where(function ($query) use ($filters) {
            //     $salesAreas = SalesArea::where("sales_org_id", $filters["sales_org_id"])
            //         ->pluck("id");
            //     $salesOrgUserAssgns = SalesOrgUserAssg::where("sales_org_id", $filters["sales_org_id"])
            //         ->pluck("user_id");
            //     $query->whereIn("csa.sales_area_id", $salesAreas);
            //     $query->orWhereIn("tb1.id", $salesOrgUserAssgns);
            // });
            // End Coding Lawas

            if(count(@$filters["sales_org_id"])!=5){
                // if($users->role_id=="3"){
                    $query->leftJoin("wcm_customer_sales_area as csa", 'csa.customer_id', 'tb1.customer_id');
                // }
                $query->where(function ($query) use ($filters, $users) {
                    $salesOrgUserAssgns = SalesOrgUserAssg::where("sales_org_id", $filters["sales_org_id"])
                        ->pluck("user_id");
                        // return response()->json($salesOrgUserAssgns);
                    // if($users->role_id=="3"){
                        $salesAreas = SalesArea::where("sales_org_id", $filters["sales_org_id"])
                            ->pluck("id");
                            $query->whereIn("csa.sales_area_id", $salesAreas);
                    // }
                    $query->orWhereIn("tb1.id", $salesOrgUserAssgns);
                    $query->where("tb3.id",'!=',2);
                });
            }

            

        }
        // $query->where('tb3.id',2); 

        $columns = [
            'tb1.name' => 'name', 'tb1.username' => 'username', 'tb1.email' => 'email', 'tb1.customer_id' => 'customer_id',
            'tb1.created_at' => 'created_at', 'tb1.updated_at' => 'updated_at', 'tb3.name' => 'role_name', 'tb3.id' => 'role'
        ];
        $user = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $user->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create user');

        $attributes = $request->only(['name', 'username', 'password', 'email', 'role_id', 'sales_org_id']);

        $this->validate($attributes, User::ruleCreate());

        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;


        DB::beginTransaction();
        try {
            // Validation Role
            $role  = $request->get("role_id");
            if (!$request->user()->isAdminBaru && $role == 2) {
                throw new Exception("You dont have Permission");
            }

            /*Do Create new User*/
            $user = User::create($attributes);

            /*Find the row of user*/
            $model = User::where("username", $user->username)->firstOrFail();

            /*Sync the rule into user*/
            $this->setRoles($model->id, $request->input('role_id'));
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::commit();
        $response = responseSuccess(trans('messages.create-success'), $model);
        return response()->json($response, 201, [], JSON_PRETTY_PRINT);
    }

    public function newstore(Request $request)
    {

        $attributes = $request->only(['name', 'username', 'password', 'email', 'role_id', 'sales_org_id', 'sales_group_id', 'status']);

        $this->validate($attributes, User::newruleCreate());

        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {

            /*Do Create new User*/
            $user = User::create(['name' => $attributes['name'], 'username' => $attributes['username'], 'password' => $attributes['password'], 'email' => $attributes['email'], 'role_id' => $attributes['role_id'], 'status' => $attributes['status']]);

            /*Find the row of user*/
            $model = User::where("username", $user->username)->firstOrFail();

            /*Sync the rule into user*/
            $this->setRoles($model->id, $request->input('role_id'));

            if ($attributes['sales_org_id'] != null || $attributes['sales_org_id'] != "") {
                $sales_org = explode(';', $attributes['sales_org_id']);
                if (count($sales_org) == 5) {
                    SalesOrgUserAssg::create(['user_id' => $model->id, 'created_by' => Auth::user()->id]);
                } else {
                    foreach ($sales_org as $key => $value) {
                        # code...
                        if ($value != "") {
                            $rules_org = [
                                'id' => 'required|exists:wcm_sales_org,id'
                            ];

                            $this->validate(['id' => $value], $rules_org);
                            SalesOrgUserAssg::create(['sales_org_id' => $value, 'user_id' => $model->id, 'created_by' => Auth::user()->id]);
                        }
                    }
                }
            } else {
                SalesOrgUserAssg::create(['user_id' => $model->id, 'created_by' => Auth::user()->id]);
            }

            if ($attributes['sales_group_id'] != "") {

                $sales_group = explode(';', $attributes['sales_group_id']);

                foreach ($sales_group as $key => $value) {
                    # code...
                    if ($value != "") {
                        $rules_group = [
                            'id' => 'required|exists:wcm_sales_group,id'
                        ];

                        $this->validate(['id' => $value], $rules_group);
                        SalesGroupUserAssg::create(['sales_group_id' => $value, 'user_id' => $model->id, 'created_by' => Auth::user()->id]);
                    }
                }
            } else {
                SalesGroupUserAssg::create(['user_id' => $model->id, 'created_by' => Auth::user()->id]);
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::commit();
        $response = responseSuccess(trans('messages.create-success'), $model);
        return response()->json($response, 201, [], JSON_PRETTY_PRINT);
    }


    public function newupdate($uuid, Request $request)
    {

        $attributes = $request->only(['name', 'username', 'email', 'role_id', 'sales_org_id', 'sales_group_id', 'status']);

        $attributes['username'] = array("find" => 'id', "uuid" => $uuid, 'username' => $attributes['username'], "tables" => "users");
        $attributes['email'] = array("find" => 'id', "uuid" => $uuid, 'email' => $attributes['email'], "tables" => "users");

        $this->validate($attributes, User::newruleUpdate());

        $attributes['username'] = $attributes['username']['username'];
        $attributes['email'] = $attributes['email']['email'];

        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {

            /*Do Create new User*/
            $user = User::where('id', $uuid)->update(['name' => $attributes['name'], 'username' => $attributes['username'], 'email' => $attributes['email'], 'status' => $attributes['status']]);

            /*Find the row of user*/
            $model = User::where("username", $attributes['username'])->firstOrFail();

            /*Sync the rule into user*/
            $this->setRoles($model->id, $request->input('role_id'));


            SalesOrgUserAssg::where('user_id', $uuid)->delete();
            SalesGroupUserAssg::where('user_id', $uuid)->delete();

            if ($attributes['sales_org_id'] != null || $attributes['sales_org_id'] != "") {
                $sales_org = explode(';', $attributes['sales_org_id']);
                if (count($sales_org) == 5) {
                    SalesOrgUserAssg::create(['user_id' => $model->id, 'updated_by' => Auth::user()->id]);
                } else {
                    foreach ($sales_org as $key => $value) {
                        # code...
                        if ($value != "") {
                            $rules_org = [
                                'id' => 'required|exists:wcm_sales_org,id'
                            ];

                            $this->validate(['id' => $value], $rules_org);

                            SalesOrgUserAssg::create(['sales_org_id' => $value, 'user_id' => $model->id, 'updated_by' => Auth::user()->id]);
                        }
                    }
                }
            } else {
                SalesOrgUserAssg::create(['user_id' => $model->id, 'updated_by' => Auth::user()->id]);
            }

            if ($attributes['sales_group_id'] != "") {

                $sales_group = explode(';', $attributes['sales_group_id']);

                foreach ($sales_group as $key => $value) {
                    # code...
                    if ($value != "") {
                        $rules_group = [
                            'id' => 'required|exists:wcm_sales_group,id'
                        ];

                        $this->validate(['id' => $value], $rules_group);
                        SalesGroupUserAssg::create(['sales_group_id' => $value, 'user_id' => $model->id, 'updated_by' => Auth::user()->id]);
                    }
                }
            } else {
                SalesGroupUserAssg::create(['user_id' => $model->id, 'updated_by' => Auth::user()->id]);
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::commit();
        $response = responseSuccess(trans('messages.create-success'), $model);
        return response()->json($response, 201, [], JSON_PRETTY_PRINT);
    }

    public function storedistibutor(Request $request)
    {
        \LogActivity::addToLog('create user Dist');

        $attributes = $request->only(['name', 'username', 'password', 'email', 'customer_id', 'status']);

        $this->validate($attributes, $rules = [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email',
            'password' => 'required',
            'status' => 'required',
            'customer_id' => 'sometimes|exists:wcm_customer,id'
        ]);

        // cek email
        if ($request->has('customer_id')) {
            $mail = User::where('email', $attributes['email'])->first();
            $customer_id = User::where('customer_id', $attributes['customer_id'])->first();
            if ($mail && $customer_id) {
                if ($customer_id->email != $attributes['email'] && $mail->customer_id != $attributes['customer_id']) {
                    $response = [
                        'status'     => 0,
                        'status_txt' => "errors",
                        'message'    => ['email' => ['Email Sudah digunakan']],
                    ];
                    return response()->json($response, 400, [], JSON_PRETTY_PRINT);
                }
            }
        }

        if ($request->has('customer_id') === false || ($request->has('customer_id') && User::where('customer_id', $attributes['customer_id'])->exists() === false)) {
            $this->validate($attributes, $rules = [
                'email' => 'required|email|unique:users',
            ]);
        }

        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['created_by'] = Auth::user()->id;

        DB::beginTransaction();
        try {

            /*Do Create new User*/
            $user = User::create($attributes);

            $role = Role::where('name', 'DISTRIBUTOR')->firstOrFail();
            /*Find the row of user*/
            $model = User::where("username", $user->username)->firstOrFail();

            /*Sync the rule into user*/
            $this->setRoles($model->id, $role->id);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        DB::commit();
        $response = responseSuccess(trans('messages.create-success'), $model);
        return response()->json($response, 201, [], JSON_PRETTY_PRINT);
    }

    public function profile()
    {
        $user = $this->findData(User::class, auth()->user()->id);
        $response = $user->only([
            "id", "username", "email", "last_sign_in", "sales_org_id", "sales_org_name"
        ]);
        $role = $user->roles->first();
        $response["role"] = [
            "role_id" => @$role->id,
            "role_name" => @$role->name,
        ];


        return responseSuccess(__("messages.read-success"), $response);
    }

    public function show($id)
    {
        \LogActivity::addToLog('get user by id');

        $user = $this->findData(User::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $user);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function shownew($id)
    {
        \LogActivity::addToLog('get user by id');

        $user = $this->findData(User::class, $id);

        $user['sales_group_arr'] = SalesGroupUserAssg::where('user_id', $id)
            ->leftjoin('wcm_sales_group AS tb2', 'tb2.id', '=', 'sales_group_user_assg.sales_group_id')
            ->select('sales_group_user_assg.sales_group_id as id', 'tb2.name', 'tb2.sales_office_id')
            ->get();
        $user['sales_org_arr'] = DB::table('sales_org_user_assg')
            ->select('tb2.id', 'tb2.name')
            ->where('user_id', $id)
            ->join('wcm_sales_org AS tb2', 'tb2.id', '=', 'sales_org_user_assg.sales_org_id')
            ->get();
        $user['sales_prov_arr'] = SalesOffice::select('id as sales_office_id', 'name')->whereIn('id', collect($user['sales_group_arr'])->pluck('sales_office_id'))->get();
        $user['rule'] = Role::where('id', $user->role_id)->get();

        $response = responseSuccess(trans('messages.read-success'), $user);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        \LogActivity::addToLog('update user');

        $attributes = $request->only(['name', 'username', 'email', 'role_id', 'customer_id']);


        $attributes['username'] = array("find" => 'id', "uuid" => $id, 'username' => $attributes['username'], "tables" => "users");
        $this->validate($attributes, User::newruleUpdateDistributor());

        $attributes['username'] = $attributes['username']['username'];
        $attributes['updated_by'] = Auth::user()->id;
        $user = $this->findData(User::class, $id);

        // cek email
        if ($request->has('customer_id')) {
            $mail = User::where('email', $attributes['email'])->first();
            $customer_id = User::where('customer_id', $attributes['customer_id'])->first();
            if ($mail && $customer_id) {
                if ($customer_id->email != $attributes['email'] && $mail->customer_id != $attributes['customer_id']) {
                    $response = [
                        'status'     => 0,
                        'status_txt' => "errors",
                        'message'    => ['email' => ['Email Sudah digunakan']],
                    ];
                    return response()->json($response, 400, [], JSON_PRETTY_PRINT);
                }
            }
        }

        if ($request->has('customer_id') === false || ($request->has('customer_id') && User::where('customer_id', $attributes['customer_id'])->exists() === false)) {
            $this->validate($attributes, $rules = [
                'email' => 'required|email|unique:users',
            ]);
        }


        DB::beginTransaction();
        try {
            $model = $user->update($attributes);
            if ($request->has('role_id')) {
                $model = $this->setRoles($user->id, $request->input('role_id'));
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updatePassword(Request $request, $id)
    {
        \LogActivity::addToLog('update password');

        $attributes = $request->only(['password_new', 'retype_password_new']);
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, User::ruleUpdatePassword());

        $user = $this->findData(User::class, $id);
        // $this->findUserPassword($user, $attributes['password_old']);
        $newPassword = Hash::make($attributes['password_new']);
        DB::beginTransaction();
        try {
            $model = $user->update(["password" => $newPassword]);
            if ($request->has('role_id')) {
                $model = $this->setRoles($user->id, $request->input('role_id'));
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete user');

        $user = $this->findData(User::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'n'];

        DB::beginTransaction();
        try {
            $user->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $user);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function setRoles($userID, $roleID)
    {
        $user = $this->findData(User::class, $userID);
        $data = $user->syncRoles($roleID);

        return $data;
    }

    private function findUserPassword($user, $password)
    {
        if (Hash::check($password, $user->password)) {
            return true;
        } else {
            $response = responseFail(trans('messages.password-fail'));
            $return = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}
