<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller {

    public function login(Request $request) {
        \LogActivity::addToLog('login');

        $this->validate($request->all(), User::ruleLogin());

        $credentials = $request->only('username', 'password');
        $credentials['status'] = 'y';

        if (Auth::attempt($credentials)) {
            $username = $request->get('username');
            $user = User::where('username', $username)->first();
            $token = JWTAuth::fromUser($user);
            $this->updateUserInfo();
            $model = $this->getRole($user->id);
            $response = responseSuccess(trans('messages.login-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT)->header('Authorization', $token);
        } else {
            $response = responseFail(trans('messages.login-fail'));
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    public function logout() {
        \LogActivity::addToLog('logout');
        // Invalidate the token
        try {
            JWTAuth::invalidate();
            $response = responseSuccess(trans('messages.logout-success'));
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $response = responseFail(trans('messages.logout-fail'));
            return response()->json($response, 400, [], JSON_PRETTY_PRINT);
        }
    }

    public function detail() {
        \LogActivity::addToLog('get detail user login');

        $user = Auth::user();
        $response = responseSuccess(trans('messages.read-success'), $user);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function refresh() {
        return response([
            'status' => 'success'
        ]);
    }

    private function updateUserInfo() {
        $user = Auth::user();

        $user->update([
            "last_sign_in" => date('Y-m-d H:i:s'),
            "sign_in_count" => $user->sign_in_count + 1
        ]);
    }

    private function getRole($id){
        $data = DB::table('model_has_roles as tb1')
                ->join('roles as tb2','tb1.role_id','=','tb2.id')
                ->select('tb1.role_id','tb2.name as role_name')
                ->where('tb1.model_id','=',$id)
                ->first();

        return $data;
    }
}
