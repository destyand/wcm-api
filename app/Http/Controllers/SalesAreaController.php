<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\CustomerSalesArea;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SalesAreaController extends Controller {

    public function index(Request $request, $exported=false) {
        \LogActivity::addToLog('get all sales area');

        $where = array();
        if ($this->isAdminAnper) {
            $where['tb3.sales_org_id'] = $this->salesOrgId;
        } elseif ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }
        $query = CustomerSalesArea::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0 && !$this->isDistributor) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb3.sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb8.sales_group_id",$filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb1.customer_id' => 'customer_id',
            'tb2.uuid' => 'uuid',
            'tb2.full_name' => 'customer_name',
            'tb3.sales_org_id' => 'sales_org_id',
            'tb6.name' => 'sales_org_name',
            'tb3.distrib_channel_id' => 'distrib_channel_id',
            'tb4.name' => 'distrib_channel_name',
            'tb3.sales_division_id' => 'sales_division_id',
            'tb5.name' => 'sales_division_name',
            'tb1.term_of_payment' => 'term_of_payment',
            'tb1.top_dp' => 'top_dp',
            'tb1.tax_classification' => 'tax_classification',
            'tb1.pph22' => 'pph22',
            'tb1.top_dp_uom' => 'top_dp_uom',
            'tb7.name' => 'payment_method',
            'tb1.status' => 'status',
            'tb1.created_by' => 'created_by',
            'tb1.updated_by' => 'updated_by',
            'tb1.created_at' => 'created_at',
            'tb1.updated_at' => 'updated_at',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if ($exported) return $model->getFilteredQuery();
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query) {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query) {
        if ($request->get($value)) {
            if ($value === 'created_at' || $value === 'updated_at') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function show($id) {
        \LogActivity::addToLog('get sales area by id');

        $model = $this->findDataUuid(SalesArea::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function statusWapu(Request $request){
        \LogActivity::addToLog('get customer sales area detail');
        
        $customer_id = $request->get('customer_id');
        $sales_org_id = $request->get('sales_org_id');
        
        $data = CustomerSalesArea::getSalesAreaOrder($customer_id, $sales_org_id);

        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request, true);
        $data = $q->select(CustomerSalesArea::getExportedColumns())->get();
        return Excel::download((new Download($data)), "distributor.xls");
    }
}