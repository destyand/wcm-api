<?php

namespace App\Http\Controllers;

use App\Libraries\PaymentGateway;
use App\Libraries\RestClient;
use App\Models\Order;
use App\Rules\CustomerExistsRule;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PaymentGatewayController extends Controller
{

    public function __construct()
    {
        $this->middleware(["cors_origin"])->only(["checkLimit", "invoice"]);
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    }

    public function inquiry(Request $request, $bankName)
    {
        \LogActivity::addToLog('Get Inquiry ' . $bankName);
        try {
            $payment = $this->initializeClass("inquiry", $bankName);

            if (is_null($payment)) {
                throw new \Exception("Bank $bankName not found", 1);
            }

        }
        catch (\Exception $e) {
            return response($e->getMessage(), 404);
        }

        $rules = $payment->getValidationRule();
        $this->validate($request->all(), $rules);
        $response = $payment->response(collect($request->all()));

        $payment->writeLog($request, $response);

        $response = isset($response["sender"]) ? $response["response"] : $response;
        return response()->json($response);

    }

    public function payment(Request $request, $bankName)
    {
        \LogActivity::addToLog('Store Payment ' . $bankName);
        try {
            $payment = $this->initializeClass("payment", $bankName);

        }
        catch (\Exception $e) {
            return response(null, 404);
        }

        $rules = $payment->getValidationRule();

        $this->validate($request->all(), $rules);
        $response = $payment->response(collect($request->all()));
        $payment->writeLog($request, $response);

        $response = isset($response["sender"]) ? $response["response"] : $response;
        return response()->json($response);
    }

    public function checkLimit(Request $request, $bankName)
    {
        \LogActivity::addToLog('Check Limit ' . $bankName);
        try {

            $payment = $this->initializeClass("checkLimit", $bankName);

        }
        catch (\Exception $e) {

            return response($e->getMessage(), 404);

        }
        catch (\Throwable $t) {

            return response("Bank $bankName Not Found", 404);

        }

        $rules = [
            "sales_org_id" => 'required|exists:wcm_sales_org,id',
            'customer_id'  => ["required", new CustomerExistsRule],
        ];

        $this->validate($request->all(), $rules);

        try {
            $response = $payment->requestCheckLimit();
        }
        catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }


        return response()->json($response);
    }

    public function disbursement(Request $request, $bankName)
    {
        \LogActivity::addToLog('Disbursement ' . $bankName);
        try {

            $payment = $this->initializeClass("disbursement", $bankName);

        }
        catch (\Exception $e) {

            return response($e->getMessage(), 404);

        }
        catch (\Throwable $t) {

            return response("Bank $bankName Not Found", 404);

        }

        $rules = $payment->getParamDisbrusement();

        $this->validate($request->all(), $rules);

        try {
            $response = $payment->sendDisbursement(collect($request->all()), $bankName);
        }
        catch (\Exception $e) {
            if ($e) {
                return response()->json(responseFail($e->getMessage()), 500);
            }

        }
        $payment->writeLog($request, $response);
        $response = isset($response["sender"]) ? $response["response"] : $response;
        return response()->json($response);
    }

    public function invoice(Request $request, $bankName)
    {
        \LogActivity::addToLog('Seend Invoice ' . $bankName);
        try {

            $payment = $this->initializeClass("disbursement", $bankName);

        }
        catch (\Exception $e) {

            return response($e->getMessage(), 404);

        }
        catch (\Throwable $t) {
            return response("Bank $bankName Not Found", 404);
        }

        $rules = [
            'order_id' => 'required|exists:wcm_order_df_status,order_id',
        ];

        $this->validate($request->all(), $rules);

        $order = Order::findOrFail($request->get('order_id'));
        $payment->setOrder($order);

        if ($payment->getPaymentMethod() == $payment::DF) {
            $invoice = $orderDF = [];

            try {
                DB::transaction(function () use ($payment, $order, &$invoice, &$orderDF) {
                    $invoice = $payment->sendInvoice($order);
                    $orderDF = $payment->writeOrderDFStatus($invoice);

                });

                if ($invoice && Arr::get($invoice, "error_code", "9999") !== "0000") {
                    return response()->json($invoice, 500);
                }

                return response(
                    responseSuccess(
                        trans("messages.create-success"),
                        [
                            'order_id'         => @$order->id,
                            'send_order_count' => @$orderDF->send_order_count,
                        ]
                    )
                );

            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        return response(responseFail(trans("messages.create-fail")), 500);
    }

    /**
     * Initialized Class Payment Gateway
     *
     * @param <type> $type The type
     * @param <type> $bankName The bank name
     *
     * @return $payment intance class of PaymentGateway
     */
    private function initializeClass($type, $bankName)
    {
        $classBankName = "\App\Libraries\\" . Str::title($bankName);

        try {
            $payment = new $classBankName($type);

            if (!$payment instanceof PaymentGateway) {
                throw new \Exception("Error Processing Request", 1);
            }

            $paramAuth = $payment->getParamAuthorize(request());
            if ($payment->authorize(collect($paramAuth)) === false) {
                throw new \Exception("Invalid Request", 1);
            }

        }
        catch (\Exception $e) {
            throw $e;

        }
        catch (\Throwable $t) {
            throw $t;

        }

        return $payment;
    }

    public function reverse(Request $request, $bankName)
    {
        $rules = [
            "sales_org_id" => 'required|exists:wcm_sales_org,id',
            'customer_id'  => ["required", new CustomerExistsRule],
        ];

        $this->validate($request->all(), $rules);

        return $this->callHostToHost($request->all(), $bankName, "check-limit");
    }

    public function reverseInvoice(Request $request, $bankName)
    {
        \LogActivity::addToLog('Seend Invoice ' . $bankName);

        $rules = [
            'order_id' => 'required|exists:wcm_order_df_status,order_id',
        ];

        $this->validate($request->all(), $rules);

        return $this->callHostToHost($request->all(), $bankName, "invoice");
    }

    private function callHostToHost(array $request, $bankName, $endpoint)
    {
        $baseUrl = config("h2h.address");
        $path = sprintf("/api/payment_gateway/%s/%s", $bankName, $endpoint);
        $url = sprintf("http://%s%s", preg_replace("@/$@", "", $baseUrl), $path);

        $client = new RestClient();
        try {
            $result = $client->post($url, $request);
        }
        catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }

        return response()->json($result, 200);
    }

}
