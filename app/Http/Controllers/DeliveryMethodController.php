<?php

namespace App\Http\Controllers;

use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\DeliveryMethod;
use App\Models\PricingCondition;

class DeliveryMethodController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all delivery method');

        $model = Datatables::of(DeliveryMethod::query())
                ->filter(function($query) use ($request) {
                    $columns = ['name' => 'name'];
                    foreach ($columns as $key => $value) {
                        if ($request->has($value)) {
                            $query->where($key, 'like', "%{$request->get($value)}%");
                        }
                    }
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($id) {
        \LogActivity::addToLog('get delivery method by id');

        $model = $this->findDataUuid(DeliveryMethod::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function bySalesGroup($sales_group_id) {
        \LogActivity::addToLog('get delivery method by sales group id');

        $data = PricingCondition::getBySalesGroup($sales_group_id);
        if ($data) {
            $response = responseSuccess(trans('messages.read-success'), $data);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } else {
            $response = responseFail(trans('messages.read-fail'));
            return response()->json($response, 404, [], JSON_PRETTY_PRINT);
        }
    }

}
