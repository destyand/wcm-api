<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Excel;
use Illuminate\Support\Facades\Validator;
use App\Models\ContractGoverment;
use App\Models\ContractGovItem;
use Carbon\Carbon;

class PermentanItemController extends Controller
{

    public function index($uuid)
    {
        \LogActivity::addToLog('Get Permentan Item by uuid permentan nomer');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $where['tb1.contract_type'] = 'permentan';

        $model = ContractGovItem::getItemPermentan($where)->get();

        $datas = json_decode($model);

        $provinces = array_unique(array_column($datas, 'sales_office_id'));

        $uniq_produk = array_unique(array_column($datas, 'product_id'));

        $totals = [];

        foreach ($uniq_produk as $key => $value) {
            # code...
            $totals[$value]['id'] = $value;
            $totals[$value]['name'] = $datas[$key]->product_name;
            $totals[$value]['total'] = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $totals[$value]['month'][$j] = 0;
            }
        }

        $print = [];
        // Initial Total Di Provinsi

        // Provinces
        foreach ($provinces as $iprov => $prov) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_office_id'),  $prov);
            $prov_id = $datas[$iprov]->sales_office_id;
            $prov_name = $datas[$iprov]->provinsi;
            $globProv = $datas[$iprov];

            // Print Array Provinsi
            $provArr = array('id' => $prov_id, 'name' => $prov_name, 'product' => array());

            $products = $this->makeArr($datas, $keyItemsProv);
            // return response()->json($products);exit;
            $keyItemsProduct = array_unique(array_column($products, 'product_id'));

            foreach ($keyItemsProduct as $iprod => $vprod) {
                # code...
                $keyItemsProd = array_keys(array_column($products, 'product_id'),  $vprod);

                $product = $this->makeArr($products, $keyItemsProd);

                // Print Product Kabupaten Punya
                $prodArr = array('id' => $products[$iprod]->product_id, 'name' => $products[$iprod]->product_name, 'month' => array(), 'status_qty' => array());

                $totalMonth = 0;

                for ($i = 1; $i <= 12; $i++) {
                    # code...
                    $monKeyItems = array_keys(array_column($product, 'month'),  $i);
                    // return $monKeyItems;
                    // $statuss=
                    $qty = ($monKeyItems) ? (is_null($product[$monKeyItems[0]]->initial_qty) ? 0 : $product[$monKeyItems[0]]->initial_qty) : 0;
                    $status = ($monKeyItems) ? ($product[$monKeyItems[0]]->status ?: "-") : "-";
                    array_push($prodArr['month'], $qty);
                    array_push($prodArr['status_qty'], $status);

                    $totals[$products[$iprod]->product_id]['month'][$i] += $qty;
                    $totals[$products[$iprod]->product_id]['total'] += $qty;
                    $totalMonth += $qty;
                }




                $prodArr['total'] = $totalMonth;
                $prodArr['year'] = $product[0]->year;
                $prodArr['date'] = $product[0]->created_at;
                $prodArr['status'] = $product[0]->status_name;
                // $totals[$products[$iprod]->product_id]['sum']+=$totals[$products[$iprod]->product_id]['month'][$i];

                array_push($provArr['product'], $prodArr);
            }

            // Push Arr Prov To Print
            array_push($print, $provArr);
        }

        // make Total Global Provinsi
        $newTotals = [];
        foreach ($totals as $key => $value) {
            # code...
            array_push($newTotals, $value);
        }

        $prints = array(
            'provinsi' => $print,
            'totals' => $newTotals,
            'permentan_id' => @$datas[0]->contract_gov_id,
            'tahun' => @$datas[0]->year,
            'sales_org_id' => @$datas[0]->sales_org_id,
            'sales_org_name' => @$datas[0]->sales_name,
            'permentan_no' => @$datas[0]->number
        );

        $response = responseSuccess(trans('messages.read-success'), $prints);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function list(Request $request, $uuid = null)
    {
        \LogActivity::addToLog('get detail item permentan');
        is_uuid($uuid);;
        $where = array();
        $where['tb1.contract_type'] = 'permentan';
        $where['tb1.uuid'] = $uuid;

        $query = ContractGovItem::getDetailPermentan($where);
        $columns = [
            'tb3.id' => 'sales_org_id',
            'tb3.name' => 'sales_name',
            'tb4.name' => 'product_name',
            'tb2.initial_qty' => 'qty',
            'tb2.month ' => 'month',
            'tb2.year' => 'year',
            'tb2.create_at' => 'datetime',
            'tb2.status' => 'status'
        ];
        $model = DataTables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function single(Request $request, $uuid = null)
    {
        \LogActivity::addToLog('get detail item permentan');
        is_uuid($uuid);
        $where = array();
        $where['tb1.contract_type'] = 'permentan';
        $where['tb2.uuid'] = $uuid;

        $query = ContractGovItem::getDetailPermentan($where);
        $columns = [
            'tb3.id' => 'sales_org_id',
            'tb3.name' => 'sales_name',
            'tb4.name' => 'product_name',
            'tb2.initial_qty' => 'qty',
            'tb2.month ' => 'month',
            'tb2.year' => 'year',
            'tb2.create_at' => 'datetime',
            'tb2.status' => 'status'
        ];
        $model = DataTables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseSuccess(trans('messages.read-success'), $model->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create permentan item');

        $attributes = $request->only(['contract_gov_id', 'product_id', 'sales_office_id', 'year']);
        $this->validate($attributes, ['year' => 'required', 'contract_gov_id' => 'required|exists:wcm_contract_goverment,id', 'product_id' => 'required|exists:wcm_product,id', 'sales_office_id' => 'required|exists:wcm_sales_office,id']);

        $monthExists = ContractGovItem::where('contract_gov_id',$attributes['contract_gov_id'])
                        ->where('sales_office_id',$attributes['sales_office_id'])
                        ->where('product_id',$attributes['product_id'])
                        ->where('year',$attributes['year'])->exists();
        if ($monthExists) {
            DB::rollback();
            $response         = responseFail(['status' => ["Produk yang dipilih telah tersedia, silahkan update manual"]]);
            $response['data'] = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }

        $contract_gov = ContractGoverment::where('id', $attributes['contract_gov_id'])->where('contract_type', 'permentan')->firstorfail();
        $attrib = [
            'sales_org_id' => $contract_gov->sales_org_id,
            'rayonisasi' => [
                'sales_org_id'  => $contract_gov->sales_org_id,
                'kode_provinsi' => $attributes['sales_office_id'],
            ],

        ];
        $this->validate($attrib, ContractGovItem::PermentanStoreRule());

        $attributes['status'] = 'y';
        $attributes['active_date'] = Carbon::now()->toDateString();
        $attributes['created_by'] = Auth::user()->id;
        $attributes['created_at'] = now();
        $arr = [];
        for ($i = 1; $i <= 12; $i++) {
            # code...
            $attributes['month'] = $i;
            $attributes['initial_qty'] = 0;

            $this->checkpermentanOneStatusActive(['contract_gov_id' => $contract_gov->id, 'contract_type' => 'permentan', 'month' => $i, 'year' => $attributes['year'], 'product_id' => $attributes['product_id'], 'sales_office_id' => $attributes['sales_office_id'], 'sales_org_id' => $contract_gov->sales_org_id,]);

            array_push($arr, $attributes);
        }
        DB::beginTransaction();
        try {
            $permentan = ContractGovItem::insert($arr);
            $contract_gov->touchStatus();
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addToLog('update permentan');
        is_uuid($uuid);

        $attributes = $request->all();
        $this->validate($attributes, ContractGovItem::PermentanRuleUpdate());

        $detItem = [
            'contract_gov_id' => $attributes['contract_gov_id'],
            'product_id' => $attributes['product_id'],
            'sales_office_id' => $attributes['sales_office_id'],
            'month' => intval($attributes['month']),
            'year' => $attributes['year'],
        ];

        // cek item Duplicate
        $cekOnItem = $this->cekDuplikasiItemUpdate(ContractGovItem::class, $detItem, $uuid);


        if ($cekOnItem) {
            $error = [
                "contract_gov_id" => [trans('messages.duplicate')],
                "product_id" => [trans('messages.duplicate')],
                "sales_office_id" => [trans('messages.duplicate')],
                "month" => [trans('messages.duplicate')],
                "year" => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }


        $model = $this->findDataUuid(ContractGovItem::class, $uuid);
        if ($model['status'] == 'y') {

            DB::beginTransaction();
            try {
                $attributes['updated_by'] = Auth::user()->id;

                $model->update($attributes);
                DB::commit();
                $response = responseSuccess(trans('messages.update-success'), $model);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.update-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = "Can't update because status data";
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk permentan Item');

        $attributes = $request->all();
        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);
                if ($iv['status'] == 'y') {
                    $iv['active_date'] = Carbon::now()->toDateString();
                } else {
                    $iv['inactive_date'] = Carbon::now()->toDateString();
                }
                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(ContractGovItem::class, $iv['id']);
                unset($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);
        }


        return $data;
    }
    private function cekDuplikasiItem($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    private function cekDuplikasiItemUpdate($model, $attrib, $uuid)
    {

        $query = $model::where($attrib)
            ->where('uuid', '!=', $uuid);

        $check = $query->first();

        return $check;
    }

    private function checkpermentanOneStatusActive($attr)
    {
        $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', $attr['contract_type'])
            ->where('tb1.month', intval($attr['month']))
            ->where('tb2.year', $attr['year'])
            ->where('tb2.id', '!=', $attr['contract_gov_id'])
            ->where('tb1.product_id', $attr['product_id'])
            ->where('tb1.sales_office_id', $attr['sales_office_id'])
            ->where('tb2.sales_org_id', $attr['sales_org_id'])
            ->where('tb1.status', 'y')->exists();
        if ($query) {
            DB::rollback();
            $response           = responseFail(['status' => ["Produsen " . $attr["sales_org_id"] . " di tahun " . $attr["year"] . ", bulan " . $attr['month'] . " sudah ada yang aktif."]]);
            $response['data'] = $attr;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }
    }
}
