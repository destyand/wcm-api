<?php

namespace App\Http\Controllers;

use App\Models\BankDf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class BankDfController extends Controller
{

    //

    public function index(Request $request, $cust_uuid = null)
    {
        \LogActivity::addToLog('get all bank df');
        isset($cust_uuid) ? is_uuid($cust_uuid) : '';

        $where = array();
        if ($cust_uuid) {
            $where['tb2.uuid'] = $cust_uuid;
        }
        if ($this->isAdminAnper) {
            $where['tb1.sales_org_id'] = $this->salesOrgId;
        }
        if ($this->isDistributor) {
            $where['tb1.customer_id'] = $this->customerId;
        }

        $columns = [
            'tb2.uuid'       => 'customer_uuid',
            'tb2.full_name'  => 'customer_name',
            'tb3.name'       => 'produsen_name',
            'tb4.name'       => 'bank_name',
            'tb1.start_date' => 'start_date',
            'tb1.end_date'   => 'end_date',
            'tb1.status'     => 'status',
        ];

        $query = BankDf::getAll($where);

        $model = DataTables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addTolog('Insert Bank DF');

        $attributes = $request->only(['customer_id','sales_org_id','bank_id','start_date','end_date','status']);

        $attributes['created_by'] = Auth::user()->id;
        
        $this->validate($attributes, BankDf::ruleCreate());

        $msgDuplicate = $this->cekDuplicateBankDistributorInProdusen($attributes['customer_id'], $attributes['sales_org_id'], $attributes['bank_id'],$attributes['start_date'],$attributes['end_date']);
        
        if (!$msgDuplicate) {
            try {
                //code...
                $model = BankDf::create($attributes);
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $model);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                //throw $th;
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $error = [
                "sales_org_id" => [trans('messages.duplicate')],
                "customer_id"  => [trans('messages.duplicate')],
                "bank_id"      => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {

        \LogActivity::addToLog('Get Bank DF by uuid');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model             = BankDf::getAll($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {
            if ($value === 'start_date' || $value === 'end_date') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } else if ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addTolog('Update  Bank DF');

        $attributes = $request->only(['customer_id','sales_org_id','bank_id','start_date','end_date','status']);
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, BankDf::ruleUpdate());

        $model = $this->findDataUuid(BankDf::class, $uuid);

        $msgDuplicate = $this->cekDuplicateBankDistributorInProdusen($attributes['customer_id'], $attributes['sales_org_id'], $attributes['bank_id'],$attributes['start_date'],$attributes['end_date']);

        if (!$msgDuplicate) {

            DB::beginTransaction();
            try {
                $model->update($attributes);
                DB::commit();
                $response = responseSuccess(trans('messages.update-success'), $model);
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.update-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {

            $error = [
                "sales_org_id" => [trans('messages.duplicate')],
                "customer_id"  => [trans('messages.duplicate')],
                "bank_id"      => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function bulkStatus(Request $request)
    {
        \LogActivity::addToLog('update bulk status Bank DF');
        $attributes = $request->all();

        DB::beginTransaction();
        $rules = [
            'uuid' => 'required|exists:wcm_customer_bank_assg',
            'status' => 'required'
        ];
        
        foreach ($attributes as $iv) {
            is_uuid($iv['uuid']);
            $this->validate($iv, $rules);
            $model = $this->findDataUuid(BankDf::class, $iv['uuid']);
            unset($iv['uuid']);
            $iv['updated_by'] = Auth::user()->id;
            $model->update($iv);
        }
        
        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekDuplicateBankDistributorInProdusen($customer_id, $sales_org_id, $bank_id,$start_date,$end_date)
    {
        $model = BankDf::where('customer_id', $customer_id)
            ->where('sales_org_id', $sales_org_id)
            ->where('bank_id', $bank_id)
            ->whereBetween('start_date', [$start_date, $end_date])
            ->whereBetween('end_date', [$start_date, $end_date])
            ->where('status', 'y')
            ->exists();

        return $model;
    }

}
