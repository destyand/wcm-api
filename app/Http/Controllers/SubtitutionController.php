<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\Subtitution;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SubtitutionController extends Controller {

//

    public function index(Request $request, $cust_uuid = null, $exported=false) {
        \LogActivity::addToLog('get all subtitution');

        $where = array();
        if ($cust_uuid) {
            $where['tb2.uuid'] = $cust_uuid;
        }

        
        $query = Subtitution::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb5.sales_group_id",$filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb1.id' => 'id',
            'tb1.uuid' => 'uuid',
            'tb1.product_id' => 'product_id',
            'tb2.name' => 'product_name',
            'tb1.sales_org_id' => 'sales_org_id',
            'tb3.name' => 'sales_org_name',
            'tb1.plant_id' => 'plant_id',
            'tb5.name' => 'plant_name',
            'tb1.material_no' => 'material_no',
            'tb4.mat_desc' => 'material_name',
            'tb1.material_no_subtitution' => 'material_no_subtitution',
            'tb6.mat_desc' => 'material_name_subtitusi',
            'tb1.uom' => 'uom',
            'tb1.reason' => 'reason',
            'tb1.valid_from' => 'valid_from',
            'tb1.valid_to' => 'valid_to',
            'tb1.default' => 'default',
            'tb1.status' => 'status',
            'tb1.updated_at' => 'updated_at',
            'tb1.created_at' => 'created_at'
        ];

        $model = DataTables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if($exported) return $model->getFilteredQuery();

        $model = $model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($uuid) {

        \LogActivity::addToLog('Get Subtitusi by uuid');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model = Subtitution::getAll($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request) {

        \LogActivity::addToLog('Create Subtitution');

        $attributes=$request->all();
        $this->validate($attributes, Subtitution::ruleCreate());

        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;

        if($attributes['material_no'] == $attributes['material_no_subtitution']){
            $attributes['default'] = 'y';
            $attributes['valid_to']='9999-12-31';
        }
        $attributes['status'] = 'y';

        // Cek Duplicate Data Default
        $msgDuplicateDefault=$this->cekDuplicateDefault($attributes['product_id'],$attributes['sales_org_id'],$attributes['plant_id'],$attributes['material_no'],$attributes['material_no_subtitution']);

        if(count($msgDuplicateDefault)==0){

            // Cek Duplicate Data Active (Material And Status is Active)
            $msgDuplicateActiveSubtitution=$this->ceDuplicateDataActive($attributes);

            if($msgDuplicateActiveSubtitution){

                $response=responseFail(trans('messages.create-fail'));
                $response['errors']= array('msg'=>'Ada data masih active');
                return response()->json($response,500,[],JSON_PRETTY_PRINT);

            }else{
                // Insert When Data Duplicate not Empty && Data Not Duplicate Active
                DB::beginTransaction();
                try {
                    //code...
                    $model = Subtitution::create($attributes);
                    DB::commit();
                    $response = responseSuccess(trans('messages.create-success'), $model);
                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } catch (\Exception $ex) {
                    //Exc $ex;
                    DB::rollback();
                    $response = responseFail(trans('messages.create-fail'));
                    $response['errors'] = $ex->getMessage();
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            }
        }else{
            $response = responseFail(trans('messages.create-fail'));
            $response['message']=array('msg'=>'Duplicate data Default');
            return response()->json($response,500,[],JSON_PRETTY_PRINT);
        }

    }

    public function update (Request $request, $uuid) {
        \LogActivity::addToLog('Update Subtitution');

        $attributes = $request->only(['material_no_subtitution', 'uom', 'reason']);
        $attributes['updated_by'] = Auth::user()->id;

        $this->validate($attributes, Subtitution::ruleUpdate());

        $model = $this->findDataUuid(Subtitution::class, $uuid);

        DB::beginTransaction();
        try {
            //code...
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            //Exc $ex;
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['message'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk Subtitusi');

        $attributes = $request->all();
        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $i => $iv) {
                is_uuid($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model = $this->findDataUuid(Subtitution::class, $iv['id']);
                unset($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $attributes);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

     public function filterColumn($columns, $request, $query) {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query) {
        if ($request->get($value)) {
            if ($value === 'valid_from' || $value === 'valid_to') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } else if ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }


    private function cekDuplicateDefault($productid,$salesorg,$plantid,$material_no,$material_subtitution) {
        $model=Subtitution::select('id')
                            ->where('material_no',$material_no)
                            ->where('material_no_subtitution',$material_subtitution)
                            ->where('product_id',$productid)
                            ->where('sales_org_id',$salesorg)
                            ->where('plant_id',$plantid)
                            ->where('default','y')
                            ->get();

        return $model;
    }

    private function ceDuplicateDataActive($attributes){
        $params = array_only($attributes,["product_id","sales_org_id", "plant_id","material_no"]);

        $query  = Subtitution::where($params);

        if ($query->exists()) {

            $valid_from_param = array_get($attributes, "valid_from");

            return $query->whereRaw("'{$valid_from_param}' BETWEEN valid_from AND valid_to")->exists();
        }

        return false;
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,null, true);
        $data = $q->select(Subtitution::getExportedColumns())->get();
        return Excel::download((new Download($data)), "subtitution.xls");
    }

}
