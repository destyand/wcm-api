<?php

namespace App\Http\Controllers;

use App\models\ContractGovItem;
use App\models\ContractItem;
use App\models\ContractGoverment;
use App\Rules\SalesGroupRule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PerbupItemController extends Controller
{

    public function index($uuid_perbup)
    {
        \LogActivity::addToLog('get item perbup');

        isset($uuid_perbup) ? is_uuid($uuid_perbup) : '';
        $where = ['tb2.uuid' => $uuid_perbup];
        $model = ContractGovItem::getItemPerbup($where);
        $json  = $this->createJsonResponse($model->get()->toArray());

        $response = responseSuccess(trans('messages.read-success'), $json);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    private function createJsonResponse($datas)
    {
        $provinces  = array_unique(array_column($datas, 'sales_office_id'));
        $kabupatens = array_unique(array_column($datas, 'sales_group_id'));

        $uniq_produk = array_unique(array_column($datas, 'product_id'));

        $totals = [];

        foreach ($uniq_produk as $key => $value) {
            # code...
            $totals[$value]['id']    = $value;
            $totals[$value]['name']  = $datas[$key]->product_name;
            $totals[$value]['total'] = 0;
            for ($j = 1; $j <= 12; $j++) {
                # code...
                $totals[$value]['month'][$j] = 0;
            }
        }
        $dataProv = [];
        foreach ($provinces as $iprov => $prov) {
            $dataProv['id']   = $datas[$iprov]->sales_office_id;
            $dataProv['name'] = $datas[$iprov]->sales_office_name;
        }

        $print = [];
        // Initial Total Di Provinsi
        // Provinces
        foreach ($kabupatens as $ikab => $kab) {
            # code...
            $keyItemsProv = array_keys(array_column($datas, 'sales_group_id'), $kab);
            $kab_id       = $datas[$ikab]->sales_group_id;
            $kab_name     = $datas[$ikab]->sales_group_name;

            // Print Array Kabupaten
            $kabArr = array('id' => $kab_id, 'name' => $kab_name, 'kecamatan' => array());

            $keyItemsKec = array_keys(array_column($datas, 'sales_group_id'), $datas[$keyItemsProv[0]]->sales_group_id);

            // Make Arr Kecamatan
            $kecamatansData = $this->makeArr($datas, $keyItemsKec);
            $kecamatans     = array_unique(array_column($kecamatansData, 'sales_unit_id'));

            // Provinces
            foreach ($kecamatans as $ikec => $vkec) {
                # code...
                $keyItemsKec = array_keys(array_column($kecamatansData, 'sales_unit_id'), $vkec);
                $kec_id      = $kecamatansData[$ikec]->sales_unit_id;
                $kec_name    = $kecamatansData[$ikec]->sales_unit_name;

                // Print Array Kabupaten
                $kecArr = array('id' => $kec_id, 'name' => $kec_name, 'product' => array());

                // Make Arr Kabupaten
                $products = $this->makeArr($datas, $keyItemsKec);
                // return response()->json($products);exit;
                $keyItemsProduct = array_unique(array_column($products, 'product_id'));

                foreach ($keyItemsProduct as $iprod => $vprod) {
                    # code...
                    $keyItemsProd = array_keys(array_column($products, 'product_id'), $vprod);

                    $product = $this->makeArr($products, $keyItemsProd);

                    // Print Product Kabupaten Punya
                    $prodArr = array('id' => $products[$iprod]->product_id, 'name' => $products[$iprod]->product_name, 'month' => array(), 'status' => array());

                    $totalMonth = 0;

                    for ($i = 1; $i <= 12; $i++) {
                        # code...
                        $monKeyItems          = array_keys(array_column($product, 'month'), $i);
                        ($monKeyItems) ? $qty = is_null($product[$monKeyItems[0]]->initial_qty) ? 0 : $product[$monKeyItems[0]]->initial_qty : $qty = 0;

                        ($monKeyItems) ? $status = is_null($product[$monKeyItems[0]]->status) ? "-" : $product[$monKeyItems[0]]->status : $status = "";
                        array_push($prodArr['month'], $qty);
                        array_push($prodArr['status'], $status);

                        $totals[$products[$iprod]->product_id]['month'][$i] += $qty;
                        $totals[$products[$iprod]->product_id]['total'] += $qty;
                        $totalMonth += $qty;
                    }

                    $prodArr['total']  = $totalMonth;
                    $prodArr['year']   = $product[0]->year;
                    $prodArr['date']   = $product[0]->created_at;
                    // $totals[$products[$iprod]->product_id]['sum']+=$totals[$products[$iprod]->product_id]['month'][$i];

                    array_push($kecArr['product'], $prodArr);
                }
                // Make Arr Detail Product
                array_push($kabArr['kecamatan'], $kecArr);
            }
            // Push Arr Prov To Print
            array_push($print, $kabArr);
        }

        // make Total Global Provinsi
        $newTotals = [];
        foreach ($totals as $key => $value) {
            # code...
            array_push($newTotals, $value);
        }

        $prints = array("provinsi" => [['id' => $dataProv['id'], 'name' => $dataProv['name'], 'kabupaten' => $print, 'totals' => $newTotals, 'perbup_id' => $datas[0]->contract_gov_id, 'perbup_no' => $datas[0]->number]],
            'perbup_id' => @$datas[0]->contract_gov_id, 
            'perbup_no' => $datas[0]->number,
            'year'=> @$datas[0]->year,
            'sales_org_id'=>$datas[0]->sales_org_id,
            'sales_org_name'=>$datas[0]->sales_org_name,
        );

        return $prints;
    }

    public function store (Request $request)
    {
        \LogActivity::addToLog('create pergub item');

        $attributes = $request->only(['contract_gov_id', 'sales_office_id', 'sales_group_id', 'sales_unit_id','year', 'product_id']);

        $this->validate($attributes, [
            'contract_gov_id' => 'required|exists:wcm_contract_goverment,id',
            'product_id'      => 'required|exists:wcm_product,id',
            'sales_unit_id'   => 'required|exists:wcm_sales_unit,id',
            'sales_group_id'  => 'required|exists:wcm_sales_group,id',
            'sales_office_id' => 'required|exists:wcm_sales_office,id',
            'year'            => 'required|numeric',
            'initial_qty'     => 'numeric|min:0',
        ]);

        $contract = ContractGoverment::where('id',$attributes['contract_gov_id'])->where('contract_type','perbup')->first();
        
        if(!$contract)
        {
            $response = responseFail(['Data'=> ['Data Tidak Valid di Perbup']] );
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }


        $attrib =[
            'sales_org_id' => $contract->sales_org_id ,
            'rayonisasi' => [
                            'sales_org_id'  => $contract->sales_org_id,
                            'kode_provinsi' => $attributes['sales_office_id'],
                        ],
            'sales_group_id' => $attributes['sales_group_id'],

        ];
        $this->validate($attrib, ContractGovItem::rayonisasi_sales_org()+['sales_group_id' => new SalesGroupRule(@$attributes['sales_office_id']),]);
        
        $monthExists = ContractGovItem::where('contract_gov_id',$attributes['contract_gov_id'])
                        ->where('sales_group_id', $attributes['sales_group_id'])
                        ->where('sales_office_id',$attributes['sales_office_id'])
                        ->where('sales_unit_id',$attributes['sales_unit_id'])
                        ->where('product_id',$attributes['product_id'])
                        ->where('year',$attributes['year'])->exists();
        if ($monthExists) {
            DB::rollback();
            $response         = responseFail(['status' => ["Produk yang dipilih telah tersedia, silahkan update manual"]]);
            $response['data'] = $attributes;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }

        $attributes['status'] = 'y';
        $attributes['created_by'] = Auth::user()->id;
        $attributes['created_at'] = now();
        $arr = [];
        for ($i=1; $i <=12 ; $i++) { 
            # code...
            $attributes['month']=$i;
            $attributes['initial_qty']=0;

            $conItem = [
                'sales_org_id'    => $contract->sales_org_id,
                'contract_gov_id' => $attributes['contract_gov_id'],
                'product_id'      => $attributes['product_id'],
                'sales_office_id' => $attributes['sales_office_id'],
                'sales_group_id'  => $attributes['sales_group_id'],
                'sales_unit_id'   => $attributes['sales_unit_id'],
                'month'           => $attributes['month'],
                'year'            => $attributes['year'],
            ];

            $attr['perbup']  = $conItem; $attr['perbup']+=['contract_type'=>'perbup'];
            $attr['one_aktive'] = $attr['perbup']; 
            $this->validate($attr,ContractGovItem::headerSalesOrgPerbupStatus());
            array_push($arr, $attributes);
        }
        DB::beginTransaction();
        try {
            $pergub = ContractGovItem::insert($arr);
            $contract->touchStatus();
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $attributes);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function listItem(Request $request, $uuid_perbup)
    {
        \LogActivity::addToLog('get list item perbup');

        isset($uuid_perbup) ? is_uuid($uuid_perbup) : '';

        $where   = ['tb2.uuid' => $uuid_perbup];
        $query   = ContractGovItem::getItemPerbup($where);
        $columns = [
            'tb2.number'          => 'number',
            'tb2.sales_org_id'    => 'sales_org_id',
            'tb3.name'            => 'sales_org_name',
            'tb2.year'            => 'year',
            'tb1.month'           => 'month',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb4.name'            => 'sales_office_name',
            'tb1.sales_group_id'  => 'sales_group_id',
            'tb5.name'            => 'sales_group_name',
            'tb1.sales_unit_id'   => 'sales_unit_id',
            'tb7.name'            => 'sales_unit_name',
            'tb1.product_id'      => 'product_id',
            'tb6.name'            => 'product_name',
            'tb1.initial_qty'     => 'initial_qty',
            'tb1.status'          => 'status',
            'tb1.created_by'      => 'created_by',
            'tb1.updated_by'      => 'updated_by',
            'tb1.created_at'      => 'created_at',
            'tb1.updated_at'      => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function detail($uuid_perbup)
    {
        \LogActivity::addToLog('get detail item perbup');

        isset($uuid_perbup) ? is_uuid($uuid_perbup) : '';
        $where = ['tb1.uuid' => $uuid_perbup];
        $model = ContractGovItem::getItemPerbup($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addToLog('update item perbup');

        $attributes               = $request->except(['uuid', 'id']);
        $attributes['updated_by'] = Auth::user()->id;

        isset($uuid) ? is_uuid($uuid) : '';
        $this->validate($attributes, ContractGovItem::perbupRuleUpdate());
        $model = $this->findDataUuid(ContractGovItem::class, $uuid);
        $this->cekSPJBPerbupItem($model);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk perbup');

        $attributes = $request->all();
        DB::beginTransaction();

        foreach ($attributes as $i => $iv) {
            is_uuid($iv['id']);

            $model = $this->findDataUuid(ContractGovItem::class, $iv['id']);
            $cek   = $this->cekPerbupItemOnSPJB($model);
            if ($cek) {
                DB::rollback();
                $response         = responseFail(trans('messages.related'));
                $response['data'] = $iv;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
            unset($iv['id']);
            $iv['updated_by'] = Auth::user()->id;

            $model->update($iv);
        }
        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);
        }

        return $data;
    }

    private function cekSPJBPerbupItem($contract)
    {
        $data = $this->cekPerbupItemOnSPJB($contract);
        if ($data) {
            $response = responseFail(['fail_perbup'=>[trans('messages.related')]]);
            $return   = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    private function cekPerbupItemOnSPJB($contract)
    {
        $data = ContractItem::where('contract_gov_item_id', $contract->id)->first();
        return $data;
    }

    private function checkOneActiveInSalesUnit($itemRequest,$type)
    {
        // check update status aktif 1 product 1 kecamatan aktif type perbup
        $check = ContractGovItem::getItemPerbup(['tb1.year'=>$itemRequest['year'],'tb1.product_id'=>$itemRequest['product_id'],'tb1.month'=> intval($itemRequest['month']),'tb1.status'=>'y','tb1.sales_unit_id'=>$itemRequest['sales_unit_id']])
        ->where('tb1.contract_gov_id','!=',$itemRequest['contract_gov_id'])
        ->first();
        if($check)
        {
            DB::rollback();
            $response           = responseFail(["status"=>["Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"]]);
            $response['errors'] = ["status"=>["Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"]];
            $response['data'] = $check;
            $resp=response()->json($response, 500, [], JSON_PRETTY_PRINT);
            $resp->throwResponse();
        }
        
    }

}
