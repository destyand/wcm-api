<?php

namespace App\Http\Controllers;

use App\models\ContractGoverment;
use App\Models\ContractGovItem;
use App\Rules\PermentanRule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;

class ChangeRequestController extends Controller
{
    //
    public function index(Request $request, $type, $uuid)
    {
        $tables = [
            "permentan" => "wcm_cr_permentan",
            "pergub"    => "wcm_cr_pergub",
            "perbup"    => "wcm_cr_perbup",
        ];

        if (in_array($type, array_keys($tables))) {
            $query = DB::table($tables[$type])->where("uuid", $uuid);

            return DataTables::of($query)->make(true);
        }

        abort(404);
    }

    public function update(Request $request, $type, $uuid)
    {   
        $inputs = $request->all();
        $rets = [];
        $line = 0;
        $flag = true;$status_code=200;
        $warning=[];
        DB::beginTransaction();
        foreach($inputs as $input) {
            $input = ($input instanceof Collection) ?: collect($input);
            $line = $input->get("id", ++$line);
            $itemsValidator = [
                "sales_office_id" => "required",
                "product_id"      => "required",
                "month"           => "required",
                "year"            => "required",
                "initial_qty"     => "required|numeric",
            ];
            
            $itemsCondition = $input->only([
                "product_id", "month", "year", "initial_qty", "sales_office_id",
            ])->toArray();
    
            if ($type == "pergub") {
                $itemsValidator["sales_group_id"] = "required";
                $itemsCondition["sales_group_id"] = $input->get("sales_group_id");
            }
    
            if ($type == "perbup") {
                $itemsValidator["sales_group_id"] = "required";
                $itemsCondition["sales_group_id"] = $input->get("sales_group_id");
                $itemsValidator["sales_unit_id"]  = "required";
                $itemsCondition["sales_unit_id"]  = strval($input->get("sales_unit_id"));
            }
    
            $validator = Validator::make($input->all(), $itemsValidator);

            try {
                if ($validator->fails()) {
                    throw new Exception($validator->errors());
                }

                $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();
                if ($type !== $contract->contract_type) {
                    throw new Exception("Invalid Services", 1);
                }
    
                $items = $contract->items()->where(Arr::except($itemsCondition, 'initial_qty'))->get();

    
                if ($items->count() > 1) {
                    throw new Exception("Invalid Condition", 1);
                }
    
                if (!$items->isEmpty()) {
                    $item              = $items->first();

                    if($item->initial_qty==0 && $type=='pergub')
                    {
                        $attPerGub=['contract_type'=>$type,'month'=>$itemsCondition['month'],'year'=>$itemsCondition['year'],'product_id'=>$itemsCondition['product_id'],'sales_org_id'=>$contract->sales_org_id,'sales_office_id'=>$itemsCondition['sales_office_id'],'sales_group_id'=>$itemsCondition['sales_group_id']];
                        $query = DB::table('wcm_contract_gov_item AS tb1')
                                    ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
                                    ->where('tb2.contract_type', $attPerGub['contract_type'])
                                    ->where('tb1.month', intval($attPerGub['month']))
                                    ->where('tb2.year', $attPerGub['year'])
                                    ->where('tb1.product_id', $attPerGub['product_id'])
                                    ->where('tb1.status','y')
                                    ->where('tb2.sales_org_id',$attPerGub['sales_org_id'])
                                    ->where('tb1.sales_office_id',$attPerGub['sales_office_id'])
                                    ->where('tb1.sales_group_id',$attPerGub['sales_group_id'])
                                    ->where('tb2.uuid','!=',$uuid)->exists();
                        if($query) {
                            DB::rollback();
                            $response           = responseFail("Produsen ".$attPerGub["sales_org_id"]." Provinsi :".$attPerGub['sales_office_id']." , Kabupaten : ".$attPerGub['sales_group_id'].", di tahun ".$attPerGub["year"].", bulan ".$attPerGub['month'].", Produk : ".$attPerGub['product_id']." sudah ada yang aktif.");
                            $response['message'] = ['gagal_update' => "Produsen ".$attPerGub["sales_org_id"]." Provinsi :".$attPerGub['sales_office_id']." , Kabupaten : ".$attPerGub['sales_group_id'].", di tahun ".$attPerGub["year"].", bulan ".$attPerGub['month'].", Produk : ".$attPerGub['product_id']." sudah ada yang aktif."];
                            $response['data'] = $attPerGub;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }

                    if($item->initial_qty==0 && $type=='perbup')
                    {
                        $check = ContractGovItem::getItemPerbup(['tb1.year'=>$itemsCondition['year'],'tb1.product_id'=>$itemsCondition['product_id'],'tb1.month'=> intval($itemsCondition['month']),'tb1.status'=>'y'])->where('tb1.sales_unit_id','like',$itemsCondition['sales_unit_id'])->where('tb2.uuid','!=',$uuid)->first();
                        if($check)
                        {
                            DB::rollback();
                            $response           = responseFail("Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}");
                            $response['message'] = ['gagal_update' => "Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"];
                            $response['data'] = $check;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }

                    $item->update(['initial_qty'=> $itemsCondition['initial_qty'],'updated_by'=>Auth::user()->id]);
                } else {
                    if($type=='pergub')
                    {
                        $attPerGub=['contract_type'=>$type,'month'=>$itemsCondition['month'],'year'=>$itemsCondition['year'],'product_id'=>$itemsCondition['product_id'],'sales_org_id'=>$contract->sales_org_id,'sales_office_id'=>$itemsCondition['sales_office_id'],'sales_group_id'=>$itemsCondition['sales_group_id']];
                        $query = DB::table('wcm_contract_gov_item AS tb1')
                                    ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
                                    ->where('tb2.contract_type', $attPerGub['contract_type'])
                                    ->where('tb1.month', intval($attPerGub['month']))
                                    ->where('tb2.year', $attPerGub['year'])
                                    ->where('tb1.product_id', $attPerGub['product_id'])
                                    ->where('tb1.status','y')
                                    ->where('tb2.sales_org_id',$attPerGub['sales_org_id'])
                                    ->where('tb1.sales_office_id',$attPerGub['sales_office_id'])
                                    ->where('tb1.sales_group_id',$attPerGub['sales_group_id'])->exists();
                        if($query) {
                            DB::rollback();
                            $response           = responseFail("Produsen ".$attPerGub["sales_org_id"]." Provinsi :".$attPerGub['sales_office_id']." , Kabupaten : ".$attPerGub['sales_group_id'].", di tahun ".$attPerGub["year"].", bulan ".$attPerGub['month'].", Produk : ".$attPerGub['product_id']." sudah ada yang aktif.");
                            $response['message'] = ['gagal_update' => "Produsen ".$attPerGub["sales_org_id"]." Provinsi :".$attPerGub['sales_office_id']." , Kabupaten : ".$attPerGub['sales_group_id'].", di tahun ".$attPerGub["year"].", bulan ".$attPerGub['month'].", Produk : ".$attPerGub['product_id']." sudah ada yang aktif."];
                            $response['data'] = $attPerGub;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                    // check update status aktif 1 product 1 kecamatan aktif type perbup
                    if($type=='perbup')
                    {
                        $check = ContractGovItem::getItemPerbup(['tb1.year'=>$itemsCondition['year'],'tb1.product_id'=>$itemsCondition['product_id'],'tb1.month'=> intval($itemsCondition['month']),'tb1.status'=>'y','tb1.sales_unit_id'=>$itemsCondition['sales_unit_id']])->where('tb2.uuid','!=',$uuid)->first();
                        if($check)
                        {
                            DB::rollback();
                            $response           = responseFail("Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}");
                            $response['message'] = ['gagal_update' => "Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"];
                            $response['data'] = $check;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                    // check update status aktif 1 product 1 kecamatan aktif type perbup

                    $itemsCondition["contract_gov_id"] = $contract->id;
                    $itemsCondition["status"] = "y";
                    $itemsCondition['month']  = intval($itemsCondition['month']);
                    $itemsCondition['created_by'] = Auth::user()->id;
                    $item  = $contract->items()->insert($itemsCondition);
                }

                if($type=="pergub")
                {
                    $sumpermentan =$this->sumqty('permentan',$itemsCondition+=['sales_org_id'=>$contract->sales_org_id]);
                    $sumpergub =$this->sumqty('pergub',$itemsCondition+=['sales_org_id'=>$contract->sales_org_id]);
                    if($sumpermentan<$sumpergub)
                    {
                        array_push($warning,['Jumlah Alokasi Provinsi '.$itemsCondition['sales_office_id'].' Sales Org '.$contract->sales_org_id.', Bulan '.$itemsCondition['month'].' melebihi jumlah permentan']);
                    }
                }

                if($type=="perbup")
                {
                    $sumpergub=$this->sumqty('pergub',$itemsCondition+=['sales_org_id'=>$contract->sales_org_id],true);
                    $sumperbup=$this->sumqty('perbup',$itemsCondition+=['sales_org_id'=>$contract->sales_org_id]);
                    if($sumpergub<$sumperbup)
                    {
                         array_push($warning,['Jumlah Alokasi Kecamatan '.$itemsCondition['sales_unit_id'].', Kabupaten: '.$itemsCondition['sales_group_id'].' Provinsi : '.$itemsCondition['sales_office_id'].' Sales Org '.$itemsCondition['sales_org_id'].', Bulan '.$itemsCondition['month'].' melebihi jumlah pergub.']);
                    }
                }

                $rets[$line] = [
                    "error" => null,
                    "success" => true,
                    "data" => $input,
                ];
    
            } catch (\Exception $e) {
                $flag=false;$status_code=500;
                $rets[$line] = [
                    "error" => $e->getMessage(),
                    "success" => false,
                    "data" => $input,
                ];
            }
        }

        if ($line == 0) {
            return response(null, 400);
        }

        if(!$flag)
        {
            DB::rollback();
            $resp = responseFail(trans('messages.create-fail'));
            $resp['data'] = $rets;
            return response()->json($resp, $status_code, [], JSON_PRETTY_PRINT);
        }

        DB::commit();   
        $resp =responseSuccess(trans('updated-success'),$item);
        $resp['warning']=$warning;
        return response()->json($resp,$status_code,[], JSON_PRETTY_PRINT);

    }

    // public function updateStatus(Request $request, $type, $uuid)
    // {
    //     $response = array();
    //     $index = 0; $flag = true; $status_code=200;

    //     DB::beginTransaction();

    //     foreach($request->all() as $req) {
    //         $index++;
    //         $itemRequest = collect($req);
    //         $itemsValidator = [
    //             "sales_office_id" => "required",
    //             "product_id"      => "required",
    //             "month"           => "required",
    //             "year"            => "required",
    //             "status"          => "required|in:y,n",
    //         ];
    
    //         $itemsCondition = $itemRequest->only([
    //             "product_id", "month", "year", "status", "sales_office_id",
    //         ])->toArray();
    
    //         if ($type === "pergub") {
    //             $itemsValidator["sales_group_id"] = "required";
    //             $itemsCondition["sales_group_id"] = $itemRequest->get("sales_group_id");
    //         }
    
    //         if ($type === "perbup") {
    //             $itemsValidator["sales_group_id"] = "required";
    //             $itemsCondition["sales_group_id"] = $itemRequest->get("sales_group_id");
    //             $itemsValidator["sales_unit_id"]  = "required";
    //             $itemsCondition["sales_unit_id"]  = $itemRequest->get("sales_unit_id");
    //         }
         
    //         $validator = Validator::make($itemRequest->all(),$itemsValidator);

    //         if ($validator->fails()) {
    //             $flag=false; $status_code=400;
    //             $response[$index] = array(
    //                 "success" => false,
    //                 "validator" => $validator->errors(),
    //             );
    //             continue;
    //         }

    //         try {

    //             $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();
    //             if ($type !== $contract->contract_type) {
    //                 throw new \Exception("Invalid Services", 1);
    //             }

    //             if($type=="permentan")
    //             {
    //                 $this->checkpermentanOneStatusActive(['contract_type'=> 'permentan',
    //                         'month'=> intval($itemRequest['month']),
    //                         'year'=> $itemRequest['year'],
    //                         'product_id'=> $itemRequest['product_id'],
    //                         'sales_office_id'=> $itemRequest['sales_office_id'],
    //                         'sales_org_id'=> $contract->sales_org_id,
    //                 ]);
                   
    //                 // return $attr;
    //             }

    //             // check update status aktif 1 product 1 kecamatan aktif type perbup
    //             if($itemRequest['status']=='y' and $type=='perbup')
    //             {
    //                 $check = ContractGovItem::getItemPerbup(['tb1.year'=>$itemRequest['year'],'tb1.product_id'=>$itemRequest['product_id'],'tb1.month'=> intval($itemRequest['month']),'tb1.status'=>'y','tb1.sales_unit_id'=>$itemRequest['sales_unit_id']])->where('tb2.uuid','!=',$uuid)->first();
    //                 if($check)
    //                 {
    //                     DB::rollback();
    //                     $response           = responseFail("Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}");
    //                     $response['errors'] = ['Gagal Update' => "Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"];
    //                     $response['data'] = $check;
    //                     return response()->json($response, 500, [], JSON_PRETTY_PRINT);
    //                 }
    //             }
    //             // check update status aktif 1 product 1 kecamatan aktif type perbup
    
    //             $items = $contract->items()->where(Arr::except($itemsCondition, 'status'))->get();

    //             // return $items;
    
    //             if ($items->count() > 1) {
    //                 $status_code = 400;
    //                 throw new \Exception("Invalid Condition", 1);
    //             }
    
    //             if (!$items->isEmpty()) {
    //                 $item              = $items->first();
    //                 $item->status      = $itemsCondition['status'];
    //                 $item->save();
    //             } else {
    //                 $itemsCondition["initial_qty"] = 0;
    //                 $item= $contract->items()->create($itemsCondition);
    //             }

    //             $response[$index] = array(
    //                 "success" => true,
    //                 "error" => null,
    //             );
    //         } catch (\Exception $e) {
    //             $flag=false; $status_code=500;
    //             $response[$index] = array(
    //                 "success" => false,
    //                 "error" => $e->getMessage()
    //             );
    //         }
    //     }

    //     if(!$flag)
    //     {
    //         DB::rollback();
    //         $resp = responseFail(trans('messages.create-fail'));
    //         $resp['data'] = $response;
    //         return response()->json($resp, $status_code, [], JSON_PRETTY_PRINT);
    //     }

    //     DB::commit();   

    //     return response()->json(responseSuccess(trans('updated-success'),$response),$status_code,[], JSON_PRETTY_PRINT);

    // }


    public function updateStatus(Request $request, $type, $uuid)
    {
        $response = array();
        $index = 0; $flag = true; $status_code=200;

        DB::beginTransaction();

        foreach($request->all() as $req) {
            $index++;
            $itemRequest = collect($req);
            $itemsValidator = [
                "sales_office_id" => "required",
                "product_id"      => "required",
                "month"           => "required",
                "year"            => "required",
                "status"          => "required|in:y,n",
            ];
    
            $itemsCondition = $itemRequest->only([
                "product_id", "month", "year", "status", "sales_office_id",
            ])->toArray();
    
            if ($type === "pergub") {
                $itemsValidator["sales_group_id"] = "required";
                $itemsCondition["sales_group_id"] = $itemRequest->get("sales_group_id");
            }
    
            if ($type === "perbup") {
                $itemsValidator["sales_group_id"] = "required";
                $itemsCondition["sales_group_id"] = $itemRequest->get("sales_group_id");
                $itemsValidator["sales_unit_id"]  = "required";
                $itemsCondition["sales_unit_id"]  = $itemRequest->get("sales_unit_id");
            }
         
            $this->validate($itemRequest->all(),$itemsValidator);

            $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();
            if ($type !== $contract->contract_type) {
                DB::rollback();
                $response           = responseFail(['service'=>'Invalid Services']);
                $response['data'] = $req;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
            }

            if($itemRequest['status']=='y' and $type=="permentan")
            {
                $this->checkpermentanOneStatusActive(['contract_gov_id'=>$contract->id,'contract_type'=> 'permentan','month'=> intval($itemRequest['month']),'year'=> $itemRequest['year'],'product_id'=> $itemRequest['product_id'],'sales_office_id'=> $itemRequest['sales_office_id'],'sales_org_id'=> $contract->sales_org_id,]);
            }

            if($itemRequest['status']=='y' and $type=="pergub")
            {
                $attr['pergub']  = [
                    'contract_type'=> 'pergub',
                    'sales_org_id' => $contract->sales_org_id,
                    'year'         => $contract->year,
                    'product_id'   => $itemRequest['product_id'],
                    'month' => intval($itemRequest['month']),
                    'sales_office_id' => $itemRequest['sales_office_id'],
                    'sales_group_id' => $itemRequest['sales_group_id'],
                ];
               $this->validate($attr,ContractGovItem::headerSalesOrgPergubStatus());
            }

            if($itemRequest['status']=='y' and $type=="perbup")
            {
                $attr['perbup']  = [
                    'contract_type'=> 'perbup',
                    'sales_org_id' => $contract->sales_org_id,
                    'year'         => $contract->year,
                    'product_id'   => $itemRequest['product_id'],
                    'month' => intval($itemRequest['month']),
                    'sales_office_id' => $itemRequest['sales_office_id'],
                    'sales_group_id' => $itemRequest['sales_group_id'],
                    'sales_unit_id' => $itemRequest['sales_unit_id'],
                ];
                $attr['one_aktive'] = $attr['perbup'];
               $this->validate($attr,ContractGovItem::headerSalesOrgPerbupStatus());
            }

            $items = $contract->items()->where(Arr::except($itemsCondition, 'status'))->get();

            if ($items->count() > 1) {
                DB::rollback();
                $response           = responseFail(['service'=> ['Invalid Contition Data, Data ada Double di Bulan yang dipilih']]);
                $response['data'] = $req;
                return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
            }

            if (!$items->isEmpty()) {
                $item = $items->first();
                $item->update(['status'=>$itemsCondition['status'],'updated_by'=>Auth::user()->id]);
            } else {
                $itemsCondition["initial_qty"] = 0;
                $itemsCondition["created_by"] = Auth::user()->id;
                $itemsCondition["created_at"] = now();
                $itemsCondition["contract_gov_id"] = $contract->id;
                $itemsCondition["month"] = intval($itemsCondition["month"]);
                $item= $contract->items()->insert($itemsCondition);
            } 
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $itemsCondition);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    private function sumqty($type,$value,$selectperbup=false)
    {
        $sum = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', $type)
            ->where('tb1.month', intval($value['month']))
            ->where('tb2.year', $value['year'])
            ->where('tb1.product_id', $value['product_id'])
            ->where('tb1.status', 'y')
            ->where('tb2.sales_org_id', $value['sales_org_id'])
            ->where('tb1.sales_office_id',$value['sales_office_id']);
            if($type=="perbup"){
                $sum=$sum->where('tb1.sales_group_id',$value['sales_group_id']);
            }
            if($selectperbup==true AND  $type=='pergub')
            {
                $sum=$sum->where('tb1.sales_group_id',$value['sales_group_id']);
            }

        $sum=$sum->sum('tb1.initial_qty');
        return $sum;
    }



    private function checkpermentanOneStatusActive($attr)
    {
        $query = DB::table('wcm_contract_gov_item AS tb1')
                ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
                ->where('tb2.contract_type', $attr['contract_type'])
                ->where('tb1.month', intval($attr['month']))
                ->where('tb2.year', $attr['year'])
                ->where('tb2.id','!=',$attr['contract_gov_id'])
                ->where('tb1.product_id', $attr['product_id'])
                ->where('tb1.sales_office_id',$attr['sales_office_id'])
                ->where('tb2.sales_org_id',$attr['sales_org_id'])
                ->where('tb1.status','y')->exists();
        if($query)
        {
            DB::rollback();
            $response           = responseFail(['status'=>["Produsen ".$attr["sales_org_id"]." di tahun ".$attr["year"].", bulan ".$attr['month']." Product : ".$attr['product_id']." sudah ada yang aktif."]]);
            $response['data'] = $attr;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT)->throwResponse();
        }
    }
}
