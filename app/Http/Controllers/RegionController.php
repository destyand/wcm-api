<?php

namespace App\Http\Controllers;

use App\Models\SalesOffice;
use App\Models\SalesGroup;
use App\Models\SalesUnit;
use App\Models\SalesArea;
use App\Models\SalesOfficeAssign;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegionController extends Controller {

    public function Provinsi(Request $request) {
        \LogActivity::addToLog('get provinsi');
        $attributes = $request->all();
        $query = SalesOffice::select('*')->whereRaw("id != 0001");

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $salesarea = SalesArea::whereIn('sales_org_id',$filters['sales_org_id'])->get()->pluck('id');
                $officeAssign =SalesOfficeAssign::whereIn('sales_area_id',$salesarea)->get()->pluck('sales_office_id')->unique();
                $query->whereIn("id", $officeAssign);
            }

            if ( isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $sales_office_arr= SalesGroup::whereIn("id", $filters["sales_group_id"])->pluck('sales_office_id')->unique();
                $query->whereIn("id", $sales_office_arr );
            }
        }

        if($request->get('type')=="rdkk")
        {
            $this->validate($attributes, [
                'customer_id'  => 'required|exists:wcm_customer,id',
            ]);
            $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb1.status'=>'y','tb2.contract_type'=>'asal','tb1.year'=>date('Y')])
                ->select('tb1.sales_office_id')
                ->distinct()->get()->pluck('sales_office_id');

            $query->whereIn('id', $spjb)->where('status','y');

        }

        $columns = [
            'name' => 'name',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function Kabupaten(Request $request) {
        \LogActivity::addToLog('get kabupaten');
        $attributes=$request->all();

        $query = SalesGroup::select('*');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if (count($filters) > 0) {
            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("id", $filters["sales_group_id"] );
            }
        }

        if($request->get('type')=='relasi'){
            $this->validate($attributes, [
                'sales_org_id' => 'required|exists:wcm_sales_org,id',
                'customer_id'  => 'required|exists:wcm_customer,id',
            ]);

            $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb2.sales_org_id'=>$attributes['sales_org_id'],'tb2.contract_type'=>'asal','tb1.status'=>'y','tb1.year'=>date('Y')])
                ->select('tb1.sales_group_id')
                ->distinct()->get()->pluck('sales_group_id');

            $query->whereIn('id', $spjb)->where('status','y');

        }

        if($request->get('type')=="rdkk")
        {
            $this->validate($attributes, [
                'customer_id'  => 'required|exists:wcm_customer,id',
            ]);
            $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb1.status'=>'y','tb2.contract_type'=>'asal','tb1.year'=>date('Y')])
                ->select('tb1.sales_group_id')
                ->distinct()->get()->pluck('sales_group_id');

            $query->whereIn('id', $spjb)->where('status','y');

        }

        $columns = [
            'name' => 'name',
            'sales_office_id' => 'provinsi'
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function Kecamatan(Request $request) {
        \LogActivity::addToLog('get kecamatan');
        $attributes=$request->all();
        $query = SalesUnit::select('*');
        
        $user = $request->user();
        $filters = $user->filterRegional;
        
        if (count($filters) > 0) {
            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("sales_group_id", $filters["sales_group_id"] );
            }
        }

        if($request->get('type')=='relasi'){
           
            $this->validate($attributes, [
                'sales_org_id' => 'required|exists:wcm_sales_org,id',
                'customer_id'  => 'required|exists:wcm_customer,id',
            ]);

            $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb2.sales_org_id'=>$attributes['sales_org_id'],'tb2.contract_type'=>'asal','tb1.status'=>'y','tb1.year'=>date('Y')])
                ->select('tb1.sales_unit_id')
                ->distinct()->get()->pluck('sales_unit_id');

            $query->whereIn('id', $spjb)->where('status','y');

        }

        if($request->get('type')=="rdkk")
        {
            $this->validate($attributes, [
                'customer_id'  => 'required|exists:wcm_customer,id',
            ]);
            $spjb=DB::table('wcm_contract_item AS tb1')
                ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
                ->where(['tb2.customer_id'=> $attributes['customer_id'],'tb1.status'=>'y','tb2.contract_type'=>'asal','tb1.year'=>date('Y')])
                ->where('tb1.month', intval(date('m')))
                ->select('tb1.sales_unit_id')
                ->distinct()->get()->pluck('sales_unit_id');

            $query->whereIn('id', $spjb)->where('status','y');

        }

        $columns = [
            'name' => 'name',
            'sales_group_id' => 'kabupaten'
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }


    public function prov_sales_org_assg(Request $request)
    {
        $attributes = $request->all();
        $query = SalesOffice::select('*')->whereRaw("id != 0001");
        $columns = [
            'name' => 'name',
        ];
        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function kab_prov_selected(Request $request)
    {
        $attributes= $request->only('sales_office_id');

        $rules = [
            'sales_office_id' => 'required'
        ];

        $this->validate($attributes, $rules);

        $sales_group=explode(';', $request->get('sales_office_id'));

        $data=SalesGroup::select('id','name')
                        ->whereIn('sales_office_id',$sales_group)
                        ->get();

        $response = responseSuccess(trans('messages.read-success'), $data);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
