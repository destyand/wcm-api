<?php

namespace App\Http\Controllers;

use App\Exports\PermentanExport;
use App\Exports\PermentanDownload;
use App\Imports\PermentanImport;
use App\Models\ContractGoverment;
use App\Models\ContractGovItem;
use App\Traits\TraitContractGoverment;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class PermentanController extends Controller
{
    // use TraitContractGoverment;
    public function index(Request $request,$exported=false)
    {
        \LogActivity::addToLog('get all permentan group');

        $where                      = array();
        $where['tb1.contract_type'] = 'permentan';
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        
        $query = ContractGoverment::getAllPermentan($where,$exported);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }
        }
        $columns                    = [
            'tb1.number'     => 'number',
            'tb3.name'       => 'produsen',
            'tb1.year'       => 'year',
            'tb1.created_at' => 'created_at',
            'tb1.status'     => 'status',
        ];

        $model = DataTables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });

        if ($exported) {
            return $model->getFilteredQuery()->get();
        }

        $model=$model->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $uuid)
    {
        \LogActivity::addToLog('update permentan header');
        is_uuid($uuid);

        $attributes = $request->all();
        $this->validate($attributes, ContractGoverment::PermentanRuleUpdate());

        $detItem = [
            'contract_gov_id' => $attributes['contract_gov_id'],
            'product_id'      => $attributes['product_id'],
            'sales_office_id' => $attributes['sales_office_id'],
            'month'           => $attributes['month'],
            'year'            => $attributes['year'],
        ];

        // cek item Duplicate
        $cekOnItem = $this->cekDuplikasiItem(ContractGovItem::class, $detItem);

        if ($cekOnItem) {
            $error = [
                "contract_gov_id" => [trans('messages.duplicate')],
                "product_id"      => [trans('messages.duplicate')],
                "sales_office_id" => [trans('messages.duplicate')],
                "month"           => [trans('messages.duplicate')],
                "year"            => [trans('messages.duplicate')],
            ];
            $response = responseFail($error);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $model = $this->findDataUuid(ContractGoverment::class, $uuid);

        DB::beginTransaction();
        try {
            $attributes['updated_by'] = Auth::user()->id;

            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getImport(Request $request)
    {
        \LogActivity::addToLog('get import permentan');
        $attributes = $request->all();
        $this->validateImport($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $user = $request->user();
        $filters = $user->filterRegional;

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new PermentanImport, $file)[0];
            $cekKeys = array_keys(@$data[0]);
            
            if($cekKeys[0]!="sales_organization" or $cekKeys[1]!="no_doc" or $cekKeys[2]!="kode_provinsi" or $cekKeys[3]!="tahun" or $cekKeys[4] !="bulan" or $cekKeys[5]!="kode_jenis_produk" or $cekKeys[6]!="amount_alokasi"){
                $response           = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template'=>['Format template Tidak Sesuai'],'file'=>['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            foreach ($data as $key => $value) {
                # code...
               
                $headerData = [
                    'number'        => $value['no_doc'],
                    'contract_type' => 'permentan',
                    'sales_org_id'  => $value['sales_organization'],
                    'year'          => $value['tahun'],
                ];
                /*Find Header kecuali status n*/
                $header = ContractGoverment::where($headerData)->where('status', '!=', 'n')->first();
                if (!$header) {
                    $headerData += [
                        'rayonisasi' => [
                            'sales_org_id'  => $value['sales_organization'],
                            'kode_provinsi' => sprintf('%04s', $value['kode_provinsi']),
                        ],
                        'permentan'  => [
                            'sales_org_id' => $value['sales_organization'],
                            'year'         => $value['tahun'],
                            'product_id'   => $value['kode_jenis_produk'],
                            'sales_office_id' => sprintf('%04s', $value['kode_provinsi']),
                        ],
                    ];
                    $this->validateImportXls($headerData, ContractGoverment::PermentanRuleCreate(),($key+2));
                    // $uniqueHeader = [
                    //     'year' => $value['tahun'],
                    //     'contract_type' => 'permentan',
                    //     'status' => 'y',
                    // ];
    
                    // $uniqueItems = [
                    //     "product_id" => $value['kode_jenis_produk'],
                    //     'sales_office_id' => sprintf('%04s', $value['kode_provinsi']),
                    // ];

                    // $existsOrg = ContractGoverment::where($uniqueHeader)
                    //         ->whereHas("items", function($q) use($uniqueItems) {
                    //             $q->where($uniqueItems);
                    //         })->pluck('sales_org_id');
                  
                    // if (!$existsOrg->isEmpty() && !$existsOrg->contains($value["sales_organization"])) {
                    //     return response()->json(
                    //         responseFail(["product_tersedia"=>["Wilayah {$uniqueItems['sales_office_id']} Produk {$uniqueItems['product_id']} Sudah tersedia oleh " . $existsOrg->implode(",")]]),
                    //         400
                    //     )->throwResponse();
                    // } else {
                    //     Arr::forget($headerData, ['rayonisasi', 'permentan']);
                    //     $headerData['created_by'] = Auth::user()->id;
                    //     $header = ContractGoverment::create($headerData);
                    // }
                    Arr::forget($headerData, ['rayonisasi', 'permentan']);
                    $headerData['created_by'] = Auth::user()->id;
                    $header = ContractGoverment::create($headerData);
                }

                $detItem = [
                    'contract_gov_id' => $header->id,
                    'product_id'      => $value['kode_jenis_produk'],
                    'sales_office_id' => str_pad($value['kode_provinsi'], 4, '0', STR_PAD_LEFT),
                    'month'           => intval($value['bulan']),
                    'year'            => $value['tahun'],
                ];

                // cek item Duplicate
                $cekOnItem = $this->cekDuplikasiItem(ContractGovItem::class, $detItem);
                // return $cekOnItem;

                if ($cekOnItem) {
                    DB::rollback();
                    $response         = responseFail("data rangkap");
                    $response['errors'] = ['data_rangkap' => ['Data Rangkap pada baris ke- ' . ($key + 2)]];
                    $response['data'] = $value;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                } else {
                    $detItem['active_date'] = Carbon::now()->toDateString();
                    $detItem['initial_qty'] = $value['amount_alokasi'];
                    $detItem['rayonisasi']  = [
                        'sales_org_id'  => $value['sales_organization'],
                        'kode_provinsi' => sprintf('%04s', $value['kode_provinsi']),
                    ];
                    $detItem['sales_org_rule']  = [
                        'sales_office_id' => sprintf('%04s', $value['kode_provinsi']),
                        'month' => intval($value['bulan']),
                        'year' => $value['tahun'],
                        'contract_type' => 'permentan',
                        'product_id'      => $value['kode_jenis_produk'],
                    ];
                    $detItem['created_by']=Auth::user()->id;
                    $detItem['created_at']=now();
                    $detItem['permentan']=['contract_type'=> 'permentan','month'=> intval($value['bulan']),'year'=> $value['tahun'],'product_id'=> $value['kode_jenis_produk'],'sales_office_id'=> sprintf('%04s', $value['kode_provinsi']),'sales_org_id'=> $value['sales_organization'],];
                    // return $detItem;
                    $this->validateImportXls($detItem, ContractGovItem::PermentanRuleCreate(),($key+2));
                    unset($detItem['rayonisasi']);unset($detItem['sales_org_rule']);unset($detItem['permentan']);
                    $headerItem = ContractGovItem::insert($detItem);
                }
            }

            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'));
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateStatus(Request $request)
    {
        \LogActivity::addToLog('Update Status Header Permentan');
        $attributes = $request->all();

        $this->validate($attributes,["status" => "in:y,n","uuid" => "required|array"]);

        DB::beginTransaction();

        $contracts = ContractGoverment::whereIn("uuid", $attributes['uuid'])->get();
        $status = $contracts->pluck("status")->unique();

        if(count($status)>1){
            DB::rollback();
            $response           = responseFail(['status'=>['Status yang dipilih lebih dari 1 ']]);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        $arr=[];
        foreach ($attributes['uuid'] as $key => $value) {
            # code...
            $contract = ContractGoverment::where('uuid',$value)->where('contract_type','permentan')->first();
            if(!$contract){
                DB::rollback();
                $response           = responseFail(['data'=> ['Data tidak valid']]);
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $items = $contract->items()->get();
            
            foreach ($items as $key => $item) {
                # code...
                $attr['permentan']  = [
                    'contract_type'=> 'permentan',
                    'sales_org_id' => $contract->sales_org_id,
                    'year'         => $contract->year,
                    'product_id'   => $item['product_id'],
                    'month' => $item['month'],
                    'sales_office_id' => $item['sales_office_id'],
                ];

                $govitem=$contract->items()->where(['contract_gov_id'=>$item['contract_gov_id'],'month'=>$item['month'],'year'=>$item['year'],'sales_office_id'=> $item['sales_office_id'],'product_id'=>$item['product_id']])->first();
                
                if($attributes['status']=='y' and $govitem->status=='n'){
                    $this->validate($attr,ContractGovItem::headerUpdateStatus());
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }else{
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }
                array_push($arr,$attr);
            }

            $contract->touchStatus();
        }
        

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'),$arr);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    private function cekDuplikasiHeader($model, $attrib)
    {
        $attrib['contract_type'] = 'permentan';

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    private function cekDuplikasiItem($model, $attrib)
    {

        $query = $model::where($attrib)->where('status', '!=', 'n');

        $check = $query->first();

        return $check;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    public function destroy($uuid)
    {
        \LogActivity::addToLog('delete permentan');
        is_uuid($uuid);
        $model = $this->findDataUuid(ContractGoverment::class, $uuid);
        DB::beginTransaction();
        try {
            $model->delete($uuid);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        return Excel::download((new PermentanDownload($data)), "PermentanDownload.xls");
    }

    public function export(Request $request)
    {
        $this->validate($request->all(), ['uuid' => 'required']);
        try {

            $uuid     = $request->get('uuid');
            $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return Excel::download(
            new PermentanExport($contract),
            str_slug("{$contract->number}") . ".xls"
        );

    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'   => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'errors'    => $newMessages,
                'message'     => 'Data Error di baris : ' . $line,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }
}
