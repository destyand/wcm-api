<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Excel;
use App\Exports\OutStandSO;
use App\Exports\HeaderLaporanSO;
use App\Exports\ItemLaporanSO;

class ReportSOController extends Controller
{
    //

    public function header(Request $request,$exported=false)
    {
        \LogActivity::addToLog('Get All Report SO Header');
        $query   = Order::ReportSOHeader();
        
        $columns = [
            'nomor_sales_order'      => 'nomor_sales_order',
            'sales_organization'     => 'sales_organization',
            'deskripsi_sales_office' => 'deskripsi_sales_office',
            'deskripsi_sales_group'  => 'deskripsi_sales_group',
            'nama_distributor'       => 'nama_distributor',
            'distributor'            => 'distributor',
            'tanggal_so_dibuat_date' => 'tanggal_so_dibuat',
            'incoterm_1'             => 'incoterm_1',
            'incoterm_2'             => 'incoterm_2',
            // Filter Wilayah
            'sales_office'           => 'sales_office',
            'sales_group'            => 'sales_group',
            'product_id'             => 'material_id',
            'plant_so'               => 'kode_gudang',

        ];

        $model = DataTables::of($query)
            ->filter(function ($q) use ($columns, $request) {
                $this->filterColumn($columns, $request, $q);
            });
        if($exported) return $model->getFilteredQuery()->get();

        return    $model->make();
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->outstand($request, true);
        $columns = [
                "Sales Organization","Distribution Channel","Division","Sales Office","Deskripsi Sales Office","Sales Group","Deskripsi Sales Group","SO Legacy","Kecamatan SO","Kecamatan SO Desc","Provinsi Distributor","Kabupaten Distributor","Distributor","Nama Distributor","End User/Pengecer","Provinsi Tujuan","Deskripsi Provinsi Tujuan","Negara Tujuan","Deskripsi Negara Tujuan","Nomor Kontrak","Tanggal Mulai Kontrak","Tanggal Berakhir Kontrak","Nomor Sales Order","Tanggal SO Dibuat","Tanggal Dokumen","Tanggal SO Released","Payment Term","Payment Method","Nomor Material","Deskripsi Material","Material Group","Alokasi Asal","Alokasi Operasional","Quantity SO","Unit of Measure","Mata Uang","Harga Jual (exc. PPN)","PPN","Total","Harga/Ton(Incl PPn)","Harga Total(Incl PPn)","Nomor DO","Tanggal PGI","Plant SO","Gudang SO","Gudang SO Deskripsi","Kode Gudang","Gudang Pengambilan","Quantity DO","Quantity SO-DO","Status SO","Remarks","B/L Number","Nomor SPE","Sisa SPE","PGI qty","Outstanding SO","SO Type","SO Type Description","Provinsi Gudang","Kabupaten Gudang","Payment Term","Payment Method","Batas Akhir Pengambilan","NO PO","Tanggal PO","SO Created By","Ext. Financial Doc. No","Opening Date","Latest Shipment Date","Expiry Date","Opening Bank Key","Description","Sektor","No Billing","Billing Date","Incoterm 1","Incoterm 2","Billing Quantity","Billing Net","Billing Tax","POD Status","POD Status Desc","Finance Doc.Number","Port of Discharge","Port of Loading",];
        return Excel::download((new OutStandSO($data,$columns)), "OutStandSO.xls");
    }

    public function headerdownload(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->header($request, true);
        $columns = [
            "Nomor Sales Order","SO Item","Sales Organization","Distribution Chanel","Division","Sales Office","Deskripsi Sales Office","Sales Group","Deskripsi Sales Group","SO Legacy","Kecamatan SO","Kecamatan SO Desk","Provinsi Distributor","Kabupaten Distributor","Distributor","Nama Distributor","End User / Pengecer","Provinsi Tujuan","Deskripsi Provinsi Tujuan","Negara Tujuan","Deskripsi Negara Tujuan","Nomor Kontrak","Tanggal Mulai Kontrak","Tanggal Berakhir Kontrak","Tanggal SO Dibuat","Tanggal Dokumen","Tanggal SO Realese","Payment Term","Payment Method","Nomor Material","Deskripsi Material","Material Group","Alokasi Asal","Alokasi Operasional","Quantity SO","Unit of meassure","Mata Uang","Harga Jual","PPN","Total","Harga/ton (incl PPN)","Harag Total (incl PPN)","Nomor DO","Tanggal PGI","Plant SO","Gudang SO","Gudang SO Deksripsi","Kode Gudang","Gudang Pengambilan","Quantity DO","Quantity SO - DO","Status SO","Remarks","BL Number","Nomor SPE","Sisa SPE","PGI Qty","Total Harga tonase PGI","Outstanding SO","SO Type","SO Type Description","Provinsi Gudang","Kabupatenf Gudang","Payment Term","Payment Method","Batas Akhir Pengambilan","NO PO","Tanggal PO","SO Created By","Ext. Financial Doc. No","Opening Date","Latest Shipment Date","Expiry Date","Opening Bank Key","Description","Sektor","No. Billing","Billing Date","Incoterm 1","Incoterm 2","Billing Quantity","Billing Net","Billing Taks","POD Status","POD Status Desc","Finance Doc. Number","Port of Discharge","Port of Loading",];
        return Excel::download((new HeaderLaporanSO($data,$columns)), "HeaderLaporanSO.xls");
    }

    public function detail(Request $request,$exported=false)   {
        \LogActivity::addToLog('Get All Report SO detail');
        $query   = Order::ReportSODetail();
        $columns = [
            'nomor_sales_order'      => 'nomor_sales_order',
            'sales_organization'     => 'sales_organization',
            'deskripsi_sales_office' => 'deskripsi_sales_office',
            'deskripsi_sales_group'  => 'deskripsi_sales_group',
            'nama_distributor'       => 'nama_distributor',
            'distributor'            => 'distributor',
            'tanggal_so_dibuat_date' => 'tanggal_so_dibuat',
            'tanggal_pgi_date'       => 'tanggal_pgi',
            'incoterm_1'             => 'incoterm_1',
            'incoterm_2'             => 'incoterm_2',
            // Filter Wilayah
            'sales_office'           => 'sales_office',
            'sales_group'            => 'sales_group',
            'product_id'             => 'material_id',
            'plant_so'               => 'kode_gudang',

        ];

        $model = DataTables::of($query)
            ->filter(function ($q) use ($columns, $request) {
                $this->filterColumn($columns, $request, $q);
            });
        if($exported) return $model->getFilteredQuery()->get();

        return    $model->make();
    }

    public function detaildownload(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->detail($request, true);
        $columns = [
            " Nomor DO ","Nomor Sales Order","Deliver Item","Nomor Material","Deskripsi Material","Material Group","Sales Organization","Distribution Channel","Division","Sales Office","Deskripsi Sales Office","Sales Group","Deskripsi Sales Group","SO Legacy","Kode Kecamatan SO","Kecamatan SO","Distributor","Nama Distributor","Provinsi Distributor","Kabupaten Distributor","Provinsi Tujuan","Deskripsi Provinsi Tujuan","Negara Tujuan","Deskripsi Negara Tujuan","Nomor Kontrak","Tanggal Mulai Kontrak","Tanggal Berakhir Kontrak","Tanggal SO Dibuat","Tanggal SO Released","Payment Term","Payment Term Day","Payment Method","Payment Method Desc","Batas Akhir Pengambilan","Quantity SO","Unit Of Measure","Kode Gudang","Gudang Pengambilan","Quantity DO","Tanggal Doc. DO","Tanggal PGI","Goods Issue Status","PGI Qty","Sektor","No. Billing","Billing Date","Status DO","Status SO","Alat Angkut","Nomor Identitas Alat Angkut","ETA","Pengemudi","Remarks","B/L Number","Nomor SPE","Sisa SPE","SO Type","SO Type Desc","DO Type","DO Type Description","Provinsi Gudang","Kabupaten Gudang","End User / Pengecer","NO. PO","Tanggal PO","SO Created By","DO Created By","Penerima","POD Status","POD Desc","Incoterm 1","Incoterm 2","Shipment Planing Status","Shipment Planing Status desc","POD Date","Mata Uang","Billing Quantity","Billing Net ","Billing Tax","Tanggal Last Chg DO","Alokasi Opr SPJB",];
        return Excel::download((new ItemLaporanSO($data,$columns)), "ItemLaporanSO.xls");
    }

    public function outstand(Request $request,$exported=false)
    {
        \LogActivity::addToLog('Get All Report SO Outstand');
        $query   = Order::OutstandSO();
        
        $columns = [
            'sales_org_id'         => 'sales_org_id',
            'customer_id'          => 'customer_id',
            'order_date'           => 'order_date',
            'sales_group_id'       => 'sales_group_id',
            'sales_group_name'     => 'sales_group_name',
            'sales_office_id'      => 'sales_office_id',
            'sales_office_name'    => 'sales_office_name',
            'product_id'           => 'product_id',
            'product_name'         => 'product_name',
            'retail_id'            => 'retail_id',
            'retail_code'          => 'retail_code',
            'retail_name'          => 'retail_name',
            'so_number'            => 'so_number',
            'delivery_number'      => 'delivery_number',
            'delivery_method_name' => 'delivery_method_name',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            });
        if($exported) return $model->getFilteredQuery()->get();
        $model=$model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            $this->searchColumn($key, $value, $request, $query);
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {
            if (strpos($value, 'date') !== false) {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif (strpos($request->get($value), ';') !== false ) {
                $arr = explode(';', $request->get($value));
                if(strpos($value, 'product_id') !== false){
                    $query->whereIn($key,$arr);
                }else{
                    $query->whereBetween($key, [$arr[0], $arr[1]]);
                }
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

}
