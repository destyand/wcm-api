<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\CustomerBankAssg;

class BankController extends Controller {

    //
    public function index(Request $request) {
        \LogActivity::addToLog('get all bank');

        if ($request->has('customer_id') && $request->has('sales_org_id')) {
            $query = $this->bankAssg($request->get('customer_id'), $request->get('sales_org_id'));
        } else {
            $query = Bank::select('*');
        }

        $columns = ['id' => 'id', 'uuid' => 'uuid', 'name' => 'name', 'status' => 'status'];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    private function bankAssg($customer, $salesOrg) {
        $where = [
            ["tb1.customer_id", '=', $customer],
            ["tb1.sales_org_id", '=', $salesOrg],
            ["tb1.status", '=', 'y'],
            ["tb1.start_date", '<=', date('Y-m-d')],
            ["tb1.end_date", '>=', date('Y-m-d')]
        ];

        return CustomerBankAssg::getBankAssg($where);
    }

}
