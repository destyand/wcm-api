<?php

namespace App\Http\Controllers;

use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\DistribChannel;

class DistribChannelController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all distribution channel');

        $model = Datatables::of(DistribChannel::query())
                ->filter(function($query) use ($request) {
                    $columns = ['name' => 'name'];
                    foreach ($columns as $key => $value) {
                        if ($request->has($value)) {
                            $query->where($key, 'like', "%{$request->get($value)}%");
                        }
                    }
                })
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($id) {
        \LogActivity::addToLog('get distribution channel by id');

        $model = $this->findDataUuid(DistribChannel::class, $id);

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
