<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Farmer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Excel;
use App\Imports\InitialStockImport;
use Illuminate\Support\Facades\Validator;
use App\Models\InitialStocks;
use App\Models\InitialStockItem;
use App\Models\DistribReports;
use App\Models\Product;
use App\Models\ReportF5;
use App\Exports\Download;

class InitialStockF5Controller extends Controller {

    public function index(Request $request,$exported=false) {
        \LogActivity::addToLog('get all initial stock f5');
        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        $query = InitialStocks::getInitialStockF5($where,$exported);
        
        $user = $request->user();
        $filters = $user->filterRegional;

        // return response()->json($user->customer_id);
        if($user->customer_id != ""){
             $query->where("tb1.customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb1.sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb1.sales_group_id", $filters["sales_group_id"]);
                }
            }
        }
       
        $columns = [
            'tb1.sales_org_id' => 'sales_org_id',
            'tb1.sales_org_name' => 'sales_org_name',
            'tb1.customer_id' => 'customer_id',
            'tb1.customer_name' => 'customer_name',
            'tb1.sales_office_id' => 'sales_office_id',
            'tb1.sales_office_name' => 'sales_office_name',
            'tb1.sales_group_id' => 'sales_group_id',
            'tb1.sales_group_name' => 'sales_group_name',
            'tb1.month' => 'month',
            'tb1.year' => 'year',
        ];

        $model = Datatables::of($query)
                ->filter(function($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        if($exported) return $model->getFilteredQuery()->get();
        $model= $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function validateImport($request, $rules, $line) {
        $messages = [
            'required' => trans('messages.required'),
            'unique' => trans('messages.unique'),
            'email' => trans('messages.email'),
            'numeric' => trans('messages.numeric'),
            'exists' => trans('messages.exists'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status' => 0,
                'status_txt' => "errors",
                'message' => $newMessages,
                'errors' => trans('messages.duplicate-error') . $line,
                'data' => $request
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function upload(Request $request) {
        \LogActivity::addToLog('upload initial stock f5');
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx'
        ]);

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new InitialStockImport, $file)[0];
            
            $parProduk = [];
            foreach ($data as $key => $value) {
                if (is_null($value['produsen'])) {
                    continue;
                }
                $headerData = $this->setHeaderData($value);

                $this->validateImport($headerData, InitialStocks::ruleCreate(), intval($key) + 2);
                // cek report f5 harus draft
                $model = ReportF5::where(['sales_org_id'=> $headerData['sales_org_id'],'sales_group_id'=>$headerData['sales_group_id'],'month'=> $headerData['month'],'year'=>$headerData['year'],'customer_id'=> $headerData['customer_id']])->first();

                if(!$model)
                {
                    DB::rollback();
                    $response = responseFail(trans('messages.create-fail'));
                    $response['errors'] = ($key+2);
                    $response['data'] = ['report_f5'=> 'Report F5 belum tersedia'];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                if($model->status != 'd')
                {
                    DB::rollback();
                    $response = responseFail(trans('messages.create-fail'));
                    $response['errors'] = ($key+2);
                    $response['data'] = ['report_f5'=> 'Status Report F5 harus draft'];
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }

                $initialStockF5 = InitialStocks::where($headerData)->latest()->first();

                if($initialStockF5){
                    // cek distrib Report / Penyaluran 
                    $distrib_report = DistribReports:: where('initial_stock_id', $initialStockF5->id)->get();
                    if($distrib_report->count() > 0){
                        DB::rollback();
                        $response = responseFail(trans('messages.create-fail'));
                        $response['errors'] = ($key+2);
                        $response['data'] = ['report_f5'=> 'Masih ada penyaluran Initial Stok F5 dibulan tersebut, mohon periksa kembali'];
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }else{
                        $headerData['updated_by'] = @Auth::user()->id;
                        $headerData['updated_at'] = now();
                        $header = InitialStocks::where('id',$initialStockF5->id)->update($headerData);
                        // delete vaue item intitial stok
                        $delete = InitialStockItem::where('initial_stock_id',$initialStockF5->id)->delete();
                        // insert nilai baru
                        $this->saveItem($initialStockF5->id, $value, $model);
                    }
                }else{
                    $headerData['created_by'] = @Auth::user()->id;
                    $headerData['created_at'] = now();
                    $header = InitialStocks::insertGetId($headerData);
                    $this->saveItem($header, $value, $model);
                }
                
            }
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), []);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Produsen","Nama Distributor","Dokumen Draft","Dokumen Submited","Provinsi","Kabupaten","Bulan","Tahun","Awal","Sisa","Awal","Sisa","Awal","Sisa","Awal","Sisa","Awal","Sisa"];
        return Excel::download((new Download($data,$columns)), "Download InitialStocks F5.xls");
    }

    private function saveItem($initial_stock_id, $array, $reportF5) {
        $arrItem = [];
        foreach ($array as $key => $val) {
            if (strpos($key, '_ton') !== false || strpos($key, '_liter')!==false) {
                $kata = str_replace('_ton', '', $key);
                $kata = str_replace('_liter', '', $kata);
                $field = str_replace('_', '-', $kata);
                if($field=='org-cair'){
                    $field = "ORGANIK CAIR";
                }else if($field=='npk-kakao'){
                    $field = "NPK KAKAO";
                }                
                $produk = Product::where('name', $field)->first();
                $param['initial_stock_id'] = $initial_stock_id;
                $param['product_id'] = $produk->id;
                $param['qty'] = ($val===null) ? NULL : floatval(str_replace(',', '.', $val));
                $param['created_by'] = Auth::user()->id;
                $param['updated_by'] = Auth::user()->id;

                $item = InitialStockItem::create($param);
                $reportF5->items()
                    ->where("product_id", $produk->id)
                    ->update([
                        "stok_awal" => $param["qty"],
                        "updated_by" => $param["updated_by"],
                    ]);

                array_push($arrItem, $item);
            }
        }
        return $arrItem;
    }

    private function setHeaderData($value) {
        $headerData = [
            'type' => 'InitialStockF5',
            'sales_org_id' => $value['produsen'],
            'customer_id' => $value['no_distributor'],
            'sales_office_id' => str_pad($value['kode_provinsi'], 4, "0", STR_PAD_LEFT),
            'sales_group_id' => $value['kode_kotakabupaten'],
            'month' => intval($value['bulan']),
            'year' => $value['tahun']
        ];

        return $headerData;
    }

}
