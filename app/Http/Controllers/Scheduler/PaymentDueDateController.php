<?php

namespace App\Http\Controllers\Scheduler;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Order;

class PaymentDueDateController extends Controller {

    public function store() {

        DB::beginTransaction();
        try {
            $order = Order::where([['status', '=', 'y'], ['payment_due_date', '<=', date('Y-m-d H:m')]]);
            $result = $order->get();
            $order->update(['status' => 'x']);

            DB::commit();
            Log::info('Scheduler check payment due date', [
                'status' => 'success',
                'update' => $result->count()
            ]);
            $response = responseSuccess(trans('messages.create-success'), $result);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('Scheduler check payment due date', [
                'status' => 'fail'
            ]);
            $response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

}
