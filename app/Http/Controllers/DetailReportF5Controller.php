<?php

namespace App\Http\Controllers;

use App\Models\ContractItem;
use App\Models\DeliveryItem;
use App\Models\DistribReportItems;
use App\Models\DistribReports;
use App\Models\InitialStockItem;
use App\Models\InitialStocks;
use App\Models\Order;
use App\Models\Product;
use App\Models\ReportF5;
use App\Models\ReportF5Items;
use App\Models\ReportF6;
use App\Models\ReportF6Items;
use App\Models\Retail;
use App\Models\SalesUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use PDF;

class DetailReportF5Controller extends Controller
{
    //
    public function index($uuid)
    {
        # code...
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Get All Data ReportF5');
        is_uuid($uuid);
        $print = $this->newHirarkiF5($uuid);

        $response = responseSuccess(trans('messages.read-success'), $print);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }

    public function newHirarkiF5($uuid, $getretail=false) 
    {
        $reportf5 = ReportF5::MasterF5Report()
        ->where('tb1.uuid', $uuid)
        ->first();

        $reportf5item = ReportF5Items::where('report_f5_id', $reportf5->id)->get();

        $whereDR['tb7.uuid']           = $uuid;
        $whereDR['tb3.id']             = $reportf5->customer_id;
        $whereDR['tb1.sales_org_id']   = $reportf5->sales_org_id;
        $whereDR['tb1.month']          = intval($reportf5->month);
        $whereDR['tb1.year']           = $reportf5->year;
        $whereDR['tb1.sales_group_id'] = $reportf5->sales_group_id;
        $distrib_reports               = DistribReports::where("report_f5_id", $reportf5->id)
                                           ->where("status", "s")
                                           ->whereIn("distrib_resportable_type", ["order","InitialStockF5"])
                                           ->get();

        $m= (strlen($reportf5->month) == 1) ? "0".$reportf5->month : $reportf5->month;
       
        $orders = Order::leftJoin('wcm_delivery AS tb2', 'wcm_orders.id', '=', 'tb2.order_id')
                ->where('wcm_orders.sales_org_id',$reportf5->sales_org_id)
                ->where('wcm_orders.sales_group_id',$reportf5->sales_group_id)
                ->where('wcm_orders.sales_office_id',$reportf5->sales_office_id)
                ->where('wcm_orders.customer_id',$reportf5->customer_id)
                // ->whereIn('wcm_orders.status',['k','c'])
                ->whereMonth('tb2.delivery_date', $m)
                ->whereYear('tb2.delivery_date',$reportf5->year)
                ->select('wcm_orders.id')
                ->get()->pluck('id');
        
        // Find uniq pkp
        $uniq_pkp = $distrib_reports->pluck("number")->unique()->toArray();
        // Uniq Distrib ID
        $uniq_id = $distrib_reports->pluck("id")->unique()->toArray();
        // Find Uniq Order
        $uniq_order = $distrib_reports->pluck("order_id")->unique()->toArray();

        // Order
        $order = Order::wherein('id', $uniq_order)->get();

        // Delivery / Good Receive
        $delivery = DeliveryItem::getDeliveryReportF5()
            ->wherein('tb2.order_id', $orders)
            ->whereMonth('tb2.delivery_date', $m)
            ->get();

        // check in initial stok
        // Avalable Data Bulan Ini
        $where_initial['type'] = 'InitialStockF5';
        $where_initial['sales_org_id']             = $reportf5->sales_org_id;
        $where_initial['sales_group_id']           = $reportf5->sales_group_id;
        $where_initial['customer_id']              = $reportf5->customer_id;
        $where_initial['month']                    = intval($reportf5->month);
        $where_initial['year']                     = $reportf5->year;
        $initialstoks                              = InitialStocks::where($where_initial)->latest()->first();
       
        // Jika Bulan Ini Ndak Ada Maka Ambil Initial Stok Bulan Lalu   
        $f5_filter['sales_org_id']   = $reportf5->sales_org_id;
        $f5_filter['sales_group_id'] = $reportf5->sales_group_id;
        $f5_filter['customer_id']    = $reportf5->customer_id;
        $years                       = $reportf5->year;
        $months                      = $reportf5->month - 1;
        if ($reportf5->month == "01" || $reportf5->month == 1) {
            $years  = $reportf5->year - 1;
            $months = 12;
        }
        $f5_filter['month'] = $months;
        $f5_filter['year']  = $years;

        $f5Data = ReportF5::where($f5_filter)->first();
        $stokitems = ReportF5Items::where('report_f5_id', @$f5Data->id)->get();

        $InitialStockItem = InitialStockItem::where('initial_stock_id', @$initialstoks->id)->get();

        $distribitems = DistribReportItems::wherein('distrib_report_id', $uniq_id)
        ->where('qty','!=',0)->get();

        // Find SPJB
        $spjb = DB::table('wcm_contract_item AS tb1')
        ->leftjoin('wcm_contract AS tb2', 'tb2.id', '=', 'tb1.contract_id')
        ->select('tb1.sales_unit_id')
        ->where('tb2.customer_id',$reportf5->customer_id)
        ->where('tb2.sales_org_id', $reportf5->sales_org_id)
        ->where('tb1.month', $reportf5->month)
        ->where('tb1.year', $reportf5->year)
        ->where('tb1.status', 'y') //status aktif
        ->where('tb1.sales_group_id', $reportf5->sales_group_id)
        ->where('tb2.contract_type','asal')
        ->get()
        ->pluck('sales_unit_id')->unique();

        // $sales_units= SalesUnit::whereIn('id', $spjb)->orderby('name', 'ASC')->get();

        /* when report f5 tidak draft dan report f6 tidak draft => get retail dari f6item */
        $reportf6_status = ReportF6::where('report_f5_id', $reportf5->id)->first();
        if($reportf5->status !="d" && @$reportf6_status->status !="d"){
                $retail_f6_id = ReportF6Items::where('report_f6_id',$reportf6_status->id)->get()->pluck('retail_id')->unique()->toArray();
                $retails = DB::table('wcm_retail as a')
                ->leftJoin('wcm_sales_unit as b', function ($q) {
                    $q->on('a.sales_unit_id', '=', 'b.id');
                })
                ->whereIn('a.id', $retail_f6_id)
                ->whereRaw("a.sub_district_default = '0'")
                ->select('a.sub_district_default', 'a.id', 'a.code', 'a.uuid', 'a.name', 'a.email', 'a.owner', 'a.address', 'a.village', 'a.tlp_no', 'a.hp_no', 'a.fax_no', 'a.latitude', 'a.longitude', 'a.npwp_no', 'a.npwp_register', 'a.siup_no', 'a.valid_date_siup', 'a.situ_no', 'a.valid_date_situ', 'a.tdp_no', 'a.valid_date_tdp', 'a.recomd_letter', 'a.recomd_letter_date', 'a.old_number', 'b.id as sales_unit_id', 'b.name as sales_unit_name')
                ->orderBy('a.name', 'ASC')->get();
            /* Status F5 Note Draft */
            $sales_units= SalesUnit::whereIn('id', $retails->pluck('sales_unit_id'))->orderby('name', 'ASC')->get();
        }else{
        // Get Retail
            $where_r['f.id'] = $reportf5->customer_id;
            $where_r['g.id'] = $reportf5->sales_org_id;
            $last            = date('Y-m-t', strtotime($reportf5->year . "-" . $reportf5->month));
            $retails         = Retail::retailDitributionDO($where_r)->whereIn("e.status",['y','p'])->whereIn('a.sales_unit_id', $spjb)
                                ->whereRaw("e.created_at <= '" . $last . "'")
                                ->whereRaw("a.sub_district_default = '0'")
                                ->orderBy('a.name', 'ASC')->get();
            /* Status F5 Draft */
            $sales_units = SalesUnit::whereIn('id', $spjb)->orderby('name', 'ASC')->get();
        }        

        if($getretail){
            return $retails;
        }
        $products=Product::select('id','name')->get();
        $template = $this->makeTotal($products);
        $dataArr= [];
        foreach ($sales_units as $key) {
            # code...
            $inRetailUnit=['id' => $key['id'],'name'=>$key['name'], 'data'=>[], 'total'=>[],'total' => array('awal' => $template, 'penebusan' => $template, 'penyaluran' => $template, 'akhir' => $template)];
            $retails_sales_unit = $retails->where('sales_unit_id',$key['id']);
            if($retails_sales_unit->count()!=0){
            $total_penyaluran_sales_unit=0;
            $total_penebusan_in_sales_unit=[];
            foreach ($retails_sales_unit as $key_in_sales_unit) {
                # code...
                $dataRetail =['id'=>$key_in_sales_unit->id,'name'=>$key_in_sales_unit->name,'retail_code'=>$key_in_sales_unit->code,'data'=>[]];
                $penyaluran_product=[]; $awal_product=[] ; $akhir_product=[]; $penebusan_product=[];
                foreach ($products as $key_product => $productValue) {
                    # code...
                    $qty_p=$distribitems->where('retail_id', $key_in_sales_unit->id)->where('product_id',$productValue->id)->sum('qty');
                    array_push($penyaluran_product,['id' =>$productValue->id, 'qty'=>$qty_p]);
                    array_push($awal_product,['id' => $productValue->id, 'qty'=>0]);
                    array_push($akhir_product,['id' => $productValue->id, 'qty'=>0]);
                    array_push($penebusan_product,['id' => $productValue->id, 'qty'=>0]);
                    $inRetailUnit['total']['penyaluran'][$key_product]['qty'] += $qty_p;
                }
                $dataRetail['data']['awal']=$awal_product;
                $dataRetail['data']['penebusan']=$penebusan_product;
                $dataRetail['data']['penyaluran']=$penyaluran_product;
                $dataRetail['data']['akhir']=$akhir_product;
                
                array_push($inRetailUnit['data'],$dataRetail);
            }           
            array_push($dataArr,$inRetailUnit);
            }
        }

        $dataTotalAwal=[];$dataTotalAkhir=[];$dataTotalPenebusan=[]; $dataTotalPenyaluran=[];$postF5Item=[];
        foreach ($products as $key_product) {
            # code...
            $uuidReportF5Item='';
            $cek_f5=$reportf5item->where('product_id',$key_product['id'])->first();
            if($cek_f5){
                $uuidReportF5Item=$cek_f5->uuid;
            }
            $qty_stoks=$stokitems->where('product_id', $key_product['id'])->sum('stok_akhir');
            if(count($InitialStockItem)!=0){
                $initF5 = $InitialStockItem->where('product_id', $key_product['id'])->first();
                if($initF5){
                    if($initF5->qty!=null){
                        $qty_stoks=$initF5->qty;
                    }
                }
            }
            $qty_stoks=round($qty_stoks,3);
            $qty_penyaluran=round($distribitems->where('product_id',$key_product['id'])->sum('qty'),3);
            $qty_penebusan=round($delivery->where('product_id',$key_product['id'])->sum('delivery_qty'),3);
            array_push($dataTotalAwal,['id'=>$key_product['id'],'qty'=>$qty_stoks,'uuid'=>$uuidReportF5Item]);
            array_push($dataTotalPenyaluran,['id'=>$key_product['id'],'qty'=> $qty_penyaluran,'uuid'=>$uuidReportF5Item]);
            array_push($dataTotalPenebusan,['id'=>$key_product['id'],'qty'=> $qty_penebusan,'uuid'=>$uuidReportF5Item]);
            array_push($dataTotalAkhir,['id'=>$key_product['id'],'qty'=> round((($qty_stoks+$qty_penebusan)-$qty_penyaluran),3),'uuid'=>$uuidReportF5Item]);

            array_push($postF5Item, array(
                'uuid'         => $uuidReportF5Item,
                'product_id'   => $key_product['id'],
                'report_f5_id' => $reportf5->id,
                'stok_awal'    => $qty_stoks,
                'penebusan'    => $qty_penebusan,
                'penyaluran'   => $qty_penyaluran,
                'stok_akhir'   => round((($qty_stoks+$qty_penebusan)-$qty_penyaluran),3),
                'number_f5'    => $reportf5->id,
            ));
        }

        $findf6 = array(
            'report_f5_id'   => $reportf5->id,
            'sales_org_id'   => $reportf5->sales_org_id,
            'customer_id'    => $reportf5->customer_id,
            'sales_group_id' => $reportf5->sales_group_id,
            'year'           => $reportf5->year,
            'month'          => intval($reportf5->month),
        );

        // Find Data Uuid For F6
        $f6 = $this->findModelWhere(ReportF6::class, $findf6);

        // Exist
        if ($f6) {
            $print['posts']['f6'] = array(
                'uuid'           => $f6->uuid,
                'number'         => $f6->number,
                'report_f5_id'   => $reportf5->id,
                'sales_org_id'   => $reportf5->sales_org_id,
                'customer_id'    => $reportf5->customer_id,
                'sales_group_id' => $reportf5->sales_group_id,
                'year'           => $reportf5->year,
                'month'          => intval($reportf5->month),
                'status'         => 'd',
            );
            // Not Exit
        } else {
            $print['posts']['f6'] = array(
                'uuid'           => '',
                'number'         => '',
                'report_f5_id'   => $reportf5->id,
                'sales_org_id'   => $reportf5->sales_org_id,
                'customer_id'    => $reportf5->customer_id,
                'sales_group_id' => $reportf5->sales_group_id,
                'year'           => $reportf5->year,
                'month'          => intval($reportf5->month),
                'status'         => $reportf5->status,
                'status'         => 'd',
            );
        }
        $print['posts']['f5items']=$postF5Item;
        
        $print['header'] = [
            'uuid'              => $reportf5->uuid,
            'id'                => $reportf5->id,
            'number'            => $reportf5->number,
            'sales_org_name'    => $reportf5->sales_org_name,
            'customer_id'       => $reportf5->customer_id,
            'customer_name'     => $reportf5->customer_name,
            'sales_group_id'    => $reportf5->sales_group_id,
            'sales_group_name'  => $reportf5->sales_group_name,
            'submited_date'     => $reportf5->submited_date,
            'sales_office_name' => $reportf5->sales_office_name,
            'month'             => $this->monthName($reportf5->month),
            'year'              => $reportf5->year,
            'status'            => $reportf5->status,
            'status_name'       => $reportf5->status_name,
        ];
        $print['produks'] = $products;
        $print['data'] = $dataArr;
        $print['total']['awal'] = $dataTotalAwal;
        $print['total']['penebusan'] = $dataTotalPenebusan;
        $print['total']['penyaluran'] = $dataTotalPenyaluran;
        $print['total']['akhir'] = $dataTotalAkhir;
        return $print;

    }

    public function download($uuid)
    {
        ini_set("max_execution_time",180);
        \LogActivity::addToLog('Download Detail ReportF5 by UUID');
        is_uuid($uuid);
        $aes = new \Legierski\AES\AES;

        $datas['datas'] = $this->newHirarkiF5($uuid);
        $user           = Auth::user();
        $f5uuid         = DB::table('wcm_report_f5')->where('id', $datas['datas']['header']['id'])->first();
        // user role
        $route = DB::table('wcm_m_routes')->where('name', 'reportf5')->first();

        $access = DB::table('wcm_role_has_menus')
            ->where('role_id', $user->role_id)
            ->where('menu_id', $route->id)
            ->get();

        $acceses = array();
        foreach ($access as $key) {
            array_push($acceses, $key->action_id);
        }

        $akses = implode(",", $acceses);
        $akses = str_replace(' ', '', $akses);

        $passphrase = "sy4hr!lb3g!tut4mp4n";

        $decrypted = $aes->encrypt($akses, $passphrase);

        $datas['url'] = URL::to('/#/' . urlencode($decrypted) . '/laporan/laporan-bulanan-distributor/detail/' . $f5uuid->uuid);

        // return response()->json($datas);
        // return view ('reportf5.download', $datas);
        $pdf       = PDF::loadView('reportf5.download', $datas);
        $nama_file = 'DOWNLOAD-REPORTF5-' . $datas['datas']['header']['number'] . '-' . $datas['datas']['header']['customer_id'];
        return $pdf->setPaper( [0, 0, 841.89, 1250], 'landscape')->download($nama_file);

    }

    public function findso($uuid)
    {
        \LogActivity::addToLog('Find SO ReportF5 by UUID');
        is_uuid($uuid);
        $where_f5['tb1.uuid'] = $uuid;

        $data                 = ReportF5::where('uuid',$uuid)->first();

        $m= (strlen($data->month) == 1) ? "0".$data->month : $data->month;

        $return = Order::leftJoin('wcm_delivery AS tb2', 'wcm_orders.id', '=', 'tb2.order_id')
                ->where('wcm_orders.sales_org_id',$data->sales_org_id)
                ->where('wcm_orders.sales_group_id',$data->sales_group_id)
                ->where('wcm_orders.customer_id',$data->customer_id)
                ->whereIn('wcm_orders.status',['k','c','l','u'])
                ->whereMonth('tb2.delivery_date', $m)
                ->whereYear('tb2.delivery_date',$data->year)
                ->select('wcm_orders.uuid','wcm_orders.so_number')
                ->distinct()
                ->get();        

        $response = responseSuccess(trans('messages.read-success'), $return);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function findpkp($uuid)
    {
        \LogActivity::addToLog('Find PKP SO ReportF5 by UUID');
        is_uuid($uuid);
        $where_f5['tb1.uuid'] = $uuid;
        $latestInitialStoks =  ReportF5::find_pkp($where_f5)
                            ->where("tb2.distrib_resportable_type", "InitialStockF5")
                            ->latest()->first();
        $data = ReportF5::find_pkp($where_f5)->orderBy('tb2.distribution_date', 'DESC')
                ->whereIn('tb2.distrib_resportable_type',['order']);
                if($latestInitialStoks){
                    $data  = $data->orWhere('tb2.initial_stock_id', $latestInitialStoks->initial_stock_id);
                }
                $data=$data->select('tb2.uuid','tb2.number','tb2.distribution_date', 'tb2.distrib_resportable_type')->get();

        $response = responseSuccess(trans('messages.read-success'), $data);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('Create ReportF5');
        $attributes = $request->all();

        if (count($attributes) == 0) {
            $response             = responseFail(trans('messages.create-fail'));
            $response['messages'] = 'Format Simpan data simpan tidak didukung';
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        } else if (count($attributes[0]) == 0) {
            $response             = responseFail(trans('messages.create-fail'));
            $response['messages'] = 'Format Simpan data simpan tidak didukung';
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

        $produks = Product::all();

        DB::beginTransaction();
        try {

            $datas    = array();
            $flag     = 0;
            $status   = "y";
            $reportf5 = "";
            foreach ($attributes[0]['f5items'] as $key => $value) {
                # code...
                $value['stok_awal']  = floatval($value['stok_awal']);
                $value['penyaluran'] = floatval($value['penyaluran']);
                $value['stok_akhir'] = floatval($value['stok_akhir']);
                $value['penebusan']  = floatval($value['penebusan']);
                $status              = $value['status'];
                $reportf5            = $value['report_f5_id'];

                if ($value['uuid'] == '') {
                    unset($value['uuid']);unset($value['status']);
                    $value['created_by'] = Auth::user()->id;
                    $value['created_at'] = now();
                    array_push($datas, $value);
                } else {
                    $model = $this->findDataUuid(ReportF5Items::class, $value['uuid']);
                    unset($value['uuid']);unset($value['status']);
                    $value['updated_by'] = Auth::user()->id;
                    $value['updated_at'] = now();
                    $model->update($value);
                }

                $flag += 1;
            }

            /*Find data F5*/
            $wheref5['id'] = $reportf5;
            $modelf5       = $this->findDataWhere(ReportF5::class, $wheref5);

            $retails = $this->newHirarkiF5($modelf5->uuid,true);

            if ($flag != 0) {
                $valU['status'] = $status;
                $wheref5['id']  = $reportf5;
                $model          = $this->findDataWhere(ReportF5::class, $wheref5);
                $model->update($valU);
            }
            
            /*Drafft To Submit*/
            if ($status == 's' && $modelf5->status == 'd') {
                $wherePrevF5['sales_org_id']   = $modelf5->sales_org_id;
                $wherePrevF5['customer_id']    = $modelf5->customer_id;
                $wherePrevF5['sales_group_id'] = $modelf5->sales_group_id;
                $wherePrevF5['year']           = $modelf5->year;
                $wherePrevF5['month']          = $modelf5->month-1;

                if ($modelf5->month == 1) {
                    $wherePrevF5['year']  = $attributes[0]['f6']['year'] - 1;
                    $wherePrevF5['month'] = 12;
                }

                $modelf6Prev = ReportF6::where($wherePrevF5)->first();
                $modelf5Prev = ReportF5::where($wherePrevF5)->first();

                if ($modelf6Prev) {
                    if ($modelf6Prev->status != 'y' && $modelf6Prev->status != 's' ) {
                        DB::rollback();
                        $response            = responseFail(trans('messages.create-fail'));
                        $response['message'] = 'ReportF6 Bulan Sebelumnya Belum Berstatus Submit Atau Approve';
                        $response['data']    = $modelf6Prev;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }

                if ($modelf5Prev) {
                    if ($modelf5Prev->status != 'y' && $modelf5Prev->status != 's' ) {
                        DB::rollback();
                        $response            = responseFail(trans('messages.create-fail'));
                        $response['message'] = 'ReportF5 Bulan Sebelumnya Belum Berstatus Submit Atau Approve';
                        $response['data']    = $modelf5Prev;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            /*UnSubmit*/
            if ($status == 'd' && $modelf5->status == 's') {
                $wherePrevF5['sales_org_id']   = $modelf5->sales_org_id;
                $wherePrevF5['customer_id']    = $modelf5->customer_id;
                $wherePrevF5['sales_group_id'] = $modelf5->sales_group_id;
                $wherePrevF5['year']           = $modelf5->year;
                $wherePrevF5['month']          = $modelf5->month;

                $modelf6Prev = ReportF6::where($wherePrevF5)->first();
                

                if ($modelf6Prev) {
                    if ($modelf6Prev->status != 'd' ) {
                        DB::rollback();
                        $response            = responseFail(trans('messages.create-fail'));
                        $response['message'] = 'ReportF6 Belum Berstatus Draft';
                        $response['data']    = $modelf6Prev;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            /*Submited To Approve*/
            if ($status == 'y' && $modelf5->status == 's') {
                $wherePrevF5['sales_org_id']   = $attributes[0]['f6']['sales_org_id'];
                $wherePrevF5['customer_id']    = $attributes[0]['f6']['customer_id'];
                $wherePrevF5['sales_group_id'] = $attributes[0]['f6']['sales_group_id'];
                $wherePrevF5['year']           = $attributes[0]['f6']['year'];
                $wherePrevF5['month']          = $attributes[0]['f6']['month'] - 1;

                if ($attributes[0]['f6']['month'] == 1) {
                    $wherePrevF5['year']  = $attributes[0]['f6']['year'] - 1;
                    $wherePrevF5['month'] = 12;
                }

                $modelf6Prev = ReportF6::where($wherePrevF5)->first();

                if ($modelf6Prev) {
                    if ($modelf6Prev->status != 'y') {
                        DB::rollback();
                        $response            = responseFail(trans('messages.create-fail'));
                        $response['message'] = 'ReportF6 Bulan Sebelumnya Belum Berstatus Approve';
                        $response['data']    = $modelf6Prev;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            /*Approve To Submited*/
            if ($status == 's' && $modelf5->status == 'y') {
                $wherePrevF5['sales_org_id']   = $attributes[0]['f6']['sales_org_id'];
                $wherePrevF5['customer_id']    = $attributes[0]['f6']['customer_id'];
                $wherePrevF5['sales_group_id'] = $attributes[0]['f6']['sales_group_id'];
                $wherePrevF5['year']           = $attributes[0]['f6']['year'];
                $wherePrevF5['month']          = $attributes[0]['f6']['month'];

                $modelf6Prev = ReportF6::where($wherePrevF5)->first();

                if ($modelf6Prev) {
                    if ($modelf6Prev->status != 's' && $modelf6Prev->status != 'd') {
                        DB::rollback();
                        $response            = responseFail(trans('messages.create-fail'));
                        $response['message'] = 'ReportF6 Belum Berstatus Submit Atau Draft';
                        $response['data']    = $modelf6Prev;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                }
            }

            try {
                foreach ($datas as $data) {
                    ReportF5Items::create($data);
                }
            }catch (\Exception $e) {
                throw $e;
            }
            // Insert New Data

            // Cek Penyaluran DO
            $where_do['status']                   = 'd';
            $where_do["report_f5_id"]             = $attributes[0]['f6']['report_f5_id'];
            $where_do["sales_org_id"]             = $attributes[0]['f6']['sales_org_id'];
            $where_do["customer_id"]              = $attributes[0]['f6']['customer_id'];
            $where_do["sales_group_id"]           = $attributes[0]['f6']['sales_group_id'];
            $where_do["year"]                     = $attributes[0]['f6']['year'];
            $where_do["distrib_resportable_type"] = "order";
            $where_do["month"]                    = intval($attributes[0]['f6']['month']);

            $cekDo = DistribReports::where($where_do)->get();

            if (count($cekDo) != 0) {
                DB::rollback();
                $response["status"]  = false;
                $response['message'] = "Beberapa Penyaluran DO Masih Punya Status Draft";
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            // Insert F6 Header
            if (count($attributes[0]['f6']) != 0) {
                if ($attributes[0]['f6']['uuid'] == "") {
                    unset($attributes[0]['f6']['uuid']);
                    $attributes[0]['f6']['created_by']=Auth::user()->id;
                    $attributes[0]['f6']['created_at']=now();
                    $reportf6ID=ReportF6::insertGetId($attributes[0]['f6']);

                    $retailF6ItemArr=[];
                    foreach ($retails as $retail) {
                        # code...
                        foreach ($produks as $produk) {
                            # code...
                            array_push($retailF6ItemArr, [
                            'product_id' => $produk['id'],
                            'stok_awal' => 0, 
                            'penebusan' => 0, 
                            'penyaluran' => 0, 
                            'stok_akhir' =>0 , 
                            'report_f6_id' => $reportf6ID, 
                            'retail_id' => $retail->id, 
                            'created_by' => Auth::user()->id, 
                            'created_at' => now(), 
                            ]);
                        }
                    }

                    try {
                        foreach ($retailF6ItemArr as $item) {
                            ReportF6Items::create($item);
                        }
                    }catch (\Exception $e) {
                        throw $e;
                    }
                }
            }


            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $datas);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }

    }

    private function hirarkiArr($uuid,$getretail=false)
    {
        # code...
        // find report f5
        $where_f5['tb1.uuid'] = $uuid;
        $reportf5             = ReportF5::MasterF5Report($where_f5)->first();

        
        $date                 = explode('-', $reportf5->submited_date);

        // Report F5 Item
        $reportf5item = json_decode(ReportF5Items::where('report_f5_id', $reportf5->id)->get());

        $whereDR['tb7.uuid']           = $uuid;
        $whereDR['tb3.id']             = $reportf5->customer_id;
        $whereDR['tb1.sales_org_id']   = $reportf5->sales_org_id;
        $whereDR['tb1.month']          = intval($reportf5->month);
        $whereDR['tb1.year']           = $reportf5->year;
        $whereDR['tb1.sales_group_id'] = $reportf5->sales_group_id;
        $distrib_reports               = DistribReports::where("report_f5_id", $reportf5->id)
                                           ->where("status", "s")
                                           ->whereIn("distrib_resportable_type", ["order","InitialStockF5"])
                                           ->get();

        $m= (strlen($reportf5->month) == 1) ? "0".$reportf5->month : $reportf5->month;

        $orders = Order::leftJoin('wcm_delivery AS tb2', 'wcm_orders.id', '=', 'tb2.order_id')
                ->where('wcm_orders.sales_org_id',$reportf5->sales_org_id)
                ->where('wcm_orders.sales_group_id',$reportf5->sales_group_id)
                ->where('wcm_orders.sales_office_id',$reportf5->sales_office_id)
                ->where('wcm_orders.customer_id',$reportf5->customer_id)
                // ->whereIn('wcm_orders.status',['k','c'])
                ->whereMonth('tb2.delivery_date', $m)
                ->whereYear('tb2.delivery_date',$reportf5->year)
                ->select('wcm_orders.*')
                ->get()->pluck('id');

        // return $orders;
        // Find Produks
        $produks     = json_decode(Product::all());
        $produksUniq = array_unique(array_column($produks, 'id'));

        // Find uniq pkp
        $uniq_pkp = $distrib_reports->pluck("number")->unique()->toArray();
        // Uniq Distrib ID
        $uniq_id = $distrib_reports->pluck("id")->unique()->toArray();
        // Find Uniq Order
        $uniq_order = $distrib_reports->pluck("order_id")->unique()->toArray();

        // Order
        $order = json_decode(Order::wherein('id', $uniq_order)->get());

        // Delivery / Good Receive
        $delivery = json_decode(DeliveryItem::getDeliveryReportF5()
            ->wherein('tb2.order_id', $orders)->get());

        // check in initial stok
        // Avalable Data Bulan Ini
        $where_initial['type'] = 'InitialStockF5';
        $where_initial['sales_org_id']             = $reportf5->sales_org_id;
        $where_initial['sales_group_id']           = $reportf5->sales_group_id;
        $where_initial['customer_id']              = $reportf5->customer_id;
        $where_initial['month']                    = intval($reportf5->month);
        $where_initial['year']                     = $reportf5->year;
        $initialstoks                              = InitialStocks::where($where_initial)->latest()->first();

        $flagstokF5=0;
        if (!$initialstoks) {
            $flagstokF5=1;
            // Jika Bulan Ini Ndak Ada Maka Ambil Initial Stok Bulan Lalu
            $f5_filter['sales_org_id']   = $reportf5->sales_org_id;
            $f5_filter['sales_group_id'] = $reportf5->sales_group_id;
            $f5_filter['customer_id']    = $reportf5->customer_id;
            $years                       = $reportf5->year;
            $months                      = $reportf5->month - 1;
            if ($reportf5->month == "01" || $reportf5->month == 1) {
                $years  = $reportf5->year - 1;
                $months = 12;
            }
            $f5_filter['month'] = $months;
            $f5_filter['year']  = $years;

            $f5Data = ReportF5::where($f5_filter)->first();

            $stokitems = ReportF5Items::where('report_f5_id', @$f5Data->id)->get()->toArray();

        } else {
            $stokitems = InitialStockItem::where('initial_stock_id', $initialstoks->id)->get()->toArray();
        }
        
        // SPJB Operational Item
        $spjb_id = array_unique(array_column($order, 'contract_id'));
        $spjb    = json_decode(ContractItem::getItemSPJB()->wherein('contract_id', $spjb_id)->get());

        if($reportf5->status=="d"){
            // Retail
        $where_r['c.id'] = $reportf5->sales_group_id;
        $where_r['f.id'] = $reportf5->customer_id;
        $where_r['g.id'] = $reportf5->sales_org_id;
        $last            = date('Y-m-t', strtotime($reportf5->year . "-" . $reportf5->month));
        $retails = json_decode(Retail::retailDitributionDO($where_r)
                ->whereRaw("e.created_at <= '" . $last . "'")
                ->where('e.status','y')
                ->get());
        }else{
            $inRetails=DistribReportItems::where('wcm_distrib_report_item.report_f5_id',$reportf5->id)
            ->rightjoin('wcm_distrib_reports', function($join) use ($reportf5){
                $join->on('wcm_distrib_reports.id','=','wcm_distrib_report_item.distrib_report_id')
                     ->where('wcm_distrib_reports.distrib_resportable_type','order');
            })->pluck('retail_id')->unique();
            
            $retails = json_decode(Retail::whereIn('id',$inRetails)->where('sub_district_default','!=',1)->get());
        }

        if($getretail){
            return $retails;
        }

        // Distrib Order (Penyaluran DO)
        $distribitems = json_decode(DistribReportItems::wherein('distrib_report_id', $uniq_id)->get());

        // Find uniq Kecamatan | Sales_unit_id In SPJB
        $salesUnits = array_unique(array_column($spjb, 'sales_unit_id'));

        // Varibel Print Out
        $print['header'] = [
            'uuid'              => $reportf5->uuid,
            'id'                => $reportf5->id,
            'number'            => $reportf5->number,
            'sales_org_name'    => $reportf5->sales_org_name,
            'customer_id'       => $reportf5->customer_id,
            'customer_name'     => $reportf5->customer_name,
            'sales_group_id'    => $reportf5->sales_group_id,
            'sales_group_name'  => $reportf5->sales_group_name,
            'submited_date'     => $reportf5->submited_date,
            'sales_office_name' => $reportf5->sales_office_name,
            'month'             => $this->monthName($reportf5->month),
            'year'              => $reportf5->year,
            'status'            => $reportf5->status,
            'status_name'       => $reportf5->status_name,
        ];
        $print['data']             = [];
        $print['total']            = array('awal' => $this->makeHeaderKec($produks), 'penebusan' => $this->makeHeaderKec($produks), 'penyaluran' => $this->makeHeaderKec($produks), 'akhir' => $this->makeHeaderKec($produks));
        $print['produks']          = array();
        $print['posts']['f6']      = array();
        $print['posts']['f5items'] = array();

        // Make Arr Kecamatan

        // Looping Kecamatan
        foreach ($salesUnits as $key => $valUnit) {
            # code...
            // Set Kecamtan Atrributes Id anda Name
            $kecamatanArr = array('id' => $spjb[$key]->sales_unit_id, 'name' => $spjb[$key]->sales_unit_name, 'data' => array(), 'total' => array('awal' => $this->makeHeaderKec($produks), 'penebusan' => $this->makeHeaderKec($produks), 'penyaluran' => $this->makeHeaderKec($produks), 'akhir' => $this->makeHeaderKec($produks)));

            // Find Retail Int This Sales unit / Kecamatan
            $ratail_keys     = array_keys(array_column($retails, 'sales_unit_id'), $valUnit);
            $retail_unit_arr = $this->makeArr($retails, $ratail_keys);

            // Find Uniq Retail Pada Kecamatan
            $retailUniqKec = array_unique(array_column($retail_unit_arr, 'id'));

            // Make Arr Retail
            $retailArr = array('id' => '', 'name' => '', 'retail_code' => '', 'data' => array());

            // Lopping Retails
            foreach ($retailUniqKec as $key => $valRetails) {
                # code...
                $retailArr['id']          = $valRetails;
                $retailArr['retail_code'] = $retail_unit_arr[$key]->code;
                $retailArr['name']        = $retail_unit_arr[$key]->name;

                // Distrib item Kecamatan distribitems
                $distrib_r_keys         = array_keys(array_column($distribitems, 'retail_id'), $valRetails);
                $ditribitems_retail_arr = $this->makeArr($distribitems, $distrib_r_keys);

                // Make Arr Retail
                $produkArr = array('awal' => array(), 'penebusan' => array(), 'penyaluran' => array(), 'akhir' => array());

                // Looping Produks
                foreach ($produksUniq as $key => $valP) {
                    # code...
                    $distrib_p_keys          = array_keys(array_column($ditribitems_retail_arr, 'product_id'), $valP);
                    $ditribitems_produks_arr = $this->makeArr($ditribitems_retail_arr, $distrib_p_keys);

                    $qty = array_sum(array_column($ditribitems_produks_arr, 'qty'));

                    array_push($produkArr['penyaluran'], array('id' => $valP, 'qty' => $qty));
                    array_push($produkArr['awal'], array('id' => $valP, 'qty' => 0));
                    array_push($produkArr['akhir'], array('id' => $valP, 'qty' => 0));
                    array_push($produkArr['penebusan'], array('id' => $valP, 'qty' => 0));

                    $kecamatanArr['total']['penyaluran'][$valP]['qty'] += $qty;
                    $print['total']['penyaluran'][$valP]['qty'] += $qty;
                }

                $retailArr['data'] = $produkArr;

                array_push($kecamatanArr['data'], $retailArr);

            }
            $kecamatanArr['total']['penyaluran'] = $this->makeTotal($kecamatanArr['total']['penyaluran']);
            $kecamatanArr['total']['awal']       = $this->makeTotal($kecamatanArr['total']['awal']);
            $kecamatanArr['total']['akhir']      = $this->makeTotal($kecamatanArr['total']['akhir']);
            $kecamatanArr['total']['penebusan']  = $this->makeTotal($kecamatanArr['total']['penebusan']);

            if (count($retailArr['data']) != 0) {
                array_push($print['data'], $kecamatanArr);
            }
        }

        foreach ($produksUniq as $key => $valP) {
            # code...
            $stoks_keys = array_keys(array_column($stokitems, 'product_id'), $valP);
            $stoks_arr  = $this->makeArr($stokitems, $stoks_keys);

            if($flagstokF5==1){
                $qty_stoks = array_sum(array_column($stoks_arr, 'stok_akhir'));   
            }else{
                $qty_stoks = array_sum(array_column($stoks_arr, 'qty'));
            }

            $print['total']['awal'][$valP]['qty'] = $qty_stoks;

            // Delivery / Perhitungan Penebusan
            $delivery_keys = array_keys(array_column($delivery, 'product_id'), $valP);
            $delivery_arr  = $this->makeArr($delivery, $delivery_keys);

            $qty                                       = array_sum(array_column($delivery_arr, 'delivery_qty'));
            $print['total']['penebusan'][$valP]['qty'] = $qty;

            //
            $ratail_keys     = array_keys(array_column($reportf5item, 'product_id'), $valP);
            $retail_unit_arr = $this->makeArr($reportf5item, $ratail_keys);
            $nuuid           = "";
            if (count($retail_unit_arr) != 0) {
                $nuuid                                       = $retail_unit_arr[0]->uuid;
                $print['total']['penyaluran'][$valP]['uuid'] = $retail_unit_arr[0]->uuid;
                $print['total']['awal'][$valP]['uuid']       = $retail_unit_arr[0]->uuid;
                $print['total']['akhir'][$valP]['uuid']      = $retail_unit_arr[0]->uuid;
                $print['total']['penebusan'][$valP]['uuid']  = $retail_unit_arr[0]->uuid;
            }

            // Calculation total Akhir / Stok Akhir per Produks
            $print['total']['akhir'][$valP]['qty'] = ($print['total']['awal'][$valP]['qty'] + $print['total']['penebusan'][$valP]['qty']) - $print['total']['penyaluran'][$valP]['qty'];

            array_push($print['produks'], array('id' => $valP, 'name' => $produks[$key]->name));

            array_push($print['posts']['f5items'], array(
                'uuid'         => $nuuid,
                'product_id'   => $valP,
                'report_f5_id' => $reportf5->id,
                'stok_awal'    => $print['total']['awal'][$valP]['qty'],
                'penebusan'    => $print['total']['penebusan'][$valP]['qty'],
                'penyaluran'   => $print['total']['penyaluran'][$valP]['qty'],
                'stok_akhir'   => $print['total']['akhir'][$valP]['qty'],
                'number_f5'    => $reportf5->id,
            ));
        }

        $findf6 = array(
            'report_f5_id'   => $reportf5->id,
            'sales_org_id'   => $reportf5->sales_org_id,
            'customer_id'    => $reportf5->customer_id,
            'sales_group_id' => $reportf5->sales_group_id,
            'year'           => $reportf5->year,
            'month'          => intval($reportf5->month),
        );

        // Find Data Uuid For F6
        $f6 = $this->findModelWhere(ReportF6::class, $findf6);

        // Exist
        if ($f6) {
            $print['posts']['f6'] = array(
                'uuid'           => $f6->uuid,
                'number'         => $f6->number,
                'report_f5_id'   => $reportf5->id,
                'sales_org_id'   => $reportf5->sales_org_id,
                'customer_id'    => $reportf5->customer_id,
                'sales_group_id' => $reportf5->sales_group_id,
                'year'           => $reportf5->year,
                'month'          => intval($reportf5->month),
                'status'         => 'd',
            );
            // Not Exit
        } else {
            $print['posts']['f6'] = array(
                'uuid'           => '',
                'number'         => '',
                'report_f5_id'   => $reportf5->id,
                'sales_org_id'   => $reportf5->sales_org_id,
                'customer_id'    => $reportf5->customer_id,
                'sales_group_id' => $reportf5->sales_group_id,
                'year'           => $reportf5->year,
                'month'          => intval($reportf5->month),
                'status'         => $reportf5->status,
                'status'         => 'd',
            );
        }

        $print['total']['penyaluran'] = $this->makeTotal($print['total']['penyaluran']);
        $print['total']['awal']       = $this->makeTotal($print['total']['awal']);
        $print['total']['akhir']      = $this->makeTotal($print['total']['akhir']);
        $print['total']['penebusan']  = $this->makeTotal($print['total']['penebusan']);

        return $print;
    }

    private function makeArr($datas, $keysIndex)
    {

        $data = array();

        foreach ($keysIndex as $key => $value) {
            # code...
            array_push($data, $datas[$value]);

        }

        return $data;
    }

    private function makeHeaderKec($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'uuid' => '',
                'qty'  => 0,
                'id'   => $val->id,
            );
            $datas[$val->id] = $temps;
        }

        return $datas;
    }

    private function makeTotal($produks)
    {
        $datas = array();
        foreach ($produks as $key => $val) {
            # code...
            $temps = array(
                'uuid' => $val['uuid'],
                'id'   => $val['id'],
                'qty'  => $val['qty'],
            );
            // $datas[$val->id]=$temps;
            array_push($datas, $temps);
        }

        return $datas;
    }

    private function monthName($m)
    {
        switch ($m) {
            case "01":
                return 'Januari';
                break;
            case "02":
                return 'Februari';
                break;
            case "03":
                return 'Maret';
                break;
            case "04":
                return 'April';
                break;
            case "05":
                return 'Mei';
                break;
            case "06":
                return 'Juni';
                break;
            case "07":
                return 'Juli';
                break;
            case "08":
                return 'Agustus';
                break;
            case "09":
                return 'September';
                break;
            case "10":
                return 'Oktober';
                break;
            case "11":
                return 'November';
                break;
            case "12":
                return 'Desember';
                break;
            default:
                return '-';
        }
    }

    private function genNumF6()
    {
        $maxNumber = DB::table('wcm_report_f6')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "F6" . $number;
    }

    private function findModelWhere($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }
}
