<?php

namespace App\Http\Controllers;

use App\Imports\VillageImport;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $columns = [
            "sales_office_id"   => "sales_office_id",
            "sales_office_name" => "sales_office_name",
            "sales_group_id"    => "sales_group_id",
            "sales_group_name"  => "sales_group_name",
            "sales_unit_id"     => "sales_unit_id",
            "sales_unit_name"   => "sales_unit_name",
            "village_code"      => "village_code",
            "village_id"        => "village_id",
            "village_name"      => "village_name",
        ];

        return DataTables::of(Village::getAll())
            ->filter(function ($query) use ($columns) {
                $this->filterColumn($columns, request(), $query);
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $params = $request->only(["sales_office_id", "sales_group_id", "sales_unit_id", "code", "name"]);

        $params["village"] = [
            "sales_unit_id"   => $request->get("sales_unit_id"),
            "sales_group_id"  => $request->get("sales_group_id"),
            "sales_office_id" => $request->get("sales_office_id"),
            "village_code"    => $request->get("code"),
        ];

        $this->validate($params, Village::validator());
        unset($params["village"]);
        $params["created_by"] = $request->user()->id;
        $params["updated_by"] = $request->user()->id;

        try {
            $village = Village::create($params);
        } catch (\Exception $e) {
            return response()->json(responseFail(trans("messages.create-fail")), 500);
        }

        return response()->json(responseSuccess(trans("messages.create-success"), $village));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $village                    = Village::findOrFail($id);
        $village->sales_office_id   = $village->sales_office->id;
        $village->sales_office_name = $village->sales_office->name;

        $village->sales_group_id    = $village->sales_group->id;
        $village->sales_group_name  = $village->sales_group->name;

        $village->sales_unit_id     = $village->sales_unit->id;
        $village->sales_unit_name   = $village->sales_unit->name;

        return response()->json(responseSuccess(trans("messages.read-success"), $village));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $village = Village::findOrFail($id);

        $params = $request->only(["sales_office_id", "sales_group_id", "sales_unit_id", "code", "name"]);

        $params["village"] = [
            "sales_unit_id"   => $request->get("sales_unit_id"),
            "sales_group_id"  => $request->get("sales_group_id"),
            "sales_office_id" => $request->get("sales_office_id"),
            "village_code"    => $request->get("code"),
        ];

        $this->validate($params, Village::validator("updated"));

        try {
            $params = Arr::only($params, ["code", "name", "sales_unit_id"]);

            foreach ($params as $key => $val) {
                $village->{$key} = $val;
            }

            if (!$village->isDirty()) {
                throw new \Exception(null, 1);

            }
            $village->save();

        } catch (\Exception $e) {
            return response()->json(responseFail(trans("messages.update-fail")), 500);
        }

        return response()->json(responseSuccess(trans("messages.update-success"), $village));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $village = Village::findOrFail($id);
        try {
            $village->delete();
        } catch (\Exception $e) {
            return response()->json(responseFail(trans("messages.delete-fail")), 500);
        }

        return response()->json(responseSuccess(trans("messages.delete-success"), $village));
    }

    public function import(Request $request)
    {
        if ($request->hasFile("file")) {
            $file = $request->file('file');
            $data = Excel::toArray(new VillageImport, $file)[0];

            $rows = [];
            foreach ($data as $i => $row) {
                $params["sales_office_id"] = @$row["kode_provinsi"];
                $params["sales_group_id"]  = @$row["kode_kabupaten"];
                $params["sales_unit_id"]   = @$row["kode_kecamatan"];
                $params["code"]            = @$row["kode_desa"];
                $params["name"]            = @$row["nama_desa"];
                $params["village"]         = [
                    "sales_unit_id"   => @$row["kode_kecamatan"],
                    "sales_group_id"  => @$row["kode_kabupaten"],
                    "sales_office_id" => @$row["kode_provinsi"],
                    "village_code"    => @$row["kode_desa"],
                ];

                $this->validate($params, Village::validator("uploaded"));

                $rows[] = Arr::only($params, ["sales_unit_id", "code", "name"]);
            }

            try {
                $villages = Village::insert($rows);
            } catch (\Exception $e) {
                return response()->json(responseFail($e->getMessage()), 500);
            }

            return response()->json(responseSuccess(trans("messages.create-success"), $rows));
        }

        abort(400);
    }
}
