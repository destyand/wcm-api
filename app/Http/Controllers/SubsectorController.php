<?php

namespace App\Http\Controllers;

use App\Models\SubSector;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class SubsectorController extends Controller {

    public function index(Request $request) {
        \LogActivity::addToLog('get all sub sector');

        $query = SubSector::all();
        $model = Datatables::of($query)
                ->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}
