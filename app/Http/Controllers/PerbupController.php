<?php

namespace App\Http\Controllers;

use App\Exports\PerbupExport;
use App\Exports\Download;
use App\Imports\PerbupImport;
use App\Models\ContractGoverment;
use App\Models\ContractGovItem;
use App\Models\SalesGroup;
use App\Models\Product;
use App\Traits\TraitContractGoverment;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

use function PHPSTORM_META\map;

class PerbupController extends Controller
{  
    // use TraitContractGoverment;
    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get header perbup');

        $where = [];
        if ($this->isAdminAnper) {
            $where["tb1.sales_org_id"] = $this->salesOrgId;
        }
        $query   = ContractGoverment::getHeaderPerbup($where,$exported);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb2.sales_group_id",$filters["sales_group_id"]);
            }
        }

        $columns = [
            'tb1.number'          => 'number',
            'tb1.sales_org_id'    => 'sales_org_id',
            'tb3.name'            => 'sales_org_name',
            'tb1.year'            => 'year',
            'tb2.sales_office_id' => 'sales_office_id',
            'tb4.name'            => 'sales_office_name',
            'tb2.sales_group_id'  => 'sales_group_id',
            'tb5.name'            => 'sales_group_name',
            'tb1.status'          => 'status',
            'tb1.created_by'      => 'created_by',
            'tb1.updated_by'      => 'updated_by',
            'tb1.created_at'      => 'created_at',
            'tb1.updated_at'      => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });

        if($exported)
        {
            return $model->getFilteredQuery()->get();
        }

        $model=$model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete perbup');

        $model = $this->findDataUuid(ContractGoverment::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'n'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            ContractGovItem::where('contract_gov_id', $model->id)->update(['updated_by' => Auth::user()->id, 'status' => 'n']);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {

            unset($request['rayonisasi']);

            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'erros'      => $newMessages,
                'message'    => trans('messages.duplicate-error') . $line,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function upload_old(Request $request)
    {
        \LogActivity::addToLog('upload perbup');
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $user = $request->user();
        $filters = $user->filterRegional;

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new PerbupImport, $file)[0];
            $collect = collect($data);

            // Cek template
            $cekKeys = array_keys(@$data[0]);
            if($cekKeys[0]!="sales_organization" or $cekKeys[1]!="no_doc" or $cekKeys[2]!="kode_provinsi" or $cekKeys[3]!="kode_kabupaten" or $cekKeys[4]!="kode_kecamatan"  or  $cekKeys[5]!="tahun"  or $cekKeys[6]!="bulan" or $cekKeys[7]!="kode_jenis_produk" or $cekKeys[8]!="amount_alokasi"){
                $response           = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template'=>['Format template Tidak Sesuai'],'file'=>['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $nomor = array_unique(array_column($data, 'no_doc'));

            foreach ($nomor as $key => $value) {
                if (is_null($data[$key]['no_doc'])) {
                    continue;
                }

                
                $headerData = [
                    'number'        => $data[$key]['no_doc'],
                    'contract_type' => 'perbup',
                    'sales_org_id'  => $data[$key]['sales_organization'],
                    'year'          => $data[$key]['tahun'],
                    'region'        => [
                        'sales_office_id' => $this->cekKodeSalesOffice($data[$key]['kode_provinsi']),
                        'sales_group_id'  => $this->cekKodeSalesGroup($data[$key]['kode_kabupaten']),
                        'sales_unit_id'   => $data[$key]['kode_kecamatan'],
                    ],
                ];

                $this->validateImport($headerData, ContractGoverment::perbupRuleCreate(), intval($key) + 2);
                $uniqueHeader = [
                    'year' => $data[$key]['tahun'],
                    'contract_type' => 'perbup',
                    'status' => 'y',
                ];

                $uniqueItems = [
                    "product_id" => $data[$key]['kode_jenis_produk'],
                    'sales_unit_id'   => $data[$key]['kode_kecamatan'],
                ];

                $existsOrg = ContractGoverment::where($uniqueHeader)
                        ->whereHas("items", function($q) use($uniqueItems) {
                            $q->where($uniqueItems);
                        })->pluck('sales_org_id');
                        
                if (!$existsOrg->isEmpty() && !$existsOrg->contains($data[$key]["sales_organization"])) {
                    return response()->json(
                        responseFail("Wilayah {$uniqueItems['sales_unit_id']} Produk {$uniqueItems['product_id']} Sudah tersedia oleh " . 
                        $existsOrg->implode(",")),
                        400
                    )->throwResponse();
                } 


                $headerData['created_by'] = Auth::user()->id;
                $headerData['updated_by'] = Auth::user()->id;

                $header  = ContractGoverment::create($headerData);
                $keyItem = array_keys(array_column($data, 'no_doc'), $value);

                foreach ($keyItem as $val) {
                    if (count($filters) > 0) {
                        if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0 && !in_array($data[$val]['sales_organization'],$filters["sales_org_id"])) {
                            DB::rollback();
                            $response           = responseFail(trans('messages.create-fail'));
                            $response['message'] = ['sales_org_id'=>['Sales Organization '.$data[$val]['sales_organization'].' tidak dalam Sales Area. Pada baris Ke : ' . ($key + 2)]];
                            $response['data']   = $data[$val];
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                    $conItem = [
                        'contract_gov_id' => $header->id,
                        'product_id'      => $data[$val]['kode_jenis_produk'],
                        'sales_office_id' => $this->cekKodeSalesOffice($data[$val]['kode_provinsi']),
                        'sales_group_id'  => $this->cekKodeSalesGroup($data[$val]['kode_kabupaten']),
                        'sales_unit_id'   => $data[$val]['kode_kecamatan'],
                        'month'           => intval($data[$val]['bulan']),
                        'year'            => $data[$val]['tahun'],
                    ];

                    // check update status aktif 1 product 1 kecamatan aktif
                    $check = ContractGovItem::getItemPerbup(['tb1.year'=>$conItem['year'],'tb1.product_id'=>$conItem['product_id'],'tb1.month'=> intval($conItem['month']),'tb1.status'=>'y','tb1.sales_unit_id'=>$conItem['sales_unit_id']])->where('tb1.contract_gov_id','!=',$conItem['contract_gov_id'])->first();
                    if($check)
                    {

                        DB::rollback();
                        $response           = responseFail("Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}");
                        $response['errors'] = ['Gagal Update' => "Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"];
                        $response['data'] = $check;
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                    // End check update status aktif 1 product 1 kecamatan aktif


                    $cek = $this->cekDuplikasi(ContractGovItem::class, $conItem);
                    if ($cek) {
                        DB::rollback();
                        $response           = responseFail($this->errorDuplicate($conItem));
                        $response['errors'] = trans('messages.duplicate-error') . (intval($val) + 2);
                        $response['data']   = $data[$val];
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }
                    $conItem['initial_qty'] = $data[$val]['amount_alokasi'];
                    $conItem['created_by']  = Auth::user()->id;
                    $conItem['updated_by']  = Auth::user()->id;
                    $conItem['region'] = [
                        'sales_office_id' => $this->cekKodeSalesOffice($data[$val]['kode_provinsi']),
                        'sales_group_id' => $this->cekKodeSalesGroup($data[$val]['kode_kabupaten']),
                        'sales_unit_id' => $data[$val]['kode_kecamatan'],
                    ];
                    $conItem['rayonisasi'] = [
                        'sales_org_id' => $data[$val]['sales_organization'],
                        'kode_provinsi' => $data[$val]['kode_provinsi'],
                    ];
                    $this->validateImport($conItem, ContractGovItem::perbupRuleCreate(), (intval($val) + 2));
                    unset($conItem['region']);
                    unset($conItem['rayonisasi']);

                    ContractGovItem::create($conItem);
                }
            }
            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'), $header);
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = $ex->getMessage();
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function upload(Request $request)
    {
        \LogActivity::addToLog('upload perbup');
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $user = $request->user();
        $filters = $user->filterRegional;

        DB::beginTransaction();
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new PerbupImport, $file)[0];

            // Cek template
            $cekKeys = array_keys(@$data[0]);
            if($cekKeys[0]!="sales_organization" or $cekKeys[1]!="no_doc" or $cekKeys[2]!="kode_provinsi" or $cekKeys[3]!="kode_kabupaten" or $cekKeys[4]!="kode_kecamatan"  or  $cekKeys[5]!="tahun"  or $cekKeys[6]!="bulan" or $cekKeys[7]!="kode_jenis_produk" or $cekKeys[8]!="amount_alokasi"){
                $response           = responseFail(trans('messages.create-fail'));
                $response['message'] = ['template'=>['Format template Tidak Sesuai'],'file'=>['Format template Tidak Sesuai']];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }
            //  End Cek Tempalte
            $warning=[];
            foreach ($data as $key => $value) {
                # code...

                if(is_null($value['no_doc']))
                {
                    continue;
                }

                $this->validateImportXls(['sales_org_id'=>$value['sales_organization']], ContractGoverment::perbupRuleCreate(), intval($key) + 2);

                $arrHeader =['number' => $value['no_doc'],'contract_type' => 'perbup', 'sales_org_id'  => $value['sales_organization'],'year'=> $value['tahun']];
                // cek data header create or not
                $header = ContractGoverment::where($arrHeader)->where('status','!=','n')->first();

                if(!$header){
                    $arrHeader['created_by'] = Auth::user()->id;
                    $header  = ContractGoverment::create($arrHeader);
                }

                $conItem = [
                    'sales_org_id'    => $header->sales_org_id,
                    'contract_gov_id' => $header->id,
                    'product_id'      => strtoupper($value['kode_jenis_produk']),
                    'sales_office_id' => $this->cekKodeSalesOffice($value['kode_provinsi']),
                    'sales_group_id'  => $this->cekKodeSalesGroup($value['kode_kabupaten']),
                    'sales_unit_id'   => $value['kode_kecamatan'],
                    'month'           => intval($value['bulan']),
                    'year'            => $value['tahun'],
                ];

                $attr['perbup']  = $conItem; $attr['perbup']+=['contract_type'=>'perbup'];
                $attr['one_aktive'] = $attr['perbup'];
                
                $this->validateImportXls($attr,ContractGovItem::headerSalesOrgPerbupStatus(),intval($key)+2);
                unset($conItem['sales_org_id']);
                $cek = $this->cekDuplikasi(ContractGovItem::class, $conItem);
                if ($cek) {
                    DB::rollback();
                    $response           = responseFail(['duplicate'=>[trans('messages.duplicate-error').($key + 2)]]);
                    $response['errors'] = trans('messages.duplicate-error') ." Baris Ke :". ($key + 2);
                    $response['data']   = $value;
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
                $conItem['initial_qty'] = $value['amount_alokasi'];
                $conItem['created_by']  = Auth::user()->id;
                $conItem['updated_by']  = Auth::user()->id;
                $conItem['region'] = [
                    'sales_office_id' => $this->cekKodeSalesOffice($value['kode_provinsi']),
                    'sales_group_id' => $this->cekKodeSalesGroup($value['kode_kabupaten']),
                    'sales_unit_id' => $value['kode_kecamatan'],
                ];
                $conItem['rayonisasi'] = [
                    'sales_org_id' => $value['sales_organization'],
                    'kode_provinsi' => $value['kode_provinsi'],
                ];
                $this->validateImport($conItem, ContractGovItem::perbupRuleCreate(), ($key+2));
                unset($conItem['region']);
                unset($conItem['rayonisasi']);

                ContractGovItem::create($conItem);
                // warning
                $sumpergub=$this->sumqty('pergub',$value);
                $sumperbup=$this->sumqty('perbup',$value);
                if($sumpergub<$sumperbup)
                {
                    array_push($warning,['Jumlah SK Kab '.$conItem['sales_unit_id'].', Kabupaten: '.$conItem['sales_group_id'].' Provinsi : '.$conItem['sales_office_id'].' Sales Org '.$value['sales_organization'].', Bulan '.$value['bulan'].' melebihi jumlah pergub']);
                }
                // end warning
            }

            try {
                DB::commit();
                $response = responseSuccess(trans('messages.create-success'));
                $response['warning'] = $warning;
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
            } catch (\Exception $ex) {
                DB::rollback();
                $response           = responseFail(trans('messages.create-fail'));
                $response['errors'] = ['Exception'=>$ex->getMessage()];
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

        }else{
            $response = responseFail('File Import tidak ditemuakan');
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }


    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["No SK Dinas Kab","Produsen","Provinsi","Kabupaten","Tahun","Tanggal","Status"];
        return Excel::download((new Download($data,$columns)), "PerbupDownload.xls");
    }

    public function updateStatus(Request $request) {
        
        \LogActivity::addToLog('Update Status Header Pergub');
        $attributes = $request->all();

        $this->validate($attributes,["status" => "in:y,n","uuid" => "required|array"]);

        DB::beginTransaction();

        $contracts = ContractGoverment::whereIn("uuid", $attributes['uuid'])->get();
        $status = $contracts->pluck("status")->unique();

        if(count($status)>1){
            DB::rollback();
            $response           = responseFail(['status'=>['Status yang dipilih lebih dari 1 ']]);
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        $arr=[];
        foreach ($attributes['uuid'] as $key => $value) {
            # code...
            $contract = ContractGoverment::where('uuid',$value)->where('contract_type','perbup')->first();
            if(!$contract){
                DB::rollback();
                $response           = responseFail(['data'=> ['Data tidak valid']]);
                return response()->json($response, 500, [], JSON_PRETTY_PRINT);
            }

            $items = $contract->items()->get();
            
            foreach ($items as $key => $item) {
                # code...
                $attr['perbup']  = [
                    'contract_type'=> 'perbup',
                    'sales_org_id' => $contract->sales_org_id,
                    'year'         => $contract->year,
                    'product_id'   => $item['product_id'],
                    'month' => intval($item['month']),
                    'sales_office_id' => $item['sales_office_id'],
                    'sales_group_id' => $item['sales_group_id'],
                    'sales_unit_id' => $item['sales_unit_id'],
                ];
                $attr['one_aktive']=$attr['perbup'];

                $govitem=$contract->items()->where(['contract_gov_id'=>$item['contract_gov_id'],'month'=>$item['month'],'year'=>$item['year'],'sales_office_id'=> $item['sales_office_id'],'product_id'=>$item['product_id'],'sales_group_id'=>$item['sales_group_id'],'sales_unit_id'=>$item['sales_unit_id']])->first();
                
                if($attributes['status']=='y' and $govitem->status=='n'){
                    $this->validate($attr,ContractGovItem::headerSalesOrgPerbupStatus());
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }else{
                    $govitem->update(['status'=>$attributes['status'],'updated_by'=>Auth::user()->id]);
                }
                array_push($arr,$attr['perbup']);
            }

            $contract->touchStatus();
        }
        

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'),$arr);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function cekDuplikasi($model, $attrib)
    {

        $query = $model::where($attrib);//->where('status', '!=', 'n');

        $check = $query->first();

        return $check;
    }

    public function errorDuplicate($attrib)
    {
        $duplikasi = [];
        foreach ($attrib as $key => $value) {
            $duplikasi[$key] = [trans('messages.duplicate')];
        }
        return $duplikasi;
    }

    private function cekKodeSalesOffice($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }

    private function cekKodeSalesGroup($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length < 4) {
            $kabupaten = SalesGroup::where('district_code', $sales_group_id)->first();
            return $kabupaten->id;
        }
        return $sales_group_id;
    }

    public function export(Request $request)
    {
        $this->validate($request->all(), ['uuid' => 'required']);
        try {

            $uuid     = $request->get('uuid');
            $contract = ContractGoverment::where("uuid", $uuid)->firstOrFail();

        } catch (\Exception $e) {
            return response()->json(responseFail($e->getMessage()), 500);
        }

        return Excel::download(
            new PerbupExport($contract),
            str_slug("{$contract->number}") . ".xls"
        );
    }

    private function checkOneActiveInSalesUnit($itemRequest,$type)
    {
        // check update status aktif 1 product 1 kecamatan aktif type perbup
        $error =false;
        $check = ContractGovItem::getItemPerbup(['tb1.year'=>$itemRequest['year'],'tb1.product_id'=>$itemRequest['product_id'],'tb1.month'=> intval($itemRequest['month']),'tb1.status'=>'y','tb1.sales_unit_id'=>$itemRequest['sales_unit_id']])->where('tb2.uuid','!=',$uuid)->first();
        if($check)
        {
            DB::rollback();
            $response           = responseFail("Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}");
            $response['errors'] = ['Gagal Update' => "Kecamatan {$check->sales_unit_name} , Product : {$check->product_name}, masih berstatus aktif oleh produsen {$check->sales_org_name}"];
            $response['data'] = $check;
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
        
    }

    public function validateImportXls($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'min'      => trans('messages.min'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'errors'    => 'Data tidak valid di baris ke : ' . $line,
                'message'     => $newMessages,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    private function sumqty($type,$value)
    {
        $sumpermentan = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', $type)
            ->where('tb1.month', intval($value['bulan']))
            ->where('tb2.year', $value['tahun'])
            ->where('tb1.product_id', $value['kode_jenis_produk'])
            ->where('tb1.status', 'y')
            ->where('tb2.sales_org_id', $value['sales_organization'])
            ->where('tb1.sales_office_id',$this->cekKodeSalesOffice($value['kode_provinsi']))
            ->where('tb1.sales_group_id',$this->cekKodeSalesGroup($value['kode_kabupaten']))
            ->sum('tb1.initial_qty');
        return $sumpermentan;
    }

}
