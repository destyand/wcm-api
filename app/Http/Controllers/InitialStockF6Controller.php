<?php

namespace App\Http\Controllers;
use App\Imports\InitialStocksF6Import;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Excel;
use App\Exports\Download;
use App\Models\InitialStocks;
use App\Models\InitialStockItem;
use App\Models\Customer;
use App\Models\Retail;
use App\Models\Product;
use App\Models\SalesOrg;
use App\Models\SalesOffice;
use App\Models\SalesGroup;
use App\Models\SalesUnit;
use App\Models\ReportF6;
use App\Models\ReportF6Items;
use App\Models\DistribReports;
use App\Models\DistribReportItems;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\DataTables;

class InitialStockF6Controller extends Controller
{
    //
    private $type = 'InitialStockF6';

    public function index(Request $request, $exported=false)
    {
    	\LogActivity::addToLog('Get All Data InitialStockF6');
        $where = [];
        if ($this->isAdminAnper) {
            $where["sales_org_id"] = $this->salesOrgId;
        }

		$model = DB::table("view_initial_stock_f6")
				->leftJoin('wcm_months as tb2', 'tb2.id', '=', 'view_initial_stock_f6.month');
		if($exported){
			$model->select('sales_org_name','customer_name','status_draft','status_submit','sales_office_name','sales_group_name','retail_code','retail_name','tb2.name as month_name','year','p01_qty','p01_reduce','p02_qty','p02_reduce','p03_qty','p03_reduce','p04_qty','p04_reduce','p05_qty','p05_reduce');
		}else{
			$model->select('view_initial_stock_f6.*','tb2.name as month_name');
		}
            $model->where($where);
		
		$user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $model->where("customer_id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $model->whereIn("sales_org_id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $model->whereIn("sales_group_id", $filters["sales_group_id"]);
                }
            }
        }


    	$filteringColumns = [
			"sales_org_id" => "sales_org_id",
			"sales_org_name" => "produsen_name",
			"customer_id" => "customer_id",
			"customer_name" => "distributor_name",
			"sales_office_id" => "sales_office_id",
			"sales_office_name" => "provinsi_name",
			"sales_group_id" => "sales_group_id",
    		"sales_group_name" => "kabupaten_name",
    		"sales_unit_name" => "kecamatan_name",
    		"retail_name" => "retail_name",
    		"retail_code" => "retail_code",
    		"month"	=> "month",
    		"year" => "year",
    	];

    	$model = DataTables::of($model)
				->filter(function ($model) use($filteringColumns) {
                    $this->filterColumn($filteringColumns, request(), $model);
                });
                
        if($exported) return $model->getFilteredQuery()->get();
        return $model->make(true);
    }

    public function upload(Request $request)
    {
    	\LogActivity::addToLog('Upload InitialStockF6');
		$validator = Validator::make($request->all(), [
		    'file' => 'required|max:10000|mimes:xls,xlsx'
		]);

		if ($validator->fails()) {
			$response = responseFail(trans('messages.create-fail'));
            $response['errors'] = $validator->errors('file');
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
		}
		
    	if ($request->hasFile('file')) {
    		$file = $request->file('file');

            $rows = \Excel::toArray(new InitialStocksF6Import,$file)[0];

			$user    = $request->user();
        	$filters = $user->filterRegional;
			
			$insertData = [];

			DB::begintransaction();

			$products = Product::get();


			foreach ($rows as $row => $value) {


				$data = [
		            'type' => $this->type,
		            'sales_org_id' => $value['produsen'],
		            'customer_id' => $value['no_distributor'],
		            'sales_office_id' => $this->cekKodeSalesOffice($value['kode_provinsi']),
		            'sales_group_id' =>str_pad($value['kode_kotakabupaten'], 4, "0", STR_PAD_LEFT),
		            'sales_unit_id' =>$value['kode_kecamatan'],
		            'month' => intval($value['bulan']),
		            'year' => $value['tahun'],
		            'retail_id' => $this->cekKodeRetailId($value['no_pengecer'],intval($row) + 2),
		            'region' => [
                        'sales_office_id' => $this->cekKodeSalesOffice($value['kode_provinsi']),
                        'sales_group_id' => $this->cekKodeSalesGroup(str_pad($value['kode_kotakabupaten'], 4, "0", STR_PAD_LEFT)),
                        'sales_unit_id' => $value['kode_kecamatan'],
                    ]
		        ];

		        $this->validateImport($data, InitialStocks::ruleUpload(), intval($row) + 2);

		        // Sales org sesuai Sales Area
		        if (count($filters) > 0) {
                    if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                        if (!in_array($data['sales_org_id'], $filters["sales_org_id"])) {
                            DB::rollback();
                            $response           = responseFail(trans('messages.create-fail'));
                            $response['errors'] = 'Produsen Tidak Sesuai User Management Pada baris : ' . (intval($row) + 2);
                            $response['data']   = $value;
                            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                        }
                    }
                }

		        // Check report F6 must exist
		        $reportf6 = $this->checkReportF6($data,intval($row)+2);

		        $retail = $this->inRetails($reportf6,$value['no_pengecer'],intval($row)+2);

		        $data['created_by'] = $request->user()->id;
	            $data['updated_by'] = $request->user()->id;
                $data['created_at'] = now();
	            unset($data['region']);

				$initialStock = InitialStocks::insertGetId($data);

				
				$this->saveItem($products,$initialStock, $value);

				$attributes['sales_org_id']	= $data['sales_org_id'];
				$attributes['report_f5_id']	= $reportf6->report_f5_id;
				$attributes['number']		= $this->genUniqPKP();
				$attributes['customer_id']	= $data['customer_id'];
				$attributes['sales_group_id']	= $data['sales_group_id'];
				$attributes['month']	= (int) $data['month'];
				$attributes['year']	= $data['year'];
				$attributes['initial_stock_id'] = $initialStock;
				$attributes['status']	= 's';
				$attributes['created_by']	= Auth::user()->id;
				$attributes['distrib_resportable_type']= 'InitialStockF6';
				$attributes['distribution_date'] = now();
                $attributes['created_at'] = now();

				$distribreport = DistribReports::insertGetId($attributes);

				$this->saveItemPenyaluran($products,$distribreport, $value, $reportf6->report_f5_id, $data['retail_id']);

				$products->map(function ($item) use ($reportf6, $value) {
		    		$name = $item->name;;
		    		$item_id = $item->id;
                    if($name=="ORGANIK CAIR") {
                        $qty= is_numeric($value['org_cair_liter']) ? floatval($value['org_cair_liter']) : 0;
                    }else if($name=="NPK KAKAO"){
                        $qty= is_numeric($value['npk_kakao_ton']) ? floatval($value['npk_kakao_ton']) : 0;
                    }else{
                        $qty= is_numeric($value[ str_replace('-', '_', strtolower($name)) . "_ton"]) ? $value[ str_replace('-', '_', strtolower($name)) . "_ton"] : 0;
                    }
		    		// $qty = $value[ str_replace('-', '_', strtolower($name)) . "_ton"];
		    		$reportf6->items()
		    			->where('product_id',$item_id)
		    			->update(['stok_awal' => floatval($qty),'updated_by'=>Auth::user()->id]);
		    	});
			};

			try {
				DB::commit();
                $response = responseSuccess(trans('messages.create-success'));
                return response()->json($response, 201, [], JSON_PRETTY_PRINT);
			} catch (\Exception $e) {
				DB::rollback();
				$response = responseFail(trans('messages.create-fail'));
	            $response['errors'] = ['rocord' => [$e->getMessage()]];
	            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
			}
    	}

    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $data    = $this->index($request, true);
        $columns = ["Produsen","Nama Distributor","Dokumen Draft","Dokumen Submited","Provinsi","Kabupaten","Kode Pengecer","Pengecer","Bulan","Tahun","Awal","Sisa","Awal","Sisa","Awal","Sisa","Awal","Sisa","Awal","Sisa"];
        return Excel::download((new Download($data,$columns)), "Download InitialStocks F5.xls");
    }

    private function prepareData(array $data)
    {
    	$header = collect($data)->first();

		$detail  = collect($data)->slice(1);

		$rows = $detail->map(function ($row) use ($header) {
			return collect($header)->combine($row);
		});

		return $rows;
    }

    public function validateImport($request, $rules, $line)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {

            unset($request['rayonisasi']);

            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'errors'    => $newMessages,
                'message'     => trans('messages.duplicate-error') . $line,
                'data'       => $request,
            ];

            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }


    private function saveItem($products,$initialStock, $row)
    {
        $initialStock = InitialStocks::where('id',$initialStock)->first();
    	$products->map(function ($item) use ($initialStock, $row) {
    		$name = $item->name;
            
            if($name=="ORGANIK CAIR") {
                $qty= is_numeric($row['org_cair_liter']) ? floatval($row['org_cair_liter']) : null;
            }else if($name=="NPK KAKAO"){
                $qty= is_numeric($row['npk_kakao_ton']) ? floatval($row['npk_kakao_ton']) : null;
            }else{
                $qty= is_numeric($row[ str_replace('-', '_', strtolower($name)) . "_ton"]) ? floatval($row[ str_replace('-', '_', strtolower($name)) . "_ton"]) : null;
            }

    		$item = [
    			'product_id' => $item->id,
    			'qty' => $qty,
    			'created_by' => Auth::user()->id,
    			'updated_by' => Auth::user()->id,
    		];

    		$initialStock->items()->create($item);
    	});
    }

    private function saveItemPenyaluran($products,$distribreport, $row, $reportf5 , $retail)
    {
        $distribreport=DistribReports::where('id',$distribreport)->first();
    	$products->map(function ($item) use ($distribreport, $row, $reportf5, $retail) {
    		$name = $item->name;
            
            if($name=="ORGANIK CAIR") {
                $qty= is_numeric($row['org_cair_liter']) ? floatval($row['org_cair_liter']) : 0;
            }else if($name=="NPK KAKAO"){
                $qty= is_numeric($row['npk_kakao_ton']) ? floatval($row['npk_kakao_ton']) : 0;
            }else{
                $qty= is_numeric($row[ str_replace('-', '_', strtolower($name)) . "_ton"]) ? $row[ str_replace('-', '_', strtolower($name)) . "_ton"] : 0;
            }

    		$item = [
    			'product_id' => $item->id,
    			'qty' => floatval($qty),
    			'created_by' => Auth::user()->id,
    			'updated_by' => Auth::user()->id,
    			'report_f5_id' => $reportf5,
    			'retail_id' => $retail,
    			'distrib_report_id' => $distribreport->id,
    		];

    		$distribreport->distribItems()->create($item);
    	});
    }

    private function checkReportF6($data,$row)
    {
    	$where =['sales_org_id'=>$data['sales_org_id'],
    	'customer_id'=>$data['customer_id'],
    	'sales_group_id'=>$data['sales_group_id'],
    	'month' => $data['month'],
    	'year' => $data['year']
    	];
    	$check = ReportF6::where($where)->first();
    	if(!$check)
        {
            DB::rollback();
            $response           = responseFail("Data ReportF6 Tidak ditemukan. Baris : ".$row);
            $response['errors'] = ['ReportF6 Not Found' => "Data ReportF6 Tidak ditemukan. Baris : ".$row];
            $response['data'] = $check;
            $return = response()->json($response, 500, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }

        if($check->status!='d')
        {
            DB::rollback();
            $response           = responseFail("Status Data ReportF6 harus Draft. Baris : ".$row);
            $response['errors'] = ['status' => "Status Data ReportF6 harus Draft. Baris : ".$row];
            $response['data'] = $check;
            $return = response()->json($response, 500, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }

        // return ReportF6::where('id',$check->id)->get();
        return $check;
    }

    private function inRetails($reportf6,$retail_id,$row)
    {
    	$retail  = Retail::where('code',$retail_id)->select('id')->first();
    	$retails = ReportF6Items::where('report_f6_id',$reportf6->id)->select('retail_id')->distinct()->get()->pluck('retail_id')->unique()->toArray();
    	// return $retails;
    	if(!in_array($retail->id, $retails))
    	{
    		$response           = responseFail("No Pengecer Tidak ditemukan di Laporan F6. Baris : ".$row);
            $response['errors'] = ['Pengecer F6 Not Found' => "No Pengecer Tidak ditemukan di Laporan F6. Baris : ".$row];
            $return = response()->json($response, 500, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
    	}

    	return true;

    }

    private function cekKodeSalesOffice($sales_office_id)
    {
        $length = strlen($sales_office_id);

        if ($length < 4) {
            return str_pad($sales_office_id, 4, "0", STR_PAD_LEFT);
        }
        return $sales_office_id;
    }

    private function cekKodeSalesGroup($sales_group_id)
    {
        $length = strlen($sales_group_id);

        if ($length < 4) {
            $kabupaten = SalesGroup::where('district_code', $sales_group_id)->first();
            return $kabupaten->id;
        }
        return $sales_group_id;
    }

    private function genUniqPKP()
    {
        $maxNumber = DB::table('wcm_distrib_reports')->max('id') + 1;
        $number    = str_pad($maxNumber, 10, '0', STR_PAD_LEFT);
        return "PKP" . $number;
    }

    private function cekKodeRetailId($retail_code,$row)
    {
        $retail = Retail::where('code',$retail_code)->select('id')->first();
    	
    	if(!$retail)
    	{
    		$response           = responseFail("code Pengecer tidak ditemukan baris: ".$row);
            $response['errors'] = ['Pengecer F6 Not Found' => "code Pengecer tidak ditemukan baris: ".$row];
            $return = response()->json($response, 500, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
    	}

    	return $retail->id;
    }
}

