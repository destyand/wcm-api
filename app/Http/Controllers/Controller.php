<?php

namespace App\Http\Controllers;

use App\Libraries\RestClient;
use App\Models\Bank;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $isDev;
    public $isAdmin;
    public $isAdminAnper;
    public $isDistributor;
    public $customerId;
    public $salesOrgId;

    public function __construct()
    {
        $this->checkRole();
    }

    public function validate($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
            'date'     => trans('messages.date'),
            'min'     => trans('messages.min'),
            'numeric'     => trans('messages.numeric'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $validator->errors(),
            ];
            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function findData($model, $id)
    {
        try {
            $data = $model::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $response = responseFail(trans('messages.read-fail'));
            $return   = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }

        return $data;
    }

    public function findDataUuid($model, $uuid)
    {
        $data = $model::where('uuid', $uuid)->first();
        if ($data) {
            return $data;
        } else {
            $response = responseFail(trans('messages.read-fail'));
            $return   = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function findDataWhere($model, $where)
    {
        $data = $model::where($where)->first();
        if ($data) {
            return $data;
        } else {
            $response = responseFail(trans('messages.read-fail'));
            $return   = response()->json($response, 404, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || strpos($value, 'date') !== false || strpos($value, 'timestamp') !== false || $value === 'year') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif ($value === 'role') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $values = explode(';', $request->get($value));
                if (isValidRangeDate($values)) {
                    $query->whereBetween($key, $values);
                } else {
                    $query->whereIn($key, $values);
                }

            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function orderColumn($columns, $request, $query)
    {
        $order     = $request->get('order');
        $kolom     = $request->get('columns');
        $field     = $kolom[$order[0]['column']]['name'];
        $direction = $order[0]['dir'];
        foreach ($columns as $key => $value) {
            if ($field == $value) {
                $query->orderBy($key, $direction);
            }
        }
    }

    private function checkRole()
    {
        if (isset(Auth::user()->id)) {
            $user    = User::find(Auth::user()->id);
            $role_id = $user->roles->pluck('id')[0];

            $this->customerId = $user['customer_id'] ? $user['customer_id'] : '';
            
            if ($user->sales_org_id) {
                $this->salesOrgId = $user->sales_org_id;
            } else {
                $filters = $user->filterRegional;
                if (@$filters["sales_org_id"]) {
                    $this->salesOrgId = $filters["sales_org_id"];
                }
            }
            switch ($role_id) {
                case '1':
                    $this->isDev = true;
                    break;
                case '2':
                    $this->isAdmin = true;
                    break;
                case '3':
                    $this->isAdminAnper = true;
                    break;
                case '4':
                    $this->isDistributor = true;
                    break;
            }
        }
    }

    public function generateCode($model, $field, $prefix, $long)
    {
        $maxCode = ($model instanceof Builder) ? $model->max($field) : $model::max($field);
        if ($maxCode) {
            $val = Str::after($maxCode, $prefix);
            $code = $prefix . str_pad($val + 1, $long, "0", STR_PAD_LEFT);
        } else {
            $code = $prefix . str_pad(1, $long, "0", STR_PAD_LEFT);
        }
        return $code;
    }

    public function generateCodeBulk($val, $prefix, $long)
    {
        $code = $prefix . str_pad($val + 1, $long, "0", STR_PAD_LEFT);

        return $code;
    }

    public function getMaxCode($model)
    {
        $maxCode = ($model instanceof Builder) ? $model->max("code") : $model::max("code");
        if ($maxCode) {
            preg_match_all('!\d+!', $maxCode, $matches);
            $val = intval($matches[0][0]);

            return $val;
        } else {
            return 0;
        }
    }

    public function cekDuplikasi($model, $attrib)
    {

        $query = $model::where($attrib);

        $check = $query->first();

        return $check;
    }

    public function errorDuplicate($attrib)
    {
        $duplikasi = [];
        foreach ($attrib as $key => $value) {
            $duplikasi[$key] = [trans('messages.duplicate')];
        }
        return $duplikasi;
    }

    public function cekStatus($old, $new)
    {
        if ($old == 'd' && $new == 's') {
            return true;
        }
        else if ($old == 's' && $new == 'y') {
            return true;
        }
        // else if ($old == 'y' && $new == 's') {
        //     return true;
        // }
        else if ($old == 's' && $new == 'd') {
            return true;
        } else if ($old == 'y' && $new == 'n') {
            return true;
        } else if ($old == 'n' && $new == 'd') {
            return true;
        } else if ($old == 'n' && $new == 's') {
            return true;
        } else if ($old == 'y' && $new == 'p') {
            return true;
        } else if ($old == 'p' && $new == 'y') {
            return true;
        }
        return false;
    }

    public function validateStatusChange($uuid, $old, $new)
    {
        if (!$this->cekStatus($old, $new)) {
            $response                       = responseFail(trans('messages.status-check-fail'));
            $response['data']['uuid']       = $uuid;
            $response['data']['status_old'] = $old;
            $response['data']['status_new'] = $new;
            $return                         = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    public function validateCheckLimit($order) {
        
        if (!$order) {
            $response = response()->json(
                responseFail(trans("messages.invalid-order")), 
                400
            )
            ->throwResponse();
        }

        $bank = Bank::find($order->bank_id);
        if (!$bank) {
            $response = response()->json(
                responseFail(trans("messages.invalid-bank")), 
                400
            )
            ->throwResponse();
        }
        $bankName = Str::after(Str::lower($bank->name),"bank ");
        $client = new RestClient();
        $h2hAddress = env("H2H_ADDRESS", "36.89.154.94:8010");
        $urlCheckLimit = "http://{$h2hAddress}/api/payment_gateway/{$bankName}/check-limit";
        $response = $client->postJSON($urlCheckLimit, [
            "sales_org_id" => $order->sales_org_id,
            "customer_id" => $order->customer_id,
        ]);
        $response = is_object($response) ?: (object) $response;
        $available = (int) @$response->available;
        if (($available - $order->total_price_before_ppn) < 0) {
            return response()->json(
                responseFail(trans("messages.limit-unavailable")),
                500
            )
            ->throwResponse();
        }

        return $response;
    }
}
