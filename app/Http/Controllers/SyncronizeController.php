<?php

namespace App\Http\Controllers;

use App\Models\Syncronize;
use App\Models\Plant;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SyncronizeController extends Controller {

    public function index($params) {
        $methods = Str::camel($params);

        try {
            $query = Syncronize::mergeBindings($methods);

            \LogActivity::addToLog('Get Data Syncronize ' . $methods);
        } catch (\Exception $e) {
            return response()->json(responseFail(__($e->getMessage())), 500);
        }


        return \DataTables::of($query)->make(true);
    }
    
    public  function sync_updatePlant(Request $request)
    {
        \LogActivity::addToLog('Update Status Syncron Plant');

        $attributes = $request->all();

        DB::beginTransaction();
        try {
            $model = [];
            foreach ($attributes as $iv) {
                is_uuid($iv['id']);

                $model = $this->findDataUuid(Plant::class, $iv['id']);
                unset($iv['id']);
                $iv['updated_by'] = Auth::user()->id;

                $model->update($iv);
            }
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }
}
