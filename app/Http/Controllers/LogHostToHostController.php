<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\LogH2h;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class LogHostToHostController extends Controller
{
    //
    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('Get All Data Log Host To Host');
        $query         = LogH2h::getList();
        $filterColumns = [
            "order_id"          => "order_id",
            "timestamp_req"     => "timestamp_req",
            "pengirim_req"      => "pengirim_req",
            "ip_address_req"    => "ip_address_req",
            "pengirim_respon"   => "pengirim_respon",
            "request_type"      => "request_type",
            "timestamp_respon"  => "timestamp_respon",
            "ip_address_respon" => "ip_address_respon",
            "http_status"       => "http_status",
        ];

        $model = DataTables::of($query)
            ->filter(function ($query) use ($filterColumns) {
                $this->filterColumn($filterColumns, request(), $query);
            });
        if ($exported) return $model->getFilteredQuery();
        return $model->make(true);
    }

    public function download(Request $request)
    {
        $request = $request->merge(["length" => -1]);
        $model = $this->index($request, true);
        $data = $model->select(LogH2h::getExportedColumns());
        return Excel::download(new Download($data->get()), "log-h2h.xls");
    }
}
