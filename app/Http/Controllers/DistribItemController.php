<?php

namespace App\Http\Controllers;

use App\Models\DistribReportItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class DistribItemController extends Controller
{
    //
    public function index(Request $request)
    {
        \LogActivity::addToLog('Penyaluran Item');

        $where = [];

        $query   = DistribReportItems::DitribItems($where)->where('tb1.qty', '!=', '0');

        $user = $request->user();
        $filters = $user->filterRegional;
        
        if($user->customer_id != ""){
             $query->where("tb8.id", $user->customer_id);
        }

        if($user->customer_id == ""){
            if (count($filters) > 0) {
                if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                    $query->whereIn("tb9.id", $filters["sales_org_id"]);
                }

                if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                    $query->whereIn("tb6.id", $filters["sales_group_id"]);
                }
            }
        }

        
        $columns = [
            'tb3.uuid'                     => 'uuid',
            'tb3.number'                   => 'number_pkp',
            'tb9.id'                       => 'produsen_id',
            'tb9.name'                     => 'produsen_name',
            'tb8.full_name'                => 'customer_name',
            'tb8.id'                       => 'customer_id',
            'tb3.distrib_resportable_type' => 'distrib_type',
            'tb10.number'                  => 'order_number',
            'tb3.so_number'                => 'so_number',
            'tb3.month'                    => 'month',
            'tb3.year'                     => 'year',
            'tb3.distribution_date'        => 'distribution_date',
            'tb7.id'                       => 'sales_office_id',
            'tb7.name'                     => 'sales_office_name',
            'tb6.id'                       => 'sales_group_id',
            'tb6.name'                     => 'sales_group_name',
            'tb5.id'                       => 'sales_unit_id',
            'tb5.name'                     => 'sales_unit_name',
            'tb2.code'                     => 'retail_code',
            'tb2.name'                     => 'retail_name',
            'tb4.name'                     => 'product_name',
            'tb1.qty'                      => 'qty',
            'tb1.status'                   => 'status',
        ];
        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            })
            ->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query)
    {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query)
    {
        if ($request->get($value)) {

            if ($value === 'created_at' || $value === 'updated_at' || $value === 'distribution_date' || $value === 'year') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), 'like', "%{$request->get($value)}%");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif (strpos($request->get($value), ';') !== false) {
                $query->whereIn($key, explode(';', $request->get($value)));
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function download()
    {
        $columns = [
            "number_pkp", "produsen_id", "produsen_name",
            "customer_name", "customer_id", "distrib_type", "order_number", "so_number",
            "month", "year", "distribution_date", "sales_office_id",
            "sales_office_name", "sales_group_id", "sales_group_name", "sales_unit_id",
            "sales_unit_name", "retail_id", "retail_code", "retail_name",
            "product_id", "product_name", "qty", "status",
            "status_name", "report_f5_id", "number",
        ];

        $params    = request()->only($columns);
        $filtering = collect($params)->filter()->toArray();
        $where     = [];
        if ($this->isAdminAnper) {
            $where["tb3.sales_org_id"] = $this->salesOrgId;
        }
        $items = DistribReportItems::DitribItems($where)->addSelect("tb1.report_f5_id");
        
        $user = Auth::user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $items->whereIn("tb3.sales_org_id", $filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $items->whereIn("tb5.sales_group_id", $filters["sales_group_id"]);
            }
        }

        $sql   = DB::table(
            DB::raw("(" . $items->toSql() . ")as tbitems")
        )
            ->mergeBindings($items)
            ->join("wcm_report_f5 as tb_f5", "tbitems.report_f5_id", "tb_f5.id")
            ->leftjoin("wcm_report_f6 as tb_f6", "tb_f5.id", "tb_f6.report_f5_id")
            ->select("tbitems.*", DB::raw("
                (
                    CASE WHEN tbitems.distrib_type = 'DistribReportGroup'
                    THEN tb_f6.number
                    ELSE tb_f5.number
                    END
                ) as number ")
            )
            ->addSelect(DB::raw("
                (CASE
                    WHEN tb_f5.status = 'y' THEN 'Active'
                    WHEN tb_f5.status = 'n' THEN 'Inactive'
                    WHEN tb_f5.status = 'p' THEN 'Suspend'
                    WHEN tb_f5.status = 'd' THEN 'Draft'
                    WHEN tb_f5.status = 's' THEN 'Submited'
                    ELSE '-'
                END) as status_f5_f6")
            );

        $query = DB::table(DB::raw("(" . $sql->toSql() . ") as fix_table"))
            ->mergeBindings($sql)
            ->select("*")
            ->where(function ($q) use ($filtering) {
                if (!empty($filtering)) {
                    foreach ($filtering as $key => $value) {
                        $q->where($key, "like", "%{$value}%");
                    }
                }

            })
            ->get();

        return Excel::download(
            new \App\Exports\DistribItemsReport($query),
            "item_penyaluran.xls"
        );

    }
}
