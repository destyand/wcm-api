<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Imports\SupplyPointImport;
use App\Models\Plant;
use App\Models\PlantAssg;
use App\Models\SalesGroup;
use App\Models\SupplyPoint;
use App\Rules\CheckActiveSupplyRule;
use App\Traits\ValidationTemplateImport;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Yajra\DataTables\DataTables;

class SupplyPointController extends Controller
{
    use ValidationTemplateImport;
    public function index(Request $request, $exported=false)
    {
        \LogActivity::addToLog('get all supply point');

        $where = array();
        if ($this->isAdminAnper) {
            $where['tb1.sales_org_id'] = $this->salesOrgId;
        }
        $query   = SupplyPoint::getAll($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("tb1.sales_org_id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("tb1.sales_group_id",$filters["sales_group_id"]);
            }
        }
        $columns = [
            'tb2.name'                => 'sales_org_name',
            'tb3.id'                  => 'sales_office_id',
            'tb3.name'                => 'sales_office_name',
            'tb4.id'                  => 'sales_group_id',
            'tb4.name'                => 'sales_group_name',
            'tb1.plant_id'            => 'plant_id',
            'tb5.name'                => 'plant_name',
            'tb5.code'                => 'plant_code',
            'tb1.scnd_sales_group_id' => 'scnd_sales_group_id',
            'tb6.name'                => 'scnd_sales_group_name',
            'tb1.status'              => 'status',
            'tb1.created_by'          => 'created_by',
            'tb1.updated_by'          => 'updated_by',
            'tb1.created_at'          => 'created_at',
            'tb1.updated_at'          => 'updated_at',
        ];

        $model = Datatables::of($query)
            ->filter(function ($query) use ($request, $columns) {
                $this->filterColumn($columns, $request, $query);
            })
            ->order(function ($query) use ($request, $columns) {
                $this->orderColumn($columns, $request, $query);
            });
        if ($exported) return $model->getFilteredQuery();
        $model = $model->make(true);
        $response = responseDatatableSuccess(trans('messages.read-success'), $model->getData(true));

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function store(Request $request)
    {
        \LogActivity::addToLog('create supply point');

        $attributes = $request->all();
        // This Attribute For Check Existing Active data
        $attributes["active_check"] = Arr::only($attributes,  ["sales_org_id","sales_office_id","sales_group_id",'plant_id', 'scnd_sales_group_id']);

        $this->validate($attributes, SupplyPoint::ruleCreate());

        // Unset After Validation
        unset($attributes["active_check"]);

        $this->cekPlantOfficeStore($attributes['sales_office_id'], $attributes['plant_id']);        
        $attributes['number']     = $this->generateCode(SupplyPoint::class, 'number', 'SUP', 10);
        $attributes['created_by'] = Auth::user()->id;
        $attributes['updated_by'] = Auth::user()->id;
        $attributes['status']     = 'y';
        DB::beginTransaction();
        try {
            $model = SupplyPoint::create($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.create-success'), $model);
            return response()->json($response, 201, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.create-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function show($uuid)
    {
        \LogActivity::addToLog('get supply point by id');

        is_uuid($uuid);
        $where['tb1.uuid'] = $uuid;
        $model             = SupplyPoint::getAll($where)->get();

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        \LogActivity::addToLog('update supply point');

        $attributes               = $request->except(['uuid', 'code']);
        $attributes['updated_by'] = Auth::user()->id;
        
        // This Attribute For Check Existing Active data
        $attributes["active_check"] = Arr::only($attributes,  ["sales_org_id","sales_office_id","sales_group_id",'plant_id', 'scnd_sales_group_id']);

        $this->validate($attributes, SupplyPoint::ruleCreate());
        
        // Unset After Validation
        unset($attributes["active_check"]);

        $model = $this->findDataUuid(SupplyPoint::class, $id);

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function updateBulk(Request $request)
    {
        \LogActivity::addToLog('update bulk supply point');

        $attributes = $request->all();

        DB::beginTransaction();

        $model = [];
        foreach ($attributes as $iv) {
            is_uuid($iv['uuid']);
            $rules = [
                "uuid"   => "required|exists:wcm_supply_point,uuid",
                "status" => "required|in:n,y",
            ];

            $this->validate($iv, $rules);

            $model = $this->findDataUuid(SupplyPoint::class, $iv['uuid']);
            if ($iv["status"] != "n") {
                $validationColumns = [
                    "check_active" => $model->only("sales_org_id","sales_office_id","sales_group_id",'plant_id', 'scnd_sales_group_id'),
                ];
    
                $this->validate($validationColumns,["check_active" => [new CheckActiveSupplyRule]]);
            }
            //$this->validateStatusChange($model->uuid, $model->status, $iv['status']);
            unset($iv['id']);
            $iv['updated_by'] = Auth::user()->id;

            $model->update($iv);
        }

        try {
            DB::commit();
            $response = responseSuccess(trans('messages.update-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.update-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function destroy($id)
    {
        \LogActivity::addToLog('delete supply point');

        $model = $this->findDataUuid(SupplyPoint::class, $id);

        $attributes = ['updated_by' => Auth::user()->id, 'status' => 'o'];

        DB::beginTransaction();
        try {
            $model->update($attributes);
            DB::commit();
            $response = responseSuccess(trans('messages.delete-success'), $model);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $ex) {
            DB::rollback();
            $response           = responseFail(trans('messages.delete-fail'));
            $response['errors'] = $ex->getMessage();
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function upload(Request $request)
    {
        \LogActivity::addToLog('upload supply point');
        $attributes = $request->all();
        $this->validate($attributes, [
            'file' => 'required|mimes:xls,xlsx',
        ]);
        DB::beginTransaction();
        $flag       = true;
        $newData    = [];
        $valMax     = $this->getMaxCode(SupplyPoint::class);
        $attribSave = [];
        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $data = Excel::toArray(new SupplyPointImport, $file)[0];
            if(!$this->compareTemplate("supply", array_keys($data[0]))) {
                return response()->json(responseFail("Invalid Template"), 400)
                    ->throwResponse();
            }

            foreach ($data as $i => $iv) {
                $plant = Plant::where('code', $iv['kode_gudang'])->first();

                $attribValid['sales_org_id']        = $this->cekKeyExist('company_code', $iv);
                $attribValid['sales_office_id']     = $this->cekKeyExist('region', $iv);
                $attribValid['sales_group_id']      = @$plant->sales_group_id;
                $attribValid['plant_id']            = @$plant->id;
                $attribValid['scnd_sales_group_id'] = $this->cekKeyExist('kabupaten_yang_di_supply', $iv);
                $attribValid['status']              = $this->cekKeyExist('aktif', $iv);
                
                // This Attribute For Check Existing Active data
                $attribValid['active_check']        = Arr::only($attribValid,  ["sales_org_id","sales_office_id","sales_group_id",'plant_id', 'scnd_sales_group_id']);

                $error          = $this->validateImport($attribValid, SupplyPoint::ruleCreate());
                $cekPlantAssg   = $this->cekPlantAssg($attribValid);
                $cekSalesOffice = $this->cekPlantOfficeUpload($attribValid['sales_office_id'], $attribValid['plant_id'], $attribValid['scnd_sales_group_id']);

                $attribSave[$i]['sales_org_id']        = $this->cekKeyExist('company_code', $iv);
                $attribSave[$i]['sales_office_id']     = $this->cekKeyExist('region', $iv);
                $attribSave[$i]['sales_group_id']      = @$plant->sales_group_id; 
                $attribSave[$i]['plant_id']            = @$plant->id;
                $attribSave[$i]['scnd_sales_group_id'] = $this->cekKeyExist('kabupaten_yang_di_supply', $iv);
                $attribSave[$i]['status']              = $this->cekKeyExist('aktif', $iv) == 'x' ? 'y' : 'n';
                if ($error['status'] == 0) {
                    $flag                       = false;
                    $attribSave[$i]['validate'] = $error;
                } else if ($cekPlantAssg['status'] == 0) {
                    $flag                       = false;
                    $attribSave[$i]['validate'] = $cekPlantAssg;
                } else if ($cekSalesOffice['status'] == 0) {
                    $flag                       = false;
                    $attribSave[$i]['validate'] = $cekSalesOffice;
                } else {
                    $attribSave[$i]['created_by'] = Auth::user()->id;
                    $attribSave[$i]['updated_by'] = Auth::user()->id;
                    $attribSave[$i]['created_at'] = now();
                    $attribSave[$i]['updated_at'] = now();
                    $attribSave[$i]['number']     = $this->generateCodeBulk($valMax, 'SUP', 10);
                    $newData[]                    = $attribSave[$i];
                    $valMax++;
                }
            }
            if ($flag) {
                SupplyPoint::insert($attribSave);
                try {
                    DB::commit();
                    $response = responseSuccess(trans('messages.create-success'), $newData);
                    return response()->json($response, 201, [], JSON_PRETTY_PRINT);
                } catch (\Exception $ex) {
                    DB::rollback();
                    $response           = responseFail(trans('messages.create-fail'));
                    $response['errors'] = $ex->getMessage();
                    return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                }
            } else {
                DB::rollback();
                $response         = responseFail(trans('messages.create-fail'));
                $response['data'] = $attribSave;
                return response()->json($response, 400, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response = responseFail(trans('messages.create-fail'));
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        }
    }

    public function validateImport($request, $rules)
    {
        $messages = [
            'required' => trans('messages.required'),
            'unique'   => trans('messages.unique'),
            'email'    => trans('messages.email'),
            'numeric'  => trans('messages.numeric'),
            'exists'   => trans('messages.exists'),
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            $newMessages = [];
            foreach ($validator->errors()->messages() as $key => $value) {
                $newMessages[$key] = $value;
            }
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => $newMessages,
            ];

            return $response;
        } else {
            return responseSuccess(trans('messages.validate-check-success'), true);
        }
    }

    public function getMaxCode($model)
    {
        $maxCode = $model::max('number');
        if ($maxCode) {
            preg_match_all('!\d+!', $maxCode, $matches);
            $val = intval($matches[0][0]);

            return $val;
        } else {
            return 0;
        }
    }

    private function cekKeyExist($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        } else {
            return null;
        }
    }

    private function cekPlantAssg($attrib)
    {
        $plant = PlantAssg::where(['plant_id' => $attrib['plant_id'], 'sales_org_id' => $attrib['sales_org_id']])->count();

        if ($plant == 0) {
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => ['sales_org_id' => [trans('messages.plant-sales-org')]],
            ];
            return $response;
        }
        return responseSuccess(trans('messages.validate-check-success'), true);
    }

    private function cekPlantOfficeUpload($sales_office_id, $plant_id, $scnd_sales_group_id)
    {
        $cek = $this->getPlantOffice($sales_office_id, $plant_id);
//        $cekGroup = $this->getGroupOffice($sales_office_id, $scnd_sales_group_id);
        if (!$cek) {
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => ['sales_office_id' => [trans('messages.plant-sales-office')]],
            ];
            return $response;
        }
//        else if (!$cekGroup) {
        //            $response = [
        //                'status' => 0,
        //                'status_txt' => "errors",
        //                'message' => ['scnd_sales_group_id' => [trans('messages.scnd-group-sales-office')]],
        //            ];
        //            return $response;
        //        }
        return responseSuccess(trans('messages.validate-check-success'), true);
    }

    private function cekPlantOfficeStore($sales_office_id, $plant_id)
    {
        $cek = $this->getPlantOffice($sales_office_id, $plant_id);
        if (!$cek) {
            $response = [
                'status'     => 0,
                'status_txt' => "errors",
                'message'    => ['sales_office_id' => [trans('messages.plant-sales-office')]],
            ];
            $return = response()->json($response, 400, [], JSON_PRETTY_PRINT);
            $return->throwResponse();
        }
    }

    private function getPlantOffice($sales_office_id, $plant_id)
    {
        $sales_office_plant = Plant::find($plant_id)->salesGroup()->first()->sales_office_id;

        if ($sales_office_id == $sales_office_plant) {
            return true;
        }
        return false;
    }

    private function getGroupOffice($sales_office_id, $sales_group_id)
    {
        $sales_office = SalesGroup::where(['sales_office_id' => $sales_office_id, 'id' => $sales_group_id])->first();

        if ($sales_office) {
            return true;
        }
        return false;
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request,true);
        $data = $q->select(SupplyPoint::getExportedColumns())->get();
        return Excel::download((new Download($data)), "supplypoint.xls");
    }
}
