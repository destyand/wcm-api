<?php

namespace App\Http\Controllers;

use App\Exports\Download;
use App\Models\Customer;
use App\Models\ContactPerson;
use App\Models\Address;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\CustomerBankAssg;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DistributorController extends Controller {

    public function index(Request $request, $exported=false) {
        $where = array();
        // if ($this->isAdminAnper) {
        //     $where['h.id'] = $this->salesOrgId;
        // } else
        if ($this->isDistributor) {
            $where['a.id'] = $this->customerId;
        }

        \LogActivity::addToLog('get all distributor');
        $query = Customer::getData($where);
        $user = $request->user();
        $filters = $user->filterRegional;
        if (count($filters) > 0 && !$this->isDistributor) {
            if (isset($filters["sales_org_id"]) && count($filters["sales_org_id"]) > 0) {
                $query->whereIn("h.id",$filters["sales_org_id"]);
            }

            if (isset($filters["sales_group_id"]) && count($filters["sales_group_id"]) > 0) {
                $query->whereIn("b.sales_group_id",$filters["sales_group_id"]);
            }
        }

        if ($request->has("only_df")) {
            $org = $request->get('sales_org_id');
            $query = Customer::whereHas("paymentMethod", function($q){
                $q->where("ident_name", "DISTRIBUTOR_FINANCING");
            })
            ->whereHas("salesOrg", function($q) use ($org) {
                $q->where("sales_org_id", $org);
            });
            
            $columns = [
                "status" => "status",
                "full_name" => "full_name",
            ];
            
        } else {
            $columns = [
                'a.id' => 'id',
                'a.uuid' => 'uuid',
                'a.full_name' => 'full_name',
                'a.owner' => 'owner',
                'b.address' => 'address',
                'b.village_id' => 'village_id',
                'f.name' => 'village_name',
                'b.sales_unit_id' => 'sales_unit_id',
                'c.name' => 'sales_unit_name',
                'b.sales_group_id' => 'sales_group_id',
                'd.name' => 'sales_group_name',
                'b.sales_office_id' => 'sales_office_id',
                'e.name' => 'sales_office_name',
                'b.tlp_no' => 'tlp_no',
                'a.status' => 'status',
                'b.fax_no' => 'fax_no',
                'a.category' => 'category',
                'a.asset' => 'asset',
                'a.revenue' => 'revenue',
                'account_year' => 'account_year',
                'a.created_by' => 'created_by',
                'a.updated_by' => 'updated_by',
                'a.created_at' => 'created_at',
                'a.updated_at' => 'updated_at'
            ];
        }

        $distributor = Datatables::of($query)
                ->filter(function ($query) use ($request, $columns) {
                    $this->filterColumn($columns, $request, $query);                    
                })
                ->order(function ($query) use ($request, $columns) {
                    $this->orderColumn($columns, $request, $query);
                });
        
        if ($exported) return $distributor->getFilteredQuery();
        $distributor = $distributor->make(true);

        $response = responseDatatableSuccess(trans('messages.read-success'), $distributor->getData(true));
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function filterColumn($columns, $request, $query) {
        foreach ($columns as $key => $value) {
            if ($request->has($value)) {
                $this->searchColumn($key, $value, $request, $query);
            }
        }
    }

    public function searchColumn($key, $value, $request, $query) {
        if ($request->get($value)) {
            if ($value === 'created_at' || $value === 'updated_at') {
                $arr = explode(';', $request->get($value));
                if (count($arr) == 1) {
                    $query->where(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), '=', "{$request->get($value)}");
                } else {
                    $query->whereBetween(DB::raw("CONVERT ( VARCHAR, {$key}, 23 )"), [$arr[0], $arr[1]]);
                }
            } elseif ($value === 'status') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } elseif ($value === 'sales_office_id' || $value === 'sales_group_id') {
                $listStatus = explode(';', $request->get($value));
                $query->whereIn($key, $listStatus);
            } else {
                $query->where($key, 'like', "%{$request->get($value)}%");
            }
        }
    }

    public function show($uuid) {
        /*
         * Show table customer only, because I don't know the response needed by the front end ----------------------
         */
        \LogActivity::addToLog('get distributor by Uuid');

        is_uuid($uuid);
        $model = $this->findDataUuid(Customer::class, $uuid);
        if ($model->status == 'y') {
            $model->status = 'Active';
        } else if ($model->status == 'n') {
            $model->status = 'Inactive';
        } else if ($model->status == 'p') {
            $model->status = 'Suspend';
        } else if ($model->status == 'd') {
            $model->status = 'Draft';
        } else if ($model->status == 's') {
            $model->status = 'Submited';
        }
        // ->where('b.address_type', '=', 'FORMAL')

        if ($model) {
            $where['a.uuid'] = $uuid;

            $addresses = array();

            // ==============

            $whereAddFormal['tb1.address_type'] = 'FORMAL';
            $whereAddFormal['tb1.customer_id'] = $model->id;
            $formalAddress = Address::getAddessArea($whereAddFormal)->first();

            if ($formalAddress) {
                $formalArr = $formalAddress;
            } else {
                $formalArr = array(
                    "id" => "", "uuid" => "", "address" => "", "tlp_no" => "", "fax_no" => "", "address_type" => "", "customer_id" => "", "sales_unit_id" => "", "village_id" => "", "status" => "", "created_by" => "", "updated_by" => "", "created_at" => "", "updated_at" => "", "sales_unit_name" => "", 'sales_group_name' => '', 'sales_office_name' => ''
                );
            }
            array_push($addresses, $formalArr);

            // ==============

            $whereAddBillin['tb1.address_type'] = 'BILLING';
            $whereAddBillin['tb1.customer_id'] = $model->id;
            $billingAddress = Address::getAddessArea($whereAddBillin)->first();
            if ($billingAddress) {
                $billingArr = $billingAddress;
            } else {
                $billingArr = array(
                    "id" => "", "uuid" => "", "address" => "", "tlp_no" => "", "fax_no" => "", "address_type" => "", "customer_id" => "", "sales_unit_id" => "", "village_id" => "", "status" => "", "created_by" => "", "updated_by" => "", "created_at" => "", "updated_at" => "", "sales_unit_name" => "", 'sales_group_name' => '', 'sales_office_name' => ''
                );
            }
            array_push($addresses, $billingArr);

            $model['address'] = $addresses;

            // Contack Person
            $contact = ContactPerson::where('customer_id', $model->id)->get();

            $model['contact'] = $contact;

            $model['bank'] = DB::table('wcm_customer_bank_assg as a')->join('wcm_bank as b', 'a.bank_id', '=', 'b.id')->where('a.customer_id', $model->id)->select('b.uuid', 'b.name')->get();
        }

        $response = responseSuccess(trans('messages.read-success'), $model);
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function download(Request $request) {
        $request = $request->merge(["length" => -1]);
        $q = $this->index($request, true);
        $data = $q->select(Customer::getExportedColumns())->get();
        return Excel::download((new Download($data)), "distributor.xls");
    }
    
}
