<?php

namespace App\Rules;

use App\Models\LimitDate;
use Illuminate\Contracts\Validation\Rule;

class LimitDateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($params=null)
    {
        $this->params = $params;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $salesOrgId = request()->get("sales_org_id");
        $salesOfficeId = request()->get("sales_office_id");
        $productId = request()->get("product_id", null);
        $incoterm = request()->get("incoterm");
        $minQty = request()->get("min_qty", 0);
        $maxQty = request()->get("max_qty", 0);

        $clause = [
            "sales_org_id" => $salesOrgId,
            "sales_office_id" => $salesOfficeId,
            // "product_id" => $productId,
            "incoterm" => $incoterm
        ];
        

        $minLimit = LimitDate::where($clause);
        $maxLimit = LimitDate::where($clause);
        if($this->params)
        {
            $maxLimit = $maxLimit->where('uuid','!=',$this->params);
            $minLimit = $minLimit->where('uuid','!=',$this->params);
        }
        $minLimit = $minLimit->pluck("min_qty")->unique();
        $maxLimit = $maxLimit->pluck("max_qty")->unique();
        $valid = true;

        for($i = 0; $i<$minLimit->count();$i++) {
            if (
                (
                    $minQty >= $minLimit->get($i) &&
                    $minQty <= $maxLimit->get($i)
                ) || (
                    $maxQty >= $minLimit->get($i) && 
                    $maxQty <= $maxLimit->get($i)
                )
            ) {
                $this->message = trans("messages.inrange");
                $valid = false;
                break;
            }
        }

        return $valid;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
