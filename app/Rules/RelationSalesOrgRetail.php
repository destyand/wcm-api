<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class RelationSalesOrgRetail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $sales_org_id=$value['sales_org_id'];
        $retail_code=$value['retail_id'];

        return DB::table("wcm_customer_retailer_assg as a")->where("a.sales_org_id", $sales_org_id)
                ->join('wcm_retail as b' ,function($q) use ($retail_code){
                        $q->on('a.retail_id','=','b.id')->where("b.code", "{$retail_code}");
                })
                ->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'sales org dan retail harus berelasi .';
    }
}
