<?php

namespace App\Rules;

use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;

class RetailAdministratifSalesUnitRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $parentId;
    public function __construct($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $exists = Retail::Administratif()->where("sales_unit_id", $value)
            ->where("parent_id", $this->parentId)
            ->exists();

        return !$exists;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Pengecer Administratif Sudah Dibuat.';
    }
}
