<?php

namespace App\Rules;

use App\Models\SalesGroup;
use Illuminate\Contracts\Validation\Rule;

class SalesGroupRule implements Rule
{
    private $sales_office_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($sales_office_id)
    {
        $this->sales_office_id = $sales_office_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return SalesGroup::where('sales_office_id', $this->sales_office_id)
            ->where('id', $value)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Kabupaten tidak boleh lintas provinsi';
    }
}
