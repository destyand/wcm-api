<?php

namespace App\Rules;

use App\Models\FarmerGroup;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class FarmerGroupNameRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $sales_unit = request()->has('sales_unit_id')
        ? request()->get('sales_unit_id')
        : Arr::get($value, 'sales_unit_id', 0);

        $name = request()->has('name')
        ? request()->get('name')
        : Arr::get($value, 'name', null);

        return !FarmerGroup::where("name", $name)
            ->where("sales_unit_id", $sales_unit)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.farmer-group-name');
    }
}
