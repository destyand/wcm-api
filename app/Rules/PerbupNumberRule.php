<?php

namespace App\Rules;

use App\models\ContractGoverment;
use App\models\ContractGovItem;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class PerbupNumberRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        $salesOffice = Arr::get($value, 'sales_office_id');
        $salesGroup = Arr::get($value, 'sales_group_id');
        $salesOrg = Arr::get($value, 'sales_org_id');
        $year = Arr::get($value, 'year');
        $number = Arr::get($value, 'number');

        $contract = ContractGoverment::where('contract_type', 'perbup')
                ->where('sales_org_id', $salesOrg)
                ->where('year', $year)
                ->where('status', '!=', 'n')
                ->pluck('id');

        $item = ContractGovItem::whereIn('contract_gov_id', $contract)
                ->where('sales_office_id', $salesOffice)
                ->where('sales_group_id', $salesGroup)
                ->where('status', '!=', 'n')
                ->pluck('id')
                ->toArray();
        
        if ($item) {
            $this->errorMessage = trans('messages.check-perbup');
            return false;
        }
        
        $cekNumber = ContractGoverment::where('number', $number)
                ->where('status', '!=', 'n')
                ->pluck('id')
                ->toArray();

        if ($cekNumber) {
            $this->errorMessage = trans('messages.duplicate');
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
