<?php

namespace App\Rules;

use App\Models\DistribReports;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class CreateCertificateReleaseRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $query = DistribReports::whereIn('uuid',$value)->get();
        $pass = true;

        if($query->pluck('customer_id')->count() > 1){
            $pass = false;
            $this->errorMessage= "Hanya Diperbolehkan 1 Distributor </br>";
        }   
        if($query->pluck('sales_org_id')->count() > 1){
            $pass = false;
            $this->errorMessage= "Hanya Diperbolehkan 1 Anper </br>";
        } 
        if($query->pluck('sales_org_id')->count() > 1){
            $pass = false;
            $this->errorMessage= "Hanya Diperbolehkan dalam 1 Kabupaten";
        }   

        return $pass;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
