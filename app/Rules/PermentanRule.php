<?php

namespace App\Rules;

use App\models\ContractGoverment;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PermentanRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      
        $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', $value['contract_type'])
            ->where('tb1.month', intval($value['month']))
            ->where('tb2.year', $value['year'])
            ->where('tb1.product_id', $value['product_id'])
            ->where('tb1.sales_office_id',$value['sales_office_id'])
            ->where('tb2.sales_org_id',$value['sales_org_id'])
            ->where('tb1.status','y')->exists();
        // dd($query);
        if($query) {
            $this->errorMessage = ":attribute : Produsen ".$value["sales_org_id"]." di tahun ".$value["year"].", bulan ".$value['month'].", Produk : ".$value['product_id']." sudah ada yang aktif.";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
