<?php

namespace App\Rules;

use App\Models\SalesArea;
use App\Models\SalesOfficeAssign;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class RayonisasiRule implements Rule {

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        //
        $salesOrg = Arr::get($value, 'sales_org_id');
        $salesOfficeId = str_pad(Arr::get($value, 'kode_provinsi'), 4, "0", STR_PAD_LEFT);

        $salesArea = SalesArea::where('sales_org_id', $salesOrg)
            ->whereIn('distrib_channel_id', [10,20])
            ->where('sales_division_id', '00')
            ->pluck('id')
            ->unique();

        $salesOffices = SalesOfficeAssign::whereIn('sales_area_id', $salesArea)
            ->where('sales_office_id', $salesOfficeId)
            ->exists();


        if (!$salesOffices) {
            $this->errorMessage = "Provinsi {$salesOfficeId} tidak masuk dalam rayonisasi {$salesOrg}";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return $this->errorMessage;
    }

}
