<?php

namespace App\Rules;

use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;

class RetailOwnerUniqueRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $name        = @$value['name'];
        $owner       = @$value['owner'];
        $salesUnitId = @$value['sales_unit_id'];

        return ! Retail::where('name', "{$name}")
            ->where('owner', "{$owner}")
            ->where('sales_unit_id', "{$salesUnitId}")
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute has ben unique';
    }
}
