<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class FarmerGroupLandAreaRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $qty_group = Arr::get($value, "jumlah_anggota", 0);
        $land_area = Arr::get($value, "luas_lahan", 0);

        if ($qty_group == 1) {
            return $land_area <= 2;
        }

        return $land_area <= ($qty_group * 2);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('messages.farmer-group-land-area');
    }
}
