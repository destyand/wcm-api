<?php

namespace App\Rules;

use App\Models\Retail;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class FarmerGroupProdusenRetailRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $retailCode = Arr::get($value, 'retail_code');
        $salesOrg   = Arr::get($value, 'sales_org_id');
        $retail     = Retail::where('code', $retailCode)->first();

        if (!$retail) {
            $this->setMessage("Retail " . trans('messages.read-fail'));
            return false;
        }

        $validSalesOrg = $retail->customerRetailerAssg()
            ->where('sales_org_id', $salesOrg)
            ->exists();

        if (!$validSalesOrg) {
            $this->setMessage(trans('messages.farmer-group-produsen-retail'));
        }

        return $validSalesOrg;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }

    private function setMessage($string)
    {
        $this->errorMessage = $string;
    }
}
