<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OwnedSalesOrg implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $request;
    public function __construct()
    {
        //
        $this->request = request();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $user = @$this->request->user();

        if (!$user) {
            return false;
        }

        // User Have Org Assign
        $orgAssign = @$user->salesOrgAssign;

        if ($orgAssign) {
            $filtered = $orgAssign->pluck('sales_org_id')->filter();

            if (!$filtered->isEmpty()) return $filtered->contains($value);
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Sales Organization';
    }
}
