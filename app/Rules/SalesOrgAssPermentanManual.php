<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class SalesOrgAssPermentanManual implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
         $query = DB::table('wcm_contract_gov_item AS tb1')
            ->leftJoin('wcm_contract_goverment AS tb2', 'tb2.id', '=', 'tb1.contract_gov_id')
            ->where('tb2.contract_type', 'permentan')
            ->where('tb2.year', $value['year'])
            ->where('tb1.product_id', $value['product_id'])
            ->where('tb1.sales_office_id',$value['sales_office_id'])
            ->where('tb1.status','y')
            ->get()->pluck('sales_org_id')->unique()->toArray();
        // dd($query);
        if(count($query) >= 2 )
        {
            $sales_org= implode(",",$query);
            $this->errorMessage = "1 provinsi boleh 2 produsen dalam 1 produk aktif di bulan yang dipilih. produsen yang terdaftar : {$sales_org} ";
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage ;
    }
}
