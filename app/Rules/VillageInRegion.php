<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VillageInRegion implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $sales_office_id = $value["sales_office_id"];
        $sales_group_id  = $value["sales_group_id"];
        $sales_unit_id   = $value["sales_unit_id"];
        $village_code    = $value["village_code"];

        if (
            substr($village_code, 0, 6) == $sales_unit_id &&
            substr($village_code, 0, 4) == $sales_group_id &&
            substr($village_code, 0, 2) == $sales_office_id
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be same with Sales Office & Sales Group';
    }
}
