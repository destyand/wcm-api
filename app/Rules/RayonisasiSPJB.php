<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class RayonisasiSPJB implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $sales_office = @$value['sales_office_id'];
        $sales_group  = @$value['sales_group_id'];
        $sales_unit   = @$value['sales_unit_id'];
        $sales_org_id = @$value['sales_org_id'];

        $query = DB::table('wcm_sales_org as a')
            ->join('wcm_sales_area as b', function ($q) {
                $q->on('a.id', '=', 'b.sales_org_id')
                    ->where('distrib_channel_id', '10')
                    ->where('sales_division_id', '00');
            })
            ->join('wcm_sales_office_assg as c', function ($q) use ($sales_office) {
                $q->on('c.sales_area_id', '=', 'b.id')->where('c.sales_office_id', $sales_office);
            })
            ->join('wcm_sales_group as d', function ($q) use ($sales_group) {
                $q->on('d.sales_office_id', '=', 'c.sales_office_id')->where('d.id', $sales_group);
            })
            ->join('wcm_sales_unit as e', function ($q) use ($sales_unit) {
                $q->on('e.sales_group_id', '=', 'd.id')->where('e.id', $sales_unit);
            })
            ->where('a.id', $sales_org_id)
            ->select('a.id')
            ->first();
        return $query;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Rayonisasi SPJB Tidak Ditemukan / Region Tidak Cocok';
    }
}
