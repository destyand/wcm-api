<?php

namespace App\Jobs;

use App\Http\Controllers\SAP\SalesOrderController;
use App\Libraries\PaymentGateway;
use App\Models\Bank;
use App\Models\LogOrder;
use App\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;


class CreateSalesOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 120;
    public $retries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $bookingCode;

    public function __construct(string $bookingCode)
    {
        $this->bookingCode = $bookingCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $so = new SalesOrderController;
        $order = Order::where("booking_code", $this->bookingCode)->first();
        if ($order) {
            // Find Last Failed Log Order Code
            $logOrder = LogOrder::where("order_id", $order->id)->exists();
            $creating = (
            (
                $order->payment_method == PaymentGateway::DF && $order->status == "u"
            ) || 
            (
                $order->payment_method == PaymentGateway::CASH && $order->status == "l"
                )
            );
            
            if ($creating && !$logOrder) {
                $so->store($order->booking_code);
            }
        }
    }

    private function clearLogOrder($orderID, $lastCode) {
        if (in_array($lastCode, [3,4,6])) {
            LogOrder::where("order_id", $orderID)
                ->where("kode", "!=" , "5")
                ->where("kode", ">=", $lastCode)
                ->delete();
        }
    }
}
