<?php

namespace App\Jobs;

use App\Helpers\SAPConnect;
use App\Models\LogOrder;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DisbursementJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $retries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $soap = SAPConnect::connect('disbursement.xml');
        $result = $soap->SI_ClearingDoc_Disbursement($this->params);
        $check = checkSAPResponse($result);
        if (!$check["status"]) {
            $order = Order::where("so_number", $this->params["I_VBELN"])->firstOrFail();
            if ($order) {
                //DB::beginTransaction(function() use ($check, $order){
                    $logOrder = LogOrder::create([
                        'order_id' => $order->id,
                        'status'   => "y",
                        'kode'     => 7,
                        'message'  => "Disburment Job Failed",
                    ]);
                    $logOrder->items()->createMany($check["data"]);
                //});
            }
        }
    }
}
