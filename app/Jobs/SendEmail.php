<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use Illuminate\Support\Facades\Log;

class SendEmail implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details) {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        Log::info('Send email ', $this->details);
        Mail::send('email', [
            'name' => $this->details['name'],
            'username' => $this->details['username'],
            'password' => $this->details['password'],
            'date' => $this->details['date']
                ], function ($message) {
            $message->subject("Admin WCM PIHC");
            $message->from($this->details['from'], 'Admin WCM PIHC');
            $message->to($this->details['to']);
        });
    }

}
