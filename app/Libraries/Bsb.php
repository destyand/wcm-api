<?php

namespace App\Libraries;

use App\Models\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Bsb extends PaymentGateway
{
    public function __construct(string $type = 'payment')
    {
        $this->type = $type;
        $this->name = "BSB";
    }

    public function getRequest()
    {
        return $this->type == "payment" ? $this->getPaymentParams() : $this->getInquiryParams();
    }

    public function response(Collection $data)
    {
        $params = $data->only($this->getRequest());
        $errorCode = "00";
        $statusID  = "Sukses";
        $statusEN  = "Succeed";

        try {

            $order = Order::where("booking_code", $params->get("booking_code"))->first();
            $this->order = $order;

            if (!$this->isValidOrder()) {
                return $this->handleEndPoint($params);
            }
            
            if ($this->checkPaidOrder()) {
                throw new \Exception("B8");
            }


            if ($this->type === "payment") {
                $params->put("amount", $this->getPriceAmount());
                $this->doCreateSO($params);
            }

        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
        }

        $response = [
            "trxid"        => "-",// $params->get("trxid"),
            "status"       => $errorCode,
            "so"           => $errorCode == "00" ? $order->number : "",
            "booking_code" => $errorCode == "00" ? $order->booking_code : "",
            "distributor"  => $errorCode == "00" ? $order->customer->full_name : "",
            "tujuan"       => $errorCode == "00" ? $order->salesgroup->name : "",
            "amount"       => $errorCode == "00" ? (string) sprintf("%s",$this->getPriceAmount()) : "",
            "denda"        => "0",
            "product"      => [],
            "commCodeDesc" => $errorCode == "00" ? $order->sales_org_id . " " . $order->salesorg->name : "",
        ];

        if ($errorCode === "00") {
            $products = $order->orderItems->map(function ($item) use (&$response) {
                $product = [$item->product->name, (string)$item->qty, substr($item->plant->code, -3)];
    
                if ($this->type === "inquiry") {
                    Arr::set($response, "gudang", $item->plant->name);
                } else {
                    Arr::set($response, "prodTonaseWarehouse1", implode(" ", $product));
                }
    
                return $product;
            });
    
            Arr::set($response, "product", $products);
        }

        return $response;
    }

    public function getInquiryParams(): array
    {
        return ["trxid", "booking_code", "channel"];
    }

    public function getPaymentParams(): array
    {
        return ["booking_code"];
    }

    public function getValidationRule()
    {
        $rule = [
            // "trxid"        => "required",
            "booking_code" => "required",
            // "channel"      => "required",
        ];

        if ($this->getType() == "payment") {
            $rule = [
                "booking_code" => "required",
            ];
        }

        return $rule;
    }

    public function getBookingCode()
    {
        return "booking_code";
    }

    protected function getParamSendInvoice($data): array
    {
        return [];
    }

    public function getParamDisbrusement(): array
    {
        return [];
    }

    protected function getParamCheckLimit(): array
    {
        return [];
    }

    protected function setResponDisbrusement($params): array
    {
        // TODO: Implement setResponDisbrusement() method.
        return [];
    }

    public function getParamAuthorize(Request $params): array 
    {
        return [
            "ip_address" => $params->ip(),
        ];
    }

    public function handleInvalidOrder(\Exception $e)
    {
        $handleNotFound = '{"trxid":"-","status":"B5","booking_code":"-","so":"-","distributor":"-","tujuan":"-","amount":"-","denda":"-","gudang":"-","commCodeDesc":"-"}';
        $errors = json_decode($handleNotFound, true);
        $errors["status"] = $e->getMessage();

        return response()->json($errors, 500);
    }
}
