<?php

namespace App\Libraries;

use App\Libraries\PaymentGateway;
use App\Models\Customer;
use App\Models\CustomerSalesArea;
use App\Models\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\BankAccount;

class Bri extends PaymentGateway
{
    public function __construct(string $type = 'payment')
    {
        $this->type = $type;
        $this->name = "BRI";
    }

    public function getRequest()
    {
        return $this->type == "payment" ? $this->getPaymentParams() : $this->getInquiryParams();
    }

    public function response(Collection $data)
    {
        $params    = $data->only($this->getRequest());
        $isError   = false;
        $errorCode = "00";
        $statusID  = "Sukses";
        $statusEN  = "Succeed";
        try {

            $order = Order::where("booking_code", $params->get("BookingCode"))->first();

            $this->order = $order;

            if (!$this->isValidOrder()) {
                return $this->handleEndPoint($params);
            }

            if ($this->checkPaidOrder()) {
                throw new \Exception("B8");
            }

            if ($this->type === "payment") {
                $this->doCreateSO($params);
            }

        } catch (\Exception $e) {
            $isError   = true;
            $errorCode = $e->getMessage();
            $statusEN  = $statusID  =  self::ERROR_CODE[$e->getMessage()];
        }

        $bankAcc = BankAccount::where("sales_org_id", $this->order->sales_org_id ?? null)
->where("bank_id", "BBRI")->where("status","y")->first();

        $response = [
            "rq_uuid"           => $params->get("rq_uuid"),
            "currency"          => $errorCode == "00" ? "IDR" : "",
            "billInfo1"         => $errorCode == "00" ? @$order->customer->full_name : "",
            "billCode"          => $errorCode == "00" ? "01" : "",
            "billName"          => $errorCode == "00" ? "Pembayaran" : "",
            "billShortName"     => $errorCode == "00" ? "Pembayaran" : "",
            "bankAccountNo"     => $errorCode == "00" ? @$bankAcc->bank_account : "",
            "billAmount"        => $errorCode == "00" ? (string) $this->getPriceAmount() : "",
            "commCode"          => $errorCode == "00" ? @$order->sales_org_id : "",
            "isError"           => $isError,
            "errorCode"         => $errorCode,
            "statusDescription" => $statusID,
            "salesOrderNumber"  => $errorCode == "00" ? @$order->number : "",
            "destination"       => $errorCode == "00" ? @$order->salesgroup->name : "",

        ];

        if ($errorCode == "00" && @$order->orderItems) {
            $item    = $order->orderItems->first();
            $product = [$item->product->name, $item->qty, substr($item->plant->code, -3)];

            $products = $order->orderItems->map(function ($item) use (&$response) {
                $product = [$item->product->name, "$item->qty", substr($item->plant->code, -3)];
                return $product;
            });

            if ($this->type === "inquiry") {
                Arr::set($response, "product", $products);
                Arr::set(
                    $response, 
                    "bookingTimeLimit", 
                    Carbon::parse($order->payment_due_date)->format("d-m-Y H:i:s")    
                );
                Arr::set(
                    $response, 
                    "batasambil", 
                    Carbon::parse($order->good_redemption_due_date)->format("d-m-Y")
                );
            } else {
                Arr::forget($response, [
                    "currency", "bankAccountNo", "billAmount",
                    "billName", "billShortName", "commCode", "batasambil",
                ]);

            }

            Arr::set($response, "warehouse", "{$item->plant->name} - {$item->plant->address}");
        }

        return $response;
    }

    public function getInquiryParams(): array
    {
        return [
            "rq_uuid", "BookingCode", "channelID", "trxDateTime", "Language",
            "terminalID", "billkey1", "transmissionDateTime"
        ];
    }

    public function getPaymentParams(): array
    {
        return ["rq_uuid", "BookingCode", "channelID", "companyCode", "paymentAmount"];
    }

    public function getValidationRule()
    {
        $rule = [
            "rq_uuid"     => "required",
            "BookingCode" => "required",
            "channelID"   => "required",
            //"companyCode" => "required",
        ];

        if ($this->getType() == "payment") {
            $rule += [
                "paymentAmount" => "required",
            ];
        } else if($this->getType() == "inquiry") {
            $rule += [
                "terminalID" => "required",
                "trxDateTime" => "required",
                "Language" => "sometimes",
                "billkey1" => "required",
                "transmissionDateTime" => "required"
            ];
        }

        return $rule;
    }

    protected function getBookingCode()
    {
        return "BookingCode";
    }

    protected function getAmount()
    {
        return "paymentAmount";
    }

    /**
     * Gets the parameter send invoice.
     *
     * @param      <type>  $data   The data
     *
     * @return     array   The parameter send invoice.
     */
    protected function getParamSendInvoice($data): array
    {
        $sales_area = CustomerSalesArea::getSalesAreaOrder(
            $data->customer_id,
            $data->sales_org_id
        );
        $req_uuid = sprintf(
            "sinvphic%s-%s-%s-%s-%s", 
            str_random(2),
            str_random(4), 
            str_random(4),
            str_random(4),
            str_random(12)
        );
        return [
            "rq_uuid"          => $req_uuid,
            "timestamp"        => Carbon::parse($data->created_at)->format("Y-m-d H:i:s"),
            // "password"         => "s3m3 n1ndCo00",
            "amount"           => sprintf("%.0f",$data->total_price_before_ppn),
            "currency"         => "IDR",
            "salesOrderNumber" => $data->number,
            "so_date"          => $data->payment_due_date,
            "comm_code"        => $data->sales_org_id,
            // "term_of_payment"  => $sales_area->term_of_payment,
            "customerNo"       => $data->customer_id,
            "customer_name"    => $data->customer->full_name,
            "so_due_date"      => Carbon::parse($data->df_due_date)->format("Y-m-d H:i:s"),
        ];
    }

    protected function getParamCheckLimit(): array
    {
        $req_uuid = sprintf(
            "dfphic%s-%s-%s-%s-%s", 
            str_random(2),
            str_random(4), 
            str_random(4),
            str_random(4),
            str_random(12)
        );
        return [
            "rq_uuid"    => $req_uuid,
            //"password"   => "test1234",
            "currency"   => "IDR",
            "comm_code"  => request()->get("sales_org_id"),
            "customerNo" => request()->get("customer_id"),
            "timestamp"  => Carbon::now()->format("Y-m-d H:i:s"),
        ];
    }

    public function getParamDisbrusement(): array
    {
        return [
            "rq_uuid"          => "required",
            "doc_no"           => "required",
            "comm_code"        => "required|exists:wcm_sales_org,id",
            "payment_ref"      => "required",
            "amount"           => "required",
            "ccy"              => "required",
            "transaction_date" => "required",
            "error_code"       => "required",
            "error_msg"        => "required",
        ];
    }

    protected function setResponDisbrusement($request): array
    {

        return [
            "rq_uuid"          => $request->get("rq_uuid"),
            "timestamp"        => Carbon::now()->format("Y-m-d H:i:s"),
            "password"         => "",
            "comm_code"        => $request->get("comm_code"),
            "customerNo"       => $request->get("member_code"),
            "payment_ref"      => $request->get("payment_ref"),
            "salesOrderNumber" => $request->get("doc_no"),
            "amount"           => $request->get("amount"),
            "ccy"              => $request->get("ccy"),
            "transaction_date" => $request->get("transaction_date"),
            "error_code"       => $request->get("error_code"),
            "error_msg"        => $request->get("error_Msg"),

        ];
    }

    public function handleInvalidOrder(\Exception $e)
    {
        $notFoundRespon = '{"rq_uuid":"20160420042","currency":"","billInfo1":"","billCode":"","billName":"","billShortname":"","bankAccountNo":"","billAmount":"","bookingTimeLimit":"","commCode":"","isError":"true","errorCode":"B5","statusDescription":"Tagihan tidak ditemukan / Nomor Referensi salah","salesOrderNumber":"","product":"","warehouse":"","destination":"","batasAmbil":""}';

        return response()->json(json_decode($notFoundRespon),500);
    }

    public function getParamAuthorize(Request $params): array 
    {
        return [
            "ip_address" => $params->ip(),
        ];
    }
}
