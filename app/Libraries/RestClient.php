<?php

namespace App\Libraries;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

class RestClient
{
    private $client;

    public function __construct(array $config=[])
    {
        $this->client = new Client(["timeout" => 60, "verify" => false]);
        if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }
    }

    public function postJSON($url, $params, $auth=null)
    {
        $request = null;
        try {
            if (is_null($url) || ! $params) {
                throw new \Exception("Error Processing Request", 1);
            }
            $options = [
                RequestOptions::JSON=> $params,
            ];
            if ($auth) {
                $options["auth"] = $auth;
            }
            $request = $this->client->request('POST', $url, $options);
        } catch (RequestException $re) {
            if ($re->hasResponse()) {
                return [
                    'message'  => 'RequestException',
                    'request'  => $re->getRequest(),
                    'response' => $re->getResponse(),
                ];
            }
        } catch (ClientException $ce) {
            return [
                'message'  => 'ClientException',
                'request'  => $ce->getRequest(),
                'response' => $ce->getResponse(),
            ];
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        $body = method_exists($request,'getBody') ? $request->getBody() : null;

        return method_exists($body, 'getContents') ? json_decode($body->getContents(),true): [];
    }

    public function post($url, $params)
    {
        $request = null;
        try {
            if (is_null($url) || ! $params) {
                throw new \Exception("Error Processing Request", 1);
            }
            $request = $this->client->request('POST', $url, ["form_params" => $params]);
        } catch (RequestException $re) {
            if ($re->hasResponse()) {
                return [
                    'message'  => 'RequestException',
                    'request'  => $re->getRequest(),
                    'response' => $re->getResponse(),
                ];
            }
        } catch (ClientException $ce) {
            return [
                'message'  => 'ClientException',
                'request'  => $ce->getRequest(),
                'response' => $ce->getResponse(),
            ];
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        $body = method_exists($request,'getBody') ? $request->getBody() : null;

        return method_exists($body, 'getContents') ? json_decode($body->getContents()): [];
    }

}
