<?php

namespace App\Libraries;

use App\Libraries\PaymentGateway;
use App\Models\Order;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Models\BankAccount;

class Bca extends PaymentGateway
{
    public function __construct(string $type = 'payment')
    {
        $this->type = $type;
        $this->name = "BCA";
    }

    public function getRequest()
    {
        return $this->type == "payment" ? $this->getPaymentParams() : $this->getInquiryParams();
    }

    /**
     * Handle Response Sesuai Keinginan
     *
     * @param      \Illuminate\Support\Collection  $data   The data
     *
     * @throws     \Exception                      (description)
     *
     * @return     array                           ( description_of_the_return_value )
     */
    public function response(Collection $data)
    {
        $params    = $data->only($this->getRequest());
        $isError   = false;
        $errorCode = "00";
        $statusID  = "Sukses";
        $statusEN  = "Succeed";

        $companyCodeMapping = [
            "88020" => "B000",
            "88030" => "C000",
            "88040" => "D000",
            "88050" => "E000",
            "88060" => "F000",
        ];

        try {

            $order       = Order::where("booking_code", $params->get($this->getBookingCode()))
                ->first();

            if ($order && $order->sales_org_id !=  $companyCodeMapping[$params->get("companyCode")]) {
                throw new Exception("B5");
            }

            $this->order = $order;


            if (!$this->isValidOrder()) {
                return $this->handleEndPoint($params);

            }

            if ($this->checkPaidOrder()) {
                throw new Exception("B8");
            }

            if ($this->type === "payment") {
                $this->doCreateSO($params);
            }

        } catch (\Exception $e) {
            $isError   = true;
            $errorCode = $e->getMessage();
            $statusEN  = $statusID  = self::ERROR_CODE[$errorCode];
        }
        $bankAcc = BankAccount::where("sales_org_id", $this->order->sales_org_id ?? null)
->where("bank_id", "BBCA")->where("status","y")->first();

        $response = [
            "rq_uuid"       => $params->get("rq_uuid"),
            "billInfo1"     => !$isError ?@$order->customer->full_name : "-",
            "billCode"      => !$isError ? "01" : "-",
            "billName"      => !$isError ? "Pembayaran" : "-",
            "billShortName" => !$isError ? "Pembayaran" : "-",
            "bankAccountNo" => !$isError ? @$bankAcc->bank_account : "-",
            "isError"       => $isError ? "true" :"false",
            "errorCode"     => $errorCode,
            "bookingcode"   => !$isError ? @$order->booking_code : "-",
            "destination"   => !$isError ? @$order->salesgroup->name : "-",
        ];

        if ($this->type === "payment" && @$order->orderItems) {
            $item    = $order->orderItems->first();
            $product = [$item->product->name, $item->qty, substr($item->plant->code, -3)];

            $response += [
                "salesOrderNumber"     => !$isError ? @$order->number : "-",
                "batasAmbil"           => !$isError ? parseDate(@$order->good_redemption_due_date)->format("d-m-Y") : "-",
                "statusDescriptionID"  => $statusID,
                "statusDescriptionEN"  => $statusEN,
                "jumlahProduk"         => !$isError ? (string) $order->orderItems->count() : "-",
                "prodTonaseWarehouse1" => !$isError ? implode(" ", $product) : "-",
                "warehouse"            => !$isError ? "{$item->plant->name} - {$item->plant->address}" : "-",
            ];
        } else {
            $response += [
                "bookingTimeLimit"    => !$isError ? Carbon::parse(@$order->payment_due_date)->format("d-m-Y H:i:s") : "-",
                "statusDescriptionid" => $statusID,
                "statusDescriptionen" => $statusEN,
                "billAmount"          => (string) $this->getPriceAmount(),
                "currency" => !$isError ? "IDR" : "-",
            ];
        }

        return $response;
    }

    public function getInquiryParams(): array
    {
        return ["rq_uuid", "BookingCode", "channelID", "companyCode"];
    }

    public function getPaymentParams(): array
    {
        return ["rq_uuid", "bookingCode", "channelID", "companyCode", "paymentAmount"];
    }

    public function getValidationRule()
    {
        $rule = [
            "rq_uuid"     => "required",
            "{$this->getBookingCode()}" => "required",
            "channelID"   => "required",
            "companyCode" => "required",
        ];

        if ($this->getType() == "payment") {
            $rule += [
                "paymentAmount" => "required",
            ];
        }

        return $rule;
    }

    protected function getBookingCode()
    {
        return $this->getType() == "payment" ? "bookingCode" : "BookingCode";
    }

    protected function getAmount()
    {
        return "paymentAmount";
    }

    protected function getParamSendInvoice($data): array
    {
        return [];
    }

    public function getParamDisbrusement(): array
    {
        return [];
    }

    protected function getParamCheckLimit(): array
    {
        return [];
    }

    protected function setResponDisbrusement($params): array
    {
        // TODO: Implement setResponDisbrusement() method.
        return [];
    }

    public function handleInvalidOrder(Exception $e)
    {
        $notFoundRespon = '{"rq_uuid":"","currency":"","billInfo1":"","billCode":"","billName":"","billShortname":"","bankAccountNo":"","billAmount":"","bookingTimeLimit":"","isError":"true","errorCode":"B5","statusDescriptionID":"Tagihan tidak ditemukan / Nomor Referensi salah","statusDescriptionEN":"Tagihan tidak ditemukan / Nomor Referensi salah","bookingCode":"","destination":""}';

        return response()->json(json_decode($notFoundRespon), 500);
    }

    public function getParamAuthorize(Request $params): array 
    {
        return [
            "ip_address" => $params->ip(),
        ];
    }
}
