<?php

namespace App\Libraries;

use App\Libraries\PaymentGateway;
use App\Models\CustomerSalesArea;
use App\Models\Order;
use App\Models\Plant;
use App\Rules\CustomerExistsRule;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\BankAccount;
use Illuminate\Support\Facades\DB;

class Bni extends PaymentGateway
{
    public function __construct(string $type = 'payment')
    {
        $this->type = $type;
        $this->name = "BNI";
    }

    public function getRequest()
    {
        return $this->type == "payment" ? $this->getPaymentParams() : $this->getInquiryParams();
    }

    public function response(Collection $data)
    {
        $params    = $data->only($this->getRequest());
        $isError   = false;
        $errorCode = "00";
        $statusID  = "Sukses";
        $statusEN  = "Succeed";

        try {

            $order       = Order::where("booking_code", $params->get("booking_kode"))->first();
            $this->order = $order;

            if (!$this->isValidOrder()) {
                return $this->handleEndPoint($params);

            }

            if ($this->checkPaidOrder()) {
                throw new \Exception("B8");
            }

            if ($this->type === "payment") {
                $this->doCreateSO($params);
            }

        } catch (\Exception $e) {
            $isError   = true;
            $errorCode = $e->getMessage();
            $statusEN  = $statusID  = self::ERROR_CODE[$e->getMessage()];
        }

        $bankAcc = BankAccount::where("sales_org_id", $this->order->sales_org_id ?? null)
                ->where("bank_id", "BBNI")->where("status","y")->first();

        $response = [
            "rq_uuid"           => $params->get("trxid"),
            "currency"          => $errorCode === "00" ? "IDR" : "",
            "billInfo1"         => $errorCode === "00" ? @$order->customer->full_name : "",
            "billCode"          => $errorCode === "00" ? "01" : "",
            "billName"          => $errorCode === "00" ? "Pembayaran" : "",
            "billShortName"     => $errorCode === "00" ? "Pembayaran" : "",
            "bankAccountNo"     => $errorCode === "00" ? @$bankAcc->bank_account : "",
            "billAmount"        => $errorCode === "00" ? (string) $this->getPriceAmount() : "",
            "commCode"          => $errorCode === "00" ? @$order->sales_org_id : "",
            "isError"           => $isError ? "true" : "false",
            "errorCode"         => $errorCode,
            "statusDescription" => $statusID,
            "salesOrderNumber"  => $errorCode === "00" ? @$order->number : "",
            "destination"       => $errorCode === "00" ? @$order->salesgroup->name : "",

        ];

        if ($errorCode === "00" && @$order->orderItems) {

            $products = $order->orderItems->map(function ($item) use (&$response) {
                $product = [$item->product->name, "$item->qty", substr($item->plant->code, -3)];
                return $product;
            });

            if ($this->type === "inquiry") {
                Arr::set($response, "product", $products);
                Arr::set($response, "bookingTimeLimit", Carbon::parse($order->payment_due_date)->format("d-m-Y H:i:s"));
            } else {
                Arr::forget($response, ["currency", "bankAccountNo", "billAmount"]);
                Arr::set($response, "batasAmbil", parseDate($order->good_redemption_due_date)->format("d-m-Y"));

                $warehouses = $order->orderItems()->select(
                    DB::raw("(select code from wcm_plant where wcm_plant.id = plant_id) as plant_code"),
                    DB::raw("(select name from wcm_product where wcm_product.id = product_id) as product_name"),
                    DB::raw("sum(qty) as qty")
                )->groupBy("plant_id", "product_id")->get();

                $warehouseNo = 1;
                foreach ($warehouses as $warehouse) {
                    Arr::set($response, "prodTonaseWarehouse" . $warehouseNo, "{$warehouse->product_name} {$warehouse->qty} ". substr($warehouse->plant_code, -3));
                    $warehouseNo++;
                }

                $plant = Plant::where("code", optional($warehouses->first())->plant_code)->first();

                Arr::set($response, "warehouse", "{$plant->name} - {$plant->salesGroup->name}");
            }
        }

        return $response;
    }

    public function getInquiryParams(): array
    {
        return ["trxid", "booking_kode", "channel"];
    }

    public function getPaymentParams(): array
    {
        return ["trxid", "booking_kode", "channel", "amount"];
    }

    public function getValidationRule()
    {
        $rule = [
            "trxid"        => "required",
            "booking_kode" => "required",
            "channel"      => "required",
        ];

        if ($this->getType() == "payment") {
            $rule += [
                "amount" => "required",
            ];
        }

        return $rule;
    }

    /**
     * Gets the parameter send invoice.
     *
     * @param      <type>  $data   The data
     *
     * @return     array   The parameter send invoice.
     */
    protected function getParamSendInvoice($data): array
    {
        $sales_area = CustomerSalesArea::getSalesAreaOrder(
            $data->customer_id,
            $data->sales_org_id
        );
        $req_uuid = sprintf(
            "invphic%s-%s-%s-%s-%s", 
            str_random(2),
            str_random(4), 
            str_random(4),
            str_random(4),
            str_random(12)
        );
        $dueDate =  Carbon::parse($data->df_due_date);
        if ($dueDate->isWeekend()) {
            $dueDate->nextWeekday();
        }

        return [
            "rq_uuid"         => $req_uuid,
            "timestamp"       => Carbon::parse($data->created_at)->format("Y-m-d H:i:s"),
            "rq_datetime"     => Carbon::now()->format("Y-m-d H:i:s"),
            "password"        => "P!HcD?stribut0rF!n4nc!ng",
            "amount"          => sprintf("%.2d",$data->total_price_before_ppn),
            "ccy"             => "IDR",
            "doc_no"          => $data->number,
            "due_date"        => $dueDate->format("d/m/Y"),
            "issue_date"      => Carbon::parse($data->payment_due_date)->format("d/m/Y"),
            "comm_code"       => $data->sales_org_id,
            // "term_of_payment" => $sales_area->term_of_payment,
            "member_code"     => $data->customer_id,
        ];
    }

    protected function getParamCheckLimit(): array
    {
        $req_uuid = sprintf(
            "dfphic%s-%s-%s-%s-%s", 
            str_random(2),
            str_random(4), 
            str_random(4),
            str_random(4),
            str_random(12)
        );
        return [
            "rq_uuid"     => $req_uuid,
            "password"    => "P!HcD?stribut0rF!n4nc!ng",
            "currency"    => "IDR",
            "comm_code"   => request()->get("sales_org_id"),
            "member_code" => request()->get("customer_id"),
            "rq_datetime" => Carbon::now()->format("Y-m-d H:i:s"),
        ];
    }
    public function getParamDisbrusement(): array
    {
        return [
            "rq_uuid"          => "required",
            "doc_no"           => "required",
            "rq_datetime"      => "required",
            "password"         => "required",
            "comm_code"        => "required|exists:wcm_sales_org,id",
            "member_code"      => ["required", new CustomerExistsRule],
            "payment_ref"      => "required",
            "amount"           => "required",
            "ccy"              => "required",
            "transaction_date" => "required",
            "error_code"       => "required",
            "error_msg"        => "required",
            "status_doc"       => "required",
        ];
    }

    protected function setResponDisbrusement($request): array
    {

        return [
            "rq_uuid"          => $request->get("rq_uuid"),
            "rs_datetime"      => Carbon::now()->format("Y-m-d H:i:s"),
            "error_code"       => $request->get("error_code"),
            "error_msg"        => $request->get("error_msg"),
            "message"          => $request->get("message"),
            "status_doc"       => $request->get("status_doc"),

        ];
    }

    public function handleInvalidOrder(\Exception $e)
    {
        $notFoundRespon = '{"rq_uuid":"","currency":"","billInfo1":"","billCode":"","billName":"","billShortname":"","bankAccountNo":"","billAmount":"","bookingTimeLimit":"","commCode":"","isError":"true","errorCode":"B5","statusDescription":"Tagihan tidak ditemukan / Nomor Referensi salah","salesOrderNumber":"","product":"","warehouse":"","destination":""}';

        return response()->json(json_decode($notFoundRespon), 500);
    }

    public function getParamAuthorize(Request $params): array 
    {
        return [
            "ip_address" => $params->ip(),
        ];
    }
}
