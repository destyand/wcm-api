<?php

namespace App\Imports;

use App\models\Contract;
use App\Models\DeliveryMethod;
use App\Models\MaterialList;
use App\Models\Plant;
use App\Models\Retail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SalesOrderImport implements ToCollection, WithHeadingRow, WithMultipleSheets
{
    private $status;
    private $data;
    private $fillables;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $this->data = [];
        $this->fillables = collect([
            "no_order"                          => "number",
            "no_so"                             => "so_number",
            "produsen"                          => "sales_org_id",
            "pembeli"                           => "customer_id",
            "no_spjb"                           => "contract_id",
            "tanggal_pembuatan_so"              => "order_date",
            "kode_provinsi"                     => "sales_office_id",
            "nama_provinsi"                     => false,
            "kode_kotakabupaten"                => "sales_group_id",
            "nama_kotakab"                      => false,
            "syarat_penebusan"                  => "delivery_method_id",
            "tanggal_request_mulai_pengambilan" => "desc",
            "tipe_pembayaran"                   => "payment_method",
            "jenis_kendaraan"                   => "desc",
            "status_so"                         => "status",
            "tanggal_batas_pengambilan"         => "good_redemption_due_date",
            "jumlah_harga_sebelum_pajak"        => "total_price_before_ppn",
            "ppn"                               => "ppn",
            "pph_22"                            => "pph22",
            "uang_muka"                         => "upfront_payment",
            "total_harga"                       => "total_price",
            "kode_kecamatan"                    => false,
            "nama_kecamatan"                    => false,
            "pengecer"                          => false,
            "kode_gudang"                       => false,
            "qty_urea_ton"                      => false,
            "qty_npk_ton"                       => false,
            "qty_organik_ton"                   => false,
            "qty_za_ton"                        => false,
            "qty_sp36_ton"                      => false,
            "qty_org_cair_liter"                => false,
            "qty_npk_kakao_ton"                 => false,
        ]);

        $collection->each(function($r) {
            if ($r->values()->filter()->count() > 0 && $this->fillables->keys()->diff($r->keys())->count() == 0) {
                $row = $r->only($this->fillables->keys());
                if (!$row->isEmpty()) {
                    $row->put("kode_provinsi", sprintf("%04d", $row->get("kode_provinsi")));
                    $validator = Validator::make($row->toArray(), $this->getValidationRule(), $this->getValidationMessage());
                    if ($validator->fails()) {
                        $this->status = 403;
                        $this->data[] = [
                            "row" => $row,
                            "messages" => $validator->errors(),
                        ];
                    } else {
                        $header = $row->toArray();
                        $childs = [];

                        // Begin get Order Item
                        $salesUnit = $row->get("kode_kecamatan");
                        $retail = Retail::where("sales_unit_id", $salesUnit)
                            ->where("sub_district_default", 1)
                            ->first();
                        $plant = Plant::where("code", $row->get("kode_gudang"))
                            ->first();

                        if ($row->get("qty_urea_ton")) {
                            $childs[] = [
                                "product_id" => "P01",
                                "qty" => (float) $row->get("qty_urea_ton"),
                            ];
                        }

                        if ($row->get("qty_npk_ton")) {
                            $childs[] = [
                                "product_id" => "P02",
                                "qty" => (float) $row->get("qty_npk_ton"),
                            ];
                        }
                        
                        if ($row->get("qty_organik_ton")) {
                            $childs[] = [
                                "product_id" => "P03",
                                "qty" => (float) $row->get("qty_organik_ton"),
                            ];
                        }

                        if ($row->get("qty_za_ton")) {
                            $childs[] = [
                                "product_id" => "P04",
                                "qty" => (float) $row->get("qty_za_ton"),
                            ];
                        }

                        if ($row->get("qty_sp36_ton")) {
                            $childs[] = [
                                "product_id" => "P05",
                                "qty" => (float) $row->get("qty_sp36_ton"),
                            ];
                        }

                        if ($row->get("qty_org_cair_liter")) {
                            $childs[] = [
                                "product_id" => "P51",
                                "qty" => (float) $row->get("qty_org_cair_liter"),
                            ];
                        }

                        if ($row->get("qty_npk_kakao_ton")) {
                            $childs[] = [
                                "product_id" => "P54",
                                "qty" => (float) $row->get("qty_npk_kakao_ton"),
                            ];
                        }

                        $itemsValidation = [];
                        if (count($childs) == 0) {
                            $this->status = 403;
                            $itemsValidation["items"] = "Jumlah Order Item Tidak Boleh Nol.";
                        } else {
                            foreach($childs as $k => $val) {
                                $val["retail_id"] = $retail->id;
                                $val["plant_id"] = $plant->id;
    
                                $material = MaterialList::where("product_id", $val["product_id"])
                                    ->where("plant_id", $plant->id)
                                    ->first();
    
                                if (!$material) {
                                    $itemsValidation["material_list_id"] = "Material Product {$val["product_id"]} di Plant {$plant->id} tidak ditemukan";
                                    $this->status = 403;
                                } else {
                                    $val["material_list_id"] = $material->id;
                                }
    
                                if ($val["qty"] <= 0) {
                                    $itemsValidation["qty"] = "QTY harus lebih dari Nol";
                                    $this->status = 403;
                                }
    
                                $val["pph"] = (float) $row->get("pph_22");
                                $val["ppn"] = (float) $row->get("ppn");
                                $val["total_price"] = (float) $row->get("total_harga");
                                $val["total_price_before_ppn"] = (float) $row->get("jumlah_harga_sebelum_pajak");
    
                                $childs[$k] = $val;
                            }
                        }

                        
                        if ($this->status != 403) {
                            $existsHeader = $this->findIndexHeader($this->data, $row->toArray());
                            if ($existsHeader > -1) {
                                $existsChilds = $this->data[$existsHeader]["childs"];
                                $this->data[$existsHeader]["childs"] = array_merge($existsChilds,$childs);
                            } else {
                                $this->data[] = [
                                    "header" => $header,
                                    "childs" => $childs
                                ];
                            }
                        } else {
                            $this->data[] = [
                                "row" => $row,
                                "messages" => $itemsValidation,
                            ];
                        }
                    }
                }
            }
        });
        if (!in_array($this->status,[403])) {
            $batchInsert = [];
            foreach($this->data as $row) {
                $param = [
                    "so_upload" => 1,
                ];
                // begin Flipping row Keys
                foreach ($row["header"] as $key => $value) {
                    $dbKey = $this->fillables->get($key, false);
                    if ($key == "no_spjb") {
                        $contract = Contract::where("number", $value)->first();
                        $value = @$contract->id;
                    }
                    
                    if (strpos($key, "tanggal") !== false) {
                        if ($value) {
                            $value = parseDate($value)->format("Y-m-d");
                        } else {
                            $value = null;
                        }
                    }

                    if ($key == "syarat_penebusan") {
                        $delivery = DeliveryMethod::where("name", $value)->first();
                        $value = @$delivery->id;
                    }

                    if ($key == "kode_provinsi") {
                        $value = sprintf("%04s", $value);
                    }

                    if ($key == "status_so") {
                        $status = [
                            "good_issued" => "k",
                            'dp_paid' => "u",
                            "paid" => "l"

                        ];

                        $value = $status[Str::snake(strtolower($value))];
                    }

                    if ($dbKey !== false) {
                        $param["{$dbKey}"] = trim($value);
                    }
                }

                $batchInsert[] = [
                    "header" => $param,
                    "childs" => $row["childs"],
                ];
            }
            $this->data = $batchInsert;
        }


    }

    private function getValidationRule()
    {
        return [
            "no_so" => "required|unique:wcm_orders,so_number",
            "produsen" => "required|exists:wcm_sales_org,id",
            "pembeli" => "required|exists:wcm_customer,id",
            "no_spjb" => "required|exists:wcm_contract,number",
            "tanggal_pembuatan_so"=> "required",
            "kode_provinsi" => "required|exists:wcm_sales_office,id",
            "kode_kotakabupaten" => "required|exists:wcm_sales_group,id",
            "syarat_penebusan" => "required",
            "tipe_pembayaran" => "required",
            "jenis_kendaraan" => "required",
            "tanggal_batas_pengambilan" => "required",
            "jumlah_harga_sebelum_pajak" => "numeric|min:0|not_in:0",
            "kode_kecamatan" => "required|exists:wcm_sales_unit,id",
            "total_harga"=> "required|numeric|min:0|not_in:0",
            "ppn"=> "numeric|min:0|not_in:0",
            "uang_muka"=> "numeric|min:0|not_in:0",
            "pph_22"=> "nullable|numeric|min:0",
            'status_so' =>"required|in:Good Issued,DP Paid,Paid"
        ];
    }
    
    private function getValidationMessage()
    {
        return [
            "required" => ":attribute tidak boleh kosong.",
            "numeric" => ":attribute harus bertipe angka.",
            "exists" => ":attribute tidak benar.",
            "unique" => ":attribute tidak boleh duplikat."
        ];
    }

    public function getStatus():bool 
    {
        return !in_array($this->status, [403,500]);
    }

    public function getData()
    {
        return $this->data;
    }

     /**
     * @return array
     */
    public function sheets(): array 
    {
        return [
            0=> $this,
        ];
    }

    private function findIndexHeader($rows, $row)
    {
        $index = -1;
        foreach($rows as $key => $val) {
            $header = $val["header"];
            if (
                $header["no_so"] == $row["no_so"] && 
                $header["produsen"] == $row["produsen"] &&
                $header["pembeli"] == $row["pembeli"] && 
                $header["no_spjb"] == $row["no_spjb"] &&
                $header["tanggal_pembuatan_so"] == $row["tanggal_pembuatan_so"] && 
                $header["kode_provinsi"] == $row["kode_provinsi"] &&
                $header["kode_kotakabupaten"] == $row["kode_kotakabupaten"] && 
                $header["syarat_penebusan"] == $row["syarat_penebusan"] &&
                $header["tipe_pembayaran"] == $row["tipe_pembayaran"] &&
                $header["jenis_kendaraan"] == $row["jenis_kendaraan"] &&
                $header["status_so"] == $row["status_so"] &&
                $header["tanggal_batas_pengambilan"] == $row["tanggal_batas_pengambilan"] &&
                $header["jumlah_harga_sebelum_pajak"] == $row["jumlah_harga_sebelum_pajak"] &&
                $header["ppn"] == $row["ppn"] &&
                $header["uang_muka"] == $row["uang_muka"] &&
                $header["total_harga"] == $row["total_harga"]
            ) {
                $index = $key;
                break;
            }
        }

        return $index;
    }

}
