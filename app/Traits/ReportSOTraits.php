<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait ReportSOTraits
{
    public function scopeReportSOHeader($query)
    {
       return DB::table('vreport_so_header');
    }

    public function scopeReportSODetail($query)
    {
       return DB::table('vreport_so_detail');
    }
}
