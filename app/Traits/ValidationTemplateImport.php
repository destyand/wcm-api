<?php 
namespace App\Traits;


trait ValidationTemplateImport {
    private $rules = [
        "retail" => [
            "distributor","sales_org","kode_pengecer","nama_pengecer",
            "nama_pemilik_perusahaan","alamat_pengecer","kode_kecamatan",
            "kecamatan","desa","no_telepon_kantor","no_faks_kantor","no_handphone",
            "email","npwp","masa_berlaku_npwp","siup","masa_berlaku_siup","situ",
            "masa_berlaku_situ","tdp","masa_berlaku_tdp","surat_rekomendasi_dinas",
            "masa_berlaku_surat_rekomendasi_dinas","status_pengecer"
        ],
        "farmer_group" => [
            "kode_pengecer","sales_org","nama_kelompok_tani","alamat_kelompok_tani",
            "kode_kecamatan","kecamatan","desa","jumlah_anggota","luas_lahan",
            "aktif","sub_sektor_tanaman_pangan","sub_sektor_holtikultura",
            "sub_sektor_perkebunan_rakyat","sub_sektor_perikanan",
            "sub_sektor_peternakan","komoditas_anggur","komoditas_bunga",
            "komoditas_cabe","komoditas_cengkeh","komoditas_hijau_makanan_ternak",
            "komoditas_jagung","komoditas_jahe","komoditas_jambu_mete",
            "komoditas_jeruk","komoditas_kacang","komoditas_kakao",
            "komoditas_kedelai","komoditas_kelapa","komoditas_ketela_pohon",
            "komoditas_ketela_rambat","komoditas_kopi","komoditas_kubus",
            "komoditas_mangga","komoditas_manggis","komoditas_padi","komoditas_pandan",
            "komoditas_perikanan","komoditas_pisang","komoditas_rambutan","komoditas_salak",
            "komoditas_sawi","komoditas_sayuran","komoditas_semangka","komoditas_sengon"
        ],
        "farmer" => [
            "kode_kelompok_tani","sales_org","nama_petani","alamat_petani",
            "kode_kecamatan","kecamatan","desa","no_telepon","no_handphone",
            "luas_lahan","ktp","aktif","komoditas_anggur","komoditas_bunga",
            "komoditas_cabe","komoditas_cengkeh","komoditas_hijau_makanan_ternak",
            "komoditas_jagung","komoditas_jahe","komoditas_jambu_mete",
            "komoditas_jeruk","komoditas_kacang","komoditas_kakao",
            "komoditas_kedelai","komoditas_kelapa","komoditas_ketela_pohon",
            "komoditas_ketela_rambat","komoditas_kopi","komoditas_kubus",
            "komoditas_mangga","komoditas_manggis","komoditas_padi",
            "komoditas_pandan","komoditas_perikanan","komoditas_pisang",
            "komoditas_rambutan","komoditas_salak","komoditas_sawi",
            "komoditas_sayuran","komoditas_semangka","komoditas_sengon",
        ],
        "matrix" => [
            "kode_produsen","kode_provinsi","kode_kabupaten","kode_produk"
        ],
        "supply" => [
            "company_code","region","kode_kabupaten","kode_gudang",
            "kabupaten_yang_di_supply","aktif"
        ],
        "rdkk" => [
                "distributor","kode_distributor","kode_pengecer","kuantum_kebutuhan_npk_kg","kuantum_kebutuhan_organik_kg","kuantum_kebutuhan_sp36_kg","kuantum_kebutuhan_urea_kg","kuantum_kebutuhan_za_kg","nomor_rdkk","pengecer","periode_tanam","luas_lahan","indeks_pertanaman",
            ],
    ];

    private function getTemplateRules($type) 
    {
        return @$this->rules[$type] ? : [];
    }

    public function compareTemplate($type, $template) 
    {
        return count(array_diff($this->getTemplateRules($type), $template)) == 0;
    }
}
