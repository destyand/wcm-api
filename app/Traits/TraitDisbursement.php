<?php

namespace App\Traits;

use App\Helpers\SAPConnect;
use App\Jobs\DisbursementJob;
use App\Models\Bank;
use App\Models\LogOrder;
use App\Models\Order;
use App\Models\OrderDFStatus;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

trait TraitDisbursement
{

    public function sendDisbursement($params, $bankName = null)
    {
        $error     = false;
        $errorCode = "0000";
        $error_msg = "success";

        DB::beginTransaction();
        try {
            $order = Order::where("number", $params->get("doc_no"))->firstOrFail();

            if ($order->disbursement_status != "v") {
                throw new Exception();
            }

            $transaction_date        = str_replace("/", "-", $params->get("transaction_date"));
            $order->billing_fulldate = Carbon::parse($transaction_date);
            $order->status = "l";
            $order->disbursement_status = "w";
            $order->update();

            DB::commit();

            $bank  = str_replace('Bank ', '', Bank::find($order->bank_id)->name);
            $disbursementParams = [
                'I_BANK'  => $bank,
                'I_VBELN' => $order->so_number,
                'I_BUDAT' => parseDate($transaction_date)->format("Ymd"),
                'RETURN'  => [],
            ];

            DisbursementJob::dispatch($disbursementParams)->onQueue("low");
        } catch (ModelNotFoundException $me) {
            DB::rollback();
            return $this->requestToSap($params);
        } catch (\Exception $e) {
            $error     = true;
            if ($this->name == "Mandiri") {
                $error_msg = "Duplicate Document";
                $errorCode = "1111";
            } else {
                $error_msg = "Disbursement Sudah Dilakukan";
                $errorCode = "0030";
            }
        }

        if ($error) {
            DB::rollback();
            // $this->logOrder($params->get("doc_no"), 'Create Disbursement', 7, $error_msg);
        }

        $response                              = $this->setResponDisbrusement($params);
        $response[$this->getKeyErrorMessage()] = $error_msg;
        $response[$this->getKeyErrorCode()] = $errorCode;

        if ($this->name == "BNI") {
            $response["message"] = $error_msg;
        }

        $order_df = OrderDFStatus::where("order_id", @$order->id)->first();
        if (isset($order_df) && $order_df) {
            if ($order_df) {
                $order_df->error_message_disbursement = $error_msg;
                $order_df->update();
            }
        }
        sleep(2);
        return $response;
    }

    protected function getKeyErrorMessage()
    {
        $arr = [
            "mandiri" => "error_msg",
            "bri"     => "error_msg",
            "bni"     => "error_msg",
        ];

        return Arr::get($arr, strtolower($this->name));
    }

    protected function getKeyErrorCode()
    {
        $arr = [
            "mandiri" => "error_code",
            "bri"     => "error_code",
            "bni"     => "error_code",
        ];

        return Arr::get($arr, strtolower($this->name));
    }

    private function logOrder($number, $status, $kode, $message)
    {
        $order = Order::where('number', $number)->first();
        LogOrder::create([
            'order_id' => @$order->id ?: 0,
            'status'   => $status,
            'kode'     => $kode,
            'message'  => $message,
        ]);
    }
}
