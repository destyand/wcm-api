import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'

// language
import en from './list/en';
import id from './list/id';

Vue.use(MLInstaller)
var vueLang = (localStorage.getItem('app-lang') == undefined) ? 'id' : localStorage.getItem('app-lang');
export default new MLCreate({
  initial: vueLang,
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('en').create(en),
    new MLanguage('id').create(id)
  ]
})
