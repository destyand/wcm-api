export default {
  language: "en",
  global : {
    form: {
      labelAdd: "Add",
      labelEdit: "Edit",
      labelDelete: "Delete",
      labelUpdate: "Update",
      labelSave: "Save",
      labelUpload: "Upload Data",
      labelCancel: "Cancel",
      labelShowData: "Show Data",
    },
    language : {
        icon : "flag-icon flag-icon-gb"
    },
    filter: {
      all: "all",
      search: "search",
      reset: "reset",
      modal: {
        date: {
          titleOne: 'Range Date',
          titleTwo: 'Certain Date'
        },
        value:{
          titleOne: 'Range Value',
          titleTwo: 'Certain Value'
        },
        form: {
          labelFrom: 'From',
          labelTo: 'To',
          labelSearch: 'Search',
          labelClose: 'Close'
        }
      }
    }
  },
  header : {
    dropdownItemProfile : 'Profile',
    dropdownItemLogout : 'Sign Out',
  },
  loginPage: {
    title: "L O G I N",
    usernameLabel: "Username",
    passwordLabel: "Password",
    rememberLabel: "Remember Me",
    forgetLabel: "Forget Password ?  ",
    loginButton: "Login"
  },
  salesAreaPage: {
    breadcrumbFirst: "Sales Area",
    breadcrumbSec: "Area",
    cardtTitle: "Master Sales Area",
    download: "Download",
    table: {
      one: "Distibutor Code",
      two: "Distributor",
      three: "Sales Org Code",
      four: "Sales Org ",
      five: "Distributor Channel Code",
      six: "Distibutor Channel",
      seven: "Devision Code",
      eight: "Devision",
      nine: "Term Of Payment",
      ten: "TOP DP",
      eleven: "Tax Clasifivation",
      twelve: "Tax PPH 22",
      third: "Payment Method Type",
      fourteen: "Status"
    }
  },
  distributorMasterPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Distributor",
    cardtTitle: "Master Distributor",
    download: "Download",
    table: {
      one: "Code",
      two: "Name",
      three: "Owner Name",
      four: "Status",
      five: "Address",
      six: "Province",
      seven: "City / District",
      eight: "Sub District",
      nine: "Office Phone",
      ten: "Office FAX"
    }
  },
  userAllMasterPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master All User",
    cardtTitle: "Master All User",
    download: "Download",
    labelAdd: "Add User",
    table: {
      one: "Name",
      two: "Username",
      three: "Email",
      four: "Created At",
      five: "Updated At",
    }
  },
  userAllFormPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master All User",
    breadcrumbTrd: "Form User",
    cardtTitle: "Form User",
    password: {
      titleLabel: "Change Password",
      input:{
        one: "Old Password",
        two: "New Password",
        three: "Re new Password"
      },
      buttonLabelChange: "Change",
      buttonLabelClose: "Close",
    },
    form:{
      titleAdd: "Add User ",
      titleEdit: "Edit User ",
      one: "Name",
      two: "Username",
      three: "Email",
      four: "Password",
      five: "Re - Password",
      six: "Role",
      labelSave: "Save",
      labelDraft: "to Draft",
      labelCancel: "Back",
      changePasswordLabel: "Change Password"
    }
  },
  roleMasterPage:{
    cardtTitle:"Master Role",
    breadcrumbNames:{
      first:"Master Role",
      second:"Add Form Roles"
    },
    table:{
      one:"Name",
      two:"As Role",
      three:"Action",
    },
    addButton:"Add Role",
    filterPlaceName:"filter by name",
    titleModal:"Modal Role",
    saveBotton:"Save",
    closeButton:"Cancel"
  },
  menuMasterPage: {
    cardtTitle: "Master Menu",
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master Menu",
    labelAdd: "Add Menu",
    table: {
      one: "Name (EN)",
      two: "Name (ID)",
      three: "URI",
      four: "ORDER NO",
      five: "ICON",
      six: "Child of (EN)",
      seven: "Child of (ID)",
    }
  },
  menuRoutePage:{
    cardtTitle:"Master Route",
    breadcrumbFirst: "Route",
    table:{
      one:"Route",
      action:"Action"
    },
    addButton:"Add Route"
  },
  menuFormPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Master Menu",
    breadcrumbTrd: "Form Menu",
    cardtTitle: "Form Menu",
    form:{
      titleAdd: "Add Menu ",
      titleEdit: "Edit Menu ",
      one: "English Name",
      two: "Indonesia Name",
      three: "URL",
      four: "Order Number",
      five: "ICON",
      six: "Child of",
      seven: "Parent of English Menu name",
      labelSave: "Save",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  farmerGroupPage: {
    pageTitle : "FARMER GRUOP",
    breadcrumbFirst: "FARMER GROUP",
    breadcrumbSec: "Master Data",
    breadcrumbTrd: "Master",
    cardtTitle: "Form Menu",
    download: "Download",
    uploadData: "Upload Data",
    addButton: "Entry Data",
    downloadTemplate: "Download Template",
    downloadWilayah: "Download Kode Wilayah",
    table: {
      titleAdd: "Add Menu ",
      titleEdit: "Edit Menu ",
      one: "Sales Org",
      two: "Code",
      three: "Name",
      four: "Retail Code",
      five: "Retail Name",
      six: "Status",
      seven: "Address",
      eight: "Province",
      nine: "District",
      ten: "Sub District",
      eleven: "Village",
      twelve: "Sub Sector",
      thirteen: "Commodity",
      fourteen: "Total Member",
      fiveteen: "Land Area",
      sixteen: "Create At",
      seventeen: "Update At",
      labelSave: "",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  subtitutionPage: {
    pageTitle: "SUBTITUTION",
    breadcrumbFirst: "MASTER",
    breadcrumbSec: "SUBTITUTION",
    breadcrumbTrd: "Master",
    cardtTitle: "Form Menu",
    download: "Download",
    titleAdd: "Add Material",
    titleEdit: "Edit Menu ",
    saveBotton: "Simpan",
    closeButton: "Batal",
    table: {
      one: "Produsen",
      two: "Valid Date",
      three: "Valid Until",
      four: "Gudang",
      five: "Material",
      six: "Nama",
      seven: "Material Subtitution",
      eight: "UOM",
      nine: "Comment",
      ten: "Status",
      eleven: "Village",
      twelve: "Sub Sector",
      thirteen: "Commodity",
      fourteen: "Total Member",
      fiveteen: "Land Area",
      sixteen: "Create At",
      seventeen: "Update At",
      labelSave: "",
      labelDraft: "to Draft",
      labelCancel: "Back",
    }
  },
  privilegeMenu: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Menu Privilege",
    cardtTitle: " Management Menu Privilege",
    table: {
      one: "Role Name",
    }
  },
  petaniMasterPage: {
    breadcrumbFirst: "Data Master",
    breadcrumbSec: "Farmer",
    cardtTitle: "Master Farmer",
    download: "Download",
    title: "Farmer",
    table: {
      one: "Manufacturer",
      two: "Code",
      three: "Full Name",
      four: "Status",
      five: "Address",
      six: "Province",
      seven: "City / Regency",
      eight: "Sub District",
      nine: "Town / Village",
      ten: "No. Telp.",
      eleven: "Mobile Number",
      twelve: "Land Area",
      thirteen: "Commodities",
      fourteen: "KTP",
      fiveteen: "Created On",
      sixteen: "Updated On",
    }
  },
}
