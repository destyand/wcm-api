import * as CryptoJS from 'crypto-js';
import key from '../config/key';
export default {
  encrypt: (data) => {
    return CryptoJS.AES.encrypt(data, key.secretKey);
  },
  decrypt: (encrypt) => {
    return CryptoJS.AES.decrypt(encrypt, key.secretKey);
  },
  toText : (decrypt) => {
    return decrypt.toString(CryptoJS.enc.Utf8);
  }
}
