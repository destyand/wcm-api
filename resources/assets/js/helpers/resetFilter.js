import Vue from 'vue';


export const filterHelper = Vue.mixin({
  methods: {
    clearFilter(uriName, obj) {
			localStorage.removeItem(this.$auth.user().id+'_'+'search_'+uriName);
      localStorage.removeItem(this.$auth.user().id+'_'+'prov_'+uriName);
			localStorage.removeItem(this.$auth.user().id+'_'+'kab_'+uriName);
			localStorage.removeItem(this.$auth.user().id+'_'+'skabupaten_'+uriName);
			// console.log(this.$auth.user().id);
			Object.keys(obj).map(dt => {
				return obj[dt] = '';
			})
    },
  }
})