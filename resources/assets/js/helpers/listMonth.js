export default [
    { "val": "1", "name": "Januari" },
    { "val": "2", "name": "Februari" },
    { "val": "3", "name": "Maret" },
    { "val": "4", "name": "April" },
    { "val": "5", "name": "Mei" },
    { "val": "6", "name": "Juni" },
    { "val": "7", "name": "Juli" },
    { "val": "8", "name": "Agustus" },
    { "val": "9", "name": "September" },
    { "val": "10", "name": "Oktobe r" },
    { "val": "11", "name": "November" },
    { "val": "12", "name": "Desember" }
];