import Vue from 'vue';

export const languageHelper = Vue.mixin({
  methods: {
    getLangIcon(lang) {
      return (lang == "en") ? "flag-icon flag-icon-gb" : "flag-icon flag-icon-id";
    },
    getLangText(lang) {
      return (lang == "en") ? "English" : "Indonesia";
    }
  }
})
