export const columns = [
    {data: 'nomor_sales_order', name: 'nomor_sales_order'},
    {data: 'so_Item', name: 'so_Item'},
    {data: 'sales_organization', name: 'sales_organization'},
    {data: 'distribution_channel', name: 'distribution_channel'},
    {data: 'division', name: 'division'},
    {data: 'sales_office', name: 'sales_office'},
    {data: 'deskripsi_sales_office', name: 'deskripsi_sales_office'},
    {data: 'sales_group', name: 'sales_group'},
    {data: 'deskripsi_sales_group', name: 'deskripsi_sales_group'},
    {data: 'so_legacy', name: 'so_legacy'},
    {data: 'sales_unit_id', name: 'sales_unit_id'},
    {data: 'kecamatan_so_desc', name: 'kecamatan_so_desc'},
    {data: 'provinsi_distributor', name: 'provinsi_distributor'},
    {data: 'kabupaten_distributor', name: 'kabupaten_distributor'},
    {data: 'distributor', name: 'distributor'  }, 
    {data: 'nama_distributor', name: 'nama_distributor'},
    {data: 'pengecer', name: 'pengecer'},
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, // provinsi tujuan
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, // deskripsi provinsi tujuan
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, // negara tujuan
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, // deskripsi negara tujuan
    {data: 'nomor_kontrak', name: 'nomor_kontrak'},
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //tanggal mulai kontrak
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //tanggal berakhir kontrak
    {data: 'tanggal_so_dibuat', name: 'tanggal_so_dibuat'}, 
    {data: 'tanggal_dokumen', name: 'tanggal_dokumen'}, 
    {data: 'so_release', name: 'so_release'  }, 
    {data: 'payment_term', name: 'payment_term'}, 
    {data: 'payment_method', name: 'payment_method'},
    {data: 'nomor_material', name: 'nomor_material'},
    {data: 'deskripsi_material', name: 'deskripsi_material'},
    {data: 'material_group', name: 'material_group'},
    {data: 'alokasi_asal', name: 'alokasi_asal'},
    {data: 'alokasi_operasional', name: 'alokasi_operasional'},
    {data: 'quantity_so', name: 'quantity_so'},
    {data: 'unit_of_measure', name: 'unit_of_measure'},
    {data: 'mata_uang', name: 'mata_uang'},
    {data: 'harga_jual_exc_ppn', name: 'harga_jual_exc_ppn'},
    {data: 'ppn', name: 'ppn'},
    {data: 'total', name: 'total'},
    {data: 'harga_per_ton', name: 'harga_per_ton'},
    {data: 'harga_total', name: 'harga_total'},
    {data: 'nomor_do', name: 'nomor_do'},
    {data: 'tanggal_pgi', name: 'tanggal_pgi'},
    {data: 'plant_so', name: 'plant_so'},
    {data: 'gudang_so', name: 'gudang_so'},
    {data: 'gudang_so_deskripsi', name: 'gudang_so_deskripsi'},
    {data: 'kode_gudang', name: 'kode_gudang'},
    {data: 'gudang_pengambilan', name: 'gudang_pengambilan'},
    {data: 'quantity_do', name: 'quantity_do'},
    {data: 'quantity_so_min_do', name: 'quantity_so_min_do'},
    {data: 'status', name: 'status'},  
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //remarks
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //BL number
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //NO SPE
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //sisa SPE
    {data: 'pgi_qty', name: 'pgi_qty'},
    {data: 'total_harga_tonase_pgi', name: 'total_harga_tonase_pgi'},
    {data: 'outstanding_so', name: 'outstanding_so' }, 
    {data: 'so_type', name: 'so_type'},
    {data: 'so_type_description', name: 'so_type_description'},
    {data: 'provinsi_gudang', name: 'provinsi_gudang' }, 
    {data: 'kabupaten_gudang', name: 'kabupaten_gudang' },
    {data: 'payment_term', name: 'payment_term'},
    {data: 'paymentMethod', name: 'paymentMethod'},
    {data: 'batas_akhir_pengambilan', name: 'batas_akhir_pengambilan'},
    {data: 'no_po', name: 'no_po'},
    {data: 'tanggal_po', name: 'tanggal_po'},
    {data: 'so_created_by', name: 'so_created_by'}, 
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //ext financial doc number
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //opening date
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //latest shipmen date
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //expiry date
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //opening bank key
    {data: 'description', name: 'description'}, 
    {data: 'sektor', name: 'sektor'}, 
    {data: 'no_billing', name: 'no_billing'},
    {data: 'billing_date', name: 'billing_date'},
    {data: 'incoterm_1', name: 'incoterm_1'},
    {data: 'incoterm_2', name: 'incoterm_2'},
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //billing quantity
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //billing net
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //billing taks
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //POD status
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //POD status desc
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //Finance DOC number
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //Port of Discharge
    {data: 'batas_akhir_pengambilan', render: (d,t,f) => '<span class="red colomn-hide">No Column</span>' }, //Port Of loading
]