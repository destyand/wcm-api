export const columns = [
    { data: 'nomor_do', name: 'nomor_do' },
    { data: 'nomor_sales_order', name: 'nomor_sales_order' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //deliver item
    { data: 'nomor_material', name: 'nomor_material' },
    { data: 'deskripsi_material', name: 'deskripsi_material' },
    { data: 'material_group', name: 'material_group' },
    { data: 'sales_organization', name: 'sales_organization' },
    { data: 'distribution_channel', name: 'distribution_channel' },
    { data: 'division', name: 'division' },
    { data: 'sales_office', name: 'sales_office' },
    { data: 'deskripsi_sales_office', name: 'deskripsi_sales_office' },
    { data: 'sales_group', name: 'sales_group' },
    { data: 'deskripsi_sales_group', name: 'deskripsi_sales_group' },
    { data: 'so_legacy', name: 'so_legacy' },
    { data: 'sales_unit_id', name: 'sales_unit_id' },
    { data: 'kecamatan_so_desc', name: 'kecamatan_so_desc' },
    { data: 'distributor', name: 'distributor' },
    { data: 'nama_distributor', name:   'nama_distributor' },
    { data: 'provinsi_distributor', name: 'provinsi_distributor' },
    { data: 'kabupaten_distributor', name: 'kabupaten_distributor' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //provinsi tujuan
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //deskripsi provinsi tujuan
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //negara tujuan
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //deskriosi negara tujuan
    { data: 'nomor_kontrak', name: 'nomor_kontrak' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //tanggal mulai kontrak
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //tanggal berakhir kontrak
    { data: 'tanggal_so_dibuat', name: 'tanggal_so_dibuat' },
    { data: 'so_release', name: 'so_release' },
    { data: 'payment_term', name: 'payment_term' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //payment term day
    { data: 'paymentMethod', name: 'paymentMethod' },
    { data: 'payment_method', name: 'payment_method' },
    { data: 'batas_akhir_pengambilan', name: 'batas_akhir_pengambilan' },
    { data: 'quantity_so', name: 'quantity_so' },
    { data: 'unit_of_measure', name: 'unit_of_measure' },
    { data: 'kode_gudang', name: 'kode_gudang' },
    { data: 'gudang_pengambilan', name: 'gudang_pengambilan' },
    { data: 'quantity_do', name: 'quantity_do' },
    { data: 'tanggal_dokumen', name: 'tanggal_dokumen' },
    { data: 'tanggal_pgi', name: 'tanggal_pgi' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //good issue status
    { data: 'pgi_qty', name: 'pgi_qty' },
    { data: 'sektor', name: 'sektor' },
    { data: 'no_billing', name: 'no_billing' },
    { data: 'billing_date', name: 'billing_date' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //status do
    { data: 'status', name: 'status' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //alat angkut
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //nomor identitas alat angkut
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //eta
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //pengemudi
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //remarks
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //b/l number
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //nomor spe
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //sisa spe
    { data: 'so_type', name: 'so_type' },
    { data: 'so_type_description', name: 'so_type_description' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //do type
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //do type desc
    { data: 'provinsi_gudang', name: 'provinsi_gudang' },
    { data: 'kabupaten_gudang', name: 'kabupaten_gudang' },
    { data: 'pengecer', name: 'pengecer' },
    { data: 'no_po', name: 'no_po' },
    { data: 'tanggal_po', name: 'tanggal_po' },
    { data: 'so_created_by', name: 'so_created_by' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //do created by
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //penerima
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //pod status
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //pod desc
    { data: 'incoterm_1', name: 'incoterm_1' },
    { data: 'incoterm_2', name: 'incoterm_2' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //shipment planing status
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //shipment planing status desc
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //POD date
    { data: 'mata_uang', name: 'mata_uang' },
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //billing qty
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //billing net
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //billing tax
    { data: 'nomor_material', name: 'nomor_material', render: (d,t,f) => '-' }, //tgl last change do
    { data: 'alokasi_operasional', name: 'alokasi_operasional'}
]

export const filterColumns = (d) => {

}