<html>
<head>
    <title>Kode Booking - <?=$data[0]->booking_code?> </title>
    <style>
        html{
            font-family:sans-serif;
        }
        td{
            padding-left:5px;
            pading-right:5px;
            padding-top: 3px;
            padding-bottom: 3px;
            vertical-align: top;
        }
        p{            
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
        .font-grey {
            color: grey;
        }
    </style>
</head>
<body>
    <p style="text-align:center;font-size:20px;text-transform:underline;font-weight:bold;"><u>KODE BOOKING</u></p>    
    <p style="text-align:center;padding-top: 5px;font-size: 18px;" class="font-grey">No. <?=$data[0]->booking_code?> </p>
    <p style="text-align:center;padding-top: 5px;" class="font-grey">Tanggal <?=strtoupper($data[0]->order_date)?></p>
    <div style="margin-top:55px">
        <p style="color: grey;">Kepada Yth.</p>
        <p style="font-weight:bold"><?=$data[0]->customer_name;?></p>
        <p style="color: grey;"><?=$data[0]->address.' DESA '.$data[0]->village_name.' KEC. '.$data[0]->sales_unit_name.' '.$data[0]->sales_group_name;?> <br/> <?=$data[0]->sales_office_name.', Indonesia';?></p>
    </div>
    <div style="margin-top:50px;color: grey;">
        <p>Dengan hormat, <br/> 
        Bersama ini disampaikan informasi pembayaran yang harus dilakukan sebagai berikut:</p>
    </div>
    <div style="margin-top:40px">    
        <table>
            <tr style="font-weight:bold;color: grey;">
                <td width="40%">Nilai Pembelian</td>
                <td width="5%">:</td>
                <td width="55%"><?=$data[0]->total_price_rupiah?></td>                
            </tr>
            <tr style="font-weight:bold">
                <td>Uang muka yang harus dibayarkan</td>
                <td>:</td>
                <td><?=$data[0]->uang_muka_rupiah?></td>                
            </tr>
            <tr style="font-weight:bold">
                <td>Terbilang</td>
                <td>:</td>
                <td><?=ucfirst($data[0]->terbilang).' rupiah';?></td>
            </tr>
            <tr style="font-weight:bold">
                <td>Batas akhir pembayaran</td>
                <td>:</td>
                <td><?=strtoupper($data[0]->batas_akhir_pembayaran);?></td>
            </tr>
            <tr style="font-weight:bold">
                <td>Referensi</td>
                <td>:</td>
                <td><?=$data[0]->no_order;?></td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 40px;color: grey;">
        <p>Jika melakukan pembayaran melalui ATM, harap melakukan pembayaran di ATM dari bank yang sesuai dengan kartu ATM Anda.</p>
    </div>
    <div style="margin-top:40px;">
        <p style="color: grey;">Demikian disampaikan, atas perhatiannya diucapkan terima kasih<br/>
        <span style="font-weight:bold;color: black;"><?=$data[0]->produsen;?></p>
    </div>
</body>
</html>