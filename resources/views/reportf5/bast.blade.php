<html>
<head>
    <title>BAST</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis th, .garis td{
            border:1px solid #000000;            
        }
        .garis{
            border-collapse: collapse;            
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:2px solid #000000;            
        }
        .table-strip table{
            border-collapse: collapse;            
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{            
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }
    </style>
</head>
<body> 
    <?php
        $count=0;
        $max=count($datas);
    ?>
   
     @foreach ($datas as $item)
         <?php $count+=1; ?>
        <p style="font-size:17px;text-align:center;font-weight:bold;"> <u>BERITA ACARA SERAH TERIMA PUPUK</u></p>
        <p style="padding-top: 6px;font-size:17px;text-align:center;font-weight:bold;">Nomor : {{$item['number']}} </p>
        <div style="margin-top:40px">
            <p style="">Pada Hari  {{$date_name}}, {{$date}} {{$item['month_name']}} {{$item['year']}}. Yang bertanda tangan dibawah ini:</p> 
            <table>
                <tr><td style="">Pihak Pertama</td><td>: <b>{{$item['customer_name']}}</b> </td></tr>
                <tr><td></td><td>: <b>{{$item['customer_owner']}}</b></td></tr>
                <tr><td></td><td>: <b>Direktur</b></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td style="">Pihak Kedua</td><td>: <b>{{$item['retail_name']}}</b></td></tr>
                <tr><td></td><td>: <b>{{$item['retail_owner']}}</b></td></tr>
                <tr><td></td><td>: <b>Pimpinan</b></td></tr>
                <tr><td></td><td></td></tr>
            </table>
        </div>
        <div style="margin-top:10px">   
            <p style="">Menerangkan dengan sesungguhnya bahwa pada bulan {{$item['month_name']}} {{$item['year']}} PIHAK PERTAMA telah menyerahkan pupuk bersubsidi kepada PIHAK KEDUA di kios dalam keadaan baik, dan PIHAK KEDUA telah menerima barang tersebut juga dengan keadaan baik, dengan rincian sebagai berikut : <br/> </p> 
            </br>   
            <table class="garis" width="100%" padding-top="10px" style="font-size: 12px;" >
                <tr style="font-weight:bold;text-align:center;background-color: rgb(242,242,242);">
                    <td rowspan="2"> No</td>
                    <td rowspan="2"> Nomor SO</td>
                    <td rowspan="2" width="100px"> Tanggal Kirim</td>
                    <td colspan="7" > Jenis Pupuk (Ton)</td>
                    <td style="border-bottom: none !important;" ></td>
                </tr> 
                <tr style="font-weight:bold;text-align:center;">
                    <!-- @foreach ($product as $itemPr)
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 13px;">{{$itemPr->name}}</p></td>  
                    @endforeach -->
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">UREA</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">NPK</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">ORGANIK</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">ZA</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">SP-36</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">POC</p></td>  
                        <td width="60px" style="background-color: rgb(242,242,242);" ><p style="font-size: 12px;">KAKAO</p></td>  
                        <td style="background-color: rgb(242,242,242);border-top: none !important;"> Keterangan</td>
                </tr>

                <?php $loops=1; ?>
                @foreach ($item['data'] as $itemdata)
                    @if (
                            $itemdata['data'][0]["qty"] == 0 && $itemdata['data'][1]["qty"] == 0 &&
                            $itemdata['data'][2]["qty"] == 0 && $itemdata['data'][3]["qty"] == 0 &&
                            $itemdata['data'][4]["qty"] == 0 && $itemdata['data'][5]["qty"] == 0 &&
                            $itemdata['data'][6]["qty"] == 0
                     )
                        @continue
                    @endif
                <tr style="text-align:center;">
                    <td>{{$loops}}</td>
                    <td>{{$itemdata['so'] ?? ""}}</td>
                    <td>{{$itemdata['submited_date']}}</td>

                    <!-- @foreach ($itemdata['data'] as $prod)
                        <td>{{ number_format((float)$prod['qty'],3,',','.') }}</td>
                    @endforeach -->
                        <td>{{ number_format((float)$itemdata['data'][0]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][1]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][2]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][3]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][4]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][5]['qty'],3,',','.') }}</td>
                        <td>{{ number_format((float)$itemdata['data'][6]['qty'],3,',','.') }}</td>
                   
                    <td></td>
                </tr>
                 <?php $loops+=1; ?>
                @endforeach

                <tr style="font-weight:bold;text-align:center;">
                    <td colspan="3" style="text-align: right;"> Grand Total</td>
                    @foreach ($item['total'] as $total)
                        <td>{{ number_format((float)$total['qty'],3,',','.') }}</td>
                    @endforeach
                    <td></td>
                </tr> 
                
                      
            </table>
        </div>
        <div style="margin-top:30px;">    
            <table style="text-align:center;width:100%;">
                <tr>
                    <td width='50%' style="">Yang Menyerahkan,</td>
                    <td width='50%' style="">Yang Menerima</td>                
                </tr>
                <tr>
                    <td style="font-weight:600;">{{$item['customer_name']}}</td>
                    <td style="font-weight:600;">{{$item['retail_name']}}</td>                
                </tr>
                <tr>
                    <td><p style="margin-top:100px;">{{$item['customer_owner']}}</p></td>
                    <td><p style="margin-top:100px;">{{$item['retail_owner']}}</p></td>
                </tr>  
                <tr>
                    <td><p style="font-size:14px;">Direktur</p></td>
                    <td><p style="font-size:14px;">Pimpinan</p></td>                
                </tr>           
            </table>
        </div>
       
         @if ($count<$max)
             <div class="page_break">
             </div>
        @endif
    @endforeach
</body>
</html>