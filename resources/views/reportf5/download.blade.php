<html>
<head>
    <title>DOWNLOAD REPORT F5 {{$datas['header']['customer_id']}} {{$datas['header']['month']}} {{$datas['header']['year']}}</title>
    <style>
        html{
            font-family:sans-serif;
        }
        .garis table, .garis th, .garis td, .garis tr{
            border:1px solid #000000;
        }
        .garis{
            border-collapse: collapse;
            width:100%;
        }
        .page_break { page-break-before: always; }
        .bold{
            font-weight:bold;
        }
        .table-strip table, .table-strip table th, .table-strip table td, .table-strip table tr{
            border:1px solid #000000;
        }
        .table-strip table{
            border-collapse: collapse;
            width:100%;
        }
        td{
            padding-left:5px;
            pading-right:5px;
        }
        p{
            margin:0;
        }
        h1, h2, h3{
            margin-bottom:5px;
        }

        td.no-top-border {
            border-top: none !important;
            border-bottom: none !important;
            padding: 10px;
        }

        td.no-top-border-remove-buttom {
            border-bottom: none !important;
        }
        
    </style>
</head>
<body>
     {{-- @foreach ($datas['total'] as $item => $val)
        @foreach ($val as $items)
            <p style="font-weight:600;text-align:center">  {{$items['qty']}}  </p>
        @endforeach
    @endforeach --}}

     <div style="position: absolute;"><img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(220)->errorCorrection('H')->generate($url)) !!}" alt="BST" /> </div>
     <div> <br> <br/> <br></div>
        <p style="margin-left:80%;padding:0;font-weight:500; ">Kepada Yth. </p>
        <p style="margin-left:80%;padding:0;font-weight:500; ">{{$datas['header']['sales_org_name']}}</p>
        <p style="margin-left:80%;padding:0;font-weight:500; ">di Tempat </p>

        <p style="font-weight:700;text-align:center">{{$datas['header']['number']}}</p>
        @if ($datas['header']['status']=='d')
            <p style="font-weight:700;text-align:center">[DRAFT] LAPORAN BULANAN DISTRIBUTOR</p>
        @else
            <p style="font-weight:700;text-align:center">LAPORAN BULANAN DISTRIBUTOR</p>
        @endif

        <p style="font-weight:700;text-align:center">DISTRIBUTOR {{$datas['header']['customer_name']}}  {{strtoupper($datas['header']['sales_group_name']) }}</p>
        <p style="font-weight:700;text-align:center">PERIODE {{strtoupper($datas['header']['month'])}} {{$datas['header']['year']}}</p>
        <br>
        <br>
             <p style="font-weight:bold;font-size:10px;margin-right:5;text-align:right"><i>(DALAM SATUAN TON)</i> </p>
            <table class="garis" width="100%" style="font-size:12px;">
                <thead>
                <tr style="background-color: rgb(242,242,242);">
                    <td rowspan="2" colspan="2"><p style="font-weight:600;text-align:center">  KABUPATEN/KECAMATAN/PENGECER  </p> </td>
                    @foreach ($datas['total'] as $item => $val)
                        <td colspan={{count($datas['produks'])}}> <p style="font-weight:600;text-align:center"> {{strtoupper($item)}} </p></td>
                    @endforeach
                </tr>
                <tr style="background-color: rgb(242,242,242);">
                    @foreach ($datas['total'] as $item => $val)
                        @foreach ($datas['produks'] as $itemP)
                        <td width="40px">
                        <p style="font-weight:bold;font-size:10px;text-align:center">  
                            @if($itemP['name']=="ORGANIK CAIR")
                                 POC
                            @elseif($itemP['name']=="NPK KAKAO")
                                KAKAO  
                            @else
                                {{$itemP['name']}}
                           @endif                            
                        </p> 
                        </td>
                        
                        @endforeach
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr >
                    <td colspan="2"><p style="font-weight:400;text-align:left;font-weight: bold;">  {{$datas['header']['sales_group_name']}} </p> </td>
                     @foreach ($datas['total'] as $item => $val)
                        @foreach ($val as $itemP)
                        @if ($itemP['qty']!=0)
                            <td><p style="font-weight:bold;text-align:center">  {{number_format((float)$itemP['qty'],3,',','.')}}  </p> </td>
                        @else
                            <td><p style="font-weight:400;text-align:center"> </p> </td>
                        @endif

                        @endforeach
                    @endforeach
                </tr>
                {{-- Looper Per Kecamatan --}}
                
                @foreach ($datas['data'] as $itemData)
                    <tr>
                        <td><p style="font-weight:600;text-align:center">Kecamatan</p></td>
                        <td><p style="font-weight:600;text-align:center">Pengecer</p></td>
                        @foreach ($itemData['total'] as $itemT => $valT)
                            @foreach ($valT as $itemT)
                                <td><p style="font-weight:400;text-align:center">  </p> </td>
                            @endforeach
                        @endforeach
                    </tr>
                    <?php
                        $count=0;
                    ?>
                    @foreach ($itemData['data'] as $itemD)
                    <tr style="">
                        <?php
                            $count+=1;
                        ?>
                       @if ($count==1)
                           <td class="no-top-border-remove-buttom">{{ $itemData['name'] }}</td>
                       @else
                            <td class="no-top-border"></td>
                       @endif

                        <td>{{ $itemD['name'] }}</td>
                        @foreach ($itemD['data'] as $itemRT => $valRT)

                            @foreach ($valRT as $itemDRT)

                            @if ($itemRT=='penyaluran' && $itemDRT['qty']!=0 )
                                    <td><p style="font-weight:400;text-align:center"> {{ number_format((float)$itemDRT['qty'],3,',','.')}} </p> </td>
                            @else
                                    <td><p style="font-weight:400;text-align:center"> </p> </td>
                            @endif

                            @endforeach
                        @endforeach

                    </tr>
                    @endforeach
                    <tr >
                        <td colspan="2"><p style="font-weight:600;text-align:left;">Jumlah Kec. {{ $itemData['name'] }} </p></td>
                        @foreach ($itemData['total'] as $itemT => $valT)
                            @foreach ($valT as $itemT)
                                @if ($itemT['qty']!=0)
                                    <td><p style="text-align:center">  {{ number_format((float)$itemT['qty'],3,',','.') }}  </p> </td>
                                @else
                                    <td><p style="font-weight:600;text-align:center">   </p> </td>
                                @endif

                            @endforeach
                        @endforeach
                    </tr>
                @endforeach
                {{-- End Looper Per Kecamatan --}}
                <tr style="padding-top: 5px;">
                    <td colspan="{{(count($datas['produks'])*4)+2}}" style="border-left: 0px solid;border-right: 0px solid;"><p> </p></td>
                </tr>
                <tr >
                    <td colspan="2"><p style="text-align:left;font-weight: bold;">  JUMLAH </p> </td>
                     @foreach ($datas['total'] as $item => $val)
                        @foreach ($val as $itemP)
                            <td><p style="font-weight:bold;text-align:center">
                                    @if($itemP['qty']!=0)
                                    {{ number_format((float)$itemP['qty'],3,',','.')}}
                                    @else
                                        0
                                    @endif
                                </p>
                            </td>
                        @endforeach
                    @endforeach
                </tr>
                <tr>
                   <td colspan="{{ (count($datas['produks'])*3)+2 }}">
                        <p style="font-size:12px;font-weight:300;text-align:left;">Tembusan :  </p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">1. Kepala Dinas Perindag Provinsi {{ $datas['header']['sales_office_name'] }} </p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">2. Kepala Dinas Pertanian Provinsi {{ $datas['header']['sales_office_name'] }} </p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">3. Kepala Dinas Perindag Kabupaten/Kota {{ $datas['header']['sales_group_name'] }} </p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">4. Kepala Dinas Pertanian Kabupaten/Kota {{ $datas['header']['sales_group_name'] }} </p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">5. Komisi Pengawas Pupuk dan Pestisida Provinsi {{ $datas['header']['sales_office_name'] }}</p>
                        <p style="font-size:12px;font-weight:300;text-align:left;">6. Komisi Pengawas Pupuk dan Pestisida Kabupaten/Kota {{ $datas['header']['sales_group_name'] }}</p>
                   </td>
                   <td colspan="{{ count($datas['produks']) }}" style="font-size:12px;font-weight:300;text-align:center;">
                       <p>
                           {{$datas['header']['sales_group_name']}}, ... {{$datas['header']['month']}} {{$datas['header']['year']}} <br>
                           {{$datas['header']['customer_name']}} <br>
                           <br>
                           <br>
                           <br>
                           ..............................
                       </p>
                   </td>
                </tr>
                </tbody>
            </table>
</body>
</html>
