<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Account Info WCM PIHC</title>
    </head>
    <body class="">

        <img src="{{asset('/images/logo/pihc-logo.png')}}">
        <p>Kepada Yth. {{ $username }}</p>
        <br>
        <p>Berikut ini kami informasikan bahwa akun anda telah terdaftar di dalam sistem Penebusan Pupuk Subsidi PIHC pada tanggal <b>{{ $date }}</b></p>
        <p>Silahkan menggunakan username dan password berikut ini</p>
        <hr>
        <p>Username : <b>{{ $username }}</b></p>
        <hr>
        <p>Password : <b>{{ $password }}</b></p>
        <hr>
        <p>
            Untuk memulai penebusan pupuk bersubsidi klik <a href="http://36.89.214.42:8000/#/login" target="_blank">disini</a> atau copy paste link berikut : http://36.89.214.42:8000/#/login
        </p>
        <p>
            Untuk informasi lebih lanjut dapat menghubungi Contact Center PIHC di 0213345552 atau email contact@pihc.com
        </p>
        <br>
        <p>
            Terima Kasih.
        </p>
    </body>
</html>