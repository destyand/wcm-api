<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Forget Password WCM PIHC</title>
    </head>
    <body class="">

        <img src="{{asset('images/logo/pihc-logo.png')}}">
        <p>Kepada Yth. {{ $users->name }} </p>
        <br>
        <p>Permintaan Perubahan Password Anda Telah Kami Konfirmasi, <b></b></p>
        <p>Silahkan klik url berikut <b> sebelum  {{$valid_to}} </b> untuk mengganti password anda :</p>
        <hr>
        <p> <a href={{$link}} target="_blank">{{$link}}</a>  <b></b></p>
        <hr>
        <p>
            Untuk informasi lebih lanjut dapat menghubungi Contact Center PIHC di 0213345552 atau email contact@pihc.com
        </p>
        <br>
        <p>
            Terima Kasih.
        </p>
    </body>
</html>