/**
 * @api {get} /distributor Get All Distributor
 * @apiVersion 1.0.0
 * @apiName Get All Distributor
 * @apiGroup Distributor
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id distributor
 * @apiSuccess {String} data.full_name nama distributor
 * @apiSuccess {String} data.owner nama pemilik
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.address alamat distributor
 * @apiSuccess {String} data.unit_id id unit (id kecematan)
 * @apiSuccess {String} data.unit_name nama unit (nama kecematan)
 * @apiSuccess {String} data.group_id id kabupaten (id kabupaten)
 * @apiSuccess {String} data.group_name nama unit (nama kabupaten)
 * @apiSuccess {String} data.office_id id unit (id provinsi)
 * @apiSuccess {String} data.office_name nama unit (nama provinsi)
 * @apiSuccess {String} data.tlp_no no telephone
 * @apiSuccess {String} data.fax_no no fax
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /distributor/id/:uuid Get Distributor By UUID
 * @apiVersion 1.0.0
 * @apiName Get Distributor By UUID
 * @apiGroup Distributor
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id uuid Distributor
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id customer
 * @apiSuccess {String} data.uuid UUID customer
 * @apiSuccess {String} data.owner pemilik
 * @apiSuccess {String} data.register_date data registrasi
 * @apiSuccess {String} data.code_cust_group -
 * @apiSuccess {String} data.name_cust_group -
 * @apiSuccess {String} data.old_number -
 * @apiSuccess {String} data.npwp_no no NPWP
 * @apiSuccess {String} data.npwp_register registrasi date "may be"
 * @apiSuccess {String} data.recomd_letter_date -
 * @apiSuccess {String} data.recomd_letter -
 * @apiSuccess {String} data.valid_date_tdp -
 * @apiSuccess {String} data.tdp_no -
 * @apiSuccess {String} data.situ_no -
 * @apiSuccess {String} data.valid_date_situ -
 * @apiSuccess {String} data.siup_no -
 * @apiSuccess {String} data.category -
 * @apiSuccess {String} data.asset -
 * @apiSuccess {String} data.revenue -
 * @apiSuccess {String} data.account_year -
 * @apiSuccess {String} data.status -
 * @apiSuccess {Array} data.contact -
 * @apiSuccess {String} data.created_by -
 * @apiSuccess {String} data.updated_by -
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
