/**
 * @api {post} /role Create Role
 * @apiVersion 1.0.0
 * @apiName Create Role
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama role
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id role
 * @apiSuccess {String} data.name nama role
 * @apiSuccess {String} data.guard_name guard name
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /role Get All Role
 * @apiVersion 1.0.0
 * @apiName Get All Role
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id role
 * @apiSuccess {String} data.name nama role
 * @apiSuccess {String} data.guard_name guard name
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /role/:id Update Role
 * @apiVersion 1.0.0
 * @apiName Update Role
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} id id role
 * @apiParam {String} name nama role
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id role
 * @apiSuccess {String} data.name nama role
 * @apiSuccess {String} data.guard_name guard name
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /role/:id Delete Role
 * @apiVersion 1.0.0
 * @apiName Delete Role
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} id id role
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id role
 * @apiSuccess {String} data.name nama role
 * @apiSuccess {String} data.guard_name guard name
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

   /**
 * @api {get} /menu/permission/:role_id Permission Menu by Role id
 * @apiVersion 1.0.0
 * @apiName Show Permission Menu
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id role id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id menu
 * @apiSuccess {String} data.name nama menu sesuai bahasa
 * @apiSuccess {String} data.CRUD action yang sudah diset (Y= Sudah, N=Tidak)
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

  /**
 * @api {post} /menu/setpermission/:role_id Set Permission Menu
 * @apiVersion 1.0.0
 * @apiName Set Permission Menu
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} role_id role id (param untuk URL)
 * @apiParam {Array[]} data Data berbentuk array 2 dimensi
 * @apiParam {Integer} data.role_id id role
 * @apiParam {Integer} data.menu_id id menu
 * @apiParam {String} data.permission_id id id permission (action)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [{"role_id":1,"menu_id":1,"action_id":"C"},{"role_id":1,"menu_id":1,"action_id":"R"}]
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.name nama menu
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 
   /**
 * @api {get} /menu/permission/:role_id Permission Route by Role id
 * @apiVersion 1.0.0
 * @apiName Show Permission Role
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id role id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id route
 * @apiSuccess {String} data.name nama route
 * @apiSuccess {String} data.CRUD action yang sudah diset (Y= Sudah, N=Tidak)
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

  /**
 * @api {post} /route/setpermission/:role_id Set Permission Route
 * @apiVersion 1.0.0
 * @apiName Set Permission Route
 * @apiGroup Role
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} role_id role id (param untuk URL)
 * @apiParam {Array[]} data Data berbentuk array 2 dimensi
 * @apiParam {Integer} data.role_id id role
 * @apiParam {Integer} data.route_id id route
 * @apiParam {String} data.action_id id id action
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [{"role_id":1,"route_id":2,"action_id":"C"},{"role_id":1,"route_id":2,"action_id":"R"}]
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.name nama menu
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
