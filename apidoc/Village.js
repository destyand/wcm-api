/**
 * @api {get} village/ get All Village
 * @apiVersion 1.0.0
 * @apiName get All Village
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} sales_office_id id provinsi
 * @apiParam {String} sales_group_id id kabupaten
 * @apiParam {String} sales_unit_id id kecamatan
 * @apiParam {String} village_code code desa
 * @apiParam {String} village_name nama desa
 * @apiParam {String} sales_office_name nama Provinsi
 * @apiParam {String} sales_group_name nama Kabupaten
 * @apiParam {String} sales_office_name nama Kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



/**
 * @api {get} village/:id get Village by ID
 * @apiVersion 1.0.0
 * @apiName get Village by ID
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} id id desa
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id desa
 * @apiSuccess {String} data.name nama desa
 * @apiSuccess {String} data.code code desa
 * @apiSuccess {String} data.sales_unit_id code kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {post} village/ Create Village
 * @apiVersion 1.0.0
 * @apiName Create Village
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} name nama desa
 * @apiParam {String} code code desa
 * @apiParam {String} sales_unit_id Kode Kecamatan
 * @apiParam {String} sales_group_id Kode Kabupaten
 * @apiParam {String} sales_office_id Kode Provinsi
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id desa
 * @apiSuccess {String} data.name nama desa
 * @apiSuccess {String} data.code code desa
 * @apiSuccess {String} data.sales_unit_id code kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} village/:id Update Village
 * @apiVersion 1.0.0
 * @apiName Update Village
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} id id desa
 *
 * @apiParam {String} name nama desa
 * @apiParam {String} code code desa
 * @apiParam {String} sales_unit_id Kode Kecamatan
 * @apiParam {String} sales_group_id Kode Kabupaten
 * @apiParam {String} sales_office_id Kode Provinsi
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id desa
 * @apiSuccess {String} data.name nama desa
 * @apiSuccess {String} data.code code desa
 * @apiSuccess {String} data.sales_unit_id code kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {delete} village/:id Delete Village
 * @apiVersion 1.0.0
 * @apiName Delete Village
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} id id desa
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id desa
 * @apiSuccess {String} data.name nama desa
 * @apiSuccess {String} data.code code desa
 * @apiSuccess {String} data.sales_unit_id code kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



/**
 * @api {post} village/import Import Village
 * @apiVersion 1.0.0
 * @apiName Import Village
 * @apiGroup Village
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file type xls,xlsx
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id desa
 * @apiSuccess {String} data.name nama desa
 * @apiSuccess {String} data.code code desa
 * @apiSuccess {String} data.sales_unit_id code kecamatan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
