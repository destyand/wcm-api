/**
 * @api {get} /sap/sales-office Sync Sales Office
 * @apiVersion 1.0.0
 * @apiName Sync Sales Office
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id provinsi
 * @apiSuccess {String} data.name nama provinsi
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-group Sync Sales Group
 * @apiVersion 1.0.0
 * @apiName Sync Sales Group
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id kabupaten
 * @apiSuccess {String} data.name nama kabupaten
 * @apiSuccess {String} data.sales_office_id id provinsi
 * @apiSuccess {String} data.district_code kode 3 digit
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-unit Sync Sales Unit
 * @apiVersion 1.0.0
 * @apiName Sync Sales Unit
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id kecamatan
 * @apiSuccess {String} data.name nama kecamatan
 * @apiSuccess {String} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-division Sync Sales Division
 * @apiVersion 1.0.0
 * @apiName Sync Sales Division
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.desc deskripsi
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/plant-master Sync Plant Master
 * @apiVersion 1.0.0
 * @apiName Sync Plant Master
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {integer} data.id id_plant
 * @apiSuccess {String} data.code code plant
 * @apiSuccess {String} data.name nama plant
 * @apiSuccess {Integer} data.sales_group_id id sales_group_id
 * @apiSuccess {String} data.status (y/n)
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/plant-assign Sync Plant Assign
 * @apiVersion 1.0.0
 * @apiName Sync Plant Assign
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/distrib-channel Sync Distribution Channel
 * @apiVersion 1.0.0
 * @apiName Sync Distribution Channel
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id distrib channel
 * @apiSuccess {String} data.name nama distrib channel
 * @apiSuccess {String} data.desc description
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-area-master Sync Master Sales Area
 * @apiVersion 1.0.0
 * @apiName Sync Master Sales Area
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales area
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.distrib_channel_id id distribution channel
 * @apiSuccess {String} data.sales_division_id id sales division
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-area-assign Sync Assign Sales Area
 * @apiVersion 1.0.0
 * @apiName Sync Assign Sales Area
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id sales office assign
 * @apiSuccess {String} data.sales_area_id id sales area
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-master Sync Master Customer
 * @apiVersion 1.0.0
 * @apiName Sync Master Customer
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-address Sync Address Customer
 * @apiVersion 1.0.0
 * @apiName Sync Address Customer
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-contact Sync Contact Customer
 * @apiVersion 1.0.0
 * @apiName Sync Contact Customer
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-payment Sync Payment Method Customer
 * @apiVersion 1.0.0
 * @apiName Sync Payment Method Customer
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-salesorg Sync Customer Sales Org Assignment
 * @apiVersion 1.0.0
 * @apiName Sync Customer Sales Org Assignment
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-sales-area Sync Sales Area Customer
 * @apiVersion 1.0.0
 * @apiName Sync Sales Area Customer
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/customer-partner-function Sync Customer Partner Function
 * @apiVersion 1.0.0
 * @apiName Sync Customer Partner Function
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/material Sync Material
 * @apiVersion 1.0.0
 * @apiName Sync Material
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/sales-order/:kode_penebusan Create SO
 * @apiVersion 1.0.0
 * @apiName Create SO
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} kode_penebusan kode penebusan
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
/**
 * @api {get} /sap/check-do-from-so Check Delivery Order from SO
 * @apiVersion 1.0.0
 * @apiName Check DO From SO
 * @apiGroup SAP
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {string} so_number the SO number
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.inserted_or_updated count Inserted or updated Record
 * @apiSuccess {Integer} data.deleted Count Deleted Record
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
/**
 * @api {get} /sap/check-status-so/ Check Complete SO
 * @apiVersion 1.0.0
 * @apiName Check Complete
 * @apiGroup SAP
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Array[]} data.so_number array data of so_number
 * @apiSuccess {Integer} data.updated count updated complete order
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
