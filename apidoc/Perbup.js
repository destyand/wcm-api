/**
 * @api {post} /perbup/upload Upload Excel Perbup
 * @apiVersion 1.0.0
 * @apiName Upload Excel Perbup
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {Array[]} message pesan kesalahan
 * @apiError {String} errors baris yang salah pada excel
 * @apiError {Array[]} data yang salah
 */

/**
 * @api {get} /perbup Get Perbup Header
 * @apiVersion 1.0.0
 * @apiName Get Perbup Header
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id perbup header
 * @apiSuccess {String} data.uuid uuid perbup header
 * @apiSuccess {String} data.number nomor perbup
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {Integer} data.year tahun perbup
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /perbup/:uuid Delete Perbup
 * @apiVersion 1.0.0
 * @apiName Delete Perbup
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid perbup
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id perbup header
 * @apiSuccess {String} data.uuid uuid perbup header
 * @apiSuccess {String} data.number nomor perbup
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {Integer} data.year tahun perbup
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /perbup/detail/:uuid Get Perbup Item by UUID (hirarki)
 * @apiVersion 1.0.0
 * @apiName Get Perbup Item by UUID (hirarki)
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid perbup
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Array[]} data.provinsi array provinsi
 * @apiSuccess {String} data.provinsi.id id provinsi
 * @apiSuccess {String} data.provinsi.name nama provinsi
 * @apiSuccess {Array[]} data.provinsi.kabupaten array kabupaten
 * @apiSuccess {String} data.provinsi.kabupaten.id id kabupaten
 * @apiSuccess {String} data.provinsi.kabupaten.name nama kabupaten
 * @apiSuccess {Array[]} data.provinsi.kabupaten.kecamatan array kecamatan
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.id id kecamatan
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.name nama kecamatan
 * @apiSuccess {Array[]} data.provinsi.kabupaten.kecamatan.product array produk
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.product.id id produk
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.product.name nama produk
 * @apiSuccess {Array[]} data.provinsi.kabupaten.kecamatan.product.month jumlah alokasi per bulan
 * @apiSuccess {Integer} data.provinsi.kabupaten.kecamatan.product.total total alokasi dalam 1 tahun sesua produk
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.product.year tahun permentan
 * @apiSuccess {Date} data.provinsi.kabupaten.kecamatan.product.date tanggal upload
 * @apiSuccess {String} data.provinsi.kabupaten.kecamatan.product.status status
 * @apiSuccess {Array[]} data.provinsi.totals array total dari seluruh provinsi berdasarkan produk
 * @apiSuccess {String} data.provinsi.total.id id produk
 * @apiSuccess {String} data.provinsi.total.name nama produk
 * @apiSuccess {String} data.provinsi.total.total total alokasi dalam 1 tahun per produk
 * @apiSuccess {Array[]} data.provinsi.total.month array alokasi per bulan
 * @apiSuccess {String} data.provinsi.perbup_id id perbup
 * @apiSuccess {String} data.provinsi.perbup_no no doc perbup
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /perbup Create Perbup Item
 * @apiVersion 1.0.0
 * @apiName Create Perbup Item
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} contract_gov_id id contract goverment atau id permentan header
 * @apiParam {String} product_id id produk
 * @apiParam {String} sales_office_id id sales office
 * @apiParam {String} sales_group_id id sales group
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {String} month bulan permentan item
 * @apiParam {String} year tahun permentan item
 * @apiParam {String} initial_qty jumlah alokasi
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.contract_gov_id nomor perbup
 * @apiSuccess {String} data.product_id tipe contract
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.month bulan perbup
 * @apiSuccess {String} data.year tahun perbup
 * @apiSuccess {String} data.initial_qty kuantitas
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {Timestamp} data.active_at waktu update data untuk aktif data
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /perbup/item/list/:uuid Get List Perbup Item by UUID
 * @apiVersion 1.0.0
 * @apiName Get List Perbup Item by UUID
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid perbup
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id item perbup
 * @apiSuccess {String} data.uuid uuid item perbup
 * @apiSuccess {Integer} data.contract_gov_id id perbup
 * @apiSuccess {String} data.contract_gov_uuid uuid perbup
 * @apiSuccess {String} data.number nomor perbup
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_org_name nama sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {String} data.district_code kode 3 digit sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.sales_unit_name nama sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {String} data.product_name nama produk / material
 * @apiSuccess {String} data.month bulan perbup
 * @apiSuccess {String} data.year tahun perbup
 * @apiSuccess {String} data.initial_qty kuantitas
 * @apiSuccess {String} data.status status penyimpanan
 * @apiSuccess {String} data.status_name nama status penyimpanan
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /perbup/item/:uuid Get Detail Perbup Item by UUID
 * @apiVersion 1.0.0
 * @apiName Get Detail Perbup Item by UUID
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid item perbup
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id item perbup
 * @apiSuccess {String} data.uuid uuid item perbup
 * @apiSuccess {Integer} data.contract_gov_id id perbup
 * @apiSuccess {String} data.contract_gov_uuid uuid perbup
 * @apiSuccess {String} data.number nomor perbup
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.sales_org_name nama sales org / produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit / kecamatan
 * @apiSuccess {String} data.sales_unit_name nama sales unit / kecamatan
 * @apiSuccess {String} data.product_id id produk / material
 * @apiSuccess {String} data.product_name nama produk / material
 * @apiSuccess {String} data.month bulan perbup
 * @apiSuccess {String} data.year tahun perbup
 * @apiSuccess {String} data.initial_qty kuantitas
 * @apiSuccess {String} data.status status penyimpanan
 * @apiSuccess {String} data.status_name nama status penyimpanan
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /perbup/item/:uuid Update Item Perbup
 * @apiVersion 1.0.0
 * @apiName Update Item Perbup
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} sales_office_id id sales office atau provinsi
 * @apiParam {String} sales_group_id id sales group atau kabupaten
 * @apiParam {String} sales_unit_id id sales unit atau kecamatan
 * @apiParam {String} product_id id product
 * @apiParam {String} initial_qty jumlah alokasi
 * @apiParam {String} month bulan permentan item
 * @apiParam {String} year tahun permentan item
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.uuid id perbup item
 * @apiSuccess {String} data.contract_gov_id id perbup header
 * @apiSuccess {String} data.sales_office_id id sales office atau provinsi
 * @apiSuccess {String} data.sales_group_id id sales group atau kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit atau kecamatan
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /perbup/update/bulk Update Multiple Status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Multiple Status
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request-Example (data):
 *     [
	{
		"id":"65FBF6F9-939F-4C45-81DC-A3B7AE0A8A20",
		"status":"n"
	},
		{
		"id":"6A0F448E-BFE5-4D53-AAF0-372825824229",
		"status":"n"
	}
]
 * @apiParam {String} data.status status penyimpanan (y: active, n:inactive)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.uuid uuid perbup item
 * @apiSuccess {String} data.contract_gov_id id contract goverment atau perbup header
 * @apiSuccess {String} data.product_id id product
 * @apiSuccess {String} data.sales_office_id id sales office atau provinsi
 * @apiSuccess {String} data.sales_group_id id sales group atau kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit atau kecamatan
 * @apiSuccess {String} data.month bulan perbup item
 * @apiSuccess {String} data.year tahun perbup item
 * @apiSuccess {String} data.initial_qty jumlah alokasi
 * @apiSuccess {Date} data.active_date tanggal aktif data
 * @apiSuccess {Date} data.inactive_date tanggal deaktif data
 * @apiSuccess {String} data.status status penyimpanan (y: active, n:inactive)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /perbup/download export perbup
 * @apiVersion 1.0.0
 * @apiName export Perbup
 * @apiGroup Perbup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
