/**
 * @api {post} /permentan/upload Upload Excel Permentan
 * @apiVersion 1.0.0
 * @apiName Upload Excel Permentan
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /permentan Get Permentan Header
 * @apiVersion 1.0.0
 * @apiName Get Permentan Header
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} number nomor doc permentan
 * @apiParam {String} produsen produsen
 * @apiParam {String} year tahun permentan
 * @apiParam {String} date tanggal upload
 * @apiParam {String} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id contract goverment
 * @apiSuccess {String} data.uuid uuid contract goverment
 * @apiSuccess {String} data.number nomor permentan
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.year tahun permentan
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /permentan/item/list/:uuid Get List Item Permentan
 * @apiVersion 1.0.0
 * @apiName Get List Item Permentan
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} sales_name nama provinsi
 * @apiParam {String} product_name nama produk
 * @apiParam {String} qty jumlah alokasi
 * @apiParam {String} month bulan
 * @apiParam {String} year tahun
 * @apiParam {String} datetime tanggal dibuat
 * @apiParam {String} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.header_id id contract goverment
 * @apiSuccess {Integer} data.item_id id contract goverment Item
 * @apiSuccess {String} data.uuid uuid contract goverment
 * @apiSuccess {String} data.contract_type tipe contract
 * @apiSuccess {Interger} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {Integer} data.month bulan permentan
 * @apiSuccess {Integer} data.year tahun permentan
 * @apiSuccess {Interger} data.product_id id produk
 * @apiSuccess {String} data.product_name nama product
 * @apiSuccess {String} data.initial_qty jumlah alokasi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /permentan/item/:uuid Get single Item Permentan
 * @apiVersion 1.0.0
 * @apiName Get Single Item Permentan
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} sales_name nama provinsi
 * @apiParam {String} product_name nama produk
 * @apiParam {String} qty jumlah alokasi
 * @apiParam {String} month bulan
 * @apiParam {String} year tahun
 * @apiParam {String} datetime tanggal dibuat
 * @apiParam {String} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.header_id id contract goverment
 * @apiSuccess {Integer} data.item_id id contract goverment Item
 * @apiSuccess {String} data.uuid uuid contract goverment
 * @apiSuccess {String} data.contract_type tipe contract
 * @apiSuccess {Interger} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {Integer} data.month bulan permentan
 * @apiSuccess {Integer} data.year tahun permentan
 * @apiSuccess {Interger} data.product_id id produk
 * @apiSuccess {String} data.product_name nama product
 * @apiSuccess {String} data.initial_qty jumlah alokasi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {post} /permentan Create Permentan Item
 * @apiVersion 1.0.0
 * @apiName Create Permentan Item
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} contract_gov_id id contract goverment atau id permentan header
 * @apiParam {String} product_id id produk
 * @apiParam {String} sales_office_id id sales office
 * @apiParam {String} month bulan permentan item
 * @apiParam {String} year tahun permentan item
 * @apiParam {String} initial_qty jumlah alokasi
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.contract_gov_id nomor permentan
 * @apiSuccess {String} data.product_id tipe contract
 * @apiSuccess {String} data.sales_office_id id sales org
 * @apiSuccess {String} data.month tahun permentan
 * @apiSuccess {String} data.year sementara null
 * @apiSuccess {String} data.initial_qty sementara null
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {Timestamp} data.active_at waktu update data untuk aktif data
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /permentan/item/:uuid Update Permentan
 * @apiVersion 1.0.0
 * @apiName Update Item Permentan
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} contract_gov_id id contract_gov_id contract goverment
 * @apiParam {String} sales_office_id id sales office atau provinsi
 * @apiParam {String} product_id id product
 * @apiParam {String} initial_qty jumlah alokasi
 * @apiParam {String} month bulan permentan item
 * @apiParam {String} year tahun permentan item
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.uuid id permentan item
 * @apiSuccess {String} data.contract_gov_id id permentan header
 * @apiSuccess {String} data.sales_office_id id sales office atau provinsi
 * @apiSuccess {String} data.sales_group_id id sales group atau kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit atau kecamatan
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /permentan/update/bulk Update Multiple Status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update - multiple status
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request-Example (data):
 *     [
	{
		"id":"65FBF6F9-939F-4C45-81DC-A3B7AE0A8A20",
		"status":"n"
	},
		{
		"id":"6A0F448E-BFE5-4D53-AAF0-372825824229",
		"status":"n"
	}
]
 * @apiParam {String} data.status status penyimpanan (y: active, n:inactive)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.uuid uuid permentan item
 * @apiSuccess {String} data.contract_gov_id id contract goverment atau permentan header
 * @apiSuccess {String} data.product_id id product
 * @apiSuccess {String} data.sales_office_id id sales office atau provinsi
 * @apiSuccess {String} data.sales_group_id id sales group atau kabupaten
 * @apiSuccess {String} data.sales_unit_id id sales unit atau kecamatan
 * @apiSuccess {String} data.month bulan permentan item
 * @apiSuccess {String} data.year tahun permentan item
 * @apiSuccess {String} data.initial_qty jumlah alokasi
 * @apiSuccess {Date} data.active_date tanggal aktif data
 * @apiSuccess {Date} data.inactive_date tanggal deaktif data
 * @apiSuccess {String} data.status status penyimpanan (y: active, n:inactive)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data

 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /permentan/detail/:uuid Get Permentan Item by UUID
 * @apiVersion 1.0.0
 * @apiName Get Permentan Item by uuid
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Array[]} data.provinsi array provinsi
 * @apiSuccess {String} data.provinsi.id id provinsi
 * @apiSuccess {String} data.provinsi.name nama provinsi
 * @apiSuccess {String} data.provinsi.product array product
 * @apiSuccess {String} data.provinsi.product.id id produk
 * @apiSuccess {String} data.provinsi.product.name nama produk
 * @apiSuccess {string} data.provinsi.product.month jumlah alokasi per bulan
 * @apiSuccess {Interger} data.provinsi.product.total total alokasi dalam 1 tahun sesua produk
 * @apiSuccess {String} data.provinsi.product.year tahun permentan
 * @apiSuccess {Date} data.provinsi.product.date tanggal upload
 * @apiSuccess {String} data.provinsi.product.status status
 * @apiSuccess {Array[]} data.totals array total dari seluruh provinsi berdasarkan produk
 * @apiSuccess {String} data.total.id id produk
 * @apiSuccess {String} data.total.name nama produk
 * @apiSuccess {String} data.total.total total alokasi dalam 1 tahun per produk
 * @apiSuccess {Array[]} data.total.month array alokasi per bulan
 * @apiSuccess {String} data.total.month.1 alokasi bulan ke berapa
 * @apiSuccess {String} data.permentan_id id permentan
 * @apiSuccess {String} data.permentan_no no doc permentan
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {delete} /permentan/:uuid delete permentan Hearder
 * @apiVersion 1.0.0
 * @apiName Delete Permentan Header
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.header_id id contract goverment
 * @apiSuccess {Integer} data.item_id id contract goverment Item
 * @apiSuccess {String} data.uuid uuid contract goverment
 * @apiSuccess {String} data.contract_type type contract goverment (default permentan)
 * @apiSuccess {String} data.number Nomor doc permentan
 * @apiSuccess {Interger} data.sales_org_id id sales org
 * @apiSuccess {Integer} data.year tahun permentan
 * @apiSuccess {Date} data.from_date tanggal mulai valid data
 * @apiSuccess {Date} data.thru_date tanggal akhir valid data
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /permentan/export/ export permentan items
 * @apiVersion 1.0.0
 * @apiName export Permentan Items
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /permentan/download Download Datatable 
 * @apiVersion 1.0.0
 * @apiName export Permentan Items
 * @apiGroup Permentan
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
