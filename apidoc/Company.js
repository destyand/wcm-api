/**
 * @api {get} /company/distributor Get Distributor
 * @apiVersion 1.0.0
 * @apiName Get Distributor
 * @apiGroup Company
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id distributor
 * @apiParam {String} name nama distributor
 * @apiParam {String} uuid uuid distributor
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {String} type type ('order') untuk pembeli / distributor permintaan penebusan
 * @apiParam {Integer} sales_unit_id   sales_unit_id
 * @apiParam {String} sales_unit_name   sales_unit_name
 * @apiParam {Integer} sales_group_id   sales_group_id
 * @apiParam {String} sales_group_name   sales_group_name
 * @apiParam {Integer} sales_office_id   sales_office_id
 * @apiParam {String} sales_office_name   sales_office_name
 * @apiParam {String} type   type=order Untuk Modal Form Menu Order
 * @apiParam {String} retail  retail=retail Untuk Modal Form Menu Petani
 * @apiParam {String} type  type=relasi Untuk Modal Form Relasi Pengecer Distributor
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id distributor
 * @apiSuccess {String} data.full_name nama distributor
 * @apiSuccess {String} data.owner nama pemilik
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.address alamat distributor
 * @apiSuccess {String} data.unit_id id unit (id kecematan)
 * @apiSuccess {String} data.unit_name nama unit (nama kecematan)
 * @apiSuccess {String} data.group_id id kabupaten (id kabupaten)
 * @apiSuccess {String} data.group_name nama unit (nama kabupaten)
 * @apiSuccess {String} data.office_id id unit (id provinsi)
 * @apiSuccess {String} data.office_name nama unit (nama provinsi)
 * @apiSuccess {String} data.tlp_no no telephone
 * @apiSuccess {String} data.fax_no no fax
 * @apiSuccess {Integer} data.sales_org_id Sales Org id
 * @apiSuccess {String} data.sales_org_name Sales Org Name
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /company/produsen Get Produsen
 * @apiVersion 1.0.0
 * @apiName Get Produsen
 * @apiGroup Company
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id produsen
 * @apiParam {String} name nama produsen
 * @apiParam {String} uuid uuid produsen
 * @apiParam {String} customer_id id distributor
 * @apiParam {String} sales_org_id sales_org_id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id produsen
 * @apiSuccess {String} data.uuid uuid produsen
 * @apiSuccess {String} data.name nama produsen
 * @apiSuccess {String} data.status status
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {String} data.customer_id id distributor
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /company/pengecer Get Pengecer
 * @apiVersion 1.0.0
 * @apiName Get Pengecer
 * @apiGroup Company
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id pengecer
 * @apiParam {String} name nama pengecer
 * @apiParam {String} uuid uuid pengecer
 * @apiParam {String} customer_id id distributor
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pengecer
 * @apiSuccess {String} data.uuid uuid pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.status status
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {String} data.customer_id id distributor
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */