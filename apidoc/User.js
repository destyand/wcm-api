/**
 * @api {post} /user Create User
 * @apiVersion 1.0.0
 * @apiName Create User
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama user
 * @apiParam {String} username username user
 * @apiParam {String} password password user
 * @apiParam {String} email email user
 * @apiParam {Integer} role_id id role
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {Integer} data.role_id id role
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {post} /userdistributor Create User Distributor
 * @apiVersion 1.0.0
 * @apiName Create User Distributor
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama user
 * @apiParam {String} username username user
 * @apiParam {String} password password user
 * @apiParam {String} email email user
 * @apiParam {Integer} customer_id Distributor ID yang Diikuti
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {Integer} data.customer_id Distributor ID yang Diikuti
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /user Get All User
 * @apiVersion 1.0.0
 * @apiName Get All User
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama user
 * @apiParam {String} username username user
 * @apiParam {String} email email user
 * @apiParam {Date} created_at waktu create data (dd-mm-yyyy), multiple date gunakan separator ";"
 * @apiParam {Date} updated_at waktu update data (dd-mm-yyyy), multiple date gunakan separator ";"
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /user/id/:id Get User By ID
 * @apiVersion 1.0.0
 * @apiName Get User By ID
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id user
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /user/:id Update User
 * @apiVersion 1.0.0
 * @apiName Update User
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id user
 * @apiParam {String} name nama user
 * @apiParam {String} username username user
 * @apiParam {String} email email user
 * @apiParam {Integer} role_id id role
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /user/password/:id Edit Password
 * @apiVersion 1.0.0
 * @apiName Edit Password
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id user
 * @apiParam {String} password_old password lama
 * @apiParam {String} password_new password baru
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {String} data.id id user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /user/:id Delete User
 * @apiVersion 1.0.0
 * @apiName Delete User
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id user
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id user
 * @apiSuccess {String} data.name nama user
 * @apiSuccess {String} data.username username user
 * @apiSuccess {String} data.email email user
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
  * @api {get} /user/profile get User Profile
  * @apiVersion 1.0.0
  * @apiName  User profile
  * @apiGroup User
  * 
  * @apiSuccessExample Response success:
  * HTTP/1.1 200 OK
  * {
  *    "status": 1,
  *	    "status_txt": "success",
  *	    "message": "Get data successfully.",
  *	    "data": {
  *	        "id": "58450DD2-AADA-4297-8185-F290C583E404",
  *	        "username": "admin",
  *	        "email": "admin@tes.tes",
  *	        "last_sign_in": "2019-03-28 14:49:41",
  *	        "sales_org_id": "Id Of Sales Org",
  *         "salaes_org_name": "Name Of Sales Org"
  *	    }
  *	}
  *	
  * @apiErrorExample {json} Error-response :
  * HTTP/1.1 5xx,4xx
  * {
  *    "status": 0,
  *    "status_txt": "errors",
  *    "message": "Message Of Error"
  * }
  */
 