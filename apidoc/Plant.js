/**
 * @api {get} /plant Get Gudang Plant
 * @apiVersion 1.0.0
 * @apiName Get Gudang Plant
 * @apiGroup Gudang Plant
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id plant
 * @apiParam {code} uuid code plant
 * @apiParam {String} name nama plant
 * @apiParam {String} kabupaten id kabupaten
 * @apiParam {Integer} sales_group_id
 * @apiParam {String} status (y/n)
 * @apiParam {Integer} plant_assg_id id plant_assg_id
 * @apiParam {string} plant_assg_uuid id plant_assg_uuid
 * @apiParam {Integer} sales_org_id id sales_org_id
 * @apiParam {Integer} distrib_channel_id id distrib_channel_id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {integer} data.id id_plant
 * @apiSuccess {code} data.uuid code plant
 * @apiSuccess {String} data.name nama plant
 * @apiSuccess {String} data.kabupaten id kabupaten
 * @apiSuccess {Integer} data.sales_group_id id sales_group_id
 * @apiSuccess {String} data.status (y/n)
 * @apiSuccess {Integer} data.plant_assg_id id plant_assg_id
 * @apiSuccess {String} data.plant_assg_uuid id plant_assg_uuid
 * @apiSuccess {Integer} data.sales_org_id id sales_org_id
 * @apiSuccess {Integer} data.distrib_channel_id id distrib_channel_id
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /plant/distinct Get Distinct Gudang Plant
 * @apiVersion 1.0.0
 * @apiName Get Distinct Gudang Plant
 * @apiGroup Gudang Plant
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id plant
 * @apiParam {code} uuid code plant
 * @apiParam {String} name nama plant
 * @apiParam {String} kabupaten id kabupaten
 * @apiParam {Integer} sales_group_id
 * @apiParam {String} status (y/n)
 * @apiParam {Integer} sales_org_id id sales_org_id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {integer} data.id id_plant
 * @apiSuccess {code} data.uuid code plant
 * @apiSuccess {String} data.name nama plant
 * @apiSuccess {String} data.kabupaten id kabupaten
 * @apiSuccess {Integer} data.sales_group_id id sales_group_id
 * @apiSuccess {String} data.status (y/n)
 * @apiSuccess {Integer} data.sales_org_id id sales_org_id
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /plant/supply Get Gudang Plant & Supply Point
 * @apiVersion 1.0.0
 * @apiName Get Get Gudang Plant & Supply Point
 * @apiGroup Gudang Plant
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id id plant
 * @apiParam {String} uuid uuid plant
 * @apiParam {String} code kode plant
 * @apiParam {String} name nama plant
 * @apiParam {Integer} sales_group_id id sales group
 * @apiParam {Integer} sales_org_id id sales org
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {integer} data.id id_plant
 * @apiSuccess {String} data.uuid uuid plant
 * @apiSuccess {String} data.code code plant
 * @apiSuccess {String} data.name nama plant
 * @apiSuccess {Integer} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.sales_org_id id sales org
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
