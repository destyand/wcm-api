/**
 * @api {get} /f5/create Create F5 Job
 * @apiVersion 1.0.0
 * @apiName Create job F5
 * @apiGroup JOB F5
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id Report F5
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * 
 */
