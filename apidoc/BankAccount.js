/**
 * @api {get} /bankaccounts/ Get All Bank Accounts
 * 
 * @apiVersion 1.0.0
 * @apiName Get All BankAccounts
 * @apiGroup BankAccounts
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * 
 * @apiParam {String} sales_org_id  Param to filter Kode Produsen
 * @apiParam {String} bank_name  Param to filter Bank Name
 * @apiParam {String} bank_account  Param to filter Bank Account
 * @apiParam {String} bank_gl_account Param to filter Bank GL Account
 * @apiParam {String} bank_clearing_acct Param to filter Bank Clearing acct
 * 
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 * 		"draw": 0,
 *		"recordsTotal": 1,
 *		"recordsFiltered": 1,
 *		"data": [
 *		    {
 *		        "id": "1",
 *		        "sales_org_id": "A000",
 *		        "bank_name": "BCA",
 *		        "bank_account": "AIi",
 *		        "bank_gl_account": "asas",
 *		        "bank_clearing_acct": "asas",
 *		        "status": "Active"
 *		    }
 *		],
 * }
 * 
 * 
 */

/**
 * @api {get} /bankaccounts/:id Get BankAccounts By Id
 * 
 * @apiVersion 1.0.0
 * @apiName Get BankAccounts By Id
 * @apiGroup BankAccounts
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} bank_id  Bank ID
 * 
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "status": 1,
 *    "status_txt": "success",
 *    "message": "Get data successfully.",
 *    "data": {
 *      "id": "id",
 *      "sales_org_id": "Produsen Kode",
 *      "bank_id": "Bank ID",
 *      "currency": "Currency",
 *      "bank_account": "Bank Account",
 *      "bank_gl_account": "Bank GL Account",
 *      "bank_clearing_acct": "Bank Clearing Account",
 *      "status": "Status in active or inactive",
 *      "created_at": "datetime created record",
 *      "updated_at": "datetime Updated record",
 *      "bank_name" : "Bank Name"
 *      "sales_org_name": "Produsen Name"
 *    },
 * }
 * 
 * 
 * @apiErrorExample {json} Error-response
 * HTTP/1.1 5xx,4xx
 *{
 *    "status": 0,
 *    "status_txt": "errors",
 *    "message": "Message Of Error"
 *}
 * 
 */

 /**
  * @api {post} /bankaccounts/ Create Bank Account
  * @apiVersion 1.0.0
  *	@apiName Create BankAccounts
  * @apiGroup BankAccounts
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * 
  * @apiParam {String{..4}} sales_org_id Sales Org ID
  * @apiParam {String} bank_id  Bank ID
  * @apiParam {String} currency currency
  * @apiParam {String} bank_account Bank Account
  * @apiparam {String} bank_gl_account Bank GL Account
  * @apiParam {String} bank_clearing_acct  bank Clearing Account
  * 
  * @apiSuccessExample {json} Success-response:
  * HTTP/1.1 200 OK
  *{
  *    "status": 1,
  *    "status_txt": "success",
  *    "message": "Data has been successfully added.",
  *    "data": {
  *        "sales_org_id": "b000",
  *        "bank_id": "B002",
  *        "currency": "IDR",
  *        "bank_account": "AIi",
  *        "bank_gl_account": "asas",
  *        "bank_clearing_acct": "asas",
  *        "status": "Y",
  *        "updated_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "created_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "updated_at": "2019-03-28 10:23:53",
  *        "created_at": "2019-03-28 10:23:53",
  *        "id": 2
  *    }
  *}
  * 
  * @apiErrorExample {json} Error-response
  * HTTP/1.1 5xx,4xx
  *{
  *    "status": 0,
  *    "status_txt": "errors",
  *    "message": "Message Of Error"
  *}
  * 
  */
  
  /**
  * @api {put} /bankaccounts/:id Updated Bank Account
  * @apiVersion 1.0.0
  *	@apiName Updated BankAccounts
  * @apiGroup BankAccounts
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * 
  * @apiParam {String{..4}} sales_org_id Sales Org ID
  * @apiParam {String} bank_id  Bank ID
  * @apiParam {String} currency currency
  * @apiParam {String} bank_account Bank Account
  * @apiparam {String} bank_gl_account Bank GL Account
  * @apiParam {String} bank_clearing_acct  bank Clearing Account
  * 
  * @apiSuccessExample {json} Success-response:
  * HTTP/1.1 200 OK
  *{
  *    "status": 1,
  *    "status_txt": "success",
  *    "message": "Data has been successfully updated.",
  *    "data": {
  *        "sales_org_id": "b000",
  *        "bank_id": "B002",
  *        "currency": "IDR",
  *        "bank_account": "AIi",
  *        "bank_gl_account": "asas",
  *        "bank_clearing_acct": "asas",
  *        "status": "Y",
  *        "updated_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "created_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "updated_at": "2019-03-28 10:23:53",
  *        "created_at": "2019-03-28 10:23:53",
  *        "id": 2
  *    }
  *}
  * 
  * @apiErrorExample {json} Error-response
  * HTTP/1.1 5xx,4xx
  *{
  *    "status": 0,
  *    "status_txt": "errors",
  *    "message": "Message Of Error"
  *}
  * 
  */

  /**
  * @api {put} /bankaccounts/ multiple Update status Bank Account
  * @apiVersion 1.0.0
  *	@apiName ultiple Update status BankAccounts
  * @apiGroup BankAccounts
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * 
  * @apiParam {Array} ids the id
  * @apiParam {String} status the status of update
  * 
  * @apiSuccessExample {json} Success-response:
  * HTTP/1.1 200 OK
  *{
  *    "status": 1,
  *    "status_txt": "success",
  *    "message": "Data has been successfully updated.",
  *    "data": [{
  *        "sales_org_id": "b000",
  *        "bank_id": "B002",
  *        "currency": "IDR",
  *        "bank_account": "AIi",
  *        "bank_gl_account": "asas",
  *        "bank_clearing_acct": "asas",
  *        "status": "Y",
  *        "updated_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "created_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "updated_at": "2019-03-28 10:23:53",
  *        "created_at": "2019-03-28 10:23:53",
  *        "id": 2
  *    },]
  *}
  * 
  * @apiErrorExample {json} Error-response
  * HTTP/1.1 5xx,4xx
  *{
  *    "status": 0,
  *    "status_txt": "errors",
  *    "message": "Message Of Error"
  *}
  * 
  */

  /**
  * @api {delete} /bankaccounts/ Delete Bank Account
  * @apiVersion 1.0.0
  *	@apiName delete BankAccounts
  * @apiGroup BankAccounts
  * @apiHeader {String} Authorization token (Bearer)
  * @apiHeader {String} Language bahasa (id = indonesia; en = english)
  * 
  * @apiParam {String} id the id if multiple delete please delimiter by comma
  * 
  * @apiSuccessExample {json} Success-response:
  * HTTP/1.1 200 OK
  *{
  *    "status": 1,
  *    "status_txt": "success",
  *    "message": "Data has been successfully deleted.",
  *    "data": [{
  *        "sales_org_id": "b000",
  *        "bank_id": "B002",
  *        "currency": "IDR",
  *        "bank_account": "AIi",
  *        "bank_gl_account": "asas",
  *        "bank_clearing_acct": "asas",
  *        "status": "Y",
  *        "updated_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "created_by": "58450DD2-AADA-4297-8185-F290C583E404",
  *        "updated_at": "2019-03-28 10:23:53",
  *        "created_at": "2019-03-28 10:23:53",
  *        "id": 2
  *    },]
  *}
  * 
  * @apiErrorExample {json} Error-response
  * HTTP/1.1 5xx,4xx
  *{
  *    "status": 0,
  *    "status_txt": "errors",
  *    "message": "Message Of Error"
  *}
  * 
  */
 
 