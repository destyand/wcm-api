/**
 * @api {post} /pergub/upload Upload Excel Pergup
 * @apiVersion 1.0.0
 * @apiName Upload Excel Pergup
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



 /**
 * @api {post} /pergub Tambah Data Baru
 * @apiVersion 1.0.0
 * @apiName Tambah Data Baru
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} contract_gov_id  id header pergub
 * @apiParam {Integer} sales_office_id  provisi id
 * @apiParam {Integer} sales_group_id  kabupaten id
 * @apiParam {String} product_id  product id
 * @apiParam {Integer} initial_qty  jumlah product
 * @apiParam {Integer} month  Bulan
 * @apiParam {Integer} year  tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {get} /pergub/id/:uuid Get Item Hirarki master Detail Table
 * @apiVersion 1.0.0
 * @apiName Get Item master Detail Table
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

  /**
 * @api {get} /pergub/item/id/:uuid Get Detail Data Item Pergub
 * @apiVersion 1.0.0
 * @apiName Get Detail Data Item Pergub
 * @apiGroup Pergup
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiSuccess {Integer} id   Item
 * @apiSuccess {String} uuid  uuid Item
 * @apiSuccess {Integer} contract_gov_id  id pergub
 * @apiSuccess {String} pergubno   detail pergub
 * @apiSuccess {Integer} product_id   id produk
 * @apiSuccess {String} product_name   nama produk
 * @apiSuccess {Integer} sales_office_id    id provinsi
 * @apiSuccess {String} provinsi   nama provinsi
 * @apiSuccess {Integer} sales_group_id id kabupaten
 * @apiSuccess {String} kabupaten   nama kabupaten
 * @apiSuccess {Integer} month  bulan
 * @apiSuccess {Integer} initial_qty  jumlah qty
 * @apiSuccess {Integer} year  Tahun
 * @apiSuccess {String} Status  y/n
 * @apiSuccess {String} Status_name Active
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

   /**
 * @api {put} /pergub/uuid Update Item
 * @apiVersion 1.0.0
 * @apiName Update Item
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} contract_gov_id  id header pergub
 * @apiParam {Integer} sales_office_id  provisi id
 * @apiParam {Integer} sales_group_id  kabupaten id
 * @apiParam {Integer} product_id  product id
 * @apiParam {Integer} initial_qty  jumlah product
 * @apiParam {Integer} month  Bulan
 * @apiParam {Integer} year  tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id   Item
 * @apiSuccess {String} data.uuid  uuid Item
 * @apiSuccess {Integer} data.contract_gov_id  id pergub
 * @apiSuccess {String} data.pergubno   detail pergub
 * @apiSuccess {Integer} data.product_id   id produk
 * @apiSuccess {String} data.product_name   nama produk
 * @apiSuccess {Integer} data.sales_office_id    id provinsi
 * @apiSuccess {String} data.provinsi   nama provinsi
 * @apiSuccess {Integer} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.kabupaten   nama kabupaten
 * @apiSuccess {Integer} data.month  bulan
 * @apiSuccess {Integer} data.initial_qty  jumlah qty
 * @apiSuccess {Integer} data.year  Tahun
 * @apiSuccess {String} data.Status  y/n
 * @apiSuccess {String} data.Status_name Active
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


  /**
 * @api {get} /pergub Get Master Table Header
 * @apiVersion 1.0.0
 * @apiName Get Master Table Header
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} contract_gov_id  id header pergub
 * @apiParam {Integer} sales_office_id  provisi id
 * @apiParam {Integer} sales_group_id  kabupaten id
 * @apiParam {Integer} product_id  product id
 * @apiParam {Integer} initial_qty  jumlah product
 * @apiParam {Integer} month  Bulan
 * @apiParam {Integer} year  tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */



/**
 * @api {post} /pergub/update/bulk Update relation status - multiple (raw)
 * @apiVersion 1.0.0
 * @apiName Update status data multiple
 * @apiGroup Pergup
 *
 * @apiParamExample {json} Request-Example (data):
 * [ { "id":"7B822CC3-E5D8-33A7-88BD-C512F5CFB351", "status":"d" } ]
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.uuid  id item detail
 * @apiSuccess {Integer} data.contract_gov_id  id contract gov
 * @apiSuccess {Integer} data.product_id  product id
 * @apiSuccess {Integer} data.sales_office_id  provinsi id
 * @apiSuccess {Integer} data.sales_group_id  kabupaten id
 * @apiSuccess {Integer} data.sales_unit_id
 * @apiSuccess {Integer} data.month  bulan
 * @apiSuccess {Integer} data.year  tahun
 * @apiSuccess {Integer} data.initial_qty  jml qty
 * @apiSuccess {Timetamp} data.active_date
 * @apiSuccess {Timetamp} data.inactive_date
 * @apiSuccess {String} data.status  status
 * @apiSuccess {Timetamp} data.created_by  created_by
 * @apiSuccess {Timetamp} data.updated_by  updated_by
 * @apiSuccess {Timetamp} data.created_at  created_at
 * @apiSuccess {Timetamp} data.updated_at  updated_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */


  /**
 * @api {get} /pergub/detail/id/{uuid} Get Master Table Detail Item BY UUID Goverment
 * @apiVersion 1.0.0
 * @apiName Get Master Table Detail Item BY UUID Goverment
 * @apiGroup Pergup
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id id detail Item
 * @apiParam {Integer} uuid Uuid Detail Item
 * @apiParam {Integer} contract_gov_id contract gov id
 * @apiParam {Integer} pergubno Nama Pergub
 * @apiParam {Integer} sales_office_id provinsi id
 * @apiParam {Integer} provinsi nama provinsi
 * @apiParam {Integer} sales_group_id id kabupaten
 * @apiParam {Integer} kabupaten nama kabupaten
 * @apiParam {Integer} product_id id produk
 * @apiParam {Integer} product_name Nama Produk
 * @apiParam {Integer} month Bulan
 * @apiParam {Integer} initial_qty Qty
 * @apiParam {Integer} year Tahun
 * @apiParam {Integer} status Status
 * @apiParam {Timestamp} created_at Tanggal Buat
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id  id item detail
 * @apiSuccess {String} data.uuid  uuid Item Detail
 * @apiSuccess {Integer} data.contract_gov_id  id contract gov
 * @apiSuccess {String} data.pergubno  Nomer Pergub
 * @apiSuccess {String} data.product_id  Produk ID
 * @apiSuccess {String} data.product_name Produk Name
 * @apiSuccess {Integer} data.sales_office_id  Provinsi ID
 * @apiSuccess {String} data.provinsi Nama Provinsi
 * @apiSuccess {Integer} data.sales_group_id kabupaten id
 * @apiSuccess {String} data.kabupaten Kabupaten Name
 * @apiSuccess {Integer} data.month  Bulan
 * @apiSuccess {Integer} data.initial_qty Jumlah (Qty)
 * @apiSuccess {Integer} data.year  Tahun
 * @apiSuccess {String} data.status  Status
 * @apiSuccess {String} data.status_name  StatusName
 * @apiSuccess {Timestamp} data.created_at  ": "21-01-2019"
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /pergub/download export Pergub
 * @apiVersion 1.0.0
 * @apiName export Pergub
 * @apiGroup Pergub
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} uuid The UUID
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
