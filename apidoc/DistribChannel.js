/**
 * @api {get} /distribchannel Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Distrib Channel
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id distrib channel
 * @apiSuccess {String} data.uuid uuid distrib channel
 * @apiSuccess {String} data.name nama distrib channel
 * @apiSuccess {String} data.desc description
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /distribchannel/id/:uuid Get Distrib Channel By ID
 * @apiVersion 1.0.0
 * @apiName Get Distrib Channel By ID
 * @apiGroup Distrib Channel
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid distrib channel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id distrib channel
 * @apiSuccess {String} data.uuid uuid distrib channel
 * @apiSuccess {String} data.name nama distrib channel
 * @apiSuccess {String} data.desc description
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
