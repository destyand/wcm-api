/**
 * @api {post} /farmer Create Farmer
 * @apiVersion 1.0.0
 * @apiName Create Farmer
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} farmer_group_id id kelompok petani
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} hp_no nomor handphone
 * @apiParam {String} village desa
 * @apiParam {Integer} land_area luas area
 * @apiParam {Numeric} ktp_no nomor ktp
 * @apiParam {Date} ktp_valid_date tanggal valid ktp
 * @apiParam {String} sub_sector_id id sub sector (separator semicolon [;] )
 * @apiParam {String} commodity_id id commodity (separator semicolon [;] )
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /farmer Get All Farmer
 * @apiVersion 1.0.0
 * @apiName Get All Farmer
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.farmer_group_code kode kelompok petani
 * @apiSuccess {String} data.farmer_group_name nama kelompok petani
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_code kode pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /farmer/id/:uuid Get Farmer By ID
 * @apiVersion 1.0.0
 * @apiName Get Farmer By ID
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} uuid uuid farmer
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.farmer_group_code kode kelompok petani
 * @apiSuccess {String} data.farmer_group_name nama kelompok petani
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_code kode pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /farmer/:uuid Update Farmer
 * @apiVersion 1.0.0
 * @apiName Update Farmer
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid farmer
 * @apiParam {Integer} farmer_group_id id kelompok petani
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} hp_no nomor handphone
 * @apiParam {String} village desa
 * @apiParam {Integer} land_area luas area
 * @apiParam {Numeric} ktp_no nomor ktp
 * @apiParam {Date} ktp_valid_date tanggal valid ktp
 * @apiParam {String} sub_sector_id id sub sector (separator semicolon [;] )
 * @apiParam {String} commodity_id id commodity (separator semicolon [;] )
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /farmer/:uuid Delete Farmer
 * @apiVersion 1.0.0
 * @apiName Delete Farmer
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid farmer
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmer/upload Upload Excel Farmer
 * @apiVersion 1.0.0
 * @apiName Upload Excel Farmer
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama petani
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Array[]} data.sub_sector sub sector
 * @apiSuccess {Integer} data.sub_sector.id id sub sector
 * @apiSuccess {String} data.sub_sector.name nama sub sector
 * @apiSuccess {Array[]} data.commodity commodity
 * @apiSuccess {Integer} data.commodity.id id commodity
 * @apiSuccess {String} data.commodity.name nama commodity
 * @apiSuccess {Array[]} data.validate validate
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmer/update/bulk update multiple (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Multiple Data
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "uuid":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "uuid":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /farmer/cust_id/{uuid} Get All Farmer By Distributor UUID
 * @apiVersion 1.0.0
 * @apiName Get All Farmer By Distributor UUID
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.farmer_group_name nama kelompok petani
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmer/json/bulk Save Data Json (data raw)
 * @apiVersion 1.0.0
 * @apiName Save Data Json
 * @apiGroup Farmer
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "farmer_group_code":"A001", "name":"Sucahyo", "address":"jalan jalan", "sales_unit_id":"320509", "sub_sector_id":"1;2;3;4;", "commodity_id":"1;2;3;4;", "village":"Ds. Waw", "telp_no":"031xxx", "hp_no":"031xxx", "ktp_no":"352504xxxx", "ktp_valid_date":"2019-01-01", "latitude":"", "longitude":"", "status":"d" } ]
 *
 * @apiParam {Integer} farmer_group_code kode kelompok petani
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} hp_no nomor handphone
 * @apiParam {String} village desa
 * @apiParam {Integer} land_area luas area
 * @apiParam {Numeric} ktp_no nomor ktp
 * @apiParam {Date} ktp_valid_date tanggal valid ktp
 * @apiParam {String} sub_sector_id id sub sector (separator semicolon [;] )
 * @apiParam {String} commodity_id id commodity (separator semicolon [;] )
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 * @apiParam {Char} status status
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer
 * @apiSuccess {String} data.uuid uuid farmer
 * @apiSuccess {String} data.code code farmer
 * @apiSuccess {Integer} data.farmer_group_id id kelompok petani
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.hp_no nomor hp
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} land_area luas area
 * @apiSuccess {Numeric} ktp_no nomor ktp
 * @apiSuccess {Date} ktp_valid_date tanggal valid ktp
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */