/**
 * @api {post} /payment_gateway/:bank_name/inquiry Inquiry host to host
 * @apiVersion 1.0.0
 * @apiName Get Data Inquiry Host To Host
 * @apiGroup Payment Gateway
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} bank_name bank mandiri,bni,bri,bca,bsb
 *
 * @apiParamExample {json} bank mandiri:
 * {
 *      "trxid": "1218BFJ3340001",
 *      "booking_kode": "8070872408",
 *      "channel": "1"
 * }
 *
 * @apiParamExample {json} bank BRI:
 * {
 *      "rq_uuid": "2018111501367",
 *      "BookingCode": "8070872460",
 *      "channelID": "9"
 * }
 *
 * @apiParamExample {json} bank BNI:
 * {
 *      "trxid": "20181115144920",
 *      "booking_kode": "8070872346",
 *      "channel": "3"
 * }
 *
 * @apiParamExample {json} bank BCA:
 * {
 *      "rq_uuid": "1115164503762449906387I",
 *      "companyCode": "88020",
 *      "BookingCode": "8070872418",
 *      "channelID": "6"
 * }
 *
 * @apiParamExample {json} bank BCB:
 * {
 *      "trxid":"121632D1747001",
 *      "booking_code":"8800015936",
 *      "channel":"1"
 * }
 *
 *
 * @apiSuccessExample {json} bank mandiri:
 * {
 *      "trxid": "1218BFJ3340001",
 *      "status": "00",
 *      "booking_code": "8070872408",
 *      "so": "3100368728",
 *      "distributor": "MUNCUL SUBUR",
 *      "tujuan": "KAB. MALANG",
 *      "amount": "12909080",
 *      "denda": "0",
 *      "product": [ [ "ORGANIKKTG", "40", "3AP" ] ],
 *      "gudang": "MALANG - PAKISAJI",
 *      "commCodeDesc": "B000 PT Petrokimia Gresik"
 * }
 *
 * @apiSuccessExample {json} bank BRI:
 * {
 *      "rq_uuid": "2018111501367",
 *      "currency": "IDR",
 *      "billInfo1": "Karyawan Keluarga Besar PG",
 *      "billCode": "01",
 *      "billName": "Pembayaran",
 *      "billShortName": "Pembayaran",
 *      "bankAccountNo": "002601000101307",
 *      "billAmount": "5227273",
 *      "bookingTimeLimit": "16-11-2018 02:48:48",
 *      "commCode": "B000",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusDescription": "Sukses",
 *      "salesOrderNumber": "3100368789",
 *      "product": [ [ "NPKSUB", "25", "3AF" ] ],
 *      "warehouse": "TRENGGALEK",
 *      "destination": "KAB. TRENGGALEK",
 *      "batasambil": "07-12-2018"
 * }
 *
 * @apiSuccessExample {json} bank BNI:
 * {
 *      "rq_uuid": "20181115144920",
 *      "currency": "IDR",
 *      "billInfo1": "INDO BARU MANDIRI",
 *      "billCode": "01",
 *      "billName": "Pembayaran",
 *      "billShortName": "Pembayaran",
 *      "bankAccountNo": "44536223",
 *      "billAmount": "40909100",
 *      "bookingTimeLimit": "15-11-2018 20:30:48",
 *      "commCode": "B000",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusDescription": "Sukses",
 *      "salesOrderNumber": "3100368661",
 *      "product": [ [ "UREASUB", "25", "372" ] ],
 *      "warehouse": "BOJONEGORO 2 - SUMBEREJO",
 *      "destination": "KAB. BOJONEGORO"
 * }
 *
 * @apiSuccessExample {json} bank BCA:
 * {
 *      "rq_uuid": "1115164503762449906387I",
 *      "currency": "IDR",
 *      "billInfo1": "HIMIKARTA",
 *      "billCode": "01",
 *      "billName": "Pembayaran",
 *      "billShortName": "Pembayaran",
 *      "bankAccountNo": "7900499994",
 *      "billAmount": "61136349",
 *      "bookingTimeLimit": "15-11-2018 22:15:06",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusDescriptionid": "Sukses",
 *      "statusDescriptionen": "Succeed",
 *      "bookingcode": "8070872418",
 *      "destination": "KAB. MALANG"
 * }
 *
 * @apiSuccessExample {json} bank BCB:
 * {
 *      "trxid":"121632D1747001",
 *      "status":"00",
 *      "booking_code":"8800015936",
 *      "so":"3820007356",
 *      "distributor":"RAMAYANA",
 *      "tujuan":"Kota Metro",
 *      "amount":"38487270",
 *      "denda":"",
 *      "product":[["UREASUB","24,000","209"]],
 *      "gudang":"GD PADI MAS",
 *      "commCodeDesc":"F000 PT Pupuk Sriwidjaja"
 * }
 *
 */
/**
 * @api {post} /payment_gateway/:bank_name/payment payment host to host
 * @apiVersion 1.0.0
 * @apiName Get Data Payment Host To Host
 * @apiGroup Payment Gateway
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} bank_name bank mandiri,bni,bri,bca,bsb
 *
 * @apiParamExample {json} bank mandiri:
 * {
 *      "trxid": "1218BFJ3340001",
 *      "booking_kode": "8070872408",
 *      "channel": "1",
 *      "amount": "12909080",
 * }
 *
 * @apiParamExample {json} bank BRI:
 * {
 *      "rq_uuid": "2018111501367",
 *      "BookingCode": "8070872460",
 *      "channelID": "9" ,
 *      "paymentAmount": "5227273",
 * }
 *
 * @apiParamExample {json} bank BNI:
 * {
 *      "trxid": "20181115144920",
 *      "booking_kode": "8070872346",
 *      "channel": "3" ,
 *      "amount": "40909100.000",
 * }
 *
 * @apiParamExample {json} bank BCA:
 * {
 *      "rq_uuid": "1115164503762449906387I",
 *      "companyCode": "88020",
 *      "BookingCode": "8070872418",
 *      "channelID": "6",
 *      "paymentAmount": "5227273",
 * }
 *
 * @apiParamExample {json} bank BCB:
 * {
 *      "booking_code":"8800015936",
 * }
 *
 *
 * @apiSuccessExample {json} bank mandiri:
 * {
 *         "trxid": "1218BFJ3341001",
 *         "status": "00",
 *         "booking_code": "8070872408",
 *         "so": "3100368728",
 *         "distributor": "MUNCUL SUBUR",
 *         "tujuan": "KAB. MALANG",
 *         "amount": "12909080",
 *         "denda": "0",
 *         "product": [ [ "ORGANIKKTG", "40", "3AP" ] ],
 *         "prodTonaseWarehouse1": "ORGANIKKTG 40 3AP",
 *         "commCodeDesc": "B000 PT Petrokimia Gresik"
 * }
 *
 * @apiSuccessExample {json} bank BRI:
 * {
 *      "rq_uuid": "2018111501368",
 *      "billInfo1": "Karyawan Keluarga Besar PG",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusDescription": "Sukses",
 *      "salesOrderNumber": "3100368789",
 *      "warehouse": "TRENGGALEK",
 *      "destination": "KAB. TRENGGALEK"
 * }
 *
 * @apiSuccessExample {json} bank BNI:
 * {
 *      "rq_uuid": "20181115144923", "
 *      billInfo1": "INDO BARU MANDIRI",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusdescription": "Sukses",
 *      "salesOrderNumber": "3100368661",
 *      "batasAmbil": "07-12-2018",
 *      "warehouse": "BOJONEGORO 2 - SUMBEREJO",
 *      "destination": "KAB. BOJONEGORO",
 *      "prodTonaseWarehouse1": "UREASUB 25 372"
 * }
 *
 *
 * @apiSuccessExample {json} bank BCA:
 * {
 *      "rq_uuid": "201811151645038802000353128342P",
 *      "billInfo1": "HIMIKARTA",
 *      "isError": "false",
 *      "errorCode": "00",
 *      "statusDescriptionID": "Sukses",
 *      "statusDescriptionEN": "Succeed",
 *      "salesOrderNumber": "3100368741",
 *      "batasAmbil": "07-12-2018",
 *      "prodTonaseWarehouse1": "ZAKTG 50 3AP",
 *      "destination": "KAB. MALANG",
 *      "jumlahProduk": "1",
 *      "warehouse": "MALANG - PAKISAJI"
 * }
 *
 *
 * @apiSuccessExample {json} bank BCB:
 * {
 *      "trxid":"",
 *      "status":"00",
 *      "booking_code":"8800016257",
 *      "so":"3820007573",
 *      "distributor":"TUAN SEKAWAN",
 *      "tujuan":"KAB. OGAN KOMERING ULU TIMU",
 *      "amount":"23274",
 *      "denda":"",
 *      "product":[["ORGANIKKTG","0,080","304"]],
 *      "prodTonaseWarehouse1":"ORGANIKKTG 0,080 304",
 *      "commCodeDesc":"F000 PT Pupuk Sriwidjaja"
 * }
 */
/**
 * @api {post} /payment_gateway/:bank_name/check-limit Check Limit Plafon
 * @apiVersion 1.0.0
 * @apiName Check Limit Plafon
 * @apiGroup Payment Gateway
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} bank_name bank mandiri,bni,bri,bca,bsb
 *
 * @apiParam {String} customer_id  customer_id
 * @apiParam {String} sales_group_id  sales_group_id
 *
 * @apiSuccess {String} expired_date expired_date
 * @apiSuccess {Integer} credit_limit credit_limit
 * @apiSuccess {Integer} outstanding_df outstanding_df
 * @apiSuccess {Integer} available_amount available_amount
 * @apiSuccess {Integer} hold_amount hold_amount
 * @apiSuccess {Array} outstanding_so_number outstanding_so_number
 * @apiSuccess {Integer} outstanding_so outstanding_so
 * @apiSuccess {Integer} amount amount
 * @apiSuccess {Integer} available available
 * @apiSuccess {String} customer_id customer_id
 * @apiSuccess {String} customer_name customer_name
 * @apiSuccess {String} sales_org_id sales_org_id
 * @apiSuccess {String} sales_org_name sales_org_name
 * @apiSuccess {String} bank_name bank_name
 *
 */
/**
 * @api {post} /payment_gateway/:bank_name/disbursement Disbursement
 * @apiVersion 1.0.0
 * @apiName Disbursment
 * @apiGroup Payment Gateway
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {String} bank_name bank mandiri,bni,bri,bca,bsb
 *
 * @apiParamExample {json} bank BRI:
 * {
 *      "rq_uuid":"161005091042yTU",
 *      "doc_no":"3280017762",
 *      "comm_code":"C000",
 *      "payment_ref":"201610043280017762",
 *      "amount":"36113641.00",
 *      "ccy":"IDR",
 *      "transaction_date":"05/10/2016",
 *      "error_code":"0000",
 *      "error_msg":"Disbursement berhasilTransaction Successful"
 * }
 *
 * @apiParamExample {json} bank MANDIRI:
 * {
 *      "rq_uuid":"f7d48584-876b-4369-8aa7-05792bf24a3b",
 *      "doc_no":"3280017855",
 *      "rq_datetime":"2016-10-05 14:02:23",
 *      "password":"m4ndiRiSupplyCHA4in$GoplUs2973449344443",
 *      "comm_code":"C000",
 *      "member_code":"1000000011",
 *      "payment_ref":"DUP1610051DUF4",
 *      "amount":"35840915.00",
 *      "ccy":"IDR",
 *      "transaction_date":"2016-10-05",
 *      "error_code":"0000",
 *      "error_msg":"success :"
 * }
 * @apiParamExample {json} bank BNI:
 * {
 *      "rq_uuid":"a63437e6-f8a5-4784-80d3-02321512f2d6",
 *      "doc_no":"3280018576",
 *      "rq_datetime":"2016-10-18 17:18:05",
 *      "password":"P!HcD?stribut0rF!n4nc!ng",
 *      "comm_code":"C000",
 *      "member_code":"1000000222",
 *      "payment_ref":"CPO74063",
 *      "amount":"9559090.00",
 *      "ccy":"IDR",
 *      "transaction_date":"14/10/2016",
 *      "error_code":"0000",
 *      "error_msg":"Disbursement success"
 * }
 *
 * @apiSuccessExample {json} bank BRI:
 * {
 *      "rq_uuid":"161005091042yTU",
 *      "timestamp":"2016-10-05 08:53:44",
 *      "password":"",
 *      "comm_code":"C000",
 *      "customerNo":"",
 *      "payment_ref":"",
 *      "salesOrderNumber":"",
 *      "amount":"",
 *      "currency":"",
 *      "transaction_date":"",
 *      "error_code":"0000",
 *      "error_msg":"Sukses"
 * }
 *
 * @apiSuccessExample {json} bank MANDIRI:
 * {
 *      "rq_uuid":"f7d48584-876b-4369-8aa7-05792bf24a3b",
 *      "timestamp":"2016-10-05 14:02:26",
 *      "error_code":"0000",
 *      "error_Msg":"Sukses",
 *      "message":""
 * }
 * @apiSuccessExample {json} bank BNI:
 * {
 *      "rq_uuid":"a63437e6-f8a5-4784-80d3-02321512f2d6",
 *      "timestamp":"2016-10-18 17:18:10",
 *      "PASSWORD":"",
 *      "comm_code":"C000",
 *      "customerNo":"",
 *      "payment_ref":"",
 *      "salesOrderNumber":"",
 *      "amount":"",
 *      "currency":"",
 *      "transaction_date":"",
 *      "error_code":"0000",
 *      "error_msg":"Sukses"
 * }
 *
 */

/**
 * @api {post} /payment_gateway/:bank_name/invoice Send Invoice
 * @apiVersion 1.0.0
 * @apiName Send or Resend Invoice
 * @apiGroup Payment Gateway
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiHeader  {String} bank_name bank mandiri,bni,bri,bca,bsb
 * @apiParam  {Integer} order_id the Order ID
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer}  data.order_id the order id
 * @apiSuccess {Integer}  data.send_order_count count send order
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
