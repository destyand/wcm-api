/**
 * @api {post} /initialstockf5/upload Upload Initial Stock F5
 * @apiVersion 1.0.0
 * @apiName Upload Initial Stock F5
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id initial stock
 * @apiSuccess {String} data.type tipe initial stock
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.month bulan
 * @apiSuccess {Integer} data.year tahun
 * @apiSuccess {Char} data.status status
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /initialstockf5 Get All Initial Stock F5
 * @apiVersion 1.0.0
 * @apiName Get All Initial Stock F5
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id initial stock
 * @apiSuccess {Integer} data.uuid uuid initial stock
 * @apiSuccess {String} data.type tipe initial stock
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {Integer} data.month bulan
 * @apiSuccess {Integer} data.year tahun
 * @apiSuccess {Float} data.p01_qty kuantitas produk P01 awal
 * @apiSuccess {Float} data.p02_qty kuantitas produk P02 awal
 * @apiSuccess {Float} data.p03_qty kuantitas produk P03 awal
 * @apiSuccess {Float} data.p04_qty kuantitas produk P04 awal
 * @apiSuccess {Float} data.p05_qty kuantitas produk P05 awal
 * @apiSuccess {Float} data.p01_reduce kuantitas produk P01 terpakai
 * @apiSuccess {Float} data.p02_reduce kuantitas produk P02 terpakai
 * @apiSuccess {Float} data.p03_reduce kuantitas produk P03 terpakai
 * @apiSuccess {Float} data.p04_reduce kuantitas produk P04 terpakai
 * @apiSuccess {Float} data.p05_reduce kuantitas produk P05 terpakai
 * @apiSuccess {Integer} status_draft jumlah status draft
 * @apiSuccess {Integer} status_submit Jumlah Status Submit
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /penyaluraninitialstockf5 Create Penyaluran Initial Stock F5
 * @apiVersion 1.0.0
 * @apiName Create Penyaluran Initial Stock F5
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {Integer} initial_stock_id id initial stock
 * @apiParam {String} sales_group_id id sales group
 * @apiParam {String} customer_id id customer
 * @apiParam {Date} distribution_date tanggal distribusi
 * @apiParam {String} month bulan
 * @apiParam {String} year tahun
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id penyaluran
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {Integer} data.initial_stock_id id initial stock
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {Date} data.distribution_date tanggal distribusi
 * @apiSuccess {String} data.month bulan
 * @apiSuccess {String} data.year tahun
 * @apiSuccess {Integer} data.report_f5_id id report f5
 * @apiSuccess {String} data.number nomor penyaluran f5
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /penyaluraninitialstockf5/id/:uuid Get Penyaluran Initial Stock F5 By UUID Initial Stock
 * @apiVersion 1.0.0
 * @apiName Get Header Penyaluran Initial Stock F5 By UUID Initial Stock
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid initial stock
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id penyaluran
 * @apiSuccess {String} data.uuid uuid penyaluran 
 * @apiSuccess {String} data.number nomor penyaluran f5
 * @apiSuccess {Integer} data.report_f5_id id report f5
 * @apiSuccess {String} data.report_f5_number nomor report f5
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.month bulan
 * @apiSuccess {String} data.year tahun
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /penyaluraninitialstockf5/item/:uuid Get Item Penyaluran Initial Stock F5
 * @apiVersion 1.0.0
 * @apiName Get Item Penyaluran Initial Stock F5
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid penyaluran initial stock f5
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Array[]} data.header array data
 * @apiSuccess {Integer} data.header.id id penyaluran
 * @apiSuccess {String} data.header.uuid uuid penyaluran 
 * @apiSuccess {String} data.header.number nomor penyaluran f5
 * @apiSuccess {String} data.header.report_f5_id id report f5
 * @apiSuccess {String} data.header.customer_id id customer
 * @apiSuccess {String} data.header.customer_name nama customer
 * @apiSuccess {String} data.header.sales_org_id id sales org
 * @apiSuccess {String} data.header.sales_org_name nama sales org
 * @apiSuccess {String} data.header.sales_group_id id sales group
 * @apiSuccess {String} data.header.sales_group_name nama sales group
 * @apiSuccess {String} data.header.month bulan
 * @apiSuccess {String} data.header.year tahun
 * @apiSuccess {String} data.header.distribution_date tanggal penyaluran
 * @apiSuccess {String} data.header.status kode status
 * @apiSuccess {String} data.header.status_name nama status
 * @apiSuccess {Array[]} data.data array data
 * @apiSuccess {Array[]} data.data.header array data
 * @apiSuccess {String} data.data.header.id id product
 * @apiSuccess {String} data.data.header.name nama product
 * @apiSuccess {Float} data.data.header.qty kuantitas
 * @apiSuccess {Float} data.data.header.reduce kuantitas yang sudah digunakan
 * @apiSuccess {Float} data.data.header.balance sisa kuantitas
 * @apiSuccess {Array[]} data.data.kecamatan array data
 * @apiSuccess {String} data.data.kecamatan.id id kecamatan
 * @apiSuccess {String} data.data.kecamatan.name nama kecamatan
 * @apiSuccess {Array[]} data.data.kecamatan.retail array data
 * @apiSuccess {String} data.data.kecamatan.retail.id id pengecer
 * @apiSuccess {String} data.data.kecamatan.retail.uuid uuid pengecer
 * @apiSuccess {String} data.data.kecamatan.retail.code kode pengecer
 * @apiSuccess {String} data.data.kecamatan.retail.name nama pengecer
 * @apiSuccess {Array[]} data.data.kecamatan.retail.product array data
 * @apiSuccess {String} data.data.kecamatan.retail.product.id id product
 * @apiSuccess {String} data.data.kecamatan.retail.product.name nama product
 * @apiSuccess {String} data.data.kecamatan.retail.product.sales_unit_id id sales unit
 * @apiSuccess {Float} data.data.kecamatan.retail.product.qty kuantitas
 * @apiSuccess {Array[]} data.data.kecamatan.product array data
 * @apiSuccess {String} data.data.kecamatan.product.id id product
 * @apiSuccess {String} data.data.kecamatan.product.name nama product
 * @apiSuccess {Float} data.data.kecamatan.product.qty kuantitas
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /penyaluraninitialstockf5/item Create Item Penyaluran Initial Stock F5
 * @apiVersion 1.0.0
 * @apiName Create Item Penyaluran Initial Stock F5
 * @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [{"distrib_report_id": "1", "product_id": "P01", "report_f5_id": "1", "retail_id": "1", "qty": "12", "status": "y"}]
 * 
 * @apiParam {Integer} distrib_report_id id penyaluran
 * @apiParam {String} product_id id produk
 * @apiParam {Integer} report_f5_id id report f5
 * @apiParam {Integer} retail_id id retail
 * @apiParam {Integer} qty qty produk
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {Integer} data.distrib_report_id id penyaluran
 * @apiSuccess {String} data.product_id id produk
 * @apiSuccess {Integer} data.report_f5_id id report f5
 * @apiSuccess {Integer} data.retail_id id retail
 * @apiSuccess {Integer} data.qty qty produk
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
* @api {delete} /penyaluraninitialstockf5/id/{uuid}  DELETE Item Penyaluran Initial Stock F5
* @apiVersion 1.0.0
* @apiName DELETE Item Penyaluran Initial Stock F5
* @apiGroup Initial Stock F5
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
 * @api {put} /penyaluraninitialstockf5/id/{uuid} Update Status Item Penyaluran Initial Stock F5
* @apiVersion 1.0.0
* @apiName Update Status Item Penyaluran Initial Stock F5
* @apiGroup Initial Stock F5
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {status} status status d atau s
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */