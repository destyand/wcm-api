/**
 * @api {post} /supplypoint Create Supply Point
 * @apiVersion 1.0.0
 * @apiName Create Supply Point
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} number angka
 * @apiParam {String} sales_org_id id produsen
 * @apiParam {String} sales_office_id id sales office / provinsi
 * @apiParam {String} sales_group_id id sales group / kabupaten
 * @apiParam {Integer} plant_id id plant gudang
 * @apiParam {String} scnd_sales_group_id id sales group / kabupaten
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /supplypoint Get All Supply Point
 * @apiVersion 1.0.0
 * @apiName Get All Supply Point
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} sales_org_name nama produsen
 * @apiParam {String} sales_office_name nama sales office / provinsi
 * @apiParam {String} sales_group_name nama sales group / kabupaten
 * @apiParam {Integer} plant_id id plant gudang
 * @apiParam {String} plant_name nama plant gudang
 * @apiParam {String} plant_code kode plant gudang
 * @apiParam {String} scnd_sales_group_id id sales group / kabupaten
 * @apiParam {String} scnd_sales_group_name nama sales group / kabupaten
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {Integer} data.plant_name nama plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.scnd_sales_group_name nama sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /supplypoint/id/:uuid Get Supply Point By ID
 * @apiVersion 1.0.0
 * @apiName Get Supply Point By ID
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} uuid uuid supply point
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_office_name nama sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.sales_group_name nama sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {Integer} data.plant_name nama plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {String} data.scnd_sales_group_name nama sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /supplypoint/:uuid Update Supply Point
 * @apiVersion 1.0.0
 * @apiName Update Supply Point
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid supply point
 * @apiParam {Integer} number angka
 * @apiParam {String} sales_org_id id produsen
 * @apiParam {String} sales_office_id id sales office / provinsi
 * @apiParam {String} sales_group_id id sales group / kabupaten
 * @apiParam {Integer} plant_id id plant gudang
 * @apiParam {String} scnd_sales_group_id id sales group / kabupaten
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /supplypoint/:uuid Delete Supply Point
 * @apiVersion 1.0.0
 * @apiName Delete Supply Point
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} uuid uuid supply point
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /supplypoint/upload Upload Excel Supply Point
 * @apiVersion 1.0.0
 * @apiName Upload Excel Supply Point
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /supplypoint/update/bulk update multiple (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Multiple Data
 * @apiGroup Supply Point
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParamExample {json} Request-Example (data):
 *     [ { "uuid":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "uuid":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id supply point
 * @apiSuccess {String} data.uuid uuid supply point
 * @apiSuccess {Integer} data.number angka
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_office_id id sales office / provinsi
 * @apiSuccess {String} data.sales_group_id id sales group / kabupaten
 * @apiSuccess {Integer} data.plant_id id plant gudang
 * @apiSuccess {String} data.scnd_sales_group_id id sales group / kabupaten
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
