/**
 * @api {post} /reportso/header Get All Data Header Report SO
 * @apiVersion 1.0.0
 * @apiName Get All Data Header Report SO
 * @apiGroup Report SO
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccessExample {json} Header:
 *
 * {
 *       id: "69",
 *       nomor_sales_order: null,
 *       so_Item: "10",
 *       sales_organization: "B000",
 *       distribution_channel: "20",
 *       division: "00",
 *       sales_office: "0035",
 *       deskripsi_sales_office: "JAWA TIMUR",
 *       sales_group: "3520",
 *       deskripsi_sales_group: "Kab. Magetan",
 *       so_legacy: "Yanuar Jaya",
 *       sales_unit_id: null,
 *       kecamatan_so_desc: null,
 *       kabupaten_distributor: null,
 *       provinsi_distributor: null,
 *       nama_distributor: "Yanuar",
 *       pengecer: null,
 *       nomor_kontrak: "3068/TU.04.06/24/SP/2019",
 *       tanggal_so_dibuat: "01-04-2019",
 *       tanggal_dokumen: "01-04-2019",
 *       payment_method: "Cash",
 *       nomor_material: null,
 *       deskripsi_material: null,
 *       material_group: null,
 *       alokasi_asal: null,
 *       alokasi_operasional: null,
 *       quantity_so: null,
 *       unit_of_measure: null,
 *       mata_uang: "IDR",
 *       harga_jual_exc_ppn: null,
 *       ppn: null,
 *       total: null,
 *       harga_per_ton: null,
 *       harga_total: null,
 *       nomor_do: null,
 *       tanggal_pgi: null,
 *       plant_so: null,
 *       gudang_so: "",
 *       gudang_so_deskripsi: null,
 *       kode_gudang: "",
 *       gudang_pengambilan: null,
 *       quantity_do: null,
 *       quantity_so_min_do: null,
 *       pgi_qty: null,
 *       total_harga_tonase_pgi: null,
 *       so_type: "Z3SU",
 *       so_type_description: "Penj. Subsidi (Web)",
 *       payment_term: "Z000",
 *       paymentMethod: "E",
 *       batas_akhir_pengambilan: null,
 *       tanggal_po: "01-04-2019",
 *       no_po: "ORD000000023",
 *       name: "Admin",
 *       sektor: "SUBSIDI",
 *       no_billing: null,
 *       billing_date: null,
 *       incoterm_1: "FOT",
 *       incoterm_2: "3520"
 *   }
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /reportso/detail Get All Data Report SO Detail
 * @apiVersion 1.0.0
 * @apiName Get All Data Detail Report SO
 * @apiGroup Report SO
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccessExample json
 *
 *    {
 *        "draw": 0,
 *        "recordsTotal": 60,
 *        "recordsFiltered": 60,
 *        "data": [
 *            {
 *                "id": "47",
 *                "nomor_sales_order": null,
 *                "so_Item": "10",
 *                "sales_organization": "B000",
 *                "distribution_channel": "20",
 *                "division": "00",
 *                "sales_office": "0035",
 *                "deskripsi_sales_office": "JAWA TIMUR",
 *                "sales_group": "3520",
 *                "deskripsi_sales_group": "Kab. Magetan",
 *                "so_legacy": "AGRO AFIAT NUSANTARA, PT",
 *                "sales_unit_id": "352003",
 *                "kecamatan_so_desc": "Lembeyan",
 *                "kabupaten_distributor": "Kab. Magetan",
 *                "provinsi_distributor": "JAWA TIMUR",
 *                "distributor": "1000000017",
 *                "nama_distributor": "",
 *                "pengecer": "Pengecer no 17 Tes",
 *                "nomor_kontrak": "9992/MG.03.19/26/KG/19",
 *                "tanggal_so_dibuat": "29-03-2019",
 *                "tanggal_dokumen": "29-03-2019",
 *                "payment_method": "Cash",
 *                "nomor_material": "16891",
 *                "deskripsi_material": "Urea PRL SUB @50 KG",
 *                "material_group": "UREA",
 *                "alokasi_asal": "15",
 *                "alokasi_operasional": "15",
 *                "quantity_so": "4",
 *                "unit_of_measure": "TO",
 *                "mata_uang": "IDR",
 *                "harga_jual_exc_ppn": "1209463.5",
 *                "ppn": "654546",
 *                "total": "4837854",
 *                "harga_per_ton": "5001490.5",
 *                "harga_total": "5492400",
 *                "nomor_do": null,
 *                "tanggal_pgi": null,
 *                "plant_so": "B211",
 *                "gudang_so": "",
 *                "gudang_so_deskripsi": "DC GRESIK KIG Q",
 *                "kode_gudang": "",
 *                "gudang_pengambilan": "DC GRESIK KIG Q",
 *                "quantity_do": null,
 *                "quantity_so_min_do": null,
 *                "pgi_qty": null,
 *                "total_harga_tonase_pgi": null,
 *                "so_type": "Z3SU",
 *                "so_type_description": "Penj. Subsidi (Web)",
 *                "payment_term": "Z025",
 *                "paymentMethod": "E",
 *                "batas_akhir_pengambilan": null,
 *                "tanggal_po": "29-03-2019",
 *                "no_po": "ORD000000001",
 *                "so_created_by": "Admin",
 *                "sektor": "SUBSIDI",
 *                "no_billing": null,
 *                "billing_date": null,
 *                "incoterm_1": "FOT",
 *                "incoterm_2": "3520",
 *                "so_release": "29-03-2019",
 *                "status": "r",
 *                "outstanding_so": null,
 *                "provinsi_gudang": "",
 *                "kabupaten_gudang": "",
 *                "description": ""
 *            },
 *        ]
 *    }
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
