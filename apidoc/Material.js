/**
 * @api {get} /material Get Material List
 * @apiVersion 1.0.0
 * @apiName Material List
 * @apiGroup MaterialList
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} id product_id
 * @apiParam {Integer} id sales_org_id
 * @apiParam {Integer} id plant_id
 * @apiparam {Integer} product_id  product_id
 * @apiparam {String} product_name  product_name
 * @apiparam {Integer} sales_org_id  sales_org_id
 * @apiparam {String} sales_org_name  sales_org_name
 * @apiparam {String} mat_desc  mat_desc
 * @apiparam {Integer} plant_id  plant_id
 * @apiparam {String} plant_name  plant_name
 * @apiparam {String} status  status
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data array bertingkar (parent child)
 * @apiSuccess {String} data.product_id product id
 * @apiSuccess {String} data.product_name product name
 * @apiSuccess {String} data.sales_org_id sales_org id
 * @apiSuccess {String} data.sales_org_name sales_org name
 * @apiSuccess {String} data.mat_desc material desc
 * @apiSuccess {String} data.plant_id plant id
 * @apiSuccess {String} data.plant_name plant name
 * @apiSuccess {String} data.status status
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /material/product Get Material Product List
 * @apiVersion 1.0.0
 * @apiName Material Product List
 * @apiGroup MaterialList
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} plant_id plant_id
 * @apiparam {Integer} sales_org_id  sales_org_id
 * @apiparam {String} status status
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data array bertingkar (parent child)
 * @apiSuccess {String} data.product_id product id
 * @apiSuccess {String} data.product_name product name
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /material/list Get More spesific Filter Material List
 * @apiVersion 1.0.0
 * @apiName Material List more spesific
 * @apiGroup MaterialList
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} sales_org_id sales org id
 * @apiparam {Integer} plant_id  plant id
 * @apiParam {Integer} product_id product id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data array
 * @apiSuccess {String} data.product_id product id
 * @apiSuccess {String} data.product_name product name
 * @apiSuccess {String} data.sales_org_id sales_org id
 * @apiSuccess {String} data.sales_org_name sales_org name
 * @apiSuccess {String} data.mat_desc material desc
 * @apiSuccess {String} data.plant_id plant id
 * @apiSuccess {String} data.plant_name plant name
 * @apiSuccess {String} data.status status
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {get} /material/product/list Get More spesific Filter Material Product List
 * @apiVersion 1.0.0
 * @apiName Material Product List more spesific
 * @apiGroup MaterialList
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {Integer} plant_id plant_id
 * @apiparam {Integer} sales_org_id  sales_org_id
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data 
 * @apiSuccess {String} data.product_id product id
 * @apiSuccess {String} data.product_name product name
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */