/**
 * @api {post} /retail Create Retail
 * @apiVersion 1.0.0
 * @apiName Create Retail
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} owner pemilik
 * @apiParam {Sftring} address alamat pengecer
 * @apiParam {String} sales_unit_id id kecamatan (sales unit)
 * @apiParam {String} village desa pengecer 
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no handphone
 * @apiParam {String} fax_no fax 
 * @apiParam {decimal} latitude latitude
 * @apiParam {decimal} longitude longitude 
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal registrasi situ
 * @apiParam {String} tdp_no tdp no
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 * @apiParam {String} status status penyimpanan (y: submit, d:save)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.code code pengecer
 * @apiSuccess {String} data.uuid id pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /retail Get All Retail
 * @apiVersion 1.0.0
 * @apiName Get All Retail
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.uuid uuid pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner peilik
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no smart
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longtitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal situ
 * @apiSuccess {String} data.tdp_no no tdp
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.unit_id id kecamatan
 * @apiSuccess {String} data.unit_name nama kecamatan
 * @apiSuccess {String} data.group_id id kabupaten
 * @apiSuccess {String} data.group_name nama kabupaten
 * @apiSuccess {String} data.office_id id provinsi
 * @apiSuccess {String} data.office_name nama provinsi
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
 
 
/**
 * @api {get} /retail/cust_id/{uuid} Get All Retail By Distributor UUID
 * @apiVersion 1.0.0
 * @apiName Get All Retail by Dsitributor UUID
 * @apiGroup Retail
 *
 *  
 * @apiParam {String} uuid uuid customer (distributor)
 * 
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.uuid uuid pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner peilik
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no smart
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longtitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal situ
 * @apiSuccess {String} data.tdp_no no tdp
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.unit_id id kecamatan
 * @apiSuccess {String} data.unit_name nama kecamatan
 * @apiSuccess {String} data.group_id id kabupaten
 * @apiSuccess {String} data.group_name nama kabupaten
 * @apiSuccess {String} data.office_id id provinsi
 * @apiSuccess {String} data.office_name nama provinsi
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
 

/**
 * @api {get} /retail/id/:uuid Get Retail By uuid
 * @apiVersion 1.0.0
 * @apiName Get Retail By UUID
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} id uuid retail
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.uuid uuid pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.code code pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner peilik
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longtitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal situ
 * @apiSuccess {String} data.tdp_no no tdp
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.unit_id id kecamatan
 * @apiSuccess {String} data.unit_name nama kecamatan
 * @apiSuccess {String} data.group_id id kabupaten
 * @apiSuccess {String} data.group_name nama kabupaten
 * @apiSuccess {String} data.office_id id provinsi
 * @apiSuccess {String} data.office_name nama provinsi
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 
/**
 * @api {put} /retail/:uuid Update Retail
 * @apiVersion 1.0.0
 * @apiName Update Retail
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} name nama role
 * @apiParam {String} owner pemilik
 * @apiParam {String} address alamat pengecer
 * @apiParam {String} sales_unit_id id kecamatan (sales unit)
 * @apiParam {String} village desa pengecer 
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no handphone
 * @apiParam {String} fax_no fax
 * @apiParam {decimal} latitude latitude
 * @apiParam {decimal} longitude longitude 
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal registrasi situ
 * @apiParam {String} tdp_no tdp no
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pengecer
 * @apiSuccess {String} data.uuid id pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /retail/:uuid Delete Retail
 * @apiVersion 1.0.0
 * @apiName Delete Retail
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} id uuid retail
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pengecer
 * @apiSuccess {String} data.uuid id pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 
/**
 * @api {post} /retail/upload Show Data by Excel
 * @apiVersion 1.0.0
 * @apiName Get Data by Excel
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.customer_id id customer (id distributor)
 * @apiSuccess {String} data.customer_name nama customer (nama distributor)
 * @apiSuccess {String} data.sales_org_id id sales organization (id produsen)
 * @apiSuccess {String} data.sales_org_name nama sales organization (nama produsen)
 * @apiSuccess {String} data.id id pengecer 
 * @apiSuccess {String} data.name nama pengecer 
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {Array[]} data.validate data array data (validasi data setiap row)
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */
 
/**
 * @api {post} /retail/json/bulk Save data json (data raw)
 * @apiVersion 1.0.0
 * @apiName Save By Json Excel
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [{"name":"Sucahyo","email":"rifqis1@mail.com","owner":"Rifqi","address":"Jl. bla bla bla","sales_unit_id":"110101","village":"Ds. Waw","tlp_no":"031 xxx","hp_no":"","fax_no":"","latitude":"0","longitude":"0","npwp_no":"13","npwp_register":"","siup_no":"13","valid_date_siup":"","situ_no":"","valid_date_situ":"","tdp_no":"","valid_date_tdp":"","recomd_letter":"","recomd_letter_date":"","old_number":"","status":"d"},{"name":"Sucahyo","email":"rifqis2@mail.com","owner":"Rifqi","address":"Jl. bla bla bla","sales_unit_id":"110101","village":"Ds. Waw","tlp_no":"031 xxx","hp_no":"","fax_no":"","latitude":"0","longitude":"0","npwp_no":"14","npwp_register":"","siup_no":"14","valid_date_siup":"","situ_no":"","valid_date_situ":"","tdp_no":"","valid_date_tdp":"","recomd_letter":"","recomd_letter_date":"","old_number":"","status":"d"}]
 *
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} owner pemilik
 * @apiParam {String} address alamat pengecer
 * @apiParam {String} sales_unit_id id kecamatan (sales unit)
 * @apiParam {String} village desa pengecer 
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no handphone
 * @apiParam {String} fax_no fax 
 * @apiParam {decimal} latitude latitude
 * @apiParam {decimal} longitude longitude 
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal registrasi situ
 * @apiParam {String} tdp_no tdp no
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 * @apiParam {String} status status penyimpanan (y: submit, d:save)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.code code pengecer 
 * @apiSuccess {String} data.name nama pengecer 
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */

 /**
 * @api {post} /retail/bulk Save data from excel
 * @apiVersion 1.0.0
 * @apiName Save By Excel
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data true
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */

/**
 * @api {post} /retail/update/bulk update multiple status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update - multiple status
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "id":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "id":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.code code pengecer 
 * @apiSuccess {String} data.name nama pengecer 
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


 /**
 * @api {post} /retailadministratif Create Retail Administratif
 * @apiVersion 1.0.0
 * @apiName Create Retail Administratif
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} owner pemilik
 * @apiParam {String} address alamat pengecer
 * @apiParam {String} sales_unit_id id kecamatan (sales unit)
 * @apiParam {String} village desa pengecer 
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no handphone
 * @apiParam {String} fax_no fax 
 * @apiParam {decimal} latitude latitude
 * @apiParam {decimal} longitude longitude 
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal registrasi situ
 * @apiParam {String} tdp_no tdp no
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 * @apiParam {String} status status penyimpanan (y: submit, d:save)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.code code pengecer
 * @apiSuccess {String} data.uuid id pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {put} /retailadministratif/:uuid Update Retail Administratif
 * @apiVersion 1.0.0
 * @apiName Update Retail Administratif
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} name nama role
 * @apiParam {String} owner pemilik
 * @apiParam {String} address alamat pengecer
 * @apiParam {String} sales_unit_id id kecamatan (sales unit)
 * @apiParam {String} village desa pengecer 
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no handphone
 * @apiParam {String} fax_no fax
 * @apiParam {decimal} latitude latitude
 * @apiParam {decimal} longitude longitude 
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal registrasi situ
 * @apiParam {String} tdp_no tdp no
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id pengecer
 * @apiSuccess {String} data.uuid id pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner pemilik 
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.sales_unit_id id kecamatan (sales unit)
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no handphone
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal registrasi situ
 * @apiSuccess {String} data.tdp_no tdp no
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.status status penyimpanan (y: submit, d:save)
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 
/**
 * @api {get} /retail/export Download retail
 * @apiVersion 1.0.0
 * @apiName Download retail
 * @apiGroup Retail
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} uuid uuid pengecer
 * @apiParam {String} name nama pengecer
 * @apiParam {String} email email pengecer
 * @apiParam {String} owner peilik
 * @apiParam {String} address alamat pengecer
 * @apiParam {String} village desa pengecer
 * @apiParam {String} tlp_no telephone
 * @apiParam {String} hp_no smart
 * @apiParam {String} fax_no fax
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longtitude
 * @apiParam {String} npwp_no no npwp
 * @apiParam {Date} npwp_register tanggal registrasi npwp
 * @apiParam {String} siup_no no siup
 * @apiParam {Date} valid_date_siup tanggal siup
 * @apiParam {String} situ_no no situ
 * @apiParam {Date} valid_date_situ tanggal situ
 * @apiParam {String} tdp_no no tdp
 * @apiParam {Date} valid_date_tdp tanggal tdp
 * @apiParam {String} recomd_letter recomd letter
 * @apiParam {Date} recomd_letter_date tanggal recomd letter
 * @apiParam {String} old_number old number
 * @apiParam {String} unit_id id kecamatan
 * @apiParam {String} unit_name nama kecamatan
 * @apiParam {String} group_id id kabupaten
 * @apiParam {String} group_name nama kabupaten
 * @apiParam {String} office_id id provinsi
 * @apiParam {String} office_name nama provinsi
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */