/**
 * @api {post} /order Create Permintaan Penebusan
 * @apiVersion 1.0.0
 * @apiName Create Permintaan Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} sales_org_id id sales org / produsen
 * @apiParam {String} payment_method jenis pembayaran
 * @apiParam {Date} order_date tanggal order
 * @apiParam {Integer} contract_id id contract / spjb
 * @apiParam {String} customer_id id customer / pembeli
 * @apiParam {String} partner_id id partner / penerima
 * @apiParam {String} sales_office_id id sales office
 * @apiParam {String} sales_group_id id sales group
 * @apiParam {Integer} delivery_method_id id delivery method
 * @apiParam {String} bank_id id bank
 * @apiParam {String} desc jenis truk
 * @apiParam {String} reference_code kode referensi
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.payment_method jenis pembayaran
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {Integer} data.contract_id id contract / spjb
 * @apiSuccess {String} data.customer_id id customer / pembeli
 * @apiSuccess {String} data.partner_id id partner / penerima
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {String} data.desc jenis truk
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /order/:uuid Update Permintaan Penebusan
 * @apiVersion 1.0.0
 * @apiName Update Permintaan Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid permintaan penebusan
 * @apiParam {String} sales_org_id id sales org / produsen
 * @apiParam {String} payment_method jenis pembayaran
 * @apiParam {Date} order_date tanggal order
 * @apiParam {Integer} contract_id id contract / spjb
 * @apiParam {String} customer_id id customer / pembeli
 * @apiParam {String} partner_id id partner / penerima
 * @apiParam {String} sales_office_id id sales office
 * @apiParam {String} sales_group_id id sales group
 * @apiParam {Integer} delivery_method_id id delivery method
 * @apiParam {String} bank_id id bank
 * @apiParam {String} desc jenis truk
 * @apiParam {String} reference_code kode referensi
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.payment_method jenis pembayaran
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {Integer} data.contract_id id contract / spjb
 * @apiSuccess {String} data.customer_id id customer / pembeli
 * @apiSuccess {String} data.partner_id id partner / penerima
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {String} data.desc jenis truk
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /order/status/:uuid Ubah Status Permintaan Penebusan
 * @apiVersion 1.0.0
 * @apiName Ubah Status Permintaan Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid permintaan penebusan
 * @apiParam {Char} status kode status
 * @apiParam {Integer} upfront_payment upfront payment
 * @apiParam {Integer} total_price total price
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.payment_method jenis pembayaran
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {Integer} data.contract_id id contract / spjb
 * @apiSuccess {String} data.customer_id id customer / pembeli
 * @apiSuccess {String} data.partner_id id partner / penerima
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {String} data.desc jenis truk
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /order/item Create Item Permintaan Penebusan
 * @apiVersion 1.0.0
 * @apiName Create Item Permintaan Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [{"order_id": "1", "product_id": "P01", "plant_id": "1", "retail_id": "1", "qty": "12", "sales_unit_id": "110101", "status": "y"}]
 * 
 * @apiParam {Integer} order_id id permintaan penebusan
 * @apiParam {String} product_id id produk
 * @apiParam {Integer} plant_id id plant
 * @apiParam {Integer} retail_id id retail
 * @apiParam {Integer} qty qty produk
 * @apiParam {Integer} total_price total harga
 * @apiParam {Integer} total_price_before_ppn total harga sebelum ppn
 * @apiParam {Integer} ppn ppn
 * @apiParam {Integer} pph22 pph22
 * @apiParam {Integer} zhet zhet
 * @apiParam {Integer} zbki zbki
 * @apiParam {Integer} zbdi zbdi
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Char} status status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {Integer} data.order_id id permintaan penebusan
 * @apiSuccess {String} data.product_id id produk
 * @apiSuccess {Integer} data.plant_id id plant
 * @apiSuccess {Integer} data.retail_id id retail
 * @apiSuccess {Integer} data.qty qty produk
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.material_list_id id material list (subtitusi)
 * @apiSuccess {String} data.total_price total harga
 * @apiSuccess {String} data.total_price_before_ppn harga sebelum ppn
 * @apiSuccess {String} data.ppn pajak ppn
 * @apiSuccess {String} data.pph22 pajak pph
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /order Get Rencana Penebusan
 * @apiVersion 1.0.0
 * @apiName Get Rencana Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {Integer} data.contract_id id kontrak / spjb
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.delivery_method_name nama delivery method
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {String} data.payment_method_id id payment method
 * @apiSuccess {String} data.payment_method_name nama payment method
 * @apiSuccess {String} data.desc deskripsi
 * @apiSuccess {String} data.booking_code kode booking
 * @apiSuccess {Integer} data.total_price total price 
 * @apiSuccess {Integer} data.total_price_before_ppn total price before ppn
 * @apiSuccess {Integer} data.upfront_payment upfront payment
 * @apiSuccess {Integer} data.ppn ppn
 * @apiSuccess {Integer} data.pph22 pph22
 * @apiSuccess {Date} data.billing_date billing date
 * @apiSuccess {Date} data.payment_due_date payment due date
 * @apiSuccess {Integer} data.bank_id id bank
 * @apiSuccess {Date} data.good_redemption_due_date good redemption due date
 * @apiSuccess {Date} data.df_due_date df due date
 * @apiSuccess {Date} data.df_due_date_plan df due date plan
 * @apiSuccess {Date} data.pickup_due_date pickup due date
 * @apiSuccess {String} data.delivery_location delivery location
 * @apiSuccess {String} data.so_number so number
 * @apiSuccess {String} data.sap_status sap status
 * @apiSuccess {Date} data.billing_fulldate billing fulldate
 * @apiSuccess {String} data.sap_product_desc sap product desc
 * @apiSuccess {String} data.sap_miscs sap miscs
 * @apiSuccess {String} data.reference_code reference code
 * @apiSuccess {String} data.flag flag
 * @apiSuccess {String} data.seq seq
 * @apiSuccess {Date} data.approve_date approve date
 * @apiSuccess {String} data.approve_type approve type
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /order/id/{uuid} Get Rencana Penebusan By UUID
 * @apiVersion 1.0.0
 * @apiName Get Rencana Penebusan By UUID
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {string} UUID UUID Order
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {Integer} data.contract_id id kontrak / spjb
 * @apiSuccess {String} data.customer_id id distributor
 * @apiSuccess {String} data.customer_name nama distributor
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.delivery_method_name nama delivery method
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {String} data.payment_method nama payment method
 * @apiSuccess {String} data.partner_id id partner
 * @apiSuccess {String} data.partner_name nama partner
 * @apiSuccess {String} data.desc deskripsi
 * @apiSuccess {String} data.booking_code kode booking
 * @apiSuccess {Integer} data.total_price total price 
 * @apiSuccess {Integer} data.total_price_before_ppn total price before ppn
 * @apiSuccess {Integer} data.upfront_payment upfront payment
 * @apiSuccess {Integer} data.ppn ppn
 * @apiSuccess {Integer} data.pph22 pph22
 * @apiSuccess {Date} data.billing_date billing date
 * @apiSuccess {Date} data.payment_due_date payment due date
 * @apiSuccess {Integer} data.bank_id id bank
 * @apiSuccess {Date} data.good_redemption_due_date good redemption due date
 * @apiSuccess {Date} data.df_due_date df due date
 * @apiSuccess {Date} data.df_due_date_plan df due date plan
 * @apiSuccess {Date} data.pickup_due_date pickup due date
 * @apiSuccess {String} data.delivery_location delivery location
 * @apiSuccess {String} data.so_number so number
 * @apiSuccess {String} data.sap_status sap status
 * @apiSuccess {Date} data.billing_fulldate billing fulldate
 * @apiSuccess {String} data.sap_product_desc sap product desc
 * @apiSuccess {String} data.sap_miscs sap miscs
 * @apiSuccess {String} data.reference_code reference code
 * @apiSuccess {String} data.flag flag
 * @apiSuccess {String} data.seq seq
 * @apiSuccess {Date} data.approve_date approve date
 * @apiSuccess {String} data.approve_type approve type
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /order/item/spjb?order_id= Get List SPJB Item Penebusan
 * @apiVersion 1.0.0
 * @apiName Get List SPJB Item Penebusan
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {integer} order_id id order
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Array[]} data.kecamatan array kecamatan
 * @apiSuccess {String} data.kecamatan.id id sales unit / kecamatan
 * @apiSuccess {String} data.kecamatan.name nama sales unit / kecamatan
 * @apiSuccess {Array[]} data.kecamatan.product array produk
 * @apiSuccess {String} data.kecamatan.product.id id produk
 * @apiSuccess {String} data.kecamatan.product.name nama produk
 * @apiSuccess {String} data.kecamatan.product.initial_qty kuantitas alokasi
 * @apiSuccess {String} data.kecamatan.product.rdkk kuantitas rdkk
 * @apiSuccess {String} data.kecamatan.product.ZHET array ZHET
 * @apiSuccess {String} data.kecamatan.product.ZHET.harga nilai ZHET
 * @apiSuccess {String} data.kecamatan.product.ZHET.per_uom per uom ZHET
 * @apiSuccess {String} data.kecamatan.product.ZBKI array ZBKI
 * @apiSuccess {String} data.kecamatan.product.ZBKI.harga nilai ZBKI
 * @apiSuccess {String} data.kecamatan.product.ZBKI.per_uom per uom ZBKI
 * @apiSuccess {String} data.kecamatan.product.ZBDI array ZBDI
 * @apiSuccess {String} data.kecamatan.product.ZBDI.harga nilai ZBDI
 * @apiSuccess {String} data.kecamatan.product.ZBDI.per_uom per uom ZBDI
 * @apiSuccess {String} data.kecamatan.product.status status isian product
 * @apiSuccess {String} data.kecamatan.product.limit batas minimal input
 * @apiSuccess {String} data.kecamatan.product.deduction_amount pengurang rddk & spjb
 * @apiSuccess {Array[]} data.kecamatan.retail array pengecer
 * @apiSuccess {Integer} data.kecamatan.retail.id id pengecer
 * @apiSuccess {String} data.kecamatan.retail.name nama pengecer
 * @apiSuccess {String} data.kecamatan.retail.address alamat pengecer
 * @apiSuccess {Array[]} data.kecamatan.retail.item array item
 * @apiSuccess {Integer} data.kecamatan.retail.item.id id order item
 * @apiSuccess {String} data.kecamatan.retail.item.uuid uuid order item
 * @apiSuccess {Integer} data.kecamatan.retail.item.qty qty order item
 * @apiSuccess {String} data.kecamatan.retail.item.product_id id produk
 * @apiSuccess {String} data.kecamatan.retail.item.product_name nama produk
 * @apiSuccess {Integer} data.kecamatan.retail.item.plant_id id gudang
  * @apiSuccess {String} data.kecamatan.retail.item.plant_code kode gudang
 * @apiSuccess {String} data.kecamatan.retail.item.plant_name nama gudang
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {post} /order/upload Upload Order
 * @apiVersion 1.0.0
 * @apiName Upload Order
 * @apiGroup Order
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {file} file the xls,xlsx file
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data 
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {Array[]}  per baris pesan kesalahan
 */
