/**
 * @api {get} /sap/check-do-from-so Check Delivery Order from SO
 * @apiVersion 1.0.0
 * @apiName Check DO From SO
 * @apiGroup SAP
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * @apiParam  {string} so_number the SO number
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.inserted_or_updated count Inserted or updated Record
 * @apiSuccess {Integer} data.deleted Count Deleted Record
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
