/**
* @api {get} /distribitem Penyaluran Item
* @apiVersion 1.0.0
* @apiName Penyaluran Item
* @apiGroup distrib item
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {uuid} uuid  uuid
* @apiParam {String} number_pkp  number_pkp
* @apiParam {Integer} produsen_id  produsen_id
* @apiParam {String} produsen_name  produsen_name
* @apiParam {String} customer_name  customer_name
* @apiParam {Integer} customer_id  customer_id
* @apiParam {String} distrib_type  distrib_type
* @apiParam {Integer} order_number  order_number
* @apiParam {String} so_number  so_number
* @apiParam {Integer} month  month
* @apiParam {Integer} year  year
* @apiParam {String} distribution_date  distribution_date
* @apiParam {Integer} sales_office_id  sales_office_id
* @apiParam {String} sales_office_name  sales_office_name
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {Integer} sales_unit_id  sales_unit_id
* @apiParam {String} sales_unit_name  sales_unit_name
* @apiParam {String} retail_code  retail_code
* @apiParam {String} retail_name  retail_name
* @apiParam {String} product_name  product_name
* @apiParam {Integer} qty  qty
* @apiParam {String} status  status
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {String} data.number_pkp  number_pkp
* @apiSuccess {Integer} data.produsen_id  produsen_id
* @apiSuccess {String} data.produsen_name  produsen_name
* @apiSuccess {String} data.customer_name  customer_name
* @apiSuccess {Integer} data.customer_id  customer_id
* @apiSuccess {String} data.distrib_type  distrib_type
* @apiSuccess {Integer} data.order_number  order_number
* @apiSuccess {String} data.so_number  so_number
* @apiSuccess {Integer} data.month  month
* @apiSuccess {Integer} data.year  year
* @apiSuccess {String} data.distribution_date  distribution_date
* @apiSuccess {Integer} data.sales_office_id  sales_office_id
* @apiSuccess {String} data.sales_office_name  sales_office_name
* @apiSuccess {Integer} data.sales_group_id  sales_group_id
* @apiSuccess {String} data.sales_group_name  sales_group_name
* @apiSuccess {Integer} data.sales_unit_id  sales_unit_id
* @apiSuccess {String} data.sales_unit_name  sales_unit_name
* @apiSuccess {String} data.retail_code  retail_code
* @apiSuccess {String} data.retail_name  retail_name
* @apiSuccess {String} data.product_name  product_name
* @apiSuccess {Integer} data.qty  qty
* @apiSuccess {String} data.status  status
* @apiSuccess {String} data.status_name  status_name
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /distribitem/download Download Penyaluran Item
* @apiVersion 1.0.0
* @apiName Download Penyaluran Item
* @apiGroup distrib item
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiParam {String} number_pkp  number_pkp
* @apiParam {String} number  number_f5_or_f6
* @apiParam {Integer} produsen_id  produsen_id
* @apiParam {String} produsen_name  produsen_name
* @apiParam {String} customer_name  customer_name
* @apiParam {Integer} customer_id  customer_id
* @apiParam {String} distrib_type  distrib_type
* @apiParam {Integer} order_number  order_number
* @apiParam {String} so_number  so_number
* @apiParam {Integer} month  month
* @apiParam {Integer} year  year
* @apiParam {String} distribution_date  distribution_date
* @apiParam {Integer} sales_office_id  sales_office_id
* @apiParam {String} sales_office_name  sales_office_name
* @apiParam {Integer} sales_group_id  sales_group_id
* @apiParam {String} sales_group_name  sales_group_name
* @apiParam {Integer} sales_unit_id  sales_unit_id
* @apiParam {String} sales_unit_name  sales_unit_name
* @apiParam {String} retail_code  retail_code
* @apiParam {String} retail_name  retail_name
* @apiParam {String} product_name  product_name
* @apiParam {Integer} qty  qty
* @apiParam {String} status_f5_f6  status_f5_or_f6
* @apiParam {String} status  status
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/
