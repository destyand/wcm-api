/**
 * @api {post} /retail_assg/assg/bulk/ Assignment relation - multiple (raw)
 * @apiVersion 1.0.0
 * @apiName Assignment data multiple 
 * @apiGroup Retail Assg
 * 
 * @apiParamExample {json} Request-Example (data):
 * [ { "retail_id":"AEC6A44D-DD87-4091-A508-FAFC29042486", "customer_id":"1000000001", "sales_org_id":"B000" } ]
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.retail_code code retail 
 * @apiSuccess {String} data.retail_name nama retail 
 * @apiSuccess {String} data.customer_id code customer 
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id code produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */


/**
 * @api {get} /retail_assg Get All Retail Assg
 * @apiVersion 1.0.0
 * @apiName Get All Retail Assg
 * @apiGroup Retail Assg
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
* @apiParam   {uuid} uuid  uuid
* @apiParam   {Insteger} customer_id  customer_id
* @apiParam   {String} customer_name  customer_name
* @apiParam   {String} customer_owner  customer_owner
* @apiParam   {String} customer_uuid  customer uuid
* @apiParam   {Insteger} sales_org_id  sales_org_id
* @apiParam   {String} sales_org_name  sales_org_name
* @apiParam   {Insteger} retail_id  retail_id
* @apiParam   {String} retail_code  retail_code
* @apiParam   {String} retail_name  retail_name
* @apiParam   {String} retail_owner  retail_owner
* @apiParam   {String} address  alamat Retail
* @apiParam   {Insteger} sales_unit_id  sales_unit_id
* @apiParam   {String} sales_unit_name  sales_unit_name
* @apiParam   {Insteger} sales_group_id  sales_group_id
* @apiParam   {String} sales_group_name  sales_group_name
* @apiParam   {Insteger} sales_office_id  sales_office_id
* @apiParam   {String} sales_office_name  sales_office_name
* @apiParam   {String} status  status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.uuid uuid relation
 * @apiSuccess {String} data.customer_id id customer (distributor)
 * @apiSuccess {String} data.customer_uuid uuid customer (distributor)
 * @apiSuccess {String} data.customer_name nama customer (distributor)
 * @apiSuccess {String} data.sales_org_id id produsen
 * @apiSuccess {String} data.sales_org_name nama produsen
 * @apiSuccess {String} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_code code pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.address Alamat retail
 * @apiSuccess {String} data.sales_unit_id id kecamatan
 * @apiSuccess {String} data.sales_unit_name nama kecamatan
 * @apiSuccess {String} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.sales_group_name nama kabupaten
 * @apiSuccess {String} data.sales_office_id id provinsi
 * @apiSuccess {String} data.sales_office_name nama provinsi
 * @apiSuccess {String} data.status status relation
 * @apiSuccess {String} data.status_name nama status relation
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
 
 
/**
 * @api {post} /retail/retail_assg/update/bulk/ Update relation status - multiple (raw)
 * @apiVersion 1.0.0
 * @apiName Update data multiple 
 * @apiGroup Retail Assg
 * 
 * @apiParamExample {json} Request-Example (data):
 * [ { "id":"7B822CC3-E5D8-33A7-88BD-C512F5CFB351", "status":"d" } ]
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id relation 
 * @apiSuccess {String} data.uuid uuid relation 
 * @apiSuccess {String} data.customer_id code customer 
 * @apiSuccess {String} data.retail_id code retail
 * @apiSuccess {String} data.sales_org_id code produsen
 * @apiSuccess {String} data.start_date -
 * @apiSuccess {String} data.end_date -
 * @apiSuccess {Decimal} data.status status relation
 * @apiSuccess {String} data.created_by pembuat
 * @apiSuccess {String} data.updated_by pengubah
 * @apiSuccess {Timestamp} data.created_at dibuat pada
 * @apiSuccess {Timestamp} data.updated_at diubah pada
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */

/**
* @api {get} /retail/distributor Get Distributor
* @apiVersion 1.0.0
* @apiName Get Distributor
* @apiGroup Retail Assg
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.id id distributor
* @apiSuccess {String} data.uuid uuid distributor
* @apiSuccess {String} data.name nama distributor
* @apiSuccess {String} data.sales_org_id id sales org
* @apiSuccess {String} data.sales_org_name nama sales org
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /retail/produsen?sales_org_id={sales_org_id} Get Produsen by distributor (fleksible)
* @apiVersion 1.0.0
* @apiName Get Produsen
* @apiGroup Retail Assg
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
* 
* @apiParam {String} sales_org_id id sales org
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {Integer} data.id id produsen
* @apiSuccess {String} data.uuid uuid produsen
* @apiSuccess {String} data.name nama produsen
* @apiSuccess {String} data.customer_id id customer
* @apiSuccess {String} data.customer_name nama customer
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
 * @api {get} /retail?except=true&customer_uuid={customer_uuid}&sales_org_uuid={sales_org_uuid}&sales_unit_id={sales_unit_id} Get Retail Exception by Params
 * @apiVersion 1.0.0
 * @apiName Get Retail Exception by Params
 * @apiGroup Retail Assg
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} customer_uuid uuid customer (distributor)
 * @apiParam {String} sales_org_uuid uuid sales org (produsen)
 * @apiParam {String} sales_unit_id id sales unit (kecamatan)
 * @apiParam {String} except except (true)
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.uuid uuid pengecer
 * @apiSuccess {String} data.name nama pengecer
 * @apiSuccess {String} data.email email pengecer
 * @apiSuccess {String} data.owner peilik
 * @apiSuccess {String} data.address alamat pengecer
 * @apiSuccess {String} data.village desa pengecer
 * @apiSuccess {String} data.tlp_no telephone
 * @apiSuccess {String} data.hp_no smart
 * @apiSuccess {String} data.fax_no fax
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longtitude
 * @apiSuccess {String} data.npwp_no no npwp
 * @apiSuccess {Date} data.npwp_register tanggal registrasi npwp
 * @apiSuccess {String} data.siup_no no siup
 * @apiSuccess {Date} data.valid_date_siup tanggal siup
 * @apiSuccess {String} data.situ_no no situ
 * @apiSuccess {Date} data.valid_date_situ tanggal situ
 * @apiSuccess {String} data.tdp_no no tdp
 * @apiSuccess {Date} data.valid_date_tdp tanggal tdp
 * @apiSuccess {String} data.recomd_letter recomd letter
 * @apiSuccess {Date} data.recomd_letter_date tanggal recomd letter
 * @apiSuccess {String} data.old_number old number
 * @apiSuccess {String} data.sales_unit_id id kecamatan
 * @apiSuccess {String} data.sales_unit_name nama kecamatan
 * @apiSuccess {String} data.sales_group_id id kabupaten
 * @apiSuccess {String} data.sales_group_name nama kabupaten
 * @apiSuccess {String} data.sales_office_id id provinsi
 * @apiSuccess {String} data.sales_office_name nama provinsi
 * @apiSuccess {String} data.created_by pembuat data
 * @apiSuccess {String} data.updated_by pengubah data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
 