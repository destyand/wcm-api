/**
 * @api {post} /manualso Create Manual SO
 * @apiVersion 1.0.0
 * @apiName Create Manual SO
 * @apiGroup Manual SO
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} booking_code kode booking
 * @apiParam {String} sender nama pengirim
 * @apiParam {String} bank_id id bank
 * @apiParam {Date} payment_date tanggal pembayaran
 * @apiParam {Integer} amount jumlah pembayaran
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id manual so
 * @apiSuccess {String} data.booking_code kode booking
 * @apiSuccess {String} data.sender nama pengirim
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {Date} data.payment_date tanggal pembayaran
 * @apiSuccess {Integer} data.amount jumlah pembayaran
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * @apiSuccess {String} amount pesan pengecekan amount
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
