/**
 * @api {get} /manualapproval/setting Get All Approval Setting
 * @apiVersion 1.0.0
 * @apiName Get All Approval Setting
 * @apiGroup Manual Approval
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id approval setting
 * @apiSuccess {String} data.uuid uuid approval setting
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.status_name nama status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /manualapproval/setting/status Update Status Approval Setting
 * @apiVersion 1.0.0
 * @apiName Update Status Approval Setting
 * @apiGroup Manual Approval
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid approval setting
 * @apiParam {Char} status kode status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id approval setting
 * @apiSuccess {String} data.uuid uuid approval setting
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /manualapproval Get All Manual Approval
 * @apiVersion 1.0.0
 * @apiName Get All Manual Approval
 * @apiGroup Manual Approval
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.order_id id order
 * @apiSuccess {String} data.order_uuid uuid order
 * @apiSuccess {Integer} data.order_item_id id item order
 * @apiSuccess {String} data.order_item_uuid uuid item order
 * @apiSuccess {String} data.so_number nomor so
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.payment_method metode pembayaran
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office 
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group 
 * @apiSuccess {Integer} data.contract_id id contract / spjb
 * @apiSuccess {String} data.spjb nomor spjb 
 * @apiSuccess {String} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {Integer} data.material_list_id id material
 * @apiSuccess {String} data.material_no nomor material
 * @apiSuccess {String} data.mat_desc nama material
 * @apiSuccess {Integer} data.qty kuantitas
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /manualapproval/status Update Status Approval Order
 * @apiVersion 1.0.0
 * @apiName Update Status Approval Order
 * @apiGroup Manual Approval
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid order
 * @apiParam {Char} status kode status
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id order
 * @apiSuccess {String} data.uuid uuid order
 * @apiSuccess {String} data.number nomor order
 * @apiSuccess {String} data.sales_org_id id sales org / produsen
 * @apiSuccess {String} data.payment_method jenis pembayaran
 * @apiSuccess {Date} data.order_date tanggal order
 * @apiSuccess {Integer} data.contract_id id contract / spjb
 * @apiSuccess {String} data.customer_id id customer / pembeli
 * @apiSuccess {String} data.partner_id id partner / penerima
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {Integer} data.delivery_method_id id delivery method
 * @apiSuccess {String} data.bank_id id bank
 * @apiSuccess {String} data.desc jenis truk
 * @apiSuccess {String} data.reference_code kode referensi
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */