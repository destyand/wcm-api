/**
* @api {get} /plantingperiode Master Planting Period
* @apiVersion 1.0.0
* @apiName Master Planting Period
* @apiGroup plantingperiod
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
* @apiParam {uuid} uuid uuid
* @apiParam {Numeric} code code
* @apiParam {String} desc desc
* @apiParam {String} status status
* @apiParam {Timestamp} from_date from_date
* @apiParam {Timestamp} thru_date thru_date
* @apiParam {Timestamp} created_at created_at
* @apiParam {Timestamp} updated_at updated_at
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {Numeric} data.code  code
* @apiSuccess {String} data.desc  desc
* @apiSuccess {String} data.status  status
* @apiSuccess {Timestamp} data.from_date  from_date
* @apiSuccess {Timestamp} data.thru_date  thru_date
* @apiSuccess {Timestamp} data.created_at  created_at
* @apiSuccess {Timestamp} data.updated_at  updated_at
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
* @api {get} /plantingperiode/{uuid} Detail Planting Period
* @apiVersion 1.0.0
* @apiName Detail Planting Period
* @apiGroup plantingperiod
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {Numeric} data.code  code
* @apiSuccess {String} data.desc  desc
* @apiSuccess {String} data.status  status
* @apiSuccess {Timestamp} data.from_date  from_date
* @apiSuccess {Timestamp} data.thru_date  thru_date
* @apiSuccess {Timestamp} data.created_at  created_at
* @apiSuccess {Timestamp} data.updated_at  updated_at
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/

/**
 * @api {post} /plantingperiode/bulk Update Multiple Status (data raw)
 * @apiVersion 1.0.0
 * @apiName Update - multiple status
 * @apiGroup plantingperiod
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [
	{
		"id":"65FBF6F9-939F-4C45-81DC-A3B7AE0A8A20",
		"status":"n"
	},
		{
		"id":"6A0F448E-BFE5-4D53-AAF0-372825824229",
		"status":"n"
	}
]
 * @apiParam {String} data.status status penyimpanan (y: active, n:inactive)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data  
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {delete} /plantingperiode/{uuid} Delete Planting Period
 * @apiVersion 1.0.0
 * @apiName Delete Planting Period
 * @apiGroup plantingperiod 
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data  
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
* @api {post} /plantingperiode Create Planting Period
* @apiVersion 1.0.0
* @apiName Create Planting Period
* @apiGroup plantingperiod
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)

* @apiParam {Numeric} code code
* @apiParam {String} desc desc
* @apiParam {Timestamp} from_date from_date
* @apiParam {Timestamp} thru_date thru_date
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {Numeric} data.code  code
* @apiSuccess {String} data.desc  desc
* @apiSuccess {String} data.status  status
* @apiSuccess {Timestamp} data.from_date  from_date
* @apiSuccess {Timestamp} data.thru_date  thru_date
* @apiSuccess {Timestamp} data.created_at  created_at
* @apiSuccess {Timestamp} data.updated_at  updated_at
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/


 /**
* @api {put} /plantingperiode/{id} Create Planting Period
* @apiVersion 1.0.0
* @apiName Create Planting Period
* @apiGroup plantingperiod
*
* @apiHeader {String} Authorization token (Bearer)
* @apiHeader {String} Language bahasa (id = indonesia; en = english)

* @apiParam {Numeric} code code
* @apiParam {String} desc desc
* @apiParam {Timestamp} from_date from_date
* @apiParam {Timestamp} thru_date thru_date
*
* @apiSuccess {Integer} status 1
* @apiSuccess {String} status_txt success
* @apiSuccess {String} message pesan sukses
* @apiSuccess {Array[]} data array data
* @apiSuccess {uuid} data.uuid  uuid
* @apiSuccess {Numeric} data.code  code
* @apiSuccess {String} data.desc  desc
* @apiSuccess {String} data.status  status
* @apiSuccess {Timestamp} data.from_date  from_date
* @apiSuccess {Timestamp} data.thru_date  thru_date
* @apiSuccess {Timestamp} data.created_at  created_at
* @apiSuccess {Timestamp} data.updated_at  updated_at
*
* @apiError {Integer} status 0
* @apiError {String} status_txt errors
* @apiError {String} message pesan kesalahan
*/