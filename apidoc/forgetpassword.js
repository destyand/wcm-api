
/**
 * @api {post} /forgetpassword Send Email
 * @apiVersion 1.0.0
 * @apiName Send Email
 * @apiGroup FORGET PASSWORD
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * 
 * @apiparam {email} email email
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {uuid} data.uuid  uuid in url
 * @apiSuccess {uuid} data.user_id  uuid user
 * @apiSuccess {timestamps} data.link_create_date  tanggal membuat Link
 * @apiSuccess {timestamps} data.link_valid_to_date  Tanggal Expired Link
 * @apiSuccess {status} data.status  status
 * @apiSuccess {timestamps} data.created_at  created_at
 * @apiSuccess {timestamps} data.updated_at  updated_at
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */


/**
 * @api {get} /forgetpassword/{uuid} Cek Link Expired 
 * @apiVersion 1.0.0
 * @apiName Cek Link Expired 
 * @apiGroup FORGET PASSWORD
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {String} messages Description

 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiSuccess {String} messages Description
 */

 /**
 * @api {post} /forgetpassword/{uuid} Ubah Konfirm password
 * @apiVersion 1.0.0
 * @apiName Ubah Konfirm password
 * @apiGroup FORGET PASSWORD
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string}  password password baru
 * @apiParam {string}  re-password  ulangi password baru
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data 
 * @apiSuccess {timestamps} data.updated  waktu berhasil diupdate
 * @apiSuccess {user} data.user  user yang diupdate
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiSuccess {String} messages Description
 */

