
/**
 * @api {get} /matrix-rdkk/ Get All Matrix RDKK
 * @apiVersion 1.0.0
 * @apiName Get Matrix RDKK
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string} sales_org_id
 * @apiParam {string} sales_org_name
 * @apiParam {string} sales_group_id
 * @apiParam {string} sales_group_name
 * @apiParam {string} sales_office_id
 * @apiParam {string} sales_office_name
 * @apiParam {string} product_id
 * @apiParam {string} product_name
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /matrix-rdkk/{id} Get Matrix RDKK By ID
 * @apiVersion 1.0.0
 * @apiName Get Matrix RDKK By ID
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string} id the id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /matrix-rdkk/ Create Matrix RDKK By ID
 * @apiVersion 1.0.0
 * @apiName Create Matrix RDKK
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string} id the id
 * @apiParam {string} sales_org_id
 * @apiParam {string} sales_office_id
 * @apiParam {string} sales_group_id
 * @apiParam {string} product_id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /matrix-rdkk/{id} Update Matrix RDKK By ID
 * @apiVersion 1.0.0
 * @apiName Update Matrix RDKK By ID
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string} id the id
 * @apiParam {string} sales_org_id
 * @apiParam {string} sales_office_id
 * @apiParam {string} sales_group_id
 * @apiParam {string} product_id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /matrix-rdkk/{id} Update Matrix RDKK By ID
 * @apiVersion 1.0.0
 * @apiName Update Matrix RDKK By ID
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {string} id the id
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /matrix-rdkk/import Upload Matrix RDKK
 * @apiVersion 1.0.0
 * @apiName Upload Matrix RDKK
 * @apiGroup Matrix RDKK
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
