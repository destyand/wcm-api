/**
 * @api {post} /farmergroup Create Farmer Group
 * @apiVersion 1.0.0
 * @apiName Create Farmer Group
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} retail_id id pengecer
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {String} customer_id id customer
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} fax_no nomor fax
 * @apiParam {String} village desa
 * @apiParam {String} sub_sector_id id sub sector (separator semicolon [;] )
 * @apiParam {String} commodity_id id commodity (separator semicolon [;] )
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 * @apiParam {Char} status status
 * @apiParam {Numeric} qty_group qty_group
 * @apiParam {Numeric} land_area land_area
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {Numeric} data.qty_group qty_group
 * @apiSuccess {Numeric} data.land_area land_area
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /farmergroup/subsectors Get Subsectors Bulk
 * @apiVersion 1.0.0
 * @apiName Get Subsectors Bulk
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} subsectors subsectors Contoh (1;2;3)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id  id
 * @apiSuccess {String} data.uuid  uuid
 * @apiSuccess {String} data.name  name
 * @apiSuccess {Integer} data.status  status
 * @apiSuccess {String} data.created_by  created_by
 * @apiSuccess {String} data.updated_by  updated_by
 * @apiSuccess {String} data.created_at  created_at
 * @apiSuccess {String} data.updated_at  updated_at
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

 /**
 * @api {get} /farmergroup/commodities  Get Commodities Bulk
 * @apiVersion 1.0.0
 * @apiName  Get Commodities Bulk
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} commodities commodities Contoh (1;2;3)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id  id
 * @apiSuccess {String} data.uuid  uuid
 * @apiSuccess {String} data.name  name
 * @apiSuccess {Integer} data.status  status
 * @apiSuccess {String} data.created_by  created_by
 * @apiSuccess {String} data.updated_by  updated_by
 * @apiSuccess {String} data.created_at  created_at
 * @apiSuccess {String} data.updated_at  updated_at
 *
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /farmergroup Get All Farmer Group
 * @apiVersion 1.0.0
 * @apiName Get All Farmer Group
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {Integer} data.retail_code code pengecer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude 
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Integer} data.qty_group qty group
 * @apiSuccess {Integer} data.land_area land area
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /farmergroup/id/:uuid Get Farmer Group By ID
 * @apiVersion 1.0.0
 * @apiName Get Farmer Group By ID
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {Integer} uuid uuid farmer group
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {Integer} data.retail_code code pengecer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude 
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Integer} data.qty_group qty group
 * @apiSuccess {Integer} data.land_area land area
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {put} /farmergroup/:uuid Update Farmer Group
 * @apiVersion 1.0.0
 * @apiName Update Farmer Group
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid farmer group
 * @apiParam {String} customer_id id customer
 * @apiParam {Integer} retail_id id pengecer
 * @apiParam {String} customer_id id customer
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {Integer} qty_group qty group
 * @apiParam {String} land_area land area
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} fax_no nomor fax
 * @apiParam {String} village desa
 * @apiParam {Integer} sub_sector_id id sub sector
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {Integer} data.qty_group qty group
 * @apiSuccess {String} data.land_area land area
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} data.sub_sector_id id sub sector
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {delete} /farmergroup/:uuid Delete Farmer Group
 * @apiVersion 1.0.0
 * @apiName Delete Farmer Group
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid farmer group
 * @apiParam {String} customer_id id customer
 * @apiParam {Integer} retail_id id pengecer
 * @apiParam {Integer} qty_group qty group
 * @apiParam {String} land_area land area
 * @apiParam {String} name nama
 * @apiParam {String} address alamat
 * @apiParam {String} sales_unit_id id sales unit
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} fax_no nomor fax
 * @apiParam {String} village desa
 * @apiParam {Integer} sub_sector_id id sub sector
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {Integer} data.qty_group qty group
 * @apiSuccess {String} data.land_area land area
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {Integer} data.sub_sector_id id sub sector
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmergroup/upload Upload Excel Farmer Group
 * @apiVersion 1.0.0
 * @apiName Upload Excel Farmer Group
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {File} file file excel
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.retail_code kode pengecer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.land_area luas lahan
 * @apiSuccess {String} data.qty_group jumlah anggota
 * @apiSuccess {Array[]} data.sub_sector sub sector
 * @apiSuccess {Integer} data.sub_sector.id id sub sector
 * @apiSuccess {String} data.sub_sector.name nama sub sector
 * @apiSuccess {Array[]} data.commodity commodity
 * @apiSuccess {Integer} data.commodity.id id commodity
 * @apiSuccess {String} data.commodity.name nama commodity
 * @apiSuccess {Array[]} data.validate validate
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmergroup/update/bulk Update Multiple (data raw)
 * @apiVersion 1.0.0
 * @apiName Update Multiple Data
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "uuid":"47EBC402-D235-4C41-8B45-7C7B2728B761", "status":"d" }, { "uuid":"23400B59-8203-4EBE-ADAB-DC7951A2ABB4", "status":"d" } ]
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {post} /farmergroup/json/bulk Save Data Json (data raw)
 * @apiVersion 1.0.0
 * @apiName Save Data Json
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParamExample {json} Request-Example (data):
 *     [ { "retail_id":"A001", "name":"Sucahyo", "address":"jalan jalan", "sales_unit_id":"320509", "customer_id":"1000000000", "sales_org_id":"A000", "sub_sector_id":"1;2;3;4;", "commodity_id":"1;2;3;4;", "village":"Ds. Waw", "telp_no":"031xxx", "fax_no":"031xxx", "latitude":"", "longitude":"", "status":"d" } ]
 *
 * @apiParam {String} retail_id id pengecer
 * @apiParam {String} name nama kelompok tani
 * @apiParam {String} address alamat kelompok tani
 * @apiParam {String} sales_unit_id id sales unit / kecamatan
 * @apiParam {String} customer_id id customer / distributor
 * @apiParam {String} sales_org_id id sales org
 * @apiParam {Numeric} telp_no nomor telepon
 * @apiParam {Numeric} fax_no nomor fax
 * @apiParam {String} village desa
 * @apiParam {String} sub_sector_id id sub sector (separator semicolon [;] )
 * @apiParam {String} commodity_id id commodity (separator semicolon [;] )
 * @apiParam {Decimal} latitude latitude
 * @apiParam {Decimal} longitude longitude
 * @apiParam {Char} status status
 * 
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude longitude
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 * 
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 * @apiError {Array[]} data kesalahan
 */

/**
 * @api {get} /farmergroup/cust_id/{uuid} Get All Farmer Group By Distributor UUID
 * @apiVersion 1.0.0
 * @apiName Get All Farmer Group By Distributor UUID
 * @apiGroup Farmer Group
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id id farmer group
 * @apiSuccess {String} data.uuid uuid farmer group
 * @apiSuccess {String} data.code code farmer group
 * @apiSuccess {Integer} data.retail_id id pengecer
 * @apiSuccess {String} data.name nama
 * @apiSuccess {String} data.address alamat
 * @apiSuccess {String} data.sales_unit_id id sales unit
 * @apiSuccess {Numeric} data.telp_no nomor telepon
 * @apiSuccess {Numeric} data.fax_no nomor fax
 * @apiSuccess {String} data.village desa
 * @apiSuccess {String} data.sub_sector_id id sub sector
 * @apiSuccess {String} data.commodity_id id commodity
 * @apiSuccess {Decimal} data.latitude latitude
 * @apiSuccess {Decimal} data.longitude 
 * @apiSuccess {String} data.retail_name nama pengecer
 * @apiSuccess {String} data.sales_unit_name nama sales unit
 * @apiSuccess {String} data.customer_id id customer
 * @apiSuccess {String} data.customer_name nama customer
 * @apiSuccess {String} data.sales_org_id id sales org
 * @apiSuccess {String} data.sales_org_name nama sales org
 * @apiSuccess {String} data.sales_group_id id sales group
 * @apiSuccess {String} data.sales_group_name nama sales group
 * @apiSuccess {String} data.sales_office_id id sales office
 * @apiSuccess {String} data.sales_office_name nama sales office
 * @apiSuccess {Integer} data.qty_group qty group
 * @apiSuccess {Integer} data.land_area land area
 * @apiSuccess {Char} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */