
/**
 * @api {post} /login Login
 * @apiVersion 1.0.0
 * @apiName Login
 * @apiGroup User
 *
 * @apiDescription Login user untuk mendapatkan kode akses
 *
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiParam {String} username username
 * @apiParam {String} password password
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses

 * @apiSuccess {String} token kode token (Bearer)

 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.role_id id role
 * @apiSuccess {String} data.role_name nama role

 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /logout Logout
 * @apiVersion 1.0.0
 * @apiName Logout
 * @apiGroup User
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 *
 */
