/**
 * @api {get} /product Get All Data
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id product
 * @apiSuccess {String} data.uuid uuid product
 * @apiSuccess {String} data.name nama product
 * @apiSuccess {String} data.multiplier multiplier
 * @apiSuccess {String} data.ident_measr_name ident_measr_name
 * @apiSuccess {String} data.measr_name measr_name
 * @apiSuccess {String} data.measr_symbol measr_symbol
 * @apiSuccess {String} data.measr_desc measr_desc
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */

/**
 * @api {get} /product/id/:uuid Get Product By ID
 * @apiVersion 1.0.0
 * @apiName Get Product By ID
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 * 
 * @apiParam {String} uuid uuid product
 *
 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {String} data.id id product
 * @apiSuccess {String} data.uuid uuid product
 * @apiSuccess {String} data.name nama product
 * @apiSuccess {String} data.multiplier multiplier
 * @apiSuccess {String} data.ident_measr_name ident_measr_name
 * @apiSuccess {String} data.measr_name measr_name
 * @apiSuccess {String} data.measr_symbol measr_symbol
 * @apiSuccess {String} data.measr_desc measr_desc
 * @apiSuccess {String} data.status status
 * @apiSuccess {String} data.created_by id user create data
 * @apiSuccess {String} data.updated_by id user update data
 * @apiSuccess {Timestamp} data.updated_at waktu update data
 * @apiSuccess {Timestamp} data.created_at waktu create data
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
