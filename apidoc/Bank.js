
/**
 * @api {get} /bank Get All Data Bank
 * @apiVersion 1.0.0
 * @apiName Get All Data
 * @apiGroup Bank API
 *
 * @apiHeader {String} Authorization token (Bearer)
 * @apiHeader {String} Language bahasa (id = indonesia; en = english)
 *
 * 
 * @apiparam {Integer} id  bank id
 * @apiparam {String} uuid  bank uuid
 * @apiparam {String} name  bank name
 * @apiparam {String} status  bank status

 * @apiSuccess {Integer} status 1
 * @apiSuccess {String} status_txt success
 * @apiSuccess {String} message pesan sukses
 * @apiSuccess {Array[]} data array data
 * @apiSuccess {Integer} data.id  bank id
 * @apiSuccess {String} data.uuid  bank uuid
 * @apiSuccess {String} data.name  bank name
 * @apiSuccess {String} data.status  bank status
 * @apiSuccess {Timestamp} data.start_date valid start
 * @apiSuccess {Timestamp} data.end_date valid end
 * @apiSuccess {string} data.status Active/Inactive
 * @apiSuccess {uuid} data.uuid uuid bankdf
 *
 * @apiError {Integer} status 0
 * @apiError {String} status_txt errors
 * @apiError {String} message pesan kesalahan
 */
