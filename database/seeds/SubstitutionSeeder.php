<?php

use Illuminate\Database\Seeder;
use App\Models\Subtitution;

class SubstitutionSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dtRoute = [
            [
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'plant_id' => 1,
                'material_no' => 5,
                'material_no_subtitution' => 7,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-12-31',
            ],
            [
                'product_id' => 'P01',
                'sales_org_id' => 'C000',
                'plant_id' => 1,
                'material_no' => 5,
                'material_no_subtitution' => 7,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-12-31',
            ]
        ];
        Subtitution::insert($dtRoute);

        $this->command->info("subtitution berhasil diinsert");
    }

}
