<?php

use Illuminate\Database\Seeder;
use App\Models\MonitoringDF;
use Carbon\Carbon;

class MonitoringDFSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order=DB::table('wcm_orders')->get();
        $jumlah=count($order)-3;        
        foreach($order as $key =>$value){
            if($key <= $jumlah){
                $status="0";
                $keterangan1="request time out";
                $keterangan2="over limit";
            }else{
                $status="1";
                $keterangan1="success";
                $keterangan2="-";
            }
            $data=array(
                'order_id'=>$value->id,
                'is_sent'=>$status,
                'send_order_count'=>1,
                'error_message_send_order'=>$keterangan1,
                'error_message_disbursement'=>$keterangan2,
                'status'=>'y'                

            );
            MonitoringDF::create($data);
        }                                      
    }
}
