<?php

use Illuminate\Database\Seeder;
use App\Models\PricingCondition;

class PricingSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dataZHET = array(
            array(
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'distrib_channel_id' => '10',
                'condition_type' => 'ZHET',
                'amount' => 2000000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P02',
                'sales_org_id' => 'B000',
                'distrib_channel_id' => '10',
                'condition_type' => 'ZHET',
                'amount' => 2200000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'distrib_channel_id' => '10',
                'condition_type' => 'ZHET',
                'amount' => 2300000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P04',
                'sales_org_id' => 'B000',
                'distrib_channel_id' => '10',
                'condition_type' => 'ZHET',
                'amount' => 2100000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
        );
        $dataZBDI = array(
            array(
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 10000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P02',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 12000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 13000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110114',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 11000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110113',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 10000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P02',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110113',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 12000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110113',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBDI',
                'amount' => 13000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
        );
        $dataZBKI = array(
            array(
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 20000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P02',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 22000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110112',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 23000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110113',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 21000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P01',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110114',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 20000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P02',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110114',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 22000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
            array(
                'product_id' => 'P03',
                'sales_org_id' => 'B000',
                'sales_group_id' => '1114',
                'sales_unit_id' => '110114',
                'delivery_method_id' => '1',
                'condition_type' => 'ZBKI',
                'amount' => 23000,
                'valid_from' => '2019-01-01',
                'valid_to' => '2019-11-01',
            ),
        );

        PricingCondition::insert($dataZBDI);
        PricingCondition::insert($dataZBKI);
        PricingCondition::insert($dataZHET);
        $this->command->info("Pricing berhasil diinsert");
    }

}
