<?php

use Illuminate\Database\Seeder;
use App\Models\SubSector;

class SubSectorSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [
                'name' => 'Tanaman Pangan',
            ],
            [
                'name' => 'Holtikultura',
            ],
            [
                'name' => 'Perkebunan Rakyat',
            ],
            [
                'name' => 'Perikanan',
            ],
            [
                'name' => 'Peternakan',
            ]
        ];

        foreach ($data as $value) {
            SubSector::create($value);
        }
    }

}
