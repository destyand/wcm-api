<?php

use Illuminate\Database\Seeder;
use App\Models\DistribReportItems;

class DistribReportItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 1,
                'product_id'=> 'P01',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 1,
                'product_id'=> 'P02',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 1,
                'product_id'=> 'P03',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 11,
                'product_id'=> 'P01',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 11,
                'product_id'=> 'P02',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 11,
                'product_id'=> 'P03',
                'qty'=> 1,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 12,
                'product_id'=> 'P01',
                'qty'=> 3,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 12,
                'product_id'=> 'P02',
                'qty'=> 4,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            array(
                'distrib_report_id'=> 1,
                'retail_id'=> 12,
                'product_id'=> 'P03',
                'qty'=> 5,
                'report_f5_id' => 1,
                'status' => 'y' ,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
        ];
        DistribReportItems::insert($datas);
        $this->command->info('Sukses Insert DistribReportItemsTableSeeder');
    }
}
