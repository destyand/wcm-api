<?php

use Illuminate\Database\Seeder;
use App\Models\Address;

class AddressSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['address' => 'Jl. Cemara Putih', 'tlp_no' => '08573009098', 'address_type' => 'office', 'customer_id' => '1000000000', 'sales_unit_id' => '110101', 'village_id' => 1],
            ['address' => 'Jl. Cemara Merah', 'tlp_no' => '08173009098', 'address_type' => 'billing', 'customer_id' => '1000000000', 'sales_unit_id' => '110101', 'village_id' => 1],
        ];

        foreach ($data as $value) {
            Address::create([
                'address' => $value['address'], 
                'tlp_no' => $value['tlp_no'], 
                'address_type' => $value['address_type'], 
                'customer_id' => $value['customer_id'], 
                'sales_unit_id' => $value['sales_unit_id'],
                'village_id' => $value['village_id']
            ]);
        }
    }

}
