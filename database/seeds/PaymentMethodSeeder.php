<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $datas=[
            array('customer_id'=> 1000000000,
            'sales_org_id'=> 'B000',
            'ident_name'=> 'CASH',
            'name'=> 'CASH',
            ),
        ];
        PaymentMethod::insert($datas);
        $this->command->info("Sukses Insert Payment Method Seeder");
    }
}
