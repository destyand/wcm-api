<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;


class OrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array('order_id' => 1,'product_id' => 'P01','qty' => 1,'plant_id' => 1,'retail_id' => 1,'material_list_id' =>1),
            array('order_id' => 1,'product_id' => 'P02','qty' => 1,'plant_id' => 1,'retail_id' => 2,'material_list_id' =>1),
            array('order_id' => 1,'product_id' => 'P03','qty' => 1,'plant_id' => 1,'retail_id' => 3,'material_list_id' =>1),
        ];
        OrderItem::insert($datas);
    }
}
