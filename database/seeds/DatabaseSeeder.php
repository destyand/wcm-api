<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        /*
         * Configuration App ------------------------
         */
        $this->call(AdministratorSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(ModelHasRoleSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(RouteSeeder::class);
        $this->call(MActionSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(PermissionSeeder::class);
        /*
         * Master Data ------------------------
         */
        $this->call(SalesOfficeSeeder::class);
        $this->call(SalesGroupSeeder::class);
        $this->call(SalesUnitSeeder::class);
        $this->call(VillageSeeder::class);
        $this->call(AddressSeeder::class);
        $this->call(SalesOrgSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(CustomerBankAssgSeeder::class);
        $this->call(SalesDivisionSeeder::class);
        $this->call(DistribChannelSeeder::class);
        $this->call(RetailSeeder::class);
        $this->call(SalesAreaSeeder::class);
        $this->call(SubSectorSeeder::class);
        $this->call(CommoditySeeder::class);
        $this->call(FarmerGroupSeeder::class);
        $this->call(CustSalesOrgAssgSeeder::class);
        $this->call(CustomerRetailerAssgSeeder::class);
        $this->call(CustomerSalesAreaSeeder::class);
        $this->call(PlantSeeder::class);
        $this->call(DeliveryMethodSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(Plant_assg::class);
        $this->call(MaterialListSeeder::class);
        $this->call(PermentanSeeder::class);
        $this->call(RdkkSeeder::class);
        $this->call(PerbubSeeder::class);
        $this->call(SpjbSeeder::class);
        $this->call(PaymentMethodSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(OrderItemSeeder::class);
        $this->call(DeliverSeeder::class);
        $this->call(DeliveryItemSeeder::class);
        $this->call(InitialStockTableSeeder::class);
        $this->call(ReportF5TableSeeder::class);
        $this->call(DistribReportsTableSeeder::class);
        $this->call(DistribReportItemsTableSeeder::class);
        $this->call(MonitoringOrderSeeder::class);
        $this->call(MonitoringDFSeeder::class);
        $this->call(PricingSeeder::class);
        $this->call(SubstitutionSeeder::class);
        $this->call(ApprovalSettingSeeder::class);
        $this->call(ReportF6Seeder::class);
        $this->call(ReportF6ItemSeeder::class);
    }
    
}
