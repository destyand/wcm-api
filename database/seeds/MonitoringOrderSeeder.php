<?php

use Illuminate\Database\Seeder;
use App\Models\MonitoringOrder;
use Carbon\Carbon;

class MonitoringOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sales_org=DB::table('wcm_sales_org')->get();
        $data=array();
        foreach($sales_org as $key => $value){
            $customer=2+$key;
            $number=str_pad($customer, 9, '0', STR_PAD_LEFT);
            $data=array(
                'number'=>'ORD'.$number,
                'sales_org_id'=>$value->id,
                'contract_id'=>'1',
                'customer_id'=>'1000000000',
                'sales_office_id'=>'0011',
                'sales_group_id'=>'1103',
                'delivery_method_id'=>'1',
                'order_date'=>date('2019-01-20'),
                'desc'=>'Truk',
                'payment_method'=>'CASH',
                'booking_code'=>'89000322',
                'total_price'=>'220000000',
                'total_price_before_ppn'=>'200000000',
                'upfront_payment'=>'20000',
                'ppn'=>'20000000',
                'payment_due_date'=>date('2019-01-30'),
                'bank_id'=>'B001',
                'pickup_due_date'=>date('2019-02-01'),
                'so_number'=>'555003',
                'reference_code'=>'wew',
                'status'=>'c',
                'billing_date'=>date('2019-01-30'),
                'df_due_date_plan'=>date('2019-01-30'),
                'billing_fulldate'=>Carbon::now()->toDateString(),
                'created_by' => 'AA9244B4-CDC2-43BD-B638-B60FA23FB3AE',
            );
            MonitoringOrder::create($data);
        }
    }
}
