<?php

use Illuminate\Database\Seeder;
use App\Models\SalesOffice;

class SalesOfficeSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => '0001', 'name' => 'Sales Office South'],
            ['id' => '0011', 'name' => 'ACEH'],
            ['id' => '0012', 'name' => 'SUMATERA UTARA'],
            ['id' => '0013', 'name' => 'SUMATERA BARAT'],
            ['id' => '0014', 'name' => 'RIAU'],
            ['id' => '0015', 'name' => 'JAMBI'],
            ['id' => '0016', 'name' => 'SUMATERA SELATAN'],
            ['id' => '0017', 'name' => 'BENGKULU'],
            ['id' => '0018', 'name' => 'LAMPUNG'],
            ['id' => '0019', 'name' => 'KEP. BANGKA BELITUNG'],
            ['id' => '0021', 'name' => 'KEPULAUAN RIAU'],
            ['id' => '0031', 'name' => 'DKI JAKARTA'],
            ['id' => '0032', 'name' => 'JAWA BARAT'],
            ['id' => '0033', 'name' => 'JAWA TENGAH'],
            ['id' => '0034', 'name' => 'DI YOGYAKARTA'],
            ['id' => '0035', 'name' => 'JAWA TIMUR'],
            ['id' => '0036', 'name' => 'BANTEN'],
            ['id' => '0051', 'name' => 'BALI'],
            ['id' => '0052', 'name' => 'NUSA TENGGARA BARAT'],
            ['id' => '0053', 'name' => 'NUSA TENGGARA TIMUR'],
            ['id' => '0061', 'name' => 'KALIMANTAN BARAT'],
            ['id' => '0062', 'name' => 'KALIMANTAN TENGAH'],
            ['id' => '0063', 'name' => 'KALIMANTAN SELATAN'],
            ['id' => '0064', 'name' => 'KALIMANTAN TIMUR'],
            ['id' => '0065', 'name' => 'KALIMANTAN UTARA'],
            ['id' => '0071', 'name' => 'SULAWESI UTARA'],
            ['id' => '0072', 'name' => 'SULAWESI TENGAH'],
            ['id' => '0073', 'name' => 'SULAWESI SELATAN'],
            ['id' => '0074', 'name' => 'SULAWESI TENGGARA'],
            ['id' => '0075', 'name' => 'GORONTALO'],
            ['id' => '0076', 'name' => 'SULAWESI BARAT'],
            ['id' => '0081', 'name' => 'MALUKU'],
            ['id' => '0082', 'name' => 'MALUKU UTARA'],
            ['id' => '0091', 'name' => 'PAPUA'],
            ['id' => '0092', 'name' => 'PAPUA BARAT'],
        ];

        foreach ($data as $value) {
            SalesOffice::create(['id' => $value['id'], 'name' => $value['name']]);
        }
    }

}
