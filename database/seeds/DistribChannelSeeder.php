<?php

use Illuminate\Database\Seeder;
use App\Models\DistribChannel;

class DistribChannelSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            ['id' => '00', 'name' => 'Common Distr.Channel', 'desc' => 'Common Distr.Channel'],
            ['id' => '10', 'name' => 'Dom-Intercompany', 'desc' => 'Dom-Intercompany'],
            ['id' => '20', 'name' => 'Dom-Non Intercompany', 'desc' => 'Dom-Non Intercompany'],
        ];

        foreach ($data as $value) {
            DistribChannel::create(['id' => $value['id'], 'name' => $value['name'], 'desc' => $value['desc']]);
        }
    }

}
