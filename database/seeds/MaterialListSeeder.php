<?php

use App\Models\MaterialList;
use App\Models\DistribChannel;

use Illuminate\Database\Seeder;

class MaterialListSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        $data = [
            ['product_id' => 'P01','distrib_channel_id'=>'00', 'sales_org_id' => 'D000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 3', 'mat_desc' => 'Semen Roda 3', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P01','distrib_channel_id'=>'00', 'sales_org_id' => 'B000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 3', 'mat_desc' => 'Semen Roda 3', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'B000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 4', 'mat_desc' => 'Semen Roda 4', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'C000', 'plant_id' => '2', 'material_no' => '1000036', 'unit' => 'Semen Roda 5', 'mat_desc' => 'Semen Roda 5', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P01','distrib_channel_id'=>'00', 'sales_org_id' => 'B000', 'plant_id' => '3', 'material_no' => '1000036', 'unit' => 'Semen Roda 6', 'mat_desc' => 'Semen Roda 6', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P01','distrib_channel_id'=>'00', 'sales_org_id' => 'B000', 'plant_id' => '2', 'material_no' => '1000036', 'unit' => 'Semen Roda 1', 'mat_desc' => 'Semen Roda 1', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P01','distrib_channel_id'=>'00', 'sales_org_id' => 'B000', 'plant_id' => '2', 'material_no' => '1000036', 'unit' => 'Semen Roda 2', 'mat_desc' => 'Semen Roda 2', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'E000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 7', 'mat_desc' => 'Semen Roda 7', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'E000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 8', 'mat_desc' => 'Semen Roda 8', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'D000', 'plant_id' => '1', 'material_no' => '1000036', 'unit' => 'Semen Roda 10', 'mat_desc' => 'Semen Roda 10', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
            ['product_id' => 'P02','distrib_channel_id'=>'00', 'sales_org_id' => 'C000', 'plant_id' => '3', 'material_no' => '1000036', 'unit' => 'Semen Roda 7', 'mat_desc' => 'Semen Roda 7', 'valid_from' => '2019-01-15', 'valid_to' => '2019-01-16'],
        ];

        /*
         * added this because column distib_channel_id not nullable
         * 
         */

        try {
            
            $distrib_channel_id = DistribChannel::pluck("id")->first();
            foreach ($data as $index => $row) {
                $row["distrib_channel_id"] = $distrib_channel_id;

                $data[$index] = $row;
            }

            MaterialList::insert($data);
        } catch(\Exception $e) {
            $this->command->info($e->getMessage());
        }

        $this->command->info("Berhasil Insert MaterialList");

    }

}
