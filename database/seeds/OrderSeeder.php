<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\OrderItem;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array('number' => 'ABC10000','sales_org_id' => 'B000','contract_id' => 2,'customer_id' => 1000000000,'sales_office_id' => '0035','sales_group_id' => 1114,'delivery_method_id' => 1,'payment_method' => 1,'order_date' => date('2019-01-20'),'so_number'=>2),
        ];
        Order::insert($datas);
        $this->command->info('Sukses Insert Order');

    }
}
