<?php

use Illuminate\Database\Seeder;
use App\Models\DeliveryItem;

class DeliveryItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $datas=[
            array('delivery_id'=>1,'plant_id'=>1,'product_id'=>'P01','shipping_point_id'=>1,'storage_location_id'=>1,'delivery_qty'=>1,'delivery_date'=>date('2019-01-20')),
            array('delivery_id'=>1,'plant_id'=>2,'product_id'=>'P01','shipping_point_id'=>1,'storage_location_id'=>1,'delivery_qty'=>1,'delivery_date'=>date('2019-01-20')),
        ];
        DeliveryItem::insert($datas);
        $this->command->info('Sukses Delivery item Seeder');
    }
}
