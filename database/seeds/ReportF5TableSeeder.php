<?php

use Illuminate\Database\Seeder;
use App\Models\ReportF5;

class ReportF5TableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        $datas = [
            array(
                'number' => 'F50000000001',
                'sales_org_id' => 'B000',
                'customer_id' => '1000000000',
                'sales_group_id' => '1114',
                'month' => '01',
                'year' => '2019',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ),
            [
                'number' => 'F50000000002',
                'sales_org_id' => 'C000',
                'customer_id' => '1000000010',
                'sales_group_id' => '1114',
                'month' => '01',
                'year' => '2018',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ],
            [
                'number' => 'F50000000003',
                'sales_org_id' => 'D000',
                'customer_id' => '1000000000',
                'sales_group_id' => '1113',
                'month' => '02',
                'year' => '2019',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ],
            [
                'number' => 'F50000000004',
                'sales_org_id' => 'B000',
                'customer_id' => '1000000000',
                'sales_group_id' => '1113',
                'month' => '02',
                'year' => '2019',
                'submited_date' => date('Y-m-d'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ]
        ];
        ReportF5::insert($datas);
        $this->command->info('Sukses Insert ReportF5TableSeeder');
    }

}
