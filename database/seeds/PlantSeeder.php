<?php

use Illuminate\Database\Seeder;
use App\Models\Plant;

class PlantSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create('id_ID');
        for ($i = 0; $i < 10; $i++) {
            Plant::create([
                'code' => 'ABC0000' . $i,
                'name' => $faker->name,
                'sales_group_id' => '1101',
                'address' => 'jalan palangkaraya'
            ]);
        }
    }

}
