<?php

use Illuminate\Database\Seeder;
use App\Models\Commodity;

class CommoditySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [
                'name' => 'Anggur',
            ],
            [
                'name' => 'Bunga',
            ],
            [
                'name' => 'Cabe',
            ],
            [
                'name' => 'Cengkeh',
            ],
            [
                'name' => 'Hijau Makanan Ternak',
            ],
            [
                'name' => 'Jagung',
            ],
            [
                'name' => 'Jahe',
            ],
            [
                'name' => 'Jambu Mete',
            ],
            [
                'name' => 'Jeruk',
            ],
            [
                'name' => 'Kacang',
            ],
            [
                'name' => 'Kakao',
            ],
            [
                'name' => 'Kedelai',
            ],
            [
                'name' => 'Kelapa',
            ],
            [
                'name' => 'Ketela Pohon',
            ],
            [
                'name' => 'Ketela Rambat',
            ],
            [
                'name' => 'Kopi',
            ],
            [
                'name' => 'Kubus',
            ],
            [
                'name' => 'Mangga',
            ],
            [
                'name' => 'Manggis',
            ],
            [
                'name' => 'Padi',
            ],
            [
                'name' => 'Pandan',
            ],
            [
                'name' => 'Perikanan',
            ],
            [
                'name' => 'Pisang',
            ],
            [
                'name' => 'Rambutan',
            ],
            [
                'name' => 'Salak',
            ],
            [
                'name' => 'Sawi',
            ],
            [
                'name' => 'Sayuran',
            ],
            [
                'name' => 'Semangka',
            ],
            [
                'name' => 'Sengon',
            ]
        ];

        foreach ($data as $value) {
            Commodity::create($value);
        }
    }

}
