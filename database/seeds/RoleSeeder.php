<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Spatie\Permission\Models\Role::create([
            "name" => "DEVELOPER",
            "guard_name" => "api",
        ]);
        Spatie\Permission\Models\Role::create([
            "name" => "ADMIN", //"PIHC"
            "guard_name" => "api",
        ]);
        Spatie\Permission\Models\Role::create([
            "name" => "ADMIN_ANPER", //"Petrokimia, Kujang, PIM, Kaltim dll"
            "guard_name" => "api",
        ]);
        Spatie\Permission\Models\Role::create([
            "name" => "DISTRIBUTOR", //"Distributor"
            "guard_name" => "api",
        ]);

        $this->command->info("Role Admin berhasil diinsert");
    }
}
