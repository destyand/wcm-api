<?php
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        $id = ['1000000000','1000000002','1000000003','1000000004','1000000005','1000000006','1000000007','1000000008','1000000009','1000000010'];
        for ($i = 0; $i < 10; $i++) {
            Customer::create([
                'id' => $id[$i], 
                'uuid' => $faker->uuid, 
                'full_name' =>  $faker->name, 
                'owner' => $faker->name, 
                'register_date' => '2018-12-21 10:45:30.000', 
                'code_cust_group' => 1, 
                'name_cust_group' => 'A', 
                'old_number' => 1, 
                'npwp_no' => 1, 
                'npwp_register' => '2018-12-21 10:45:30.000', 
                'recomd_letter_date' => '2018-12-21 10:45:30.000', 
                'recomd_letter' => '-', 
                'valid_date_tdp' => '2018-12-21 10:45:30.000', 
                'tdp_no' => 1, 
                'situ_no' => 1, 
                'valid_date_situ' => '2018-12-21 10:45:30.000', 
                'valid_date_siup' => '2018-12-21 10:45:30.000', 
                'siup_no' => 1, 
                'status' => 'y', 
                'status' => 'y', 
                'created_by'=> '473C8117-07FB-495F-809B-095480D8FD92', 
                'updated_by'=> '473C8117-07FB-495F-809B-095480D8FD92'
            ]);
        }
        $this->command->info("Customer berhasil diinsert");

    }
}
