<?php

use Illuminate\Database\Seeder;
use App\Models\SalesArea;

class SalesAreaSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [
                'sales_org_id' => 'C000',
                'distrib_channel_id' => '10',
                'sales_division_id' => '00',
            ],
            [
                'sales_org_id' => 'B000',
                'distrib_channel_id' => '10',
                'sales_division_id' => '00',
            ]
        ];

        SalesArea::insert($data);
    }

}
