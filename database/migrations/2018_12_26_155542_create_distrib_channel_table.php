<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDistribChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_distrib_channel', function (Blueprint $table) {
            $table->char('id', 2);
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('desc');
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_sales_area');
        Schema::dropIfExists('wcm_distrib_channel');
    }
}
