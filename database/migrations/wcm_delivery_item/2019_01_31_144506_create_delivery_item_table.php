<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_delivery_item', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('delivery_id');
            $table->foreign('delivery_id')
                ->references('id')
                ->on('wcm_delivery')
                ->onDelete("cascade");
            $table->integer('plant_id')->default(0);
            /*$table->foreign('plant_id')
            ->references('id')
            ->on('wcm_plant');*/
            $table->char('product_id', 3);
            $table->foreign('product_id')
                ->references('id')
                ->on('wcm_product');
            $table->string('shipping_point_id')->nullable();
            $table->string('storage_location_id')->nullable();
            $table->float('delivery_qty')->nullable();
            $table->date('delivery_date')->nullable();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_delivery_item');
    }
}
