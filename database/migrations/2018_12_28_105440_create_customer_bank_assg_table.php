<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCustomerBankAssgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_customer_bank_assg', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');

            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');

            $table->char('bank_id', 4);
            $table->foreign('bank_id')
                    ->references('id')
                    ->on('wcm_bank');

            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_customer_bank_assg');
    }
}
