<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLogH2h extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_product_limit', function (Blueprint $table) {
            $table->increments('id');
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->float('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_product_limit');
    }

}
