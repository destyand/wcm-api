<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewReportSoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('DROP VIEW IF EXISTS view_ditribution_do');
        DB::statement("
            DROP
                VIEW vreport_so_detail;

            CREATE
                VIEW vreport_so_detail as select
                    *,
                    quantity_so_ori - quantity_do_ori as outstanding_so,
                    '' as provinsi_gudang,
                    '' as kabupaten_gudang,
                    '' as description
                from
                    (
                    select
                        [tb1].[delivery_id],
                        [tb1].[so_number] as [nomor_sales_order],
                        ROW_NUMBER() OVER (PARTITION BY tb1.delivery_id
                    ORDER BY
                        tb1.id) * 10 as so_Item,
                        [tb1].[sales_org_id] as [sales_organization],
                        [tb1].[product_id],
                        [tb3].[distrib_channel_id] as [distribution_channel],
                        [tb3].[sales_division_id] as [division],
                        [tb1].[sales_office_id] as [sales_office],
                        [tb4].[name] as [deskripsi_sales_office],
                        [tb1].[sales_group_id] as [sales_group],
                        [tb5].[name] as [deskripsi_sales_group],
                        [tb6].[full_name] as [so_legacy],
                        [tb7].[sales_unit_id],
                        [tb8].[name] as [kecamatan_so_desc],
                        [tb9].[name] as [kabupaten_distributor],
                        [tb10].[name] as [provinsi_distributor],
                        [tb6].[id] as [distributor],
                        [tb6].[owner] as [nama_distributor],
                        [tb7].[name] as [pengecer],
                        [tb11].[number] as [nomor_kontrak],
                        FORMAT(tb1.order_date,
                        'dd-MM-yyyy') as tanggal_so_dibuat,
                        [tb1].[order_date] as tanggal_so_dibuat_date,
                        FORMAT(tb1.order_date,
                        'dd-MM-yyyy') as tanggal_dokumen,
                        [tb1].[payment_method] as [payment_method],
                        [tb2].[material_list_id] as [nomor_material],
                        [tb12].[mat_desc] as [deskripsi_material],
                        [tb13].[name] as [material_group],
                        [tb14].[initial_qty] as [alokasi_asal],
                        [tb15].[initial_qty] as [alokasi_operasional],
                        [tb2].[qty] as [quantity_so_ori],
                        FORMAT([tb2].[qty],
                        'N2',
                        'id-ID') as [quantity_so],
                        [tb12].[unit] as [unit_of_measure],
                        'IDR' as mata_uang,
                        FORMAT((tb2.total_price_before_ppn / tb2.qty),
                        'N2',
                        'id-ID') as harga_jual_exc_ppn,
                        FORMAT(([tb2].[ppn]),
                        'N2',
                        'id-ID') as [ppn],
                        FORMAT(([tb2].[total_price_before_ppn]),
                        'N2',
                        'id-ID') as [total],
                        FORMAT((tb2.total_price_before_ppn + (tb2.ppn / tb2.qty)),
                        'N2',
                        'id-ID') as harga_per_ton,
                        FORMAT(([tb2].[total_price]),
                        'N2',
                        'id-ID') as [harga_total],
                        [tb1].[number_do] as [nomor_do],
                        FORMAT([tb1].[delivery_date] ,
                        'dd-MM-yyyy') as [tanggal_pgi],
                        [tb1].[delivery_date] as [tanggal_pgi_date],
                        [tb17].[code] as [plant_so],
                        '' as gudang_so,
                        [tb17].[name] as [gudang_so_deskripsi],
                        '' as kode_gudang,
                        [tb17].[name] as [gudang_pengambilan],
                        FORMAT([tb1].[delivery_qty],
                        'N2',
                        'id-ID') as [quantity_do],
                        [tb1].[delivery_qty] as [quantity_do_ori],
                        tb2.qty - tb1.delivery_qty as quantity_so_min_do,
                        FORMAT([tb1].[delivery_qty],
                        'N2',
                        'id-ID') as [pgi_qty],
                        FORMAT(((tb2.total_price_before_ppn + (tb2.ppn / tb2.qty)) - tb1.delivery_qty),
                        'N2',
                        'id-ID') as total_harga_tonase_pgi,
                        'Z3SU' as so_type,
                        'Penj. Subsidi (Web)' as so_type_description,
                        [tb19].[term_of_payment] as [payment_term],
                        CASE
                            WHEN tb1.payment_method = 'Cash' THEN 'E'
                            ELSE 'D'
                        END as paymentMethod,
                        [tb1].[good_redemption_due_date] as [batas_akhir_pengambilan],
                        FORMAT(tb1.order_date,
                        'dd-MM-yyyy') as tanggal_po,
                        [tb1].[number] as [no_po],
                        [tb20].[name] as [so_created_by],
                        'SUBSIDI' as sektor,
                        [tb1].[sap_billing_dp_doc] as [no_billing],
                        FORMAT(tb1.billing_date,
                        'dd-MM-yyyy') as billing_date,
                        [tb21].[name] as [incoterm_1],
                        [tb1].[sales_group_id] as [incoterm_2],
                        FORMAT(tb1.order_date,
                        'dd-MM-yyyy') as so_release,
                        CASE
                            WHEN tb1.status = 'y' THEN 'Active'
                            WHEN tb1.status = 'n' THEN 'Inactive'
                            WHEN tb1.status = 'p' THEN 'Suspend'
                            WHEN tb1.status = 'd' THEN 'Draft'
                            WHEN tb1.status = 's' THEN 'Submited'
                            WHEN tb1.status = 'x' THEN 'Close'
                            WHEN tb1.status = 'c' THEN 'Complete'
                            WHEN tb1.status = 'o' THEN 'Cancel'
                            WHEN tb1.status = 'l' THEN 'Paid'
                            WHEN tb1.status = 'k' THEN 'Good Issue'
                            WHEN tb1.status = 'u' THEN 'DP Paid'
                            ELSE '-'
                        END as [status]
                    FROM
                        (
                        select
                            orders.*,
                            del.id as delivery_id,
                            del.delivery_date,
                            del.[number] as number_do,
                            del_items.product_id,
                            del_items.delivery_qty
                        from
                            wcm_delivery_item as del_items
                        join wcm_delivery as del on
                            (del_items.delivery_id = del.id)
                        join wcm_orders as orders on
                            (del.order_id = orders.id)
                        where
                            [del].[number] is not null ) as [tb1]
                    left join [wcm_order_item] as [tb2] on
                        [tb2].[id] = (
                        SELECT
                            TOP 1 id
                        FROM
                            wcm_order_item
                        where
                            wcm_order_item.order_id = tb1.id
                            and wcm_order_item.product_id = tb1.product_id )
                    left join [wcm_sales_area] as [tb3] on
                        [tb1].[sales_org_id] = [tb3].[sales_org_id]
                        and [tb3].[distrib_channel_id] = 20
                    left join [wcm_sales_office] as [tb4] on
                        [tb1].[sales_office_id] = [tb4].[id]
                    left join [wcm_sales_group] as [tb5] on
                        [tb1].[sales_office_id] = [tb5].[sales_office_id]
                        and [tb1].[sales_group_id] = [tb5].[id]
                    left join [wcm_customer] as [tb6] on
                        [tb1].[customer_id] = [tb6].[id]
                    left join [wcm_retail] as [tb7] on
                        [tb2].[retail_id] = [tb7].[id]
                    left join [wcm_sales_unit] as [tb8] on
                        [tb7].[sales_unit_id] = [tb8].[id]
                    left join [wcm_sales_group] as [tb9] on
                        [tb8].[sales_group_id] = [tb9].[id]
                    left join [wcm_sales_office] as [tb10] on
                        [tb9].[sales_office_id] = [tb10].[id]
                    left join [wcm_contract] as [tb11] on
                        [tb1].[contract_id] = [tb11].[id]
                    left join [wcm_material_list] as [tb12] on
                        [tb2].[material_list_id] = [tb12].[id]
                    left join [wcm_product] as [tb13] on
                        [tb12].[product_id] = [tb13].[id]
                    left join [wcm_contract_item] as [tb15] on
                        [tb15].[id] = (
                        SELECT
                            TOP 1 ID
                        from
                            wcm_contract_item
                        where
                            [month] = FORMAT(tb1.order_date,
                            'MM')
                            and [year] = FORMAT(tb1.order_date,
                            'yyyy')
                            and [contract_id] = [tb11].[id]
                            and [product_id] = [tb2].[product_id] )
                    left join [wcm_contract] as [contract_asal] on
                        [tb11].[number] = [contract_asal].[number]
                        and [contract_asal].[contract_type] = 'asal'
                    left join [wcm_contract_item] as [tb14] on
                        [tb14].[id] = (
                        SELECT
                            TOP 1 ID
                        from
                            wcm_contract_item
                        where
                            [month] = FORMAT(tb1.order_date,
                            'MM')
                            and [year] = FORMAT(tb1.order_date,
                            'yyyy')
                            and [contract_id] = [contract_asal].[id]
                            and [product_id] = [tb2].[product_id] )
                    left join [wcm_plant] as [tb17] on
                        [tb2].[plant_id] = [tb17].[id]
                    left join [wcm_customer_sales_area] as [tb19] on
                        [tb6].[id] = [tb19].[customer_id]
                        and [tb3].[id] = [tb19].[sales_area_id]
                    left join [users] as [tb20] on
                        [tb1].[created_by] = [tb20].[id]
                    left join [wcm_delivery_method] as [tb21] on
                        [tb1].[delivery_method_id] = [tb21].[id]) as tbl
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('DROP VIEW IF EXISTS vreport_so_detail');
    }
}
