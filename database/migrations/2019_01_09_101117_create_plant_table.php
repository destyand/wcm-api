<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePlantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_plant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12)->unique();
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('name');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group');
            $table->string('address')->nullable();
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['sales_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_plant');
    }
}
