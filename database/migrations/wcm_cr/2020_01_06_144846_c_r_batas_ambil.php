<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CRBatasAmbil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wcm_limit_due_date', function (Blueprint $table) {
            $table->char("sales_office_id", 4);
            $table->foreign("sales_office_id")
                ->reference("id")
                ->on("wcm_sales_office");
            $table->char("incoterm", 3);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wcm_limit_due_date', function (Blueprint $table) {
            $table->dropColumn("sales_office_id");
            $table->dropForeign("sales_office_id");
            $table->dropColumn("incoterm");
        });
    }
}
