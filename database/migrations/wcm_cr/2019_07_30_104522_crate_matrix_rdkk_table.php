<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CrateMatrixRdkkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('wcm_matrix_rdkk', function ($table) {
            $table->increments('id');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                ->references('id')
                ->on('wcm_sales_office');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                ->references('id')
                ->on('wcm_sales_group');
            $table->char('product_id', 3);
            $table->foreign('product_id')
                ->references('id')
                ->on('wcm_product');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wcm_matrix_rdkk');
    }
}
