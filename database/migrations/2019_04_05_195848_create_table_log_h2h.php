<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLogH2h extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_log_h2h', function (Blueprint $table) {
            $table->increments('id');
            $table->char('order_id', 12);
            $table->timestamp('timestamp_req')->useCurrent();
            $table->ipAddress('ip_address_req');
            $table->string("pengirim_req");
            $table->json('isi_req');
            $table->string("http_status", 20);
            $table->integer('status');
            $table->ipAddress('ip_address_respon');
            $table->string("pengirim_respon");
            $table->json('isi_respon');
            $table->timestamp('timestamp_respon')->useCurrent();
            $table->string("request_type", 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_log_h2h');
    }
}
