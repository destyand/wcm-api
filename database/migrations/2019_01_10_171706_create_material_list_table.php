<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_material_list', function (Blueprint $table) {
            // $table->char('id', 6)->primary();
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->integer('plant_id');
            $table->foreign('plant_id')
                    ->references('id')
                    ->on('wcm_plant');
            $table->char('distrib_channel_id', 2);
            $table->foreign('distrib_channel_id')
                    ->references('id')
                    ->on('wcm_distrib_channel');
            $table->string('material_no')->nullable();
            $table->string('unit')->nullable();
            $table->string('mat_desc')->nullable();
            $table->timestamp('valid_from')->nullable();
            $table->timestamp('valid_to')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['product_id', 'sales_org_id', 'plant_id', 'distrib_channel_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_material_list');
    }
}
