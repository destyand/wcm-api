<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColomnUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wcm_order_item', function (Blueprint $table) {
            //
            $table->addColumn("number", "uom_zhet");
            $table->addColumn("number", "uom_zbki");
            $table->addColumn("number", "uom_zbdi");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wcm_order_item', function (Blueprint $table) {
            $table->dropColumn("uom_zhet");
            $table->dropColumn("uom_zbki");
            $table->dropColumn("uom_zbdi");
        });
    }
}
