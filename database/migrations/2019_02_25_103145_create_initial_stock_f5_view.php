<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialStockF5View extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("
        CREATE VIEW view_initial_stock_f5 AS
        (
            SELECT
                    tb1.*,
                    ISNULL(tb2.p01_reduce, 0) as p01_reduce,
                    ISNULL(tb2.p02_reduce, 0) as p02_reduce,
                    ISNULL(tb2.p03_reduce, 0) as p03_reduce,
                    ISNULL(tb2.p04_reduce, 0) as p04_reduce,
                    ISNULL(tb2.p05_reduce, 0) as p05_reduce,
                                        tb7.status_draft,
                                        tb7.status_submit
            FROM
                    (
            SELECT
                    id,
                    uuid,
                    type,
                    sales_org_id,
                    sales_org_name,
                    customer_id,
                    customer_name,
                    sales_office_id,
                    sales_office_name,
                    sales_group_id,
                    sales_group_name,
                    month,
                    year,
                    status,
                    ISNULL(P01, 0) AS p01_qty,
                    ISNULL(P02, 0) AS p02_qty,
                    ISNULL(P03, 0) AS p03_qty,
                    ISNULL(P04, 0) AS p04_qty,
                    ISNULL(P05, 0) AS p05_qty 
            FROM
                    (
            SELECT
                    tb1.id,
                    tb1.uuid,
                    tb1.type,
                    tb1.sales_org_id,
                    tb3.name AS sales_org_name,
                    tb1.customer_id,
                    tb4.full_name AS customer_name,
                    tb1.sales_office_id,
                    tb5.name AS sales_office_name,
                    tb1.sales_group_id,
                    tb6.name AS sales_group_name,
                    tb1.month,
                    tb1.year,
                    tb1.status,
                    tb2.product_id,
                    tb2.qty 
            FROM
                    wcm_initial_stocks AS tb1
                    LEFT JOIN wcm_initial_stock_item AS tb2 ON tb1.id = tb2.initial_stock_id
                    LEFT JOIN wcm_sales_org AS tb3 ON tb1.sales_org_id = tb3.id
                    LEFT JOIN wcm_customer AS tb4 ON tb1.customer_id = tb4.id
                    LEFT JOIN wcm_sales_office AS tb5 ON tb1.sales_office_id = tb5.id
                    LEFT JOIN wcm_sales_group AS tb6 ON tb1.sales_group_id = tb6.id 
            WHERE
                    tb1.type = 'InitialStockF5' 
                    ) AS tba1 PIVOT ( MAX ( qty ) FOR product_id IN ( P01, P02, P03, P04, P05 ) ) piv 
                    ) AS tb1
                    LEFT JOIN (
            SELECT
                    initial_stock_id,
                    P01 AS p01_reduce,
                    P02 AS p02_reduce,
                    P03 AS p03_reduce,
                    P04 AS p04_reduce,
                    P05 AS p05_reduce 
            FROM
                    (
            SELECT
                    tb1.initial_stock_id,
                    tb2.product_id,
                    SUM ( tb2.qty ) AS qty 
            FROM
                    wcm_distrib_reports AS tb1
                    LEFT JOIN wcm_distrib_report_item AS tb2 ON tb1.id = tb2.distrib_report_id 
            GROUP BY
                    tb1.initial_stock_id,
                    tb2.product_id 
                    ) AS tba1 PIVOT ( MAX ( qty ) FOR product_id IN ( P01, P02, P03, P04, P05 ) ) piv 
                    ) AS tb2 ON tb1.id = tb2.initial_stock_id 
                        LEFT JOIN (
                SELECT 
                    initial_stock_id,
                    SUM(CASE WHEN status = 's' THEN 1 ELSE 0 END) as status_submit,
                    SUM(CASE WHEN status = 'd' THEN 1 ELSE 0 END) as status_draft
                FROM wcm_distrib_reports GROUP BY initial_stock_id
            ) as tb7 ON tb7.initial_stock_id = tb1.id
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP VIEW IF EXISTS view_initial_stock_f5');
    }

}
