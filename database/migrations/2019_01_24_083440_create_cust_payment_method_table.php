<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustPaymentMethodTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_cust_payment_method', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->string('ident_name')->nullable();
            $table->string('name')->nullable();
            $table->string('desc')->nullable();
            $table->string('system')->nullable();
            $table->string('sap_code')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['customer_id', 'sales_org_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_cust_payment_method');
    }

}
