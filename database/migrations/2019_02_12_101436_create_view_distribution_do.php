<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateViewDistributionDo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("
        CREATE VIEW view_ditribution_do AS
        (
            SELECT a.id as order_id,tb1.qty_draft,tb1.status_draft,tb2.qty_submit,tb2.status_submit FROM wcm_orders as a
            LEFT JOIN (
                SELECT
                    COUNT (a.id) AS qty_draft,
                    b.status as status_draft,
                    b.order_id
                FROM
                    wcm_orders AS a
                LEFT JOIN wcm_distrib_reports AS b ON a.id = b.order_id
                WHERE
                    b.status = 'd'
                GROUP BY
                    b.status,
                    b.order_id
            ) AS tb1 ON a.id = tb1.order_id
            LEFT JOIN (
                SELECT
                    COUNT (a.id) AS qty_submit,
                    b.status as status_submit,
                    b.order_id
                FROM
                    wcm_orders AS a
                LEFT JOIN wcm_distrib_reports AS b ON a.id = b.order_id
                WHERE
                    b.status = 's'
                GROUP BY
                    b.status,
                    b.order_id
            ) AS tb2 ON a.id = tb2.order_id
                
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('DROP VIEW IF EXISTS view_ditribution_do');
    }
}
