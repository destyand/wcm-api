<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PaymentMethodSalesArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("
            CREATE view view_payment_mothod_sales_area AS (
            Select T1.customer_id,T1.sales_org_id
                    , Stuff(
                        (
                        Select ', ' + T2.name
                        From wcm_cust_payment_method As T2
                        Where T2.customer_id = T1.customer_id AND T2.sales_org_id= T1.sales_org_id
                        Order By T2.name
                        For Xml Path(''), type
                        ).value('.', 'nvarchar(max)'), 1,2, '') As name
                From wcm_cust_payment_method As T1
                Group By T1.customer_id,T1.sales_org_id
            )
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement('DROP VIEW IF EXISTS view_payment_mothod_sales_area');
    }
}
