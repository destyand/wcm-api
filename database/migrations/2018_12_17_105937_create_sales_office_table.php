<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSalesOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_sales_office', function (Blueprint $table) {
            $table->char('id', 4);
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('code')->nullable();
            $table->string('name');
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_sales_office_assg');
        Schema::dropIfExists('wcm_address');
        Schema::dropIfExists('wcm_village');
        Schema::dropIfExists('wcm_sales_unit');
        Schema::dropIfExists('wcm_sales_group');
        Schema::dropIfExists('wcm_sales_office');
    }
}
