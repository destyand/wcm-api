<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_order_item', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('order_id');
            $table->foreign('order_id')
                    ->references('id')
                    ->on('wcm_orders');
            $table->char('product_id', 3);
            $table->foreign('product_id')
                    ->references('id')
                    ->on('wcm_product');
            $table->float('qty');
            $table->integer('plant_id');
            $table->foreign('plant_id')
                    ->references('id')
                    ->on('wcm_plant');
            $table->integer('retail_id');
            $table->foreign('retail_id')
                    ->references('id')
                    ->on('wcm_retail');
            $table->integer('material_list_id');
            $table->foreign('material_list_id')
                    ->references('id')
                    ->on('wcm_material_list');
            $table->string('sap_prodt_desc')->nullable();
            $table->decimal('zhet', 38, 10)->nullable();
            $table->decimal('zbki', 38, 10)->nullable();
            $table->decimal('zbdi', 38, 10)->nullable();
            $table->decimal('total_price', 38, 10)->nullable();
            $table->decimal('total_price_before_ppn', 38, 10)->nullable();
            $table->decimal('upfront_payment', 38, 10)->nullable();
            $table->decimal('ppn', 38, 10)->nullable();
            $table->decimal('pph22', 38, 10)->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['order_id', 'product_id', 'plant_id', 'retail_id', 'material_list_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_order_item');
    }

}
