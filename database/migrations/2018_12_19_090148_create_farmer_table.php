<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFarmerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_farmer', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('name');
            $table->char('code', 12)->unique();
            $table->float('land_area')->nullable();
            $table->string('telp_no')->nullable();
            $table->string('hp_no')->nullable();
            $table->string('ktp_no')->nullable()->unique();
            $table->timestamp('ktp_valid_date')->nullable();
            $table->string('address');
            $table->char('sales_unit_id', 6);
            $table->foreign('sales_unit_id')
                    ->references('id')
                    ->on('wcm_sales_unit');
            $table->integer('farmer_group_id');
            $table->foreign('farmer_group_id')
                    ->references('id')
                    ->on('wcm_farmer_groups');
            $table->string('commodity_id')->nullable();
            $table->string('village')->nullable();
            $table->string('sub_sector_id')->nullable();
            $table->float('latitude', 11, 10)->nullable();
            $table->float('longitude', 11, 10)->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['sales_unit_id', 'farmer_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_farmer');
    }

}
