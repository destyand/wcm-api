<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSalesOrgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_sales_org', function (Blueprint $table) {
            $table->char('id', 4);
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->timestamp('from_date')->nullable();
            $table->timestamp('thru_date')->nullable();
            $table->string('bca_df_code')->nullable();
            $table->string('bca_h2h_code')->nullable();
            $table->char('status', 1)->default('y');
            $table->char('config_lintas_alur', 1)->default(1);
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_farmer');
        Schema::dropIfExists('wcm_customer_bank_assg');
        Schema::dropIfExists('wcm_cust_sales_org_assg');
        Schema::dropIfExists('wcm_customer_retailer_assg');
        Schema::dropIfExists('wcm_sales_org');
    }
}
