<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProductTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_product', function (Blueprint $table) {
            $table->char('id', 3)->primary();
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('name');
            $table->string('multiplier')->nullable();
            $table->string('ident_measr_name')->nullable();
            $table->string('measr_name')->nullable();
            $table->string('measr_symbol')->nullable();
            $table->string('measr_desc')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_product');
    }

}
