<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDfStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_order_df_status', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->integer('order_id');
            $table->foreign('order_id')
                    ->references('id')
                    ->on('wcm_orders');            
            $table->char('is_sent', 1);
            $table->integer('send_order_count');
            $table->string('error_message_send_order')->nullable();
            $table->string('error_message_disbursement')->nullable();
            $table->char('status', 1)->default('y');          
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_order_df_status');
    }
}
