<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_initial_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('type');
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->char('sales_office_id', 4);
            $table->foreign('sales_office_id')
                    ->references('id')
                    ->on('wcm_sales_office');
            $table->char('sales_group_id', 4);
            $table->foreign('sales_group_id')
                    ->references('id')
                    ->on('wcm_sales_group'); 
            $table->char('sales_unit_id', 6)->nullable();
//            $table->foreign('sales_unit_id')
//                    ->references('id')
//                    ->on('wcm_sales_unit'); 
            $table->integer('retail_id')->nullable();
//            $table->foreign('retail_id')
//                    ->references('id')
//                    ->on('wcm_retail');
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->integer('refrerensi_code')->nullable();
            $table->string('remark')->nullable();  
            $table->char('status', 1)->default('y');          
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['sales_org_id', 'customer_id', 'sales_office_id', 'sales_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_initial_stocks');
    }
}
