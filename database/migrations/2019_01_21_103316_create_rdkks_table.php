<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRdkksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('wcm_rdkks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->char('number', 12)->unique();
            $table->integer('planting_period_id');
            $table->foreign('planting_period_id')
                    ->references('id')
                    ->on('wcm_planting_period');
            $table->char('customer_id', 10);
            $table->foreign('customer_id')
                    ->references('id')
                    ->on('wcm_customer');
            $table->integer('retail_id');
            $table->foreign('retail_id')
                    ->references('id')
                    ->on('wcm_retail');
            $table->integer('farmer_group_id');
            $table->foreign('farmer_group_id')
                    ->references('id')
                    ->on('wcm_farmer_groups');
            $table->char('sales_org_id', 4);
            $table->foreign('sales_org_id')
                    ->references('id')
                    ->on('wcm_sales_org');
            $table->char('sales_unit_id', 6);
            $table->foreign('sales_unit_id')
                    ->references('id')
                    ->on('wcm_sales_unit');
            $table->string('planting_area')->nullable();
            $table->date('planting_from_date')->nullable();
            $table->date('planting_thru_date')->nullable();
            $table->string('commodity')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            
            $table->index(['planting_period_id', 'customer_id', 'retail_id', 'farmer_group_id', 'sales_org_id', 'sales_unit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('wcm_rdkks');
    }

}
