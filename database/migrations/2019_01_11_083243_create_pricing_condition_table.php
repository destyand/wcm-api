<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wcm_pricing_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->default(DB::raw('NEWID()'));
            $table->string('sales_org_id');
            $table->string('sales_group_id')->nullable();
            $table->string('sales_unit_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('delivery_method_id')->nullable();
            $table->string('sales_office_id')->nullable();
            $table->string('sales_division_id')->nullable();
            $table->string('distrib_channel_id')->nullable();
            $table->string('product_desc')->nullable();
            $table->string('condition_type');
            $table->string('calculate_type')->nullable();
            $table->float('amount');
            $table->string('current')->nullable();
            $table->string('uom')->nullable();
            $table->string('per_uom')->nullable();
            $table->date('valid_from');
            $table->date('valid_to');
            $table->integer('level')->nullable();
            $table->string('sap_tax_classification')->nullable();
            $table->char('status', 1)->default('y');
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wcm_pricing_condition');
    }
}
